#define __TESTPRINT_C__

/*
 * libgnomeprint test program
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors:
 *   Unknown author
 *   Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright 2000-2001 Ximian, Inc. and authors
 *
 */

#include <string.h>
#include <popt-gnome.h>
#include <libgnomeprint/gnome-print-i18n.h>
#include <libgnomeprint/gnome-print.h>
#include <libgnomeprint/gnome-font-compat.h>

static struct poptOption options[] = {
	{ NULL, '\0', 0, NULL, 0 }
};

static void
print_test2_page (GnomePrintContext *pc)
{
	double image_matrix [6] = {30, 0, 0, 30, 0, 0};
	char colorimg [256][256][3];
	int x, y;
	
	for (y = 0; y < 256; y++)
		for (x = 0; x < 256; x++)
		{
			colorimg[y][x][0] = (x + y) >> 1;
			colorimg[y][x][1] = (x + (255 - y)) >> 1;
			colorimg[y][x][2] = ((255 - x) + y) >> 1;
		}

	for (y = 0; y < 256; y++){
		for (x = 0; x < 30; x++){
			colorimg [y][x][0] = 0;
			colorimg [y][x][1] = 0;
			colorimg [y][x][2] = 0;
		}
	}

	for (x = 0; x < 256; x++){
		for (y = 0; y < 30; y++){
			colorimg [y][x][0] = 0;
			colorimg [y][x][1] = 0;
			colorimg [y][x][2] = 0;
		}
	}

	gnome_print_beginpage (pc, "testprint demo page");
	gnome_print_gsave (pc);
	for (x = 0; x < 200; x += 100)
		for (y = 0; y < 200; y += 100){
			gnome_print_grestore (pc);
			gnome_print_gsave (pc);

			if (x == 100 && y == 100)
				break;
			
			image_matrix [4] = x;
			image_matrix [5] = y;			
			gnome_print_concat (pc, image_matrix);

			gnome_print_rgbimage (pc, (char *) colorimg, 256, 256, 768);
		}
	gnome_print_showpage (pc);
}

#define G_P_PIXELS 256

static void
print_test_page (GnomePrintContext *pc)

{
  GnomeFont *font;
  double matrix[6] = {0.9, 0.1, -0.1, 0.9, 0, 0};
  double matrix_slanted[6] = {0.9, 0.1, -0.8, 0.9, 0, 0};
  double matrix2[6] = {1, 0, 0, 1, 0, 100};
  double matrix3[6] = {100, 0, 0, 100, 50, 300};
  double matrix4[6] = {100, 0, 0, 100, 50, 410};



  char img      [G_P_PIXELS] [G_P_PIXELS];
  char colorimg [G_P_PIXELS] [G_P_PIXELS] [3];
  int x, y;
  double width;
  gint pixels;

  gnome_print_beginpage (pc, "testprint demo page");
  gnome_print_gsave (pc);
    gnome_print_concat (pc, matrix);
    font = gnome_font_find_closest ("Times", GNOME_FONT_BOLD, 1, 36);
    if (font == NULL)
    {
	 g_warning (
	      "Since Times cannot be found, it would appear that fonts have\n"
	      "not been correctly installed. If you have Adobe or URW .pfb font files,\n"
	      "you can try:\n\n"
	      "./gnome-font-install --system --scan --no-copy --afm-path=%s/fonts/afms --pfb-assignment=ghostscript,<path> fonts/\n\n"
	      "Or see http://www.levien.com/gnome/font-install.html", DATADIR);
	 exit (0);
    }
    gnome_print_setfont (pc, font);
    gnome_print_setrgbcolor (pc, 0.9, 0.8, 0.6);
    width = gnome_font_get_width_string (font, _("Gnome-print test page, rotated"));
    gnome_print_moveto (pc, 145, 590);
    gnome_print_lineto (pc, 155 + width, 590);
    gnome_print_lineto (pc, 155 + width, 630);
    gnome_print_lineto (pc, 145, 630);
    gnome_print_fill (pc);
    gnome_print_moveto (pc, 150, 600);
    gnome_print_setrgbcolor (pc, 0, 0, 0);
    gnome_print_show (pc, _("Gnome-print test page, rotated"));
  gnome_print_grestore (pc);

  gnome_print_gsave (pc);
    gnome_print_setfont (pc, font);
    gnome_print_moveto (pc, 150, 650);
    gnome_print_concat (pc, matrix_slanted);
    gnome_print_show (pc, _("Gnome-print test page, slanted"));
  gnome_print_grestore (pc);

  gnome_print_gsave (pc);
    font = gnome_font_find_closest ("Times", GNOME_FONT_BOLD, 1, 10);
    gnome_print_setfont (pc, font);
    gnome_print_moveto (pc, 150, 700);
    gnome_print_concat (pc, matrix_slanted);
    gnome_print_show (pc, _("Gnome-print test page, slanted. Small"));
  gnome_print_grestore (pc);

    

#if 0
  font = gnome_font_new ("NimbusRomNo9L-Regu", 36);
  gnome_print_setfont (pc, font);
  gnome_print_show (pc, "Gnome-print test page");
  gnome_print_grestore (pc);

  gnome_print_setrgbcolor (pc, 1, 0, 0.5);
  gnome_print_moveto (pc, 50, 50);
  gnome_print_lineto (pc, 50, 90);
  gnome_print_curveto (pc, 70, 90, 90, 70, 90, 50);

  gnome_print_show (pc, "Gnome-print test page");
  gnome_print_grestore (pc);
#endif

  gnome_print_setrgbcolor (pc, 1, 0, 0.5);
  gnome_print_moveto (pc, 50, 50);
  gnome_print_lineto (pc, 50, 90);
  gnome_print_curveto (pc, 70, 90, 90, 70, 90, 50);
  gnome_print_closepath (pc);
  gnome_print_fill (pc);

  pixels = G_P_PIXELS;
  
  for (y = 0; y < pixels; y++)
      for (x = 0; x < pixels; x++)
	img[y][x] = ((x+y)*256/pixels)/2;

  gnome_print_gsave (pc);
    gnome_print_concat (pc, matrix3);
    gnome_print_moveto (pc, 0, 0);
    gnome_print_grayimage (pc, (char *)img, pixels, pixels, pixels);
  gnome_print_grestore (pc);
  
#if 0
  gnome_print_gsave (pc);
    g_print ("Image slanted\n");
    gnome_print_concat (pc, matrix_slanted);
    gnome_print_moveto (pc, 10, 10);
    gnome_print_grayimage (pc, (char *)img, pixels, pixels, pixels);
    gnome_print_moveto (pc, 10, 100);
    gnome_print_grayimage (pc, (char *)img, pixels, pixels, pixels);
    gnome_print_moveto (pc, 10, 100);
    gnome_print_grayimage (pc, (char *)img, pixels, pixels, pixels);
  gnome_print_grestore (pc);
#endif	

  for (y = 0; y < pixels; y++)
      for (x = 0; x < pixels; x++)
	{
	  colorimg[y][x][0] = (x + y) >> 1;
	  colorimg[y][x][1] = (x + (pixels - 1  - y)) >> 1;
	  colorimg[y][x][2] = ((pixels - 1 - x) + y) >> 1;
	}

  gnome_print_gsave (pc);
    gnome_print_concat (pc, matrix4);
    gnome_print_moveto (pc, 0, 0);
    gnome_print_rgbimage (pc, (char *)colorimg, pixels, pixels, pixels * 3);
  gnome_print_grestore (pc);

  gnome_print_concat (pc, matrix2);
  gnome_print_setrgbcolor (pc, 0, 0.5, 0);
  gnome_print_moveto (pc, 50, 50);
  gnome_print_lineto (pc, 50, 90);
  gnome_print_curveto (pc, 70, 90, 90, 70, 90, 50);
  gnome_print_closepath (pc);
  gnome_print_stroke (pc);

  gnome_print_concat (pc, matrix2);
  gnome_print_setrgbcolor (pc, 0, 0, 0.5);
  gnome_print_moveto (pc, 50, 50);
  gnome_print_lineto (pc, 50, 90);
  gnome_print_curveto (pc, 70, 90, 90, 70, 90, 50);
  gnome_print_closepath (pc);
  gnome_print_gsave (pc);
  gnome_print_stroke (pc);
  gnome_print_grestore (pc);
  gnome_print_clip (pc);

  gnome_print_moveto (pc, 50, 50);
  gnome_font_unref (font);
  font = gnome_font_find ("Courier", 18);
  gnome_print_setfont (pc, font);
  gnome_print_show (pc, "clip!");

  gnome_print_showpage (pc);

  gnome_font_unref (font);
}

int
main (int argc, char **argv)
{
	GnomePrintConfig *config;
	GnomePrintContext *pctx;
	poptContext ctx;
	char **args;

	g_type_init ();

	/* Parse arguments */
	ctx = poptGetContext (NULL, argc, argv, options, 0);
	g_return_val_if_fail (ctx != NULL, 1);
	g_return_val_if_fail (poptGetNextOpt (ctx) == -1, 1);
	args = (char **) poptGetArgs (ctx);

	config = gnome_print_config_default ();

	if (args && args[0]) {
		gint i;
		for (i = 0; args[i] != NULL; i ++) {
			if (strchr (args[i], '=')) {
				gchar *k, *v;
				k = g_strdup (args[i]);
				v = strchr (k, '=');
				*v++ = '\0';
				if (!gnome_print_config_set (config, k, v)) {
					g_warning ("Setting %s = %s failed", k, v);
				}
			} else {
				g_warning ("Argument is not key=value pair: %s", args[i]);
			}
		}
	}

	/* Free popt context */
	poptFreeContext (ctx);

	pctx = gnome_print_context_new (config);
	g_return_val_if_fail (pctx != NULL, 1);

	print_test_page (pctx);

	gnome_print_context_close (pctx);

	return 0;
}

