#ifndef __GNOME_PRINT_META_H__
#define __GNOME_PRINT_META_H__

/*
 *  Copyright (C) 1999-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Miguel de Icaza (miguel@gnu.org)
 *    Michael Zucchi <notzed@helixcode.com>
 *    Morten Welinder (terra@diku.dk)
 *    Lauris Kaplinski <lauris@ximian.com>
 *
 *  Metafile implementation for gnome-print
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glib.h>

G_BEGIN_DECLS

#define GNOME_TYPE_PRINT_META (gnome_print_meta_get_type ())
#define GNOME_PRINT_META(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GNOME_TYPE_PRINT_META, GnomePrintMeta))
#define GNOME_PRINT_META_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GNOME_TYPE_PRINT_META, GnomePrintMetaClass))
#define GNOME_IS_PRINT_META(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GNOME_TYPE_PRINT_META))
#define GNOME_IS_PRINT_META_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GNOME_TYPE_PRINT_META))
#define GNOME_PRINT_META_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GNOME_TYPE_PRINT_META, GnomePrintMetaClass))

typedef struct _GnomePrintMeta GnomePrintMeta;
typedef struct _GnomePrintMetaClass GnomePrintMetaClass;

#include <libgnomeprint/gnome-print.h>

GType gnome_print_meta_get_type (void);
GnomePrintMeta *gnome_print_meta_new (void);
GnomePrintMeta *gnome_print_meta_new_from (const void *data);

/* Returns the value in GnomePrinterMeta structure, shared with the object */
int gnome_print_meta_access_buffer (GnomePrintMeta *meta, void **buffer, int *buflen);
/* Duplicates the metadata buffer and returns a copy of it */
int gnome_print_meta_get_copy (GnomePrintMeta *meta, void **buffer, int *buflen);

/* Returns the number of pages generated */
int gnome_print_meta_pages (const GnomePrintMeta *meta);

/*
 * Rendering a metafile into a GnomePrintContext
 */

gboolean gnome_print_meta_render_from_object (GnomePrintContext *destination, const GnomePrintMeta *source);
gboolean gnome_print_meta_render (GnomePrintContext *destination, const void *meta_stream);
gboolean gnome_print_meta_render_page (GnomePrintContext *destination, const void *meta_stream, int page);
gboolean gnome_print_meta_render_from_object_page (GnomePrintContext *destination, const GnomePrintMeta *source, int page);

G_END_DECLS

#endif /* __GNOME_PRINT_META_H__ */
