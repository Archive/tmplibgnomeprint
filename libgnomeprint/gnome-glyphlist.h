#ifndef __GNOME_GLYPHLIST_H__
#define __GNOME_GLYPHLIST_H__

/*
 *  Copyright (C) 2000-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Lauris Kaplinski <lauris@ximian.com>
 *
 *  Experimental device independent rich text representation system
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glib.h>

G_BEGIN_DECLS

#define GNOME_IS_GLYPHLIST(g) gnome_glyphlist_check (g, FALSE)

typedef struct _GnomeGlyphList GnomeGlyphList;

#include <libgnomeprint/gnome-font.h>

gboolean gnome_glyphlist_check (const GnomeGlyphList *gl, gboolean rules);

/*
 * the _dumb_ versions of glyphlist creation
 * It just places glyphs one after another - no ligaturing etc.
 * text is utf8, of course
 */

GnomeGlyphList *gnome_glyphlist_from_text_dumb (GnomeFont *font, guint32 color,
						gdouble kerning, gdouble letterspace,
						const guchar *text);

GnomeGlyphList *gnome_glyphlist_from_text_sized_dumb (GnomeFont *font, guint32 color,
						      gdouble kerning, gdouble letterspace,
						      const guchar *text, gint length);

/*
 * Well - the API is slow and dumb. Hopefully you all will be composing
 * glyphlists by hand in future...
 */

void gnome_glyphlist_glyph (GnomeGlyphList *gl, gint glyph);
void gnome_glyphlist_glyphs (GnomeGlyphList *gl, gint *glyphs, gint num_glyphs);
void gnome_glyphlist_advance (GnomeGlyphList *gl, gboolean advance);
void gnome_glyphlist_moveto (GnomeGlyphList *gl, gdouble x, gdouble y);
void gnome_glyphlist_rmoveto (GnomeGlyphList *gl, gdouble x, gdouble y);
void gnome_glyphlist_font (GnomeGlyphList *gl, GnomeFont * font);
void gnome_glyphlist_color (GnomeGlyphList *gl, guint32 color);
void gnome_glyphlist_kerning (GnomeGlyphList *gl, gdouble kerning);
void gnome_glyphlist_letterspace (GnomeGlyphList *gl, gdouble letterspace);

void gnome_glyphlist_text_dumb (GnomeGlyphList *gl, const gchar *text);
void gnome_glyphlist_text_sized_dumb (GnomeGlyphList *gl, const gchar *text, gint length);

G_END_DECLS

#endif /* __GNOME_GLYPHLIST_H__ */

