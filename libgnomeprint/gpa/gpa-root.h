#ifndef _GPA_ROOT_H_
#define _GPA_ROOT_H_

/*
 * GPARoot
 *
 * Opaque root object to gnome-print configuration tree
 *
 * Authors:
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <glib.h>

G_BEGIN_DECLS

#define GPA_TYPE_ROOT (gpa_root_get_type ())
#define GPA_ROOT(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GPA_TYPE_ROOT, GPARoot))
#define GPA_ROOT_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GPA_TYPE_ROOT, GPARootClass))
#define GPA_IS_ROOT(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GPA_TYPE_ROOT))
#define GPA_IS_ROOT_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GPA_TYPE_ROOT))
#define GPA_ROOT_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GPA_TYPE_ROOT, GPARootClass))

typedef struct _GPARoot GPARoot;
typedef struct _GPARootClass GPARootClass;

#include "gpa-list.h"
#include "gpa-node-private.h"

struct _GPARoot {
	GPANode node;
	GPAList *vendors;
	GPAList *printers;
};

struct _GPARootClass {
	GPANodeClass node_class;
};

GType gpa_root_get_type (void);

GPARoot *gpa_root_get (void);
GPARoot *gpa_root (void);

#define GPA (gpa_root ())

G_END_DECLS

#endif

