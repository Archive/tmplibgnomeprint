#define __GPA_OPTION_C__

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <string.h>
#include <xmlmemory.h>
#include "gpa-value.h"
#include "gpa-option.h"

static void gpa_option_class_init (GPAOptionClass *klass);
static void gpa_option_init (GPAOption *option);
static void gpa_option_finalize (GObject *object);

static gboolean gpa_option_verify (GPANode *node);
static guchar *gpa_option_get_value (GPANode *node);
static GPANode *gpa_option_get_child (GPANode *node, GPANode *ref);
static GPANode *gpa_option_lookup (GPANode *node, const guchar *path);
static void gpa_option_modified (GPANode *node);

static GPAOption *gpa_option_new_node_from_tree (xmlNodePtr tree, const guchar *id);
static GPAOption *gpa_option_new_key_from_tree (xmlNodePtr tree, const guchar *id);
static GPAOption *gpa_option_new_list_from_tree (xmlNodePtr tree, const guchar *id);
static GPAOption *gpa_option_new_item_from_tree (xmlNodePtr tree, const guchar *id);
static GPAOption *gpa_option_new_string_from_tree (xmlNodePtr tree, const guchar *id);

static GPANodeClass *parent_class;

GType
gpa_option_get_type (void)
{
	static GType type = 0;
	if (!type) {
		static const GTypeInfo info = {
			sizeof (GPAOptionClass),
			NULL, NULL,
			(GClassInitFunc) gpa_option_class_init,
			NULL, NULL,
			sizeof (GPAOption),
			0,
			(GInstanceInitFunc) gpa_option_init
		};
		type = g_type_register_static (GPA_TYPE_NODE, "GPAOption", &info, 0);
	}
	return type;
}

static void
gpa_option_class_init (GPAOptionClass *klass)
{
	GObjectClass *object_class;
	GPANodeClass *node_class;

	object_class = (GObjectClass *) klass;
	node_class = (GPANodeClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gpa_option_finalize;

	node_class->verify = gpa_option_verify;
	node_class->get_value = gpa_option_get_value;
	node_class->get_child = gpa_option_get_child;
	node_class->lookup = gpa_option_lookup;
	node_class->modified = gpa_option_modified;
}

static void
gpa_option_init (GPAOption *option)
{
	option->type = GPA_OPTION_NONE;
	option->children = NULL;
	option->value = NULL;
}

static void
gpa_option_finalize (GObject *object)
{
	GPAOption *option;

	option = GPA_OPTION (object);

	if (option->name) {
		option->name->parent = NULL;
		gpa_node_unref (option->name);
		option->name = NULL;
	}

	while (option->children) {
		GPANode *child;
		child = option->children;
		option->children = child->next;
		child->parent = NULL;
		child->next = NULL;
		gpa_node_unref (child);
	}

	if (option->value) {
		g_free (option->value);
		option->value = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static gboolean
gpa_option_verify (GPANode *node)
{
	GPAOption *option;
	GPANode *child;

	option = GPA_OPTION (node);

	/* Each option has to have Id */
	if (!node->id) return FALSE;

	switch (option->type) {
	case GPA_OPTION_NODE:
		/* !Name, !Value, children */
		if (option->name) return FALSE;
		if (option->value) return FALSE;
		if (!option->children) return FALSE;
		for (child = option->children; child != NULL; child = child->next) {
			if (!GPA_IS_OPTION (child)) return FALSE;
			if (!gpa_node_verify (child)) return FALSE;
		}
		break;
	case GPA_OPTION_KEY:
		/* !Name, Value || children */
		if (option->name) return FALSE;
		if (!option->value && !option->children) return FALSE;
		for (child = option->children; child != NULL; child = child->next) {
			if (!GPA_IS_OPTION (child)) return FALSE;
			if (GPA_OPTION (child)->type != GPA_OPTION_KEY) return FALSE;
			if (!gpa_node_verify (child)) return FALSE;
		}
		break;
	case GPA_OPTION_LIST:
		/* List should not have name */
		if (option->name) return FALSE;
		/* List has to have default value */
		if (!option->value) return FALSE;
		/* List has to have children of type item */
		if (!option->children) return FALSE;
		for (child = option->children; child != NULL; child = child->next) {
			if (!GPA_IS_OPTION (child)) return FALSE;
			if (GPA_OPTION (child)->type != GPA_OPTION_ITEM) return FALSE;
			if (!gpa_node_verify (child)) return FALSE;
		}
		break;
	case GPA_OPTION_ITEM:
		/* Item has to have name */
		if (!option->name) return FALSE;
		if (!gpa_node_verify (option->name)) return FALSE;
		/* Item should not have value */
		if (option->value) return FALSE;
		/* Item may have children */
		for (child = option->children; child != NULL; child = child->next) {
			if (!GPA_IS_OPTION (child)) return FALSE;
			if (!gpa_node_verify (child)) return FALSE;
		}
		break;
	case GPA_OPTION_STRING:
		/* String should not have name */
		if (option->name) return FALSE;
		/* String should have value */
		if (!option->value) return FALSE;
		/* String should not have children */
		if (option->children) return FALSE;
		break;
	default:
		return FALSE;
	}
	return TRUE;
}

static guchar *
gpa_option_get_value (GPANode *node)
{
	GPAOption *option;

	option = GPA_OPTION (node);

	if (option->value) return g_strdup (option->value);
	if (node->id) return g_strdup (node->id);

	return NULL;
}

static GPANode *
gpa_option_get_child (GPANode *node, GPANode *ref)
{
	GPAOption *option;

	option = GPA_OPTION (node);

	if (!ref) {
		if (option->children) gpa_node_ref (option->children);
		return option->children;
	}

	if (ref->next) gpa_node_ref (ref->next);

	return ref->next;
}

static GPANode *
gpa_option_lookup (GPANode *node, const guchar *path)
{
	GPAOption *option;
	GPANode *child;
	const guchar *dot, *next;
	gint len;

	option = GPA_OPTION (node);

	if (!strncmp (path, "Name", 4)) {
		if (!option->name) return NULL;
		if (!path[4]) {
			gpa_node_ref (GPA_NODE (option->name));
			return GPA_NODE (option->name);
		} else {
			g_return_val_if_fail (path[4] == '.', NULL);
			return gpa_node_lookup (GPA_NODE (option->name), path + 5);
		}
	}

	dot = strchr (path, '.');
	if (dot != NULL) {
		len = dot - path;
		next = dot + 1;
	} else {
		len = strlen (path);
		next = path + len;
	}

	for (child = option->children; child != NULL; child = child->next) {
		g_assert (GPA_IS_OPTION (child));
		if (child->id && strlen (child->id) == len && !strncmp (child->id, path, len)) {
			if (!next) {
				gpa_node_ref (child);
				return child;
			} else {
				return gpa_node_lookup (child, next);
			}
		}
	}

	return NULL;
}

static void
gpa_option_modified (GPANode *node)
{
	GPAOption *option;
	GPANode *child;

	option = GPA_OPTION (node);

	if (option->name && (GPA_NODE_FLAGS (option->name) & GPA_MODIFIED_FLAG)) {
		gpa_node_emit_modified (option->name);
	}

	child = option->children;
	while (child) {
		GPANode *next;
		next = child->next;
		if (GPA_NODE_FLAGS (child) & GPA_MODIFIED_FLAG) {
			gpa_node_ref (child);
			gpa_node_emit_modified (child);
			gpa_node_unref (child);
		}
		child = next;
	}
}

GPANode *
gpa_option_new_from_tree (xmlNodePtr tree)
{
	GPAOption *option;
	xmlChar *xmlid;

	g_return_val_if_fail (tree != NULL, NULL);

	xmlid = xmlGetProp (tree, "Id");
	if (!xmlid) {
		g_warning ("Option node does not have Id");
		return NULL;
	}

	option = NULL;

	if (!strcmp (tree->name, "Key")) {
		option = gpa_option_new_key_from_tree (tree, xmlid);
	} else if (!strcmp (tree->name, "Item")) {
		option = gpa_option_new_item_from_tree (tree, xmlid);
	} else if (!strcmp (tree->name, "Option")) {
		xmlChar *xmltype;
		xmltype = xmlGetProp (tree, "Type");
		if (!xmltype) {
			option = gpa_option_new_node_from_tree (tree, xmlid);
		} else if (!strcmp (xmltype, "List")) {
			xmlFree (xmltype);
			option = gpa_option_new_list_from_tree (tree, xmlid);
		} else if (!strcmp (xmltype, "String")) {
			xmlFree (xmltype);
			option = gpa_option_new_string_from_tree (tree, xmlid);
		} else {
			xmlFree (xmltype);
		}
	}

	xmlFree (xmlid);

	return (GPANode *) option;
}

/* !Name, !Value, children */

static GPAOption *
gpa_option_new_node_from_tree (xmlNodePtr tree, const guchar *id)
{
	GPAOption *option;
	xmlChar *xmlval;
	xmlNodePtr xmlc;
	GSList *l;

	xmlval = xmlGetProp (tree, "Default");
	if (xmlval) {
		g_warning ("Option should not have default");
		xmlFree (xmlval);
		return NULL;
	}

	xmlval = xmlGetProp (tree, "Value");
	if (xmlval) {
		g_warning ("Option should not have value");
		xmlFree (xmlval);
		return NULL;
	}

	l = NULL;

	for (xmlc = tree->xmlChildrenNode; xmlc != NULL; xmlc = xmlc->next) {
		if (xmlc->type == XML_ELEMENT_NODE) {
			if (!strcmp (xmlc->name, "Name")) {
				g_warning ("List node should not have name");
				return NULL;
			} else if (!strcmp (xmlc->name, "Option")) {
				GPANode *cho;
				cho = gpa_option_new_from_tree (xmlc);
				if (cho) l = g_slist_prepend (l, cho);
			} else {
				g_warning ("Invalid child in option tree %s", xmlc->name);
			}
		}
	}

	if (!l) {
		g_warning ("Option should have children");
		return NULL;
	}

	option = (GPAOption *) gpa_node_new (GPA_TYPE_OPTION, id);
	option->type = GPA_OPTION_NODE;

	while (l) {
		GPANode *cho;
		cho = GPA_NODE (l->data);
		l = g_slist_remove (l, cho);
		cho->parent = GPA_NODE (option);
		cho->next = option->children;
		option->children = cho;
	}

	return option;
}

/* !Name, Value || children */

static GPAOption *
gpa_option_new_key_from_tree (xmlNodePtr tree, const guchar *id)
{
	GPAOption *option;
	xmlChar *xmlval;
	xmlNodePtr xmlc;
	GSList *l;

	xmlval = xmlGetProp (tree, "Default");
	if (xmlval) {
		g_warning ("Key should not have default");
		xmlFree (xmlval);
		return NULL;
	}

	xmlval = xmlGetProp (tree, "Value");
	if (!xmlval && !tree->xmlChildrenNode) {
		g_warning ("Key node should have value or children or both");
		return NULL;
	}

	l = NULL;

	for (xmlc = tree->xmlChildrenNode; xmlc != NULL; xmlc = xmlc->next) {
		if (xmlc->type == XML_ELEMENT_NODE) {
			if (!strcmp (xmlc->name, "Name")) {
				g_warning ("Key node should not have name");
				if (xmlval) xmlFree (xmlval);
				return NULL;
			} else if (!strcmp (xmlc->name, "Key")) {
				GPANode *cho;
				cho = gpa_option_new_from_tree (xmlc);
				if (cho) l = g_slist_prepend (l, cho);
			} else {
				g_warning ("Invalid child in option tree %s", xmlc->name);
			}
		}
	}

	if (!xmlval && !l) {
		g_warning ("Key node should have value or children or both");
		return NULL;
	}

	option = (GPAOption *) gpa_node_new (GPA_TYPE_OPTION, id);
	option->type = GPA_OPTION_KEY;
	if (xmlval) {
		option->value = g_strdup (xmlval);
		xmlFree (xmlval);
	}

	while (l) {
		GPANode *cho;
		cho = GPA_NODE (l->data);
		l = g_slist_remove (l, cho);
		cho->parent = GPA_NODE (option);
		cho->next = option->children;
		option->children = cho;
	}

	return option;
}

/* !Name, default, children */

static GPAOption *
gpa_option_new_list_from_tree (xmlNodePtr tree, const guchar *id)
{
	GPAOption *option;
	xmlChar *xmlval, *xmldef;
	xmlNodePtr xmlc;
	GSList *l;
	gboolean has_default;

	xmlval = xmlGetProp (tree, "Value");
	if (xmlval) {
		g_warning ("List node should not have value");
		xmlFree (xmlval);
		return NULL;
	}

	xmldef = xmlGetProp (tree, "Default");
	if (!xmldef) {
		g_warning ("List node should have default");
		return NULL;
	}

	l = NULL;
	has_default = FALSE;

	for (xmlc = tree->xmlChildrenNode; xmlc != NULL; xmlc = xmlc->next) {
		if (xmlc->type == XML_ELEMENT_NODE) {
			if (!strcmp (xmlc->name, "Name")) {
				g_warning ("List node should not have name");
				xmlFree (xmldef);
				return NULL;
			} else if (!strcmp (xmlc->name, "Item")) {
				GPANode *cho;
				cho = gpa_option_new_from_tree (xmlc);
				if (cho) {
					l = g_slist_prepend (l, cho);
					if (!strcmp (xmldef, cho->id)) has_default = TRUE;
				}
			} else {
				g_warning ("Invalid list child in option tree %s", xmlc->name);
			}
		}
	}

	if (!has_default) {
		g_warning ("Invalid default value in option list %s", xmldef);
		while (l) {
			gpa_node_unref (GPA_NODE (l->data));
			l = g_slist_remove (l, l->data);
		}
		xmlFree (xmldef);
		return NULL;
	}

	if (!l) {
		g_warning ("List has to have children of type item");
		xmlFree (xmldef);
		return NULL;
	}

	option = (GPAOption *) gpa_node_new (GPA_TYPE_OPTION, id);
	option->type = GPA_OPTION_LIST;
	option->value = g_strdup (xmldef);
	xmlFree (xmldef);

	while (l) {
		GPANode *cho;
		cho = GPA_NODE (l->data);
		l = g_slist_remove (l, cho);
		cho->parent = GPA_NODE (option);
		cho->next = option->children;
		option->children = cho;
	}

	return option;
}

/* Name, !value, optional children */

static GPAOption *
gpa_option_new_item_from_tree (xmlNodePtr tree, const guchar *id)
{
	GPAOption *option;
	xmlChar *xmlval;
	xmlNodePtr xmlc;
	GPANode *name;
	GSList *l;

	xmlval = xmlGetProp (tree, "Value");
	if (xmlval) {
		g_warning ("Item node should not have value");
		xmlFree (xmlval);
		return NULL;
	}

	xmlval = xmlGetProp (tree, "Default");
	if (xmlval) {
		g_warning ("Item node should not have default");
		xmlFree (xmlval);
		return NULL;
	}

	name = NULL;
	l = NULL;

	for (xmlc = tree->xmlChildrenNode; xmlc != NULL; xmlc = xmlc->next) {
		if (xmlc->type == XML_ELEMENT_NODE) {
			if (!strcmp (xmlc->name, "Name")) {
				name = gpa_value_new_from_tree ("Name", xmlc);
			} else if (!strcmp (xmlc->name, "Option") || !strcmp (xmlc->name, "Key")) {
				GPANode *cho;
				cho = gpa_option_new_from_tree (xmlc);
				if (cho) l = g_slist_prepend (l, cho);
			} else {
				g_warning ("Invalid tag in option tree %s", xmlc->name);
			}
		}
	}

	if (!name) {
		g_warning ("Missing \"Name\" tag in item description");
		while (l) {
			gpa_node_unref (GPA_NODE (l->data));
			l = g_slist_remove (l, l->data);
		}
		return NULL;
	}

	option = (GPAOption *) gpa_node_new (GPA_TYPE_OPTION, id);
	option->type = GPA_OPTION_ITEM;
	option->name = name;

	while (l) {
		GPANode *cho;
		cho = GPA_NODE (l->data);
		l = g_slist_remove (l, cho);
		cho->parent = GPA_NODE (option);
		cho->next = option->children;
		option->children = cho;
	}

	return option;
}

/* !Name, value, !children */

static GPAOption *
gpa_option_new_string_from_tree (xmlNodePtr tree, const guchar *id)
{
	GPAOption *option;
	xmlChar *xmlval;
	xmlChar *defval;

	xmlval = xmlGetProp (tree, "Value");
	if (xmlval) {
		g_warning ("Item node should not have value");
		xmlFree (xmlval);
		return NULL;
	}

	defval = xmlGetProp (tree, "Default");
	if (!defval) {
		g_warning ("String node should have default");
		return NULL;
	}

	option = (GPAOption *) gpa_node_new (GPA_TYPE_OPTION, id);
	option->type = GPA_OPTION_STRING;
	option->value = g_strdup (defval);
	xmlFree (defval);

	return option;
}

/* GPAOptionList */

GPAList *
gpa_option_list_new_from_tree (xmlNodePtr tree)
{
	GPAList *options;
	xmlNodePtr xmlc;
	GSList *l;

	g_return_val_if_fail (!strcmp (tree->name, "Options"), NULL);

	l = NULL;
	for (xmlc = tree->xmlChildrenNode; xmlc != NULL; xmlc = xmlc->next) {
		if (!strcmp (xmlc->name, "Option") || !strcmp (xmlc->name, "Item") || !strcmp (xmlc->name, "Key")) {
			GPANode *option;
			option = gpa_option_new_from_tree (xmlc);
			if (option) l = g_slist_prepend (l, option);
		}
	}

	options = GPA_LIST (gpa_list_new (GPA_TYPE_OPTION, FALSE));

	while (l) {
		GPANode *option;
		option = GPA_NODE (l->data);
		l = g_slist_remove (l, option);
		option->parent = GPA_NODE (options);
		option->next = options->children;
		options->children = option;
	}

	return options;
}

/* Strictly private helpers */

GPAOption *
gpa_option_get_child_by_id (GPAOption *option, const guchar *id)
{
	GPANode *child;

	g_return_val_if_fail (option != NULL, NULL);
	g_return_val_if_fail (GPA_IS_OPTION (option), NULL);
	g_return_val_if_fail (id != NULL, NULL);
	g_return_val_if_fail (*id != '\0', NULL);

	for (child = option->children; child != NULL; child = child->next) {
		if (child->id && !strcmp (id, child->id)) {
			g_assert (GPA_IS_OPTION (child));
			gpa_node_ref (child);
			return GPA_OPTION (child);
		}
	}

	return NULL;
}

#if 0
GpaOption*
gpa_option_get_from_id (GList *option_list, const gchar *id)
{
	GpaOption *option;
	GList *list;

	list = option_list;
	for (; list != NULL; list = list->next) {
		option = list->data;
		if (strcmp (GPA_ITEM (option)->id, id) == 0)
			return option;
	}

	return NULL;
}

#if 0
/* fixme: temporary hack */
static GPAModel *
gpa_option_model (GpaOption *option)
{
	if (option->parent->parent) {
		return GPA_MODEL (GPA_NODE (option->parent->parent->parent)->parent->parent);
	} else {
		return GPA_MODEL (GPA_NODE (option->parent)->parent->parent);
	}
}
#endif

GpaOption *
gpa_option_new_from_node (xmlNodePtr tree_,
			  GpaOptions *parent,
			  GList *loaded_option_list)
{
	GpaOption *option;
	xmlNodePtr tree;
	xmlNodePtr child;
	gchar *name;
	gchar *id;

	debug (FALSE, "");

	tree = gpa_include_node (tree_);

	g_return_val_if_fail (tree != NULL, NULL);
	
	if (!gpa_xml_node_verify (tree, GPA_TAG_OPTION))
		return NULL;

	/* Get the name & id */
	name = gpa_xml_get_value_string_required (tree, GPA_TAG_NAME, NULL);
	id   = gpa_xml_get_value_string_required (tree, GPA_TAG_ID, parent->name);

	if (name == NULL)
		return NULL;
	if (id == NULL)
		return NULL;
	
	/* Find if there is another "option" object with this ID" */
	if (gpa_option_get_from_id (loaded_option_list, id) != NULL) {
		gpa_error ("The option \"%s\" is duplicated, under the "
			   "\"%s\" options.", id, GPA_ITEM (parent)->id);
		return NULL;
	}

	option = gpa_option_new (name, id, parent);

	g_free (name);
	g_free (id);

	/* Load the child options for this option */
	option->children = NULL;
	child = gpa_xml_search_child (tree, GPA_TAG_OPTIONS);
	if (child != NULL) {
		option->children = gpa_options_new_from_node (child, option);
		if (option->children == NULL)
			return NULL;
	}

	/* Load the values */
	option->values = NULL;
	child = gpa_xml_search_child (tree, GPA_TAG_OPTION_INFO);
	if (child != NULL) {
		option->values = gpa_xml_utils_new_hash_from_node (child,
							       GPA_TAG_OPTION_INFO);
		if (option->values == NULL) {
			gpa_error ("The \"%s\" node not be loaded", option->name);
			return NULL;
		}
	}

	/* Load the constraints */
	option->constraints = NULL;
	child = gpa_xml_search_child (tree, GPA_TAG_CONSTRAINTS);
	if (child != NULL)
		g_warning ("Constraints are no longer children of Options");

	child = gpa_xml_search_child (tree, GPA_TAG_DEFAULT);
	option->def = (child != NULL);

	return option;
}

/* fixme: Temporary hack (Lauris) */
GList *
gpa_option_parse_default (GpaOption *option, GList *list)
{
	g_assert (option != NULL);
	g_assert (GPA_IS_OPTION (option));

	if (option->def) {
		gchar *path;
		path = gpa_option_dup_path (option);
		list = g_list_prepend (list, path);
	}

	if (option->children) {
		list = gpa_options_parse_default (option->children, list);
	}

	return list;
}

GpaOption *
gpa_option_copy (GpaOption *option,
		 GpaOptions *parent)
{
	GpaOption *new_option;

	new_option = gpa_option_new (option->name,
				     GPA_ITEM (option)->id,
				     parent);

	new_option->def = option->def;
	new_option->children    = gpa_options_copy (option->children);
	new_option->values      = gpa_hash_copy (option->values);
	new_option->constraints = gpa_constraints_copy (option->constraints);

	return new_option;
}

xmlNodePtr
gpa_option_write (XmlParseContext *context, GpaOption *option)
{
	xmlNodePtr node;
	xmlNodePtr children;
	xmlNodePtr values = NULL;
	
	debug (FALSE, "");

	g_return_val_if_fail (option != NULL,         NULL);
	g_return_val_if_fail (option->values != NULL, NULL);

	/* Write the FileInfo */
	node = xmlNewDocNode (context->doc, context->ns, GPA_TAG_OPTION, NULL);

	/* Scan the list of options and if option->ty */
	xmlNewChild (node, context->ns, GPA_TAG_NAME, option->name);
	xmlNewChild (node, context->ns, GPA_TAG_ID,   GPA_ITEM (option)->id);

	/* Write the values */
	values = gpa_hash_write (context, option->values, GPA_TAG_OPTION_INFO);
	if (values != NULL)
		xmlAddChild (node, values);


#if 0 /* We write constraints under the model node, now ..*/
/* Write constraints */
	constraints = gpa_constraints_write (context, option);
	if (constraints != NULL)
		xmlAddChild (node, constraints);
#endif	

	/* There are no options */
	if (option->children == NULL)
		return node;

	/* The optional children structure */
	children = gpa_options_write (context, option->children);
	if (children != NULL)
		xmlAddChild (node, children);

	debug (FALSE, "");
	
	return node;
}


gboolean
gpa_option_verify (GpaOption *option)
{
	debug (FALSE, "");

	g_return_val_if_fail (option != NULL, FALSE);

	if (option->name == NULL) {
		gpa_error ("The Option object did not contained a name");
		return FALSE;
	}
	if (GPA_ITEM (option)->id == NULL) {
		gpa_error ("The Option \"%s\"object did not contained a valid id",
			   option->name);
		return FALSE;
	}

	if ((option->content != GPA_CONTENT_GENERIC) &&
	    (option->content != GPA_CONTENT_PAPER_SIZE) &&
	    (option->content != GPA_CONTENT_PAPER_MEDIA) &&
	    (option->content != GPA_CONTENT_PAPER_SOURCE) &&
	    (option->content != GPA_CONTENT_PAPER_ORIENTATION) &&
	    (option->content != GPA_CONTENT_RESOLUTION) &&
	    (option->content != GPA_CONTENT_RESOLUTION_MODE) &&
	    (option->content != GPA_CONTENT_ERROR)) {
		gpa_error ("The Option \"%s\" object contained an invalid content id [%i]",
			   option->name, option->content);
		return FALSE;
	}

	if (option->constraints != NULL)
		if (!gpa_constraints_verify (option))
			return FALSE;

	if (!GPA_IS_OPTIONS (option->parent)) {
		gpa_error ("The option \"%s\" object did not cotained a reference to a parent",
			   option->name);
		return FALSE;
	}

	if (option->parent->options_type == GPA_OPTIONS_TYPE_BOOLEAN) {
		if ( (strcmp (GPA_ITEM (option)->id, GPA_TAG_TRUE) != 0) &&
		     (strcmp (GPA_ITEM (option)->id, GPA_TAG_FALSE) != 0)) {
			gpa_error ("The boolean option \"%s\" (under \"%s\") contained "
				   "an invalid id :\"%s\"\nExpected (%s/%s)",
				   option->name, option->parent->name, GPA_ITEM (option)->id,
				   GPA_TAG_TRUE, GPA_TAG_FALSE);
			return FALSE;
		}
	}
	    
		
#if 0	/* FIXME */
	if (option->values != NULL)
		if (!gpa_values_verify_option (option))
			return FALSE;
#endif	

	return TRUE;
}


gboolean
gpa_option_verify_with_settings (GpaOption *option,
				 GpaSettings *settings)
{

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_SETTINGS (settings), FALSE);
	g_return_val_if_fail (option != NULL, FALSE);
	
	if (!gpa_option_verify (option))
		return FALSE;
		
	if (option->constraints != NULL)
		if (!gpa_constraints_verify_with_settings (option, settings))
			return FALSE;

	return TRUE;
}


GpaOption *
gpa_option_get_from_printer_and_path  (const GPAPrinter *printer, const gchar *path_)
{
	g_return_val_if_fail (path_ != NULL, NULL);
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);

	return gpa_option_get_from_options_list_and_path
		(printer->model->options_list, path_);
}

GpaOption *
gpa_option_get_from_model_and_path  (const GPAModel *model, const gchar *path_)
{
	g_return_val_if_fail (path_ != NULL, NULL);
	g_return_val_if_fail (GPA_IS_MODEL (model), NULL);

	return gpa_option_get_from_options_list_and_path
		(model->options_list, path_);
}

GpaOption *
gpa_option_get_from_settings_and_path (const GpaSettings *settings, const gchar *path_)
{
#if 0
	/* fixme: Lauris */
	g_return_val_if_fail (path_ != NULL, NULL);
	g_return_val_if_fail (GPA_IS_SETTINGS (settings), NULL);

	return gpa_option_get_from_options_list_and_path
		(settings->printer->model->options_list, path_);
#endif
	return NULL;
}


static GpaOption *
gpa_option_get_from_options_and_path (GpaOptions *options, const gchar *path)
{
	GpaOption *option = NULL;
	GList *list;
	gchar *option_id = NULL;
	gint pos;

	pos = gpa_tu_search (path, GPA_PATH_DELIMITER, TRUE);
	if (pos == -1) {
		gpa_error ("Invalid path %s, does not contain an Option\n", path);
		return NULL;
	}

	option_id = g_strndup (path, pos);
	list = options->children;
	for (; list != NULL; list = list->next) {
		option = (GpaOption *)list->data;
		if (strcmp (GPA_ITEM (option)->id, option_id) == 0)
			break;
	}

	if (list == NULL) {
		gpa_error ("Option not found from path \"%s\" in \"%s\"(2)",
			   path, gpa_options_get_name (options));
		g_free (option_id);
		return NULL;
	}

	if (strlen(path + pos + 1) > 0) {
		GpaOption *sub_option;
		gchar *subpath;
		subpath = g_strdup (path+pos+1+strlen(GPA_ITEM (option->children)->id)+1);
		sub_option = gpa_option_get_from_options_and_path (option->children,
								   subpath);
		g_free (subpath);
		g_free (option_id);
		return sub_option;
	}
	
	g_free (option_id);

	return option;

}

GpaOption *
gpa_option_get_from_options_list_and_path (GPAList *options_list, const gchar *path_)
{
	GpaOptions *options;
	GPAItem *item;
	GpaOption *option = NULL;
	gchar *options_id = NULL;
	gchar *path;
	gchar *remainder;
	gint pos;

	g_return_val_if_fail (options_list != NULL, NULL);
	g_return_val_if_fail (path_   != NULL, NULL);

	path = g_strdup_printf ("%s" GPA_PATH_DELIMITER, path_);
	pos = gpa_tu_search (path, GPA_PATH_DELIMITER, TRUE);
	if (pos == -1) {
		gpa_error ("Invalid path %s\n", path);
		goto gpa_option_get_from_options_list_and_path_error;
	}

	options_id = g_strndup (path, pos);

	options = NULL;
	for (item = options_list->items; item != NULL; item = item->next) {
		options = GPA_OPTIONS (item);
		if (strcmp (GPA_ITEM (options)->id, options_id) == 0)
			break;
	}

	if (item == NULL) {
		gpa_error ("Options not found from path \"%s\" (1.531)[%s]", path, path_);
		goto gpa_option_get_from_options_list_and_path_error;
	}

	remainder = g_strdup (path+pos+1);
	option = gpa_option_get_from_options_and_path (options, remainder);
	g_free (remainder);

	g_free (options_id);
	g_free (path);

	return option;

gpa_option_get_from_options_list_and_path_error:

	if (options_id != NULL)
		g_free (options_id);
	g_free (path);
	
	return NULL;
}

gboolean 
gpa_option_is_selected (const GpaSettings *settings,
			const GpaOption *option_findme)
{
	GpaOption *option= NULL;
	GList *list;
	gchar *path;

	debug (FALSE, "");
	
	g_return_val_if_fail (option_findme != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_SETTINGS (settings), FALSE);

	list = settings->selected_options;
	for (; list != NULL; list = list->next) {
		path = (gchar *) list->data;
		option = gpa_option_get_from_settings_and_path (settings, path);
		if (option == NULL)
			return FALSE;
		if (option == option_findme)
			return TRUE;
	}

	return FALSE;
}

gchar *
gpa_option_dup_path (GpaOption *option)
{
	gchar *path;
	
	debug (FALSE, "");

	g_return_val_if_fail (option != NULL, NULL);

	if (option->parent->parent != NULL) {
		if (option->parent->parent->parent->parent != NULL) {
			gpa_error ("We only support 1st & 2nd level option");
		}
		path  = g_strdup_printf ("%s" GPA_PATH_DELIMITER
					 "%s" GPA_PATH_DELIMITER
					 "%s" GPA_PATH_DELIMITER
					 "%s",
					 GPA_ITEM (option->parent->parent->parent)->id,
					 GPA_ITEM (option->parent->parent)->id,
					 GPA_ITEM (option->parent)->id,
					 GPA_ITEM (option)->id);
	} else {
		path = g_strdup_printf ("%s" GPA_PATH_DELIMITER "%s", GPA_ITEM (option->parent)->id, GPA_ITEM (option)->id);
	}
		
	return path;
}


gchar*
gpa_option_value_dup (const GpaOption *option, const gchar *key)
{
	GHashTable *hash;
	gchar *value;
	
	debug (FALSE, "");

	g_return_val_if_fail (option != NULL, NULL);
	g_return_val_if_fail (key != NULL,    NULL);

	hash = option->values;

	if (hash == NULL) {
		gpa_error ("Hash value not found. Option:\"%s\" Key:\"%s\"",
			   option->name,
			   key);
		return NULL;
	}

	value = g_hash_table_lookup (hash, key);

	debug (FALSE, "end");

	return g_strdup (value);
}


void
gpa_option_value_get_int (const GpaOption *option, const gchar *key, int *value)
{
	gchar *str_value;
	
	debug (FALSE, "");

	str_value = gpa_option_value_dup (option, key);

	if (str_value == NULL) 
		return;

	*value = atoi (str_value);

	g_free (str_value);
}
	
void
gpa_option_value_get_double (const GpaOption *option, const gchar *key, double *value)
{
	gchar *str_value;
	
	debug (FALSE, "");

	str_value = gpa_option_value_dup (option, key);

	if (str_value == NULL) 
		return;

	*value = atof (str_value);

	g_free (str_value);
}

/* Access to the structure */
gchar *
gpa_option_dup_name (const GpaOption *option)
{
	g_return_val_if_fail (GPA_IS_OPTION (option), NULL);

	return g_strdup (option->name);
}

const gchar *
gpa_option_get_name (const GpaOption *option)
{
	g_return_val_if_fail (GPA_IS_OPTION (option), NULL);

	return option->name;
}

const gchar *
gpa_option_get_id (const GpaOption *option)
{
	g_return_val_if_fail (GPA_IS_OPTION (option), NULL);

	return GPA_ITEM (option)->id;
}


const GpaOptions *
gpa_option_get_parent (const GpaOption *option)
{
	g_return_val_if_fail (GPA_IS_OPTION (option), NULL);

	return option->parent;
}

const GpaOptions *
gpa_option_get_children (const GpaOption *option)
{
	g_return_val_if_fail (GPA_IS_OPTION (option), NULL);

	return option->children;
}
#endif
