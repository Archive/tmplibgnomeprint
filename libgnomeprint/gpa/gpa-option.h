#ifndef __GPA_OPTION_H__
#define __GPA_OPTION_H__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <glib.h>

G_BEGIN_DECLS

#define GPA_TYPE_OPTION (gpa_option_get_type ())
#define GPA_OPTION(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GPA_TYPE_OPTION, GPAOption))
#define GPA_OPTION_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GPA_TYPE_OPTION, GPAOptionClass))
#define GPA_IS_OPTION(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GPA_TYPE_OPTION))
#define GPA_IS_OPTION_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GPA_TYPE_OPTION))
#define GPA_OPTION_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GPA_TYPE_OPTION, GPAOptionClass))

typedef struct _GPAOption GPAOption;
typedef struct _GPAOptionClass GPAOptionClass;

#include <tree.h>
#include "gpa-list.h"

typedef enum {
	GPA_OPTION_NONE,
	GPA_OPTION_NODE,
	GPA_OPTION_KEY,
	GPA_OPTION_LIST,
	GPA_OPTION_ITEM,
	GPA_OPTION_STRING
} GPAOptionType;

struct _GPAOption {
	GPANode node;
	GPAOptionType type;
	GPANode *name;
	GPANode *children;
	guchar *value;
};

struct _GPAOptionClass {
	GPANodeClass node_class;
};

GType gpa_option_get_type (void);

GPANode *gpa_option_new_from_tree (xmlNodePtr tree);

GPAList *gpa_option_list_new_from_tree (xmlNodePtr tree);

#define GPA_OPTION_IS_NODE(o) (o && GPA_IS_OPTION (o) && (o->type == GPA_OPTION_NODE))
#define GPA_OPTION_IS_KEY(o) (o && GPA_IS_OPTION (o) && (o->type == GPA_OPTION_KEY))
#define GPA_OPTION_IS_LIST(o) (o && GPA_IS_OPTION (o) && (o->type == GPA_OPTION_LIST))
#define GPA_OPTION_IS_ITEM(o) (o && GPA_IS_OPTION (o) && (o->type == GPA_OPTION_ITEM))
#define GPA_OPTION_IS_STRING(o) (o && GPA_IS_OPTION (o) && (o->type == GPA_OPTION_STRING))

/* Strictly private helpers */

GPAOption *gpa_option_get_child_by_id (GPAOption *option, const guchar *id);

#if 0
/* fixme: Temporary hack (Lauris) */
GList *gpa_option_parse_default (GpaOption *option, GList *list);

/* XML writing & reading */
xmlNodePtr gpa_option_write  (XmlParseContext *context, GpaOption *option);

GpaOption * gpa_option_new_from_node (xmlNodePtr tree_,
				      GpaOptions *parent,
				      GList *loaded_option_list);

/* Paths */
    gchar * gpa_option_dup_path (GpaOption *option);
GpaOption * gpa_option_get_from_options_list_and_path (GPAList *options_list,
						       const gchar *path_);
GpaOption * gpa_option_get_from_settings_and_path (const GpaSettings *settings,
						   const gchar *path_);
GpaOption * gpa_option_get_from_printer_and_path  (const GPAPrinter *printer,
						   const gchar *path_);
GpaOption * gpa_option_get_from_model_and_path  (const GPAModel *model,
						 const gchar *path_);



/* Brought here from public header, until resolved (Lauris) */

/* Basic "Options" Operations */
GpaOption * gpa_option_new (const gchar *name,
			    const gchar * id,
			    GpaOptions *parent);
GpaOption * gpa_option_copy (GpaOption *option,
			     GpaOptions *parent);
void       gpa_option_free   (GpaOption *option);
gboolean   gpa_option_verify (GpaOption *option);
gboolean   gpa_option_verify_with_settings (GpaOption *option,
					    GpaSettings *settings);


/* Convenience functions */
gboolean  gpa_option_is_selected (const GpaSettings *settings,
				  const GpaOption *option);
GpaOption* gpa_option_get_from_id (GList *option_list, const gchar *id);

/* Values */
gchar* gpa_option_value_dup        (const GpaOption *option, const gchar *key);
void   gpa_option_value_get_double (const GpaOption *option, const gchar *key, double *value);
void   gpa_option_value_get_int    (const GpaOption *option, const gchar *key, int *value);

/* Access the struct from the world */
const gchar * gpa_option_get_name (const GpaOption *option);
const gchar * gpa_option_get_id   (const GpaOption *option);
      gchar * gpa_option_dup_name (const GpaOption *option);

const GpaOptions * gpa_option_get_parent (const GpaOption *option);
const GpaOptions * gpa_option_get_children (const GpaOption *option);
#endif

G_END_DECLS

#endif /* __GPA_OPTION_H__ */




