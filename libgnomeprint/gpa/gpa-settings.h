#ifndef __GPA_SETTINGS_H__
#define __GPA_SETTINGS_H__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <glib.h>

G_BEGIN_DECLS

#define GPA_TYPE_SETTINGS (gpa_settings_get_type ())
#define GPA_SETTINGS(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GPA_TYPE_SETTINGS, GPASettings))
#define GPA_SETTINGS_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GPA_TYPE_SETTINGS, GPASettingsClass))
#define GPA_IS_SETTINGS(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GPA_TYPE_SETTINGS))
#define GPA_IS_SETTINGS_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GPA_TYPE_SETTINGS))
#define GPA_SETTINGS_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GPA_TYPE_SETTINGS, GPASettingsClass))

typedef struct _GPASettings GPASettings;
typedef struct _GPASettingsClass GPASettingsClass;

#include <tree.h>
#include "gpa-list.h"

/* GPASettings */

struct _GPASettings {
	GPANode node;

	GPANode *name;
	GPANode *model;
	GPANode *keys;
};


struct _GPASettingsClass {
	GPANodeClass node_class;
};

GType gpa_settings_get_type (void);

GPANode *gpa_settings_new_empty (const guchar *name);
GPANode *gpa_settings_new_from_model (GPANode *model, const guchar *name);
GPANode *gpa_settings_new_from_model_full (GPANode *model, const guchar *id, const guchar *name);
GPANode *gpa_settings_new_from_model_and_tree (GPANode *model, xmlNodePtr tree);

xmlNodePtr gpa_settings_write (xmlDocPtr doc, GPANode *settings);

gboolean gpa_settings_copy (GPASettings *dst, GPASettings *src);

#if 0
GPANode *gpa_settings_new (void);

#define gpa_settings_ref(p)   gpa_node_ref (p)
#define gpa_settings_unref(p) gpa_node_ref (p)

/* GPASettingsList */

GPANode *gpa_settings_list_new (void);

typedef struct _GpaValue GpaValue;

struct _GpaValue {
	gchar *key;
	gchar *value;
};

gboolean gpa_settigns_load_default_paths_from_node (xmlNodePtr tree_, GPAModel *model);

GPAList * gpa_settings_list_load_from_node (xmlNodePtr tree, GPAPrinter *printer);

xmlNodePtr gpa_settings_list_write (XmlParseContext *context, GPAList *settings_list);

/* From public header */

typedef enum {
	GNOME_PRINT_ORIENT_PORTRAIT,
	GNOME_PRINT_ORIENT_LANDSCAPE,
	GNOME_PRINT_ORIENT_REVERSE_PORTRAIT,
	GNOME_PRINT_ORIENT_REVERSE_LANDSCAPE
} GnomePrintOrient;

/* Basic "GpaSettings" Object operations either by an individual
 * object, or by a list of them */
GpaSettings * gpa_settings_new_from_model (GPAModel *model, GPAPrinter *printer);

/* fixme: Remove these from source eventually (Lauris) */
#define gpa_settings_copy(s) ((GpaSettings *) gpa_node_duplicate (GPA_NODE (s)))
#define gpa_settings_list_copy(s) ((GPAList *) gpa_node_duplicate (GPA_NODE (s)))

gboolean gpa_settings_verify (GpaSettings *settings, gboolean fail);
gboolean gpa_settings_list_verify (GPAList *settings_list, gboolean fail);

/* Selected Settings. Get, select and query  */
GpaSettings * gpa_settings_get_selected (GPAList *settings_list);
gboolean gpa_settings_select (GpaSettings *settings_in, GPAList *settings_list);
gboolean gpa_settings_is_selected (const GpaSettings *settings);

/* Settings name convenience functions. Used for Copy & Rename Settings */
gboolean gpa_settings_name_taken (GPAList *settings_list, const gchar *name);
void gpa_settings_name_replace (GpaSettings *settings, const gchar *name);
gchar *gpa_settings_get_free_name (GpaSettings *settings, GPAList *settings_list);

/* Values stuff. This are the actual settings that we modify */
gchar *gpa_settings_value_dup (GpaSettings *settings, const gchar *key);
gchar *gpa_settings_value_dup_required (GpaSettings *settings, const gchar *key);
gboolean gpa_settings_value_insert (GpaSettings *settings, const gchar *key, const gchar *value);
gboolean gpa_settings_value_replace (GpaSettings *settings, const gchar *key, const gchar *new_value);
gboolean gpa_settings_value_get_from_option_int (GpaSettings *settings, GpaOption *option, gint *value);
gboolean gpa_settings_value_get_from_option_double (GpaSettings *settings, GpaOption *option, gdouble *value);

/* Access to the struct */
gchar * gpa_settings_dup_name (GpaSettings *settings);
const gchar * gpa_settings_get_name (GpaSettings *settings);

gchar * gpa_settings_dup_command (GpaSettings *settings);
const gchar * gpa_settings_get_command (GpaSettings *settings);

const GPAModel* gpa_settings_get_model (GpaSettings *settings);

/* Selected options list */
GList *  gpa_settings_get_selected_options (GpaSettings *settings);
void     gpa_settings_set_selected_options (GpaSettings *settings, GList *list);
gboolean gpa_settings_select_option (GpaSettings *settings, GpaOption *option);

/* This is used to set the command to print to (i.e. "lpr")
 * should go away in the future when we add better spooler
 * manupitaion support */
void gpa_settings_replace_command (GpaSettings *settings, const gchar *command);

/* Query code for the printer drivers */
const gchar * gpa_settings_query_options (GpaSettings *settings, const gchar *options_id);
gboolean gpa_settings_query_options_boolean (GpaSettings *settings, const gchar *options_id);
#endif

G_END_DECLS

#endif /* __GPA_SETTINGS_H__ */
