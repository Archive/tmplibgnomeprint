#ifndef __GPA_UTILS_H__
#define __GPA_UTILS_H__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include "gpa-node.h"

guchar *gpa_id_new (const guchar *key);

/* Attach and detach */

GPANode *gpa_node_attach (GPANode *parent, GPANode *child);
GPANode *gpa_node_attach_ref (GPANode *parent, GPANode *child);

GPANode *gpa_node_detach (GPANode *parent, GPANode *child);
GPANode *gpa_node_detach_unref (GPANode *parent, GPANode *child);

GPANode *gpa_node_detach_next (GPANode *parent, GPANode *child);
GPANode *gpa_node_detach_unref_next (GPANode *parent, GPANode *child);

/* Lookup */

gboolean gpa_node_lookup_ref (GPANode **child, GPANode *node, const guchar *path, const guchar *key);

#if 0
#include <gpa-private.h>
#include <gpa-known.h>
#include <time.h>   /* For time() */

extern gboolean   debug_turned_on;

/* Hash handling */
GHashTable* gpa_hash_copy (GHashTable *values);
gboolean    gpa_hash_free (GHashTable *values);
xmlNodePtr  gpa_hash_write (XmlParseContext *context,
					   GHashTable *values,
					   const gchar *name);
gboolean      gpa_hash_item_set (GHashTable *hash,
						   const gchar *key,
						   const gchar *content);
const gchar * gpa_hash_item_get (GHashTable *hash,
						   const gchar *key);


gboolean gpa_hash_verify (GHashTable *hash,
					 const GpaKnownNodes *nodes,
					 gboolean xtra_flag,
					 const gchar *section);

gchar * gpa_utils_convert_date_to_string (time_t clock);

/*
gboolean
gp_hash_verify (GHashTable *hash, const GpaKnownNodes nodes, gboolean xtra_flag);
*/
#endif

END_GNOME_DECLS

#endif /* __GPA_UTILS_H__ */
