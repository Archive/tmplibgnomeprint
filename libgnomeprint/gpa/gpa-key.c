#define __GPA_KEY_C__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <string.h>
#include <malloc.h>
#include <xmlmemory.h>
#include "gpa-utils.h"
#include "gpa-reference.h"
#include "gpa-option.h"
#include "gpa-key.h"

static void gpa_key_class_init (GPAKeyClass *klass);
static void gpa_key_init (GPAKey *key);

static void gpa_key_finalize (GObject *object);

static GPANode *gpa_key_duplicate (GPANode *node);
static gboolean gpa_key_verify (GPANode *node);
static guchar *gpa_key_get_value (GPANode *node);
static gboolean gpa_key_set_value (GPANode *node, const guchar *value);
static GPANode *gpa_key_get_child (GPANode *node, GPANode *ref);
static GPANode *gpa_key_lookup (GPANode *node, const guchar *path);
static void gpa_key_modified (GPANode *node);

static gboolean gpa_key_merge_children_from_key (GPAKey *dst, GPAKey *src);

static gboolean gpa_key_merge_from_option (GPAKey *key, GPAOption *option);
static gboolean gpa_key_merge_children_from_option (GPAKey *key, GPAOption *option);

static GPANodeClass *parent_class = NULL;

GType
gpa_key_get_type (void)
{
	static GType type = 0;
	if (!type) {
		static const GTypeInfo info = {
			sizeof (GPAKeyClass),
			NULL, NULL,
			(GClassInitFunc) gpa_key_class_init,
			NULL, NULL,
			sizeof (GPAKey),
			0,
			(GInstanceInitFunc) gpa_key_init
		};
		type = g_type_register_static (GPA_TYPE_NODE, "GPAKey", &info, 0);
	}
	return type;
}

static void
gpa_key_class_init (GPAKeyClass *klass)
{
	GObjectClass *object_class;
	GPANodeClass *node_class;

	object_class = (GObjectClass *) klass;
	node_class = (GPANodeClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gpa_key_finalize;

	node_class->duplicate = gpa_key_duplicate;
	node_class->verify = gpa_key_verify;
	node_class->get_value = gpa_key_get_value;
	node_class->set_value = gpa_key_set_value;
	node_class->get_child = gpa_key_get_child;
	node_class->lookup = gpa_key_lookup;
	node_class->modified = gpa_key_modified;
}

static void
gpa_key_init (GPAKey *key)
{
	key->children = NULL;
	key->option = NULL;
	key->value = NULL;
}

static void
gpa_key_finalize (GObject *object)
{
	GPAKey *key;

	key = GPA_KEY (object);

	while (key->children) {
		GPANode *child;
		child = key->children;
		key->children = child->next;
		child->parent = NULL;
		child->next = NULL;
		gpa_node_unref (child);
	}

	if (key->option) {
		gpa_node_unref (key->option);
		key->option = NULL;
	}

	if (key->value) {
		g_free (key->value);
		key->value = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static GPANode *
gpa_key_duplicate (GPANode *node)
{
	GPAKey *key, *new;
	GPANode *child;
	GSList *l;

	key = GPA_KEY (node);

	new = (GPAKey *) gpa_node_new (GPA_TYPE_KEY, node->id);

	if (key->value) new->value = g_strdup (key->value);

	if (key->option) {
		new->option = key->option;
		gpa_node_ref (new->option);
	}

	l = NULL;
	for (child = key->children; child != NULL; child = child->next) {
		GPANode *newchild;
		newchild = gpa_node_duplicate (child);
		if (newchild) l = g_slist_prepend (l, newchild);
	}

	while (l) {
		GPANode *newchild;
		newchild = GPA_NODE (l->data);
		l = g_slist_remove (l, newchild);
		newchild->parent = GPA_NODE (new);
		newchild->next = new->children;
		new->children = newchild;
	}

	return GPA_NODE (new);
}

static gboolean
gpa_key_verify (GPANode *node)
{
	/* fixme: Verify on option */

	return TRUE;
}

static guchar *
gpa_key_get_value (GPANode *node)
{
	GPAKey *key;

	key = GPA_KEY (node);

	if (key->value) return g_strdup (key->value);

	return NULL;
}

static gboolean
gpa_key_set_value (GPANode *node, const guchar *value)
{
	GPAKey *key;
	GPAOption *option;

	key = GPA_KEY (node);

	/* fixme: Is that clever? */
	g_return_val_if_fail (value != NULL, FALSE);
	g_return_val_if_fail (key->value != NULL, FALSE);

	if (key->value && !strcmp (key->value, value)) return TRUE;

	option = GPA_KEY_OPTION (key);
	g_return_val_if_fail (option != NULL, FALSE);

	if (GPA_OPTION_IS_LIST (option)) {
		GPAOption *child;
		child = gpa_option_get_child_by_id (option, value);
		if (child) {
			/* Everything is legal */
			g_free (key->value);
			key->value = g_strdup (value);
			gpa_key_merge_children_from_option (key, child);
			gpa_node_unref (GPA_NODE (child));
			gpa_node_request_modified (node);
			return TRUE;
		}
	} else if (GPA_OPTION_IS_STRING (option)) {
		if (!value) value = option->value;
		if (value && key->value && !strcmp (value, key->value)) return TRUE;
		if (key->value) g_free (key->value);
		key->value = g_strdup (value);
		gpa_node_request_modified (node);
		return TRUE;
	}

	return FALSE;
}

static GPANode *
gpa_key_get_child (GPANode *node, GPANode *ref)
{
	GPAKey *key;

	key = GPA_KEY (node);

	if (!ref) {
		if (key->children) gpa_node_ref (key->children);
		return key->children;
	}

	if (ref->next) gpa_node_ref (ref->next);

	return ref->next;
}

static GPANode *
gpa_key_lookup (GPANode *node, const guchar *path)
{
	GPAKey *key;
	GPANode *child;
	const guchar *dot, *next;
	gint len;

	key = GPA_KEY (node);

	if (!strncmp (path, "Option", 6)) {
		if (!path[6]) {
			gpa_node_ref (GPA_NODE (key->option));
			return GPA_NODE (key->option);
		} else {
			g_return_val_if_fail (path[6] == '.', NULL);
			return gpa_node_lookup (GPA_NODE (key->option), path + 7);
		}
	}

	dot = strchr (path, '.');
	if (dot != NULL) {
		len = dot - path;
		next = dot + 1;
	} else {
		len = strlen (path);
		next = path + len;
	}

	for (child = key->children; child != NULL; child = child->next) {
		g_assert (GPA_IS_KEY (child));
		if (child->id && strlen (child->id) == len && !strncmp (child->id, path, len)) {
			if (!next) {
				gpa_node_ref (child);
				return child;
			} else {
				return gpa_node_lookup (child, next);
			}
		}
	}

	return NULL;
}

static void
gpa_key_modified (GPANode *node)
{
	GPAKey *key;
	GPANode *child;

	key = GPA_KEY (node);

	child = key->children;
	while (child) {
		GPANode *next;
		next = child->next;
		if (GPA_NODE_FLAGS (child) & GPA_MODIFIED_FLAG) {
			gpa_node_ref (child);
			gpa_node_emit_modified (child);
			gpa_node_unref (child);
		}
		child = next;
	}

	if (key->option && GPA_NODE_FLAGS (key->option) & GPA_MODIFIED_FLAG) {
		gpa_node_emit_modified (key->option);
	}
}

GPANode *
gpa_key_new_from_option (GPANode *node)
{
	GPAOption *option;
	GPAKey *key;
	GPANode *child;
	GSList *l;

	g_return_val_if_fail (node != NULL, NULL);
	g_return_val_if_fail (GPA_IS_OPTION (node), NULL);

	option = GPA_OPTION (node);

	key = NULL;

	switch (option->type) {
	case GPA_OPTION_NODE:
	case GPA_OPTION_KEY:
	case GPA_OPTION_STRING:
		key = (GPAKey *) gpa_node_new (GPA_TYPE_KEY, node->id);
		key->option = node;
		gpa_node_ref (key->option);
		if (option->value) key->value = g_strdup (option->value);
		l = NULL;
		for (child = option->children; child != NULL; child = child->next) {
			GPANode *kch;
			kch = gpa_key_new_from_option (child);
			if (kch) {
				l = g_slist_prepend (l, kch);
			}
		}
		while (l) {
			GPANode *kch;
			kch = GPA_NODE (l->data);
			l = g_slist_remove (l, kch);
			kch->parent = GPA_NODE (key);
			kch->next = key->children;
			key->children = kch;
		}
		break;
	case GPA_OPTION_LIST:
		key = (GPAKey *) gpa_node_new (GPA_TYPE_KEY, node->id);
		key->option = node;
		gpa_node_ref (key->option);
		key->value = g_strdup (option->value);
		child = (GPANode *) gpa_option_get_child_by_id (option, option->value);
		if (child) {
			GPANode *ich;
			l = NULL;
			for (ich = GPA_OPTION (child)->children; ich != NULL; ich = ich->next) {
				GPANode *kch;
				kch = gpa_key_new_from_option (ich);
				if (kch) l = g_slist_prepend (l, kch);
			}
			while (l) {
				GPANode *kch;
				kch = GPA_NODE (l->data);
				l = g_slist_remove (l, kch);
				kch->parent = GPA_NODE (key);
				kch->next = key->children;
				key->children = kch;
			}
			break;
		} else {
			g_warning ("Default was not in list (memory leak)");
		}
		break;
	case GPA_OPTION_ITEM:
		/* No key for item */
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	return (GPANode *) key;
}

xmlNodePtr
gpa_key_write (xmlDocPtr doc, GPANode *node)
{
	GPAKey *key;
	GPAOption *option;
	xmlNodePtr xmln, xmlc;
	GPANode *child;

	g_return_val_if_fail (doc != NULL, NULL);
	g_return_val_if_fail (node != NULL, NULL);
	g_return_val_if_fail (GPA_IS_KEY (node), NULL);
	g_return_val_if_fail (GPA_KEY_HAS_OPTION (node), NULL);

	key = GPA_KEY (node);

	option = GPA_KEY_OPTION (key);

	if (option->type != GPA_OPTION_KEY) {
		xmln = xmlNewDocNode (doc, NULL, "Key", NULL);
		xmlSetProp (xmln, "Id", node->id);
		if (key->value) xmlSetProp (xmln, "Value", key->value);

		for (child = key->children; child != NULL; child = child->next) {
			xmlc = gpa_key_write (doc, child);
			if (xmlc) xmlAddChild (xmln, xmlc);
		}
	} else {
		xmln = NULL;
	}

	return xmln;
}

gboolean
gpa_key_merge_from_tree (GPANode *key, xmlNodePtr tree)
{
	xmlChar *xmlid, *xmlval;
	xmlNodePtr xmlc;

	g_return_val_if_fail (key != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_KEY (key), FALSE);
	g_return_val_if_fail (tree != NULL, FALSE);
	g_return_val_if_fail (!strcmp (tree->name, "Key"), FALSE);

	xmlid = xmlGetProp (tree, "Id");
	g_assert (xmlid);
	g_assert (!strcmp (xmlid, key->id));
	xmlFree (xmlid);

	xmlval = xmlGetProp (tree, "Value");
	if (xmlval) {
		gpa_node_set_value (key, xmlval);
		xmlFree (xmlval);
	}

	for (xmlc = tree->xmlChildrenNode; xmlc != NULL; xmlc = xmlc->next) {
		if (!strcmp (xmlc->name, "Key")) {
			xmlChar *keyid;
			keyid = xmlGetProp (xmlc, "Id");
			if (keyid) {
				GPANode *kch;
				for (kch = GPA_KEY (key)->children; kch != NULL; kch = kch->next) {
					if (!strcmp (keyid, kch->id)) {
						gpa_key_merge_from_tree (kch, xmlc);
						break;
					}
				}
				xmlFree (keyid);
			}
		}
	}

	return TRUE;
}

gboolean
gpa_key_copy (GPANode *dst, GPANode *src)
{
	gboolean modified;

	g_return_val_if_fail (dst != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_KEY (dst), FALSE);
	g_return_val_if_fail (src != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_KEY (src), FALSE);

	modified = FALSE;

	return FALSE;
}

static gboolean
gpa_key_merge_children_from_key (GPAKey *dst, GPAKey *src)
{
	GPANode *child;
	GSList *dl, *sl;

	dl = NULL;
	while (dst->children) {
		dl = g_slist_prepend (dl, dst->children);
		dst->children = gpa_node_detach_next (GPA_NODE (dst), dst->children);
	}

	sl = NULL;
	for (child = src->children; child != NULL; child = child->next) {
		sl = g_slist_prepend (sl, child);
	}

	while (sl) {
		GSList *l;
		for (l = dl; l != NULL; l = l->next) {
			if (!strcmp (GPA_NODE (l->data)->id, GPA_NODE (sl->data)->id)) {
				/* We are in original too */
				child = GPA_NODE (l->data);
				dl = g_slist_remove (dl, l->data);
				child->parent = GPA_NODE (dst);
				child->next = dst->children;
				dst->children = child;
				gpa_key_merge_from_key (GPA_KEY (child), GPA_KEY (sl->data));
				break;
			}
		}
		if (!l) {
			/* Create new child */
			child = gpa_node_duplicate (GPA_NODE (sl->data));
			child->parent = GPA_NODE (dst);
			child->next = dst->children;
			dst->children = child;
		}
		sl = g_slist_remove (sl, sl->data);
	}

	while (dl) {
		gpa_node_unref (GPA_NODE (dl->data));
		dl = g_slist_remove (dl, dl->data);
	}

	gpa_node_request_modified (GPA_NODE (dst));

	return TRUE;
}

gboolean
gpa_key_merge_from_key (GPAKey *dst, GPAKey *src)
{
	g_return_val_if_fail (dst != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_KEY (dst), FALSE);
	g_return_val_if_fail (src != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_KEY (src), FALSE);

	g_return_val_if_fail (src->option != NULL, FALSE);

	if (dst->value) g_free (dst->value);
	dst->value = g_strdup (src->value);

	if (dst->option) gpa_node_unref (dst->option);
	dst->option = src->option;
	gpa_node_ref (dst->option);

	gpa_key_merge_children_from_key (dst, src);

	gpa_node_request_modified (GPA_NODE (dst));

	return TRUE;
}

static gboolean
gpa_key_merge_children_from_option (GPAKey *key, GPAOption *option)
{
	GPANode *child;
	GSList *kl, *ol;

	kl = NULL;
	while (key->children) {
		kl = g_slist_prepend (kl, key->children);
		key->children = gpa_node_detach_next (GPA_NODE (key), key->children);
	}

	ol = NULL;
	for (child = option->children; child != NULL; child = child->next) {
		ol = g_slist_prepend (ol, child);
	}

	while (ol) {
		GSList *l;
		for (l = kl; l != NULL; l = l->next) {
			if (!strcmp (GPA_NODE (l->data)->id, GPA_NODE (ol->data)->id)) {
				/* We are in original too */
				child = GPA_NODE (l->data);
				kl = g_slist_remove (kl, l->data);
				child->parent = GPA_NODE (key);
				child->next = key->children;
				key->children = child;
				gpa_key_merge_from_option (GPA_KEY (child), GPA_OPTION (ol->data));
				break;
			}
		}
		if (!l) {
			/* Create from option */
			child = gpa_key_new_from_option (GPA_NODE (ol->data));
			child->parent = GPA_NODE (key);
			child->next = key->children;
			key->children = child;
		}
		ol = g_slist_remove (ol, ol->data);
	}

	while (kl) {
		gpa_node_unref (GPA_NODE (kl->data));
		kl = g_slist_remove (kl, kl->data);
	}

	gpa_node_request_modified (GPA_NODE (key));

	return TRUE;
}

static gboolean
gpa_key_merge_from_option (GPAKey *key, GPAOption *option)
{
	GPAOption *och;

	gpa_node_unref (key->option);
	gpa_node_ref (GPA_NODE (option));
	key->option = GPA_NODE (option);

	switch (option->type) {
	case GPA_OPTION_NODE:
	case GPA_OPTION_KEY:
	case GPA_OPTION_STRING:
		if (key->value) {
			g_free (key->value);
			key->value = NULL;
		}
		if (option->value) key->value = g_strdup (option->value);
		gpa_key_merge_children_from_option (key, option);
		break;
	case GPA_OPTION_LIST:
		och = NULL;
		if (key->value) och = gpa_option_get_child_by_id (option, key->value);
		if (och) {
			gpa_key_merge_children_from_option (key, och);
		} else {
			if (key->value) g_free (key->value);
			key->value = g_strdup (option->value);
			och = gpa_option_get_child_by_id (option, key->value);
			if (och) {
				gpa_key_merge_children_from_option (key, och);
			} else {
				g_warning ("List does not contain default item");
			}
		}
		break;
	case GPA_OPTION_ITEM:
		/* No key for item */
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	gpa_node_request_modified (GPA_NODE (key));

	return TRUE;

}

#if 0
static GList *
gpa_key_paths_copy (GList *source)
{
	GList *dest = NULL;
	GList *list;
	gchar *path;

	if (source == NULL)
		return NULL;
	g_return_val_if_fail (source != NULL, NULL);

	list = source;
	for (; list != NULL; list = list->next) {
		path = list->data;
		dest = g_list_prepend (dest, g_strdup (path));
	}

	return g_list_reverse (dest);
}

GpaKey *
gpa_key_new_from_model (GPAModel *model, GPAPrinter *printer)
{
	GpaKey *key;
	GpaBackend *backend;

	g_return_val_if_fail (GPA_IS_MODEL (model), NULL);
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);
		
	/* Default backend */
	backend = gpa_model_get_default_backend (model);
	g_return_val_if_fail (backend != NULL, NULL);
	g_return_val_if_fail (GPA_IS_BACKEND (backend), NULL);

	key = GPA_KEY (gpa_key_new ());
	
	key->name             = g_strdup (_("Default Key"));
	key->command          = g_strdup ("lpr");
	key->printer          = printer;
	key->selected_options = gpa_key_paths_copy (model->default_key);
	key->values           = gpa_values_copy_list (model->default_values);
	key->selected         = TRUE;

	gpa_key_value_insert (key, GPA_TAG_BACKEND, backend->id);

	if (!gpa_key_verify (key, TRUE)) {
		gchar *value;
		value = gpa_node_get_path_value (GPA_NODE (model), "Name");
		gpa_error ("Could not create key from model \"%s\"\n", value);
		g_free (value);
		return NULL;
	}

	return key;
}

static xmlNodePtr
gpa_key_options_write (XmlParseContext *context, GpaKey *key)
{
	xmlNodePtr node;
	GList *list;
	gchar *path;

	g_return_val_if_fail (context != NULL, NULL);
	g_return_val_if_fail (GPA_IS_KEY (key), NULL);

	node = xmlNewDocNode (context->doc, context->ns, GPA_TAG_SELECTED_OPTIONS, NULL);

	list = key->selected_options;
	for (; list != NULL; list = list->next) {
		path = (gchar *) list->data;
		xmlNewChild (node, context->ns, GPA_TAG_PATH, path);
	}

	return node;
}

static xmlNodePtr
gpa_key_write (XmlParseContext *context, GpaKey *key)
{
	xmlNodePtr node;
	xmlNodePtr child;

	g_return_val_if_fail (context != NULL, NULL);
	g_return_val_if_fail (GPA_IS_KEY (key), NULL);

	node = xmlNewDocNode (context->doc, context->ns, GPA_TAG_KEY, NULL);

	xmlNewChild (node, context->ns, GPA_TAG_NAME, key->name);
	xmlNewChild (node, context->ns, GPA_TAG_COMMAND, key->command);
	xmlNewChild (node, context->ns, GPA_TAG_SELECTED,
		     key->selected ? GPA_TAG_TRUE: GPA_TAG_FALSE);

	child = gpa_key_options_write (context, key);
	if (child == NULL)
		return NULL;
	xmlAddChild (node, child);

	child = gpa_values_write_list (context, key->values,
				       GPA_TAG_KEY_INFO);
	if (child == NULL)
		return NULL;
	xmlAddChild (node, child);

	return node;
}

xmlNodePtr
gpa_key_list_write (XmlParseContext *context, GPAList *key_list)
{
	GpaKey *key = NULL;
	xmlNodePtr child;
	xmlNodePtr node;
	GPAItem *item;

	g_return_val_if_fail (context != NULL, NULL);
	g_return_val_if_fail (key_list != NULL, NULL);

	node = xmlNewDocNode (context->doc,
			      context->ns,
			      GPA_TAG_KEY_LIST,
			      NULL);
	
	item = key_list->items;
	for (; item != NULL; item = item->next) {
		key = GPA_KEY (item);
		if (!GPA_IS_KEY (key))
			return NULL;
		child = gpa_key_write (context, key);
		if (child == NULL)
			return NULL;
		xmlAddChild (node, child);
	}

	return node;
}
		
gboolean
gpa_key_verify (GpaKey *key, gboolean fail)
{
	GPAModel *model;
	GpaOption *option;
	GList *list;
	GList *list2;
	GList *remove = NULL;
	gchar *path;
	gchar *path2;
	
	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_KEY (key), FALSE);

	model = key->printer->model;

	g_return_val_if_fail (GPA_IS_MODEL (model), FALSE);

	if (key->selected_options == NULL) {
		if (key->printer->model->options_list != NULL) {
			gpa_error ("The key \"%s\", does not contain a list of selected options",
				   key->name);
		return FALSE;
		}
	}

	list = key->selected_options;
	for (; list != NULL; list = list->next) {
		path = (gchar *)list->data;
		option = gpa_option_get_from_model_and_path (model, path);
		if (option == NULL) {
			g_warning ("Removing the %s path from the selected_options list. FIXED ..",
				   path);
			if (fail)
				return FALSE;
			remove = g_list_prepend (remove, path);
			continue;
		}
		
		/* Now verify that this option is not repeated in the list */
		list2 = key->selected_options;
		for (; list2 != NULL; list2 = list2->next) {
			gint n,m;
			path2 = (gchar *)list2->data;
			if (path2 == path)
				continue;
			n = gpa_text_utils_search_backwards (path, GPA_PATH_DELIMITER_CHAR);
			m = gpa_text_utils_search_backwards (path2, GPA_PATH_DELIMITER_CHAR);
			if (strncmp (path, path2, MAX(n,m)) == 0) {
				g_warning ("The path \"%s\" was duplicated in the \"%s\" key. FIXED ..\n",
					   path, gpa_key_get_name (key));
				if (fail)
					return FALSE;
				if (!g_list_find (remove, path2))
				    remove = g_list_prepend (remove, path2);
				continue;
			}
		}
		
		if (!gpa_option_verify (option))
			return FALSE;
	}

	/* Dump the current list of key for debuggin */
	if (remove != NULL) {
		g_print ("Here is the list of key before cleaning:\n");
		list = key->selected_options;
		for (; list != NULL; list = list->next) {
			path = (gchar *)list->data;
			g_print ("[%s]\n", path);
		}
		g_print ("\n");
	}

	/* Now remove the paths that are invalid. They point to an option that does
	 * not exist. This can happen if the user updates a .model file and an option
	 * that was present before, is no longer found. (or it's id was renamed)
	 */
	list = remove;
	for (; list != NULL; list = list->next) {
		path = (gchar *)list->data;
		key->selected_options = g_list_remove (key->selected_options, path);
		g_free (path);
	}

	/* Dump the current list of key for debuggin */
	if (remove != NULL) {
		g_print ("Here is the list of key after cleaning :\n");
		list = key->selected_options;
		for (; list != NULL; list = list->next) {
			path = (gchar *)list->data;
			g_print ("[%s]\n", path);
		}
		g_print ("\n");
	}

	g_list_free (remove);

	if (!gpa_values_verify_key (key))
		return FALSE;
	
	if (!gpa_model_verify_with_key (key->printer->model, key))
		return FALSE;
	
	return TRUE;
}

gboolean 
gpa_key_list_verify (GPAList *key_list, gboolean fail)
{
	GpaKey *key;
	GPAItem *item;
	gint selected = 0;

	debug (FALSE, "");

	g_return_val_if_fail (key_list != NULL, FALSE);

	item = key_list->items;
	for (; item != NULL; item = item->next) {
		key = GPA_KEY (item);
		if (!gpa_key_verify (key, fail))
			return FALSE;
	}

	/* Verify that there is one and only one key seletected */
	item = key_list->items;
	for (; item != NULL; item = item->next) {
		key = GPA_KEY (item);
		if (key->selected)
			selected++;
			
	}

	if (selected != 1) {
		gpa_error ("There needs to be one and only one selected key "
			   "there are [%i] key selected.", selected);
		return FALSE;
	}
	
	return TRUE;
}

/* FIXME: this is a problem
 * what if 2 programs are using different key to print
 * to the same printer, For this reason, we can't determine
 * the selected key for a printer.
 */
GpaKey *
gpa_key_get_selected (GPAList *key_list)
{
	GpaKey *key = NULL;
	GPAItem *item;

	debug (FALSE, "");

	g_return_val_if_fail (key_list != NULL, NULL);

	item = key_list->items;
	for (; item != NULL; item = item->next) {
		key = GPA_KEY (item);
		if (key == NULL)
			return NULL;
		if (key->selected)
			return key;
	}

	gpa_error ("Could not determine the selected key, (auto-fixed)");

	/* Fix the problem */
	if (GPA_IS_KEY (key)) {
		key->selected = TRUE;
		return key;
	}
		
	return NULL;
}


static GList *
gpa_selected_options_new_from_node (xmlNodePtr tree)
{
	xmlNodePtr child;
	GList *list = NULL;
	gchar *path;
	
	debug (FALSE, "");

	g_return_val_if_fail (tree != NULL, NULL);

	if (!gpa_xml_node_verify (tree, GPA_TAG_SELECTED_OPTIONS))
		return NULL;


	child = tree->childs;
	while (child != NULL) {
		skip_text (child);
		path = xmlNodeGetContent (child);
		list = g_list_prepend (list, path);
		child = child->next;
	}

	return g_list_reverse (list);
}

static GpaKey *
gpa_key_new_from_node (xmlNodePtr tree, GPAPrinter *printer)
{
	GpaKey *key;
	xmlNodePtr child;
	gchar *name;
	gchar *command;
	gchar *selected;
	
	debug (FALSE, "");
	
	g_return_val_if_fail (tree != NULL, NULL);

	if (!gpa_xml_node_verify (tree, GPA_TAG_KEY))
		return NULL;

	name = gpa_xml_get_value_string_required (tree, GPA_TAG_NAME, GPA_TAG_KEY);
	if (name == NULL)
		return NULL;
	command = gpa_xml_get_value_string_required (tree, GPA_TAG_COMMAND, GPA_TAG_KEY);
	if (name == NULL)
		return NULL;
	
	key = GPA_KEY (gpa_key_new ());

	selected = gpa_xml_get_value_string (tree, GPA_TAG_SELECTED);
	if ((selected != NULL) && strcmp (selected, GPA_TAG_TRUE) == 0)
		key->selected = TRUE;
	g_free (selected);

	key->name    = name;
	key->command = command;
	key->printer = printer;

	/* Load Selected options */
	child = gpa_xml_search_child_required (tree, GPA_TAG_SELECTED_OPTIONS);
	if (child == NULL)
		return FALSE;
	key->selected_options = gpa_selected_options_new_from_node (child);


	child = gpa_xml_search_child_required (tree, GPA_TAG_KEY_INFO);
	if (child == NULL)
		return FALSE;
	key->values = gpa_values_new_from_node (child,
						     GPA_TAG_KEY_INFO);
	
	if (!gpa_key_verify (key, TRUE))
		return NULL;
	
	return key;
}


/**
 * gpa_settigns_load_default_paths_from_node:
 * @tree: the node which we should load the key from
 * 
 * Given a DefaultKeyNode, it loads all the key into a GList
 * of gchar containing key paths. Like "PaperSize-Letter" or "Resolution-300"
 * 
 * Return Value: a GList of gchar pointers
 **/
gboolean
gpa_settigns_load_default_paths_from_node (xmlNodePtr tree_, GPAModel *model)
{
	xmlNodePtr tree;
	xmlNodePtr child;
	xmlNodePtr child_child;
	GList *paths;
	GList *list;
	gchar *path;
	gchar *test_path;
	gint size;
	
	debug (FALSE, "");

	g_return_val_if_fail (tree_ != NULL, FALSE);

	tree = gpa_include_node (tree_);

	if (!gpa_xml_node_verify (tree, GPA_TAG_DEFAULT_KEY))
		return FALSE;

	paths = model->default_key;
	
	child_child = gpa_xml_search_child_required (tree, GPA_TAG_PATHS);
	if (child_child == NULL)
		return FALSE;

	child = child_child->childs;
	
	for (; child != NULL; child = child->next) {
		skip_text (child);
		path = xmlNodeGetContent (child);
		
		/* Search for the path to see if we are duplicating it,
		 * if we are, remove the old path and add the new one.
		 * we need this since you can specify default key inside
		 * the nodes or inside the <default_key> node.
		 * we take the nodes default key, unless there is an
		 * entry in the <DefaultKey->Paths> */
		for (list = paths; list != NULL; list = list->next) {
			test_path = list->data;
			size = tu_get_pos_of_last_delimiter (test_path, GPA_PATH_DELIMITER_CHAR);
			g_return_val_if_fail (size > 0, FALSE);
			if (strncmp (path, test_path, size) == 0) {
				g_free (test_path);
				list->data = path;
				path = NULL;
				break;
			}
		}

		if (path == NULL)
			continue;
		
		paths = g_list_prepend (paths, path);
	}

	model->default_key = g_list_reverse (paths);

	return TRUE;
}

GPAList *
gpa_key_list_load_from_node (xmlNodePtr tree, GPAPrinter *printer)
{
	GpaKey *key;
	xmlNodePtr child;
	GPANode *list = NULL;
	GPANode *last;

	debug (FALSE, "");
	
	g_return_val_if_fail (tree != NULL, NULL);
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);
	
	if (!gpa_xml_node_verify (tree, GPA_TAG_KEY_LIST))
		return NULL;

	list = gpa_key_list_new ();
	last = NULL;

	child = tree->childs;

	while (child != NULL) {
		skip_text (child);
		key = gpa_key_new_from_node (child, printer);
		if (!GPA_IS_KEY (key))
			return NULL;
		gpa_node_add_child (list, GPA_NODE (key), last);
		child = child->next;
		last = GPA_NODE (key);
	}
	
	return GPA_LIST (list);
}


static GList *
gpa_selected_options_copy (GList *selected_options)
{
	GList *new_list = NULL;
	GList *list;
	gchar *new_path;
	gchar *path;

	list = selected_options;
	for (; list != NULL; list = list->next) {
		path = (gchar *)list->data;
		new_path = g_strdup (path);
		new_list = g_list_prepend (new_list, new_path);
	}

	return g_list_reverse (new_list);
}

#if 0
/* This is handled by gpa_node_duplicate */
GPAList *
gpa_key_list_copy (GPAList *key_list)
{
	GpaKey *key;
	GpaKey *new_key;
	GPAList *new_key_list = NULL;
	GPAItem *item;
	GPANode *last;

	new_key_list = (GPAList *) gpa_key_list_new (NULL);
	last = NULL;

	item = key_list->items;
	for (; item != NULL; item = item->next) {
		key = (GpaKey *) item;
		
		if (!GPA_IS_KEY (key))
			return NULL;

		new_key = gpa_key_copy (key);
		if (new_key == NULL)
			return NULL;
		gpa_node_add_child (GPA_NODE (new_key_list), GPA_NODE (new_key), last);
		last = (GPANode *) new_key;
	}

	return GPA_LIST (new_key_list);
}
#endif
		
gchar*
gpa_key_value_dup (GpaKey *key, const gchar *key)
{
	GpaValue *value;
	GList *list;
	
	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_KEY (key), NULL);
	g_return_val_if_fail (key != NULL,    NULL);

	list = key->values;
	for (; list != NULL; list = list->next) {
		value = (GpaValue *)list->data;
		if (strcmp (value->key, key) == 0)
			return g_strdup (value->value);
	}

	g_warning ("Could not find the \"%s\" key in the \"%s\" key values list\n",
		   key, gpa_key_get_name (key));
	
	return NULL;
}

gchar*
gpa_key_value_dup_required (GpaKey *key, const gchar *key)
{
	gchar *value;
	
	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_KEY (key), NULL);
	g_return_val_if_fail (key != NULL,    NULL);

	value = gpa_key_value_dup (key, key);

	if (value == NULL)
		gpa_error ("The required value \"%s\" was not found in the "
			   "\"%s\" key hash table", key, key->name);
	

	debug (FALSE, "end");

	return g_strdup (value);
}

gboolean
gpa_key_value_get_from_option_double (GpaKey *key,
					   GpaOption *option,
					   gdouble *value)
{
	gchar *str_value;
	gchar *key;

	debug (FALSE, "");

	*value = 0.0;

	g_return_val_if_fail (GPA_IS_KEY (key), FALSE);
	g_return_val_if_fail (option != NULL, FALSE);

	key = g_strdup_printf ("%s" GPA_PATH_DELIMITER "%s", GPA_ITEM (option->parent)->id, GPA_ITEM (option)->id);

	str_value = gpa_key_value_dup (key, key);

	if (str_value == NULL) 
		return FALSE;

	*value = atof (str_value);

	g_free (str_value);

	return TRUE;
}

gboolean
gpa_key_value_get_from_option_int (GpaKey *key,
					GpaOption *option,
					gint *value)
{
	gchar *str_value;
	gchar *key;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_KEY (key), FALSE);
	g_return_val_if_fail (option != NULL, FALSE);

	key = g_strdup_printf ("%s" GPA_PATH_DELIMITER "%s", GPA_ITEM (option->parent)->id, GPA_ITEM (option)->id);

	str_value = gpa_key_value_dup (key, key);

	if (str_value == NULL) 
		return FALSE;

	*value = atoi (str_value);

	g_free (str_value);

	return TRUE;
}

gboolean
gpa_key_value_replace (GpaKey *key, const gchar *key, const gchar *new_value)
{
	GpaValue *value;
	GList *list;
	
	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_KEY (key), FALSE);
	g_return_val_if_fail (key != NULL,    FALSE);


	list = key->values;
	for (; list != NULL; list = list->next) {
		value = (GpaValue *)list->data;
		if (strcmp (value->key, key) == 0) {
			g_free (value->value);
			value->value = g_strdup (new_value);
			return TRUE;
		}
	}
	
	gpa_error ("Value not found. Key:\"%s\" Key:\"%s\"",
		   gpa_key_get_name (key), key);

	return TRUE;
}

gboolean
gpa_key_value_insert (GpaKey *key, const gchar *key, const gchar *value)
{
	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_KEY (key), FALSE);
	g_return_val_if_fail (key != NULL,    FALSE);

	key->values = gpa_value_insert (key->values, key, value);

	return TRUE;
}



gboolean
gpa_key_select (GpaKey *key_in,
		     GPAList *key_list)
{
	GpaKey *key = NULL;
	GPAItem *item;
	gboolean found = FALSE;

	g_return_val_if_fail (GPA_IS_KEY (key_in), FALSE);
	g_return_val_if_fail (key_list != NULL, FALSE);

	item = key_list->items;
	for (; item != NULL; item = item->next) {
		key = GPA_KEY (item);
		if (!GPA_IS_KEY (key)) {
			return FALSE;
		}
		if (key == key_in) {
			found = TRUE;
			key->selected = TRUE;
		} else {
			key->selected = FALSE;
		}
	}

	if (!found) {
		gpa_error ("Could not select key, key not found in list");
		return FALSE;
	}

	return TRUE;
}

gboolean
gpa_key_name_taken (GPAList *key_list, const gchar *name)
{
	GpaKey *key;
	GPAItem *item;

	g_return_val_if_fail (key_list != NULL, TRUE);
	g_return_val_if_fail (name != NULL, TRUE);

	item = key_list->items;
	for (; item != NULL; item = item->next) {
		key = GPA_KEY (item);
		if (key == NULL)
			return FALSE;
		if (strcmp (key->name, name) == 0)
			return TRUE;
	}

	return FALSE;
}

gchar *
gpa_key_get_free_name (GpaKey *key, GPAList *key_list)
{
	gint number = 2;
	gchar *name;

	g_return_val_if_fail (GPA_IS_KEY (key), NULL);
	g_return_val_if_fail (key_list != NULL, NULL);
	
	name = g_strdup_printf ("Copy of %s", key->name);

	while (gpa_key_name_taken (key_list, name)) {
		g_free (name);
		name = g_strdup_printf ("Copy of %s (%i)", key->name, number++);
	}

	return name;
}



/* Access to the struct from the world */
const GPAModel*
gpa_key_get_model (GpaKey *key)
{
	g_return_val_if_fail (GPA_IS_KEY (key), NULL);
	g_return_val_if_fail (GPA_IS_PRINTER (key->printer), NULL);
	
	return key->printer->model;
}

gchar *
gpa_key_dup_name (GpaKey *key)
{
	g_return_val_if_fail (GPA_IS_KEY (key), NULL);
	g_return_val_if_fail (key->name != NULL, NULL);
	
	return g_strdup (key->name);
}

const gchar *
gpa_key_get_name (GpaKey *key)
{
	g_return_val_if_fail (GPA_IS_KEY (key), NULL);
	g_return_val_if_fail (key->name != NULL, NULL);
	
	return key->name;
}


gchar *
gpa_key_dup_command (GpaKey *key)
{
	g_return_val_if_fail (GPA_IS_KEY (key), NULL);
	g_return_val_if_fail (key->command != NULL, NULL);
	
	return g_strdup (key->command);
}

const gchar *
gpa_key_get_command (GpaKey *key)
{
	g_return_val_if_fail (GPA_IS_KEY (key), NULL);
	g_return_val_if_fail (key->command != NULL, NULL);
	
	return key->command;
}

void
gpa_key_name_replace (GpaKey *key, const gchar *name)
{
	g_return_if_fail (GPA_IS_KEY (key));
	g_return_if_fail (name != NULL);

	if (key->name == NULL)
		g_warning ("Key name should not be null. [gpa-key.c]");
	else
		g_free (key->name);

	key->name = g_strdup (name);
	
}

gboolean
gpa_key_is_selected (const GpaKey *key)
{
	g_return_val_if_fail (GPA_IS_KEY (key), FALSE);

	return key->selected;
}


GList *
gpa_key_get_selected_options (GpaKey *key)
{
	g_return_val_if_fail (GPA_IS_KEY (key), NULL);

	return key->selected_options;
}

void
gpa_key_set_selected_options (GpaKey *key,
				   GList *selected_options)
{
	g_return_if_fail (GPA_IS_KEY (key));

	key->selected_options = selected_options;
}






/**
 * gpa_options_unselect:
 * @options: 
 * @key: 
 * 
 * Removes from the key List of selected paths
 * the selected option for @options. This is used when
 * a selected option is changed, we must first remove
 * the old selected option and then add the new option
 * path to the GList
 * 
 * Return Value: TRUE on success, FALSE on error
 **/
static gboolean
gpa_key_unselect_options (GpaKey *key,
			       const GpaOptions *options)
{
	GList *selected_options;
	GList *list;
	gchar *path;
	gchar *findme;
	gchar *options_path;
	gint pos;

	debug (FALSE, "");

	selected_options = gpa_key_get_selected_options (key);

	g_return_val_if_fail (selected_options != NULL, FALSE);
	g_return_val_if_fail (options != NULL, FALSE);

	/* get the option path with the last token stripped */
	options_path = gpa_options_dup_path (options);
	
	g_return_val_if_fail (options_path != NULL, FALSE);

	list = selected_options;
	for (; list != NULL; list = list->next) {
		path = (gchar *)list->data;
		pos = gpa_text_utils_search_backwards (path, GPA_PATH_DELIMITER);
		if (pos == -1) {
			gpa_error ("Invalid path %s (1)\n", path);
			return FALSE;
		}
		findme = g_strndup (path, pos);

		if (strcmp (options_path, findme) == 0) {
			/* FIXME: use the list directly */
			selected_options = g_list_remove_link (selected_options, list);
			g_free (path);
			g_free (findme);
			g_free (options_path);
			gpa_key_set_selected_options (key, selected_options);
			return TRUE;
		}
		g_free (findme);
	}

	g_free (options_path);
	
	return FALSE;
}

/**
 * gpa_key_selected_option:
 * @key: 
 * @option: 
 * 
 * Select the @option in the @key Object
 **/
gboolean
gpa_key_select_option (GpaKey *key,
			    GpaOption *option)
{
	gchar *new_path;
	
 	g_return_val_if_fail (GPA_IS_KEY (key), FALSE);
	g_return_val_if_fail (GPA_IS_OPTION (option), FALSE);

	/* Remove the previously selected option */
	if (!gpa_key_unselect_options (key, option->parent)) {
		gpa_error ("Could not unselect the Option from the %s Options\n",
			   gpa_options_get_name (option->parent));
	}

	new_path = gpa_option_dup_path (option);
	key->selected_options = g_list_prepend (key->selected_options,
						      new_path);

	return TRUE;
}
								      


void
gpa_key_replace_command (GpaKey *key,  const gchar *command)
{
	g_return_if_fail (GPA_IS_KEY (key));
	g_return_if_fail (command != NULL);

	g_free (key->command);
	key->command = g_strdup (command);
		
}



/**
 * gpa_key_query_options:
 * @key: 
 * @options_id: 
 * 
 * Query a Key object for a selected option.
 * the key are not const because if there isn't a selected
 * option, we need to select one. This can happen
 * if ... (see : gpa_options_get_selected_option)
 * 
 * Return Value: a pointer to the selected option id
 **/
const gchar *
gpa_key_query_options (GpaKey *key,
			    const gchar *options_id)
{
	const GPAModel *model;
	GpaOptions *options;
	GpaOption *option;
	
	g_return_val_if_fail (GPA_IS_KEY (key), NULL);
	g_return_val_if_fail (options_id != NULL, NULL);

	model = key->printer->model;
	
	options = gpa_model_get_options_from_id (model, options_id);

	if (options == NULL) {
		gchar *value;
		value = gpa_node_get_path_value (GPA_NODE (key->printer->model), "Name");
		gpa_error ("Could not find the \"%s\" options in the \"%s\" model "
			   "options list",
			   options_id, value);
		g_free (value);
	}

	option = gpa_options_get_selected_option (key, options, FALSE);

	g_return_val_if_fail (option != NULL, NULL);

	return gpa_option_get_id (option);
}


/**
 * gpa_key_query_options_boolean:
 * @key: 
 * @options_id: 
 * 
 * 
 * Return Value: a boolean key.
 **/
gboolean
gpa_key_query_options_boolean (GpaKey *key,
				    const gchar *options_id)
{
	const gchar *id;

	id = gpa_key_query_options (key, options_id);

	if (strcmp (id, GPA_TAG_FALSE) == 0)
		return FALSE;
	else if (strcmp (id, GPA_TAG_TRUE) == 0)
		return TRUE;

	gpa_error ("Invalid boolean option (Options [%s], OptionId [%s]\n",
		   options_id, id);

	return FALSE;

}


/* FIXME : this should go somewhere else. Is there a function that does
 * this already ?
 */
static gint
gpa_text_utils_search_backwards (const gchar *buffer, gchar findme)
{
	gint length;
	gint n;

	length = strlen (buffer);
	
	for (n = length; n > 0; n--)
		if (buffer [n-1] == findme)
			return n - 1;
	
	return -1;
}

#endif
