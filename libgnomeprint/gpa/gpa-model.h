#ifndef __GPA_MODEL_H__
#define __GPA_MODEL_H__

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <glib.h>

G_BEGIN_DECLS

#define GPA_TYPE_MODEL (gpa_model_get_type ())
#define GPA_MODEL(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GPA_TYPE_MODEL, GPAModel))
#define GPA_MODEL_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GPA_TYPE_MODEL, GPAModelClass))
#define GPA_IS_MODEL(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GPA_TYPE_MODEL))
#define GPA_IS_MODEL_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GPA_TYPE_MODEL))
#define GPA_MODEL_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GPA_TYPE_MODEL, GPAModelClass))

typedef struct _GPAModel GPAModel;
typedef struct _GPAModelClass GPAModelClass;

#include <tree.h>
#include "gpa-list.h"

/* GPAModel */

struct _GPAModel {
	GPANode node;
	
	guint loaded : 1;

	GPANode *name;
	GPANode *vendor;
	GPAList *options;
};

struct _GPAModelClass {
	GPANodeClass node_class;
};

GType gpa_model_get_type (void);

GPANode *gpa_model_new_from_info_tree (xmlNodePtr tree);
GPANode *gpa_model_new_from_tree (xmlNodePtr tree);

GPAList *gpa_model_list_new_from_info_tree (xmlNodePtr tree);

GPANode *gpa_model_get_by_id (const guchar *id);

gboolean gpa_model_load (GPAModel *model);

#define GPA_MODEL_ENSURE_LOADED(m) ((m) && GPA_IS_MODEL(m) && (GPA_MODEL(m)->loaded || gpa_model_load (GPA_MODEL(m))))

#if 0
/* GPAModelList */

#define GPA_MODEL_ID(m) (GPA_ITEM (m)->id)
#define GPA_MODEL_VENDOR(m) (GPA_NODE (m)->parent ? GPA_NODE (m)->parent->parent ? GPA_VENDOR (GPA_NODE (m)->parent->parent) : NULL : NULL)

/* Constructors */

GPAModel *gpa_model_new (const gchar *id, const gchar *name);
GPAModel *gpa_model_new_from_info_tree (xmlNodePtr tree, XmlParseContext *context);

/* Basic "GpaModel" object operations */

gboolean gpa_model_verify_with_settings (const GPAModel *model, GpaSettings *settings);
gboolean gpa_model_save (GPAModel *model);

/* Access to the struct */
GpaOptions * gpa_model_get_options_from_id (const GPAModel *model, const gchar *id);

/* Utility functions */
gboolean gpa_model_add_missing_default_settings_and_values (GPAModel *model);

GpaBackend *gpa_model_get_default_backend (GPAModel *model);
#endif

G_END_DECLS

#endif /* __GPA_MODEL_H__ */
