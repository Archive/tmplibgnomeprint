#define __GPA_SETTINGS_C__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <string.h>
#include <malloc.h>
#include <xmlmemory.h>
#include "gpa-utils.h"
#include "gpa-value.h"
#include "gpa-reference.h"
#include "gpa-model.h"
#include "gpa-option.h"
#include "gpa-key.h"
#include "gpa-settings.h"

static void gpa_settings_class_init (GPASettingsClass *klass);
static void gpa_settings_init (GPASettings *settings);

static void gpa_settings_finalize (GObject *object);

static GPANode *gpa_settings_duplicate (GPANode *node);
static gboolean gpa_settings_verify (GPANode *node);
static guchar *gpa_settings_get_value (GPANode *node);
static GPANode *gpa_settings_get_child (GPANode *node, GPANode *ref);
static GPANode *gpa_settings_lookup (GPANode *node, const guchar *path);
static void gpa_settings_modified (GPANode *node);

static GPANodeClass *parent_class = NULL;

GType
gpa_settings_get_type (void)
{
	static GType type = 0;
	if (!type) {
		static const GTypeInfo info = {
			sizeof (GPASettingsClass),
			NULL, NULL,
			(GClassInitFunc) gpa_settings_class_init,
			NULL, NULL,
			sizeof (GPASettings),
			0,
			(GInstanceInitFunc) gpa_settings_init
		};
		type = g_type_register_static (GPA_TYPE_NODE, "GPASettings", &info, 0);
	}
	return type;
}

static void
gpa_settings_class_init (GPASettingsClass *klass)
{
	GObjectClass *object_class;
	GPANodeClass *node_class;

	object_class = (GObjectClass *) klass;
	node_class = (GPANodeClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gpa_settings_finalize;

	node_class->duplicate = gpa_settings_duplicate;
	node_class->verify = gpa_settings_verify;
	node_class->get_value = gpa_settings_get_value;
	node_class->get_child = gpa_settings_get_child;
	node_class->lookup = gpa_settings_lookup;
	node_class->modified = gpa_settings_modified;
}

static void
gpa_settings_init (GPASettings *settings)
{
	settings->name = NULL;
	settings->model = NULL;
	settings->keys = NULL;
}

static void
gpa_settings_finalize (GObject *object)
{
	GPASettings *settings;

	settings = GPA_SETTINGS (object);

	settings->name = gpa_node_detach_unref (GPA_NODE (settings), GPA_NODE (settings->name));
	settings->model = gpa_node_detach_unref (GPA_NODE (settings), GPA_NODE (settings->model));

	while (settings->keys) {
		settings->keys = gpa_node_detach_unref_next (GPA_NODE (settings), GPA_NODE (settings->keys));
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static GPANode *
gpa_settings_duplicate (GPANode *node)
{
	GPASettings *settings, *new;
	GPANode *child;
	GSList *l;

	settings = GPA_SETTINGS (node);

	new = (GPASettings *) gpa_node_new (GPA_TYPE_SETTINGS, node->id);

	if (settings->name) {
		new->name = gpa_node_attach (GPA_NODE (new), gpa_node_duplicate (settings->name));
	}

	if (settings->model) {
		new->model = gpa_node_attach (GPA_NODE (new), gpa_node_duplicate (settings->model));
	}

	l = NULL;
	for (child = settings->keys; child != NULL; child = child->next) {
		GPANode *newchild;
		newchild = gpa_node_duplicate (child);
		if (newchild) l = g_slist_prepend (l, newchild);
	}

	while (l) {
		GPANode *newchild;
		newchild = GPA_NODE (l->data);
		l = g_slist_remove (l, newchild);
		newchild->parent = GPA_NODE (new);
		newchild->next = new->keys;
		new->keys = newchild;
	}

	return GPA_NODE (new);
}

static gboolean
gpa_settings_verify (GPANode *node)
{
	/* fixme: Verify on option */

	return TRUE;
}

static guchar *
gpa_settings_get_value (GPANode *node)
{
	/* fixme: */

	return NULL;
}

static GPANode *
gpa_settings_get_child (GPANode *node, GPANode *ref)
{
	GPASettings *settings;
	GPANode *child;

	settings = GPA_SETTINGS (node);

	child = NULL;
	if (!ref) {
		child = settings->name;
	} else if (ref == settings->name) {
		child = settings->model;
	} else if (ref == settings->model) {
		child = settings->keys;
	} else {
		if (ref->next) child = ref->next;
	}

	if (child) gpa_node_ref (child);

	return child;
}

static GPANode *
gpa_settings_lookup (GPANode *node, const guchar *path)
{
	GPASettings *settings;
	GPANode *child;
	const guchar *dot, *next;
	gint len;

	settings = GPA_SETTINGS (node);

	child = NULL;

	if (gpa_node_lookup_ref (&child, GPA_NODE (settings->name), path, "Name")) return child;
	if (gpa_node_lookup_ref (&child, GPA_NODE (settings->model), path, "Model")) return child;

	dot = strchr (path, '.');
	if (dot != NULL) {
		len = dot - path;
		next = dot + 1;
	} else {
		len = strlen (path);
		next = path + len;
	}

	for (child = settings->keys; child != NULL; child = child->next) {
		g_assert (GPA_IS_KEY (child));
		if (child->id && strlen (child->id) == len && !strncmp (child->id, path, len)) {
			if (!next) {
				gpa_node_ref (child);
				return child;
			} else {
				return gpa_node_lookup (child, next);
			}
		}
	}

	return NULL;
}

static void
gpa_settings_modified (GPANode *node)
{
	GPASettings *settings;
	GPANode *child;

	settings = GPA_SETTINGS (node);

	if (settings->name && GPA_NODE_FLAGS (settings->name) & GPA_MODIFIED_FLAG) {
		gpa_node_emit_modified (settings->name);
	}
	if (settings->model && GPA_NODE_FLAGS (settings->model) & GPA_MODIFIED_FLAG) {
		gpa_node_emit_modified (settings->model);
	}

	child = settings->keys;
	while (child) {
		GPANode *next;
		next = child->next;
		if (GPA_NODE_FLAGS (child) & GPA_MODIFIED_FLAG) {
			gpa_node_ref (child);
			gpa_node_emit_modified (child);
			gpa_node_unref (child);
		}
		child = next;
	}
}

GPANode *
gpa_settings_new_empty (const guchar *name)
{
	GPASettings *settings;

	g_return_val_if_fail (name != NULL, NULL);
	g_return_val_if_fail (*name != '\0', NULL);

	settings = (GPASettings *) gpa_node_new (GPA_TYPE_SETTINGS, NULL);

	settings->name = gpa_value_new ("Name", name);
	settings->name->parent = GPA_NODE (settings);
	settings->model = gpa_reference_new_empty ();
	settings->model->parent = GPA_NODE (settings);

	return GPA_NODE (settings);
}

GPANode *
gpa_settings_new_from_model (GPANode *model, const guchar *name)
{
	GPASettings *settings;
	guchar *id;

	g_return_val_if_fail (model != NULL, NULL);
	g_return_val_if_fail (GPA_IS_MODEL (model), NULL);
	g_return_val_if_fail (name != NULL, NULL);
	g_return_val_if_fail (*name != '\0', NULL);
	g_return_val_if_fail (gpa_node_verify (model), NULL);
	g_return_val_if_fail (GPA_MODEL_ENSURE_LOADED (model), NULL);

	id = gpa_id_new ("SETTINGS");
	settings = (GPASettings *) gpa_settings_new_from_model_full (model, id, name);
	g_free (id);

	return (GPANode *) settings;
}

GPANode *
gpa_settings_new_from_model_full (GPANode *model, const guchar *id, const guchar *name)
{
	GPASettings *settings;
	GPANode *child;
	GSList *l;

	g_return_val_if_fail (model != NULL, NULL);
	g_return_val_if_fail (GPA_IS_MODEL (model), NULL);
	g_return_val_if_fail (id != NULL, NULL);
	g_return_val_if_fail (*id != '\0', NULL);
	g_return_val_if_fail (name != NULL, NULL);
	g_return_val_if_fail (*name != '\0', NULL);
	g_return_val_if_fail (gpa_node_verify (GPA_NODE (model)), NULL);
	g_return_val_if_fail (GPA_MODEL_ENSURE_LOADED (model), NULL);

	settings = (GPASettings *) gpa_node_new (GPA_TYPE_SETTINGS, id);

	settings->name = gpa_node_attach (GPA_NODE (settings), gpa_value_new ("Name", name));
	settings->model = gpa_node_attach (GPA_NODE (settings), gpa_reference_new (model));

	l = NULL;
	for (child = GPA_MODEL (model)->options->children; child != NULL; child = child->next) {
		GPANode *key;
		key = gpa_key_new_from_option (child);
		if (key) l = g_slist_prepend (l, key);
	}

	while (l) {
		GPANode *key;
		key = GPA_NODE (l->data);
		l = g_slist_remove (l, key);
		key->parent = GPA_NODE (settings);
		key->next = settings->keys;
		settings->keys = key;
	}

	return (GPANode *) settings;
}

GPANode *
gpa_settings_new_from_model_and_tree (GPANode *model, xmlNodePtr tree)
{
	GPASettings *settings;
	xmlChar *xmlid;
	xmlNodePtr xmlc;

	g_return_val_if_fail (model != NULL, NULL);
	g_return_val_if_fail (GPA_IS_MODEL (model), NULL);
	g_return_val_if_fail (tree != NULL, NULL);
	g_return_val_if_fail (gpa_node_verify (GPA_NODE (model)), NULL);
	g_return_val_if_fail (GPA_MODEL_ENSURE_LOADED (model), NULL);
	g_return_val_if_fail (!strcmp (tree->name, "Settings"), NULL);

	xmlid = xmlGetProp (tree, "Id");
	g_return_val_if_fail (xmlid != NULL, NULL);

	settings = NULL;
	for (xmlc = tree->xmlChildrenNode; xmlc != NULL; xmlc = xmlc->next) {
		if (!strcmp (xmlc->name, "Name")) {
			xmlChar *content;
			content = xmlNodeGetContent (xmlc);
			if (content && *content) {
				settings = (GPASettings *) gpa_settings_new_from_model_full (model, xmlid, content);
				xmlFree (content);
			}
		} else if (!strcmp (xmlc->name, "Key") && settings) {
			xmlChar *keyid;
			keyid = xmlGetProp (xmlc, "Id");
			if (keyid) {
				GPANode *key;
				for (key = settings->keys; key != NULL; key = key->next) {
					if (!strcmp (keyid, key->id)) {
						gpa_key_merge_from_tree (key, xmlc);
						break;
					}
				}
				xmlFree (keyid);
			}
		}
	}

	xmlFree (xmlid);

	if (!settings) {
		g_warning ("Settings node does not have valid <Name> tag");
	}

	return (GPANode *) settings;
}

xmlNodePtr
gpa_settings_write (xmlDocPtr doc, GPANode *node)
{
	GPASettings *settings;
	xmlNodePtr xmln, xmlc;
	GPANode *child;

	settings = GPA_SETTINGS (node);

	xmln = xmlNewDocNode (doc, NULL, "Settings", NULL);
	xmlSetProp (xmln, "Id", node->id);

	xmlc = xmlNewChild (xmln, NULL, "Name", GPA_VALUE (settings->name)->value);

	for (child = settings->keys; child != NULL; child = child->next) {
		xmlc = gpa_key_write (doc, child);
		if (xmlc) xmlAddChild (xmln, xmlc);
	}

	return xmln;
}

gboolean
gpa_settings_copy (GPASettings *dst, GPASettings *src)
{
	GPANode *child;
	GSList *sl, *dl;

	g_return_val_if_fail (dst != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_SETTINGS (dst), FALSE);
	g_return_val_if_fail (src != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_SETTINGS (src), FALSE);

	g_return_val_if_fail (GPA_VALUE_VALUE (src->name), FALSE);
	g_return_val_if_fail (GPA_VALUE_VALUE (dst->name), FALSE);

	g_return_val_if_fail (src->model != NULL, FALSE);
	g_return_val_if_fail (dst->model != NULL, FALSE);

	gpa_value_set_value_forced (GPA_VALUE (dst->name), GPA_VALUE_VALUE (src->name));

	gpa_reference_set_reference (GPA_REFERENCE (dst->model), GPA_REFERENCE_REFERENCE (src->model));

	dl = NULL;
	while (dst->keys) {
		dl = g_slist_prepend (dl, dst->keys);
		dst->keys = gpa_node_detach_next (GPA_NODE (dst), dst->keys);
	}

	sl = NULL;
	for (child = src->keys; child != NULL; child = child->next) {
		sl = g_slist_prepend (sl, child);
	}

	while (sl) {
		GSList *l;
		for (l = dl; l != NULL; l = l->next) {
			if (!strcmp (GPA_NODE (l->data)->id, GPA_NODE (sl->data)->id)) {
				/* We are in original too */
				child = GPA_NODE (l->data);
				dl = g_slist_remove (dl, l->data);
				child->parent = GPA_NODE (dst);
				child->next = dst->keys;
				dst->keys = child;
				gpa_key_merge_from_key (GPA_KEY (child), GPA_KEY (sl->data));
				break;
			}
		}
		if (!l) {
			/* Create new child */
			child = gpa_node_duplicate (GPA_NODE (sl->data));
			child->parent = GPA_NODE (dst);
			child->next = dst->keys;
			dst->keys = child;
		}
		sl = g_slist_remove (sl, sl->data);
	}

	while (dl) {
		gpa_node_unref (GPA_NODE (dl->data));
		dl = g_slist_remove (dl, dl->data);
	}

	gpa_node_request_modified (GPA_NODE (dst));

	return TRUE;
}

#if 0
static GList *
gpa_settings_paths_copy (GList *source)
{
	GList *dest = NULL;
	GList *list;
	gchar *path;

	if (source == NULL)
		return NULL;
	g_return_val_if_fail (source != NULL, NULL);

	list = source;
	for (; list != NULL; list = list->next) {
		path = list->data;
		dest = g_list_prepend (dest, g_strdup (path));
	}

	return g_list_reverse (dest);
}

GpaSettings *
gpa_settings_new_from_model (GPAModel *model, GPAPrinter *printer)
{
	GpaSettings *settings;
	GpaBackend *backend;

	g_return_val_if_fail (GPA_IS_MODEL (model), NULL);
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);
		
	/* Default backend */
	backend = gpa_model_get_default_backend (model);
	g_return_val_if_fail (backend != NULL, NULL);
	g_return_val_if_fail (GPA_IS_BACKEND (backend), NULL);

	settings = GPA_SETTINGS (gpa_settings_new ());
	
	settings->name             = g_strdup (_("Default Settings"));
	settings->command          = g_strdup ("lpr");
	settings->printer          = printer;
	settings->selected_options = gpa_settings_paths_copy (model->default_settings);
	settings->values           = gpa_values_copy_list (model->default_values);
	settings->selected         = TRUE;

	gpa_settings_value_insert (settings, GPA_TAG_BACKEND, backend->id);

	if (!gpa_settings_verify (settings, TRUE)) {
		gchar *value;
		value = gpa_node_get_path_value (GPA_NODE (model), "Name");
		gpa_error ("Could not create settings from model \"%s\"\n", value);
		g_free (value);
		return NULL;
	}

	return settings;
}

static xmlNodePtr
gpa_settings_options_write (XmlParseContext *context, GpaSettings *settings)
{
	xmlNodePtr node;
	GList *list;
	gchar *path;

	g_return_val_if_fail (context != NULL, NULL);
	g_return_val_if_fail (GPA_IS_SETTINGS (settings), NULL);

	node = xmlNewDocNode (context->doc, context->ns, GPA_TAG_SELECTED_OPTIONS, NULL);

	list = settings->selected_options;
	for (; list != NULL; list = list->next) {
		path = (gchar *) list->data;
		xmlNewChild (node, context->ns, GPA_TAG_PATH, path);
	}

	return node;
}

static xmlNodePtr
gpa_settings_write (XmlParseContext *context, GpaSettings *settings)
{
	xmlNodePtr node;
	xmlNodePtr child;

	g_return_val_if_fail (context != NULL, NULL);
	g_return_val_if_fail (GPA_IS_SETTINGS (settings), NULL);

	node = xmlNewDocNode (context->doc, context->ns, GPA_TAG_SETTINGS, NULL);

	xmlNewChild (node, context->ns, GPA_TAG_NAME, settings->name);
	xmlNewChild (node, context->ns, GPA_TAG_COMMAND, settings->command);
	xmlNewChild (node, context->ns, GPA_TAG_SELECTED,
		     settings->selected ? GPA_TAG_TRUE: GPA_TAG_FALSE);

	child = gpa_settings_options_write (context, settings);
	if (child == NULL)
		return NULL;
	xmlAddChild (node, child);

	child = gpa_values_write_list (context, settings->values,
				       GPA_TAG_SETTINGS_INFO);
	if (child == NULL)
		return NULL;
	xmlAddChild (node, child);

	return node;
}

xmlNodePtr
gpa_settings_list_write (XmlParseContext *context, GPAList *settings_list)
{
	GpaSettings *settings = NULL;
	xmlNodePtr child;
	xmlNodePtr node;
	GPAItem *item;

	g_return_val_if_fail (context != NULL, NULL);
	g_return_val_if_fail (settings_list != NULL, NULL);

	node = xmlNewDocNode (context->doc,
			      context->ns,
			      GPA_TAG_SETTINGS_LIST,
			      NULL);
	
	item = settings_list->items;
	for (; item != NULL; item = item->next) {
		settings = GPA_SETTINGS (item);
		if (!GPA_IS_SETTINGS (settings))
			return NULL;
		child = gpa_settings_write (context, settings);
		if (child == NULL)
			return NULL;
		xmlAddChild (node, child);
	}

	return node;
}
		
gboolean
gpa_settings_verify (GpaSettings *settings, gboolean fail)
{
	GPAModel *model;
	GpaOption *option;
	GList *list;
	GList *list2;
	GList *remove = NULL;
	gchar *path;
	gchar *path2;
	
	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_SETTINGS (settings), FALSE);

	model = settings->printer->model;

	g_return_val_if_fail (GPA_IS_MODEL (model), FALSE);

	if (settings->selected_options == NULL) {
		if (settings->printer->model->options_list != NULL) {
			gpa_error ("The settings \"%s\", does not contain a list of selected options",
				   settings->name);
		return FALSE;
		}
	}

	list = settings->selected_options;
	for (; list != NULL; list = list->next) {
		path = (gchar *)list->data;
		option = gpa_option_get_from_model_and_path (model, path);
		if (option == NULL) {
			g_warning ("Removing the %s path from the selected_options list. FIXED ..",
				   path);
			if (fail)
				return FALSE;
			remove = g_list_prepend (remove, path);
			continue;
		}
		
		/* Now verify that this option is not repeated in the list */
		list2 = settings->selected_options;
		for (; list2 != NULL; list2 = list2->next) {
			gint n,m;
			path2 = (gchar *)list2->data;
			if (path2 == path)
				continue;
			n = gpa_text_utils_search_backwards (path, GPA_PATH_DELIMITER_CHAR);
			m = gpa_text_utils_search_backwards (path2, GPA_PATH_DELIMITER_CHAR);
			if (strncmp (path, path2, MAX(n,m)) == 0) {
				g_warning ("The path \"%s\" was duplicated in the \"%s\" settings. FIXED ..\n",
					   path, gpa_settings_get_name (settings));
				if (fail)
					return FALSE;
				if (!g_list_find (remove, path2))
				    remove = g_list_prepend (remove, path2);
				continue;
			}
		}
		
		if (!gpa_option_verify (option))
			return FALSE;
	}

	/* Dump the current list of settings for debuggin */
	if (remove != NULL) {
		g_print ("Here is the list of settings before cleaning:\n");
		list = settings->selected_options;
		for (; list != NULL; list = list->next) {
			path = (gchar *)list->data;
			g_print ("[%s]\n", path);
		}
		g_print ("\n");
	}

	/* Now remove the paths that are invalid. They point to an option that does
	 * not exist. This can happen if the user updates a .model file and an option
	 * that was present before, is no longer found. (or it's id was renamed)
	 */
	list = remove;
	for (; list != NULL; list = list->next) {
		path = (gchar *)list->data;
		settings->selected_options = g_list_remove (settings->selected_options, path);
		g_free (path);
	}

	/* Dump the current list of settings for debuggin */
	if (remove != NULL) {
		g_print ("Here is the list of settings after cleaning :\n");
		list = settings->selected_options;
		for (; list != NULL; list = list->next) {
			path = (gchar *)list->data;
			g_print ("[%s]\n", path);
		}
		g_print ("\n");
	}

	g_list_free (remove);

	if (!gpa_values_verify_settings (settings))
		return FALSE;
	
	if (!gpa_model_verify_with_settings (settings->printer->model, settings))
		return FALSE;
	
	return TRUE;
}

gboolean 
gpa_settings_list_verify (GPAList *settings_list, gboolean fail)
{
	GpaSettings *settings;
	GPAItem *item;
	gint selected = 0;

	debug (FALSE, "");

	g_return_val_if_fail (settings_list != NULL, FALSE);

	item = settings_list->items;
	for (; item != NULL; item = item->next) {
		settings = GPA_SETTINGS (item);
		if (!gpa_settings_verify (settings, fail))
			return FALSE;
	}

	/* Verify that there is one and only one settings seletected */
	item = settings_list->items;
	for (; item != NULL; item = item->next) {
		settings = GPA_SETTINGS (item);
		if (settings->selected)
			selected++;
			
	}

	if (selected != 1) {
		gpa_error ("There needs to be one and only one selected settings "
			   "there are [%i] settings selected.", selected);
		return FALSE;
	}
	
	return TRUE;
}

/* FIXME: this is a problem
 * what if 2 programs are using different settings to print
 * to the same printer, For this reason, we can't determine
 * the selected settings for a printer.
 */
GpaSettings *
gpa_settings_get_selected (GPAList *settings_list)
{
	GpaSettings *settings = NULL;
	GPAItem *item;

	debug (FALSE, "");

	g_return_val_if_fail (settings_list != NULL, NULL);

	item = settings_list->items;
	for (; item != NULL; item = item->next) {
		settings = GPA_SETTINGS (item);
		if (settings == NULL)
			return NULL;
		if (settings->selected)
			return settings;
	}

	gpa_error ("Could not determine the selected settings, (auto-fixed)");

	/* Fix the problem */
	if (GPA_IS_SETTINGS (settings)) {
		settings->selected = TRUE;
		return settings;
	}
		
	return NULL;
}


static GList *
gpa_selected_options_new_from_node (xmlNodePtr tree)
{
	xmlNodePtr child;
	GList *list = NULL;
	gchar *path;
	
	debug (FALSE, "");

	g_return_val_if_fail (tree != NULL, NULL);

	if (!gpa_xml_node_verify (tree, GPA_TAG_SELECTED_OPTIONS))
		return NULL;


	child = tree->childs;
	while (child != NULL) {
		skip_text (child);
		path = xmlNodeGetContent (child);
		list = g_list_prepend (list, path);
		child = child->next;
	}

	return g_list_reverse (list);
}

static GpaSettings *
gpa_settings_new_from_node (xmlNodePtr tree, GPAPrinter *printer)
{
	GpaSettings *settings;
	xmlNodePtr child;
	gchar *name;
	gchar *command;
	gchar *selected;
	
	debug (FALSE, "");
	
	g_return_val_if_fail (tree != NULL, NULL);

	if (!gpa_xml_node_verify (tree, GPA_TAG_SETTINGS))
		return NULL;

	name = gpa_xml_get_value_string_required (tree, GPA_TAG_NAME, GPA_TAG_SETTINGS);
	if (name == NULL)
		return NULL;
	command = gpa_xml_get_value_string_required (tree, GPA_TAG_COMMAND, GPA_TAG_SETTINGS);
	if (name == NULL)
		return NULL;
	
	settings = GPA_SETTINGS (gpa_settings_new ());

	selected = gpa_xml_get_value_string (tree, GPA_TAG_SELECTED);
	if ((selected != NULL) && strcmp (selected, GPA_TAG_TRUE) == 0)
		settings->selected = TRUE;
	g_free (selected);

	settings->name    = name;
	settings->command = command;
	settings->printer = printer;

	/* Load Selected options */
	child = gpa_xml_search_child_required (tree, GPA_TAG_SELECTED_OPTIONS);
	if (child == NULL)
		return FALSE;
	settings->selected_options = gpa_selected_options_new_from_node (child);


	child = gpa_xml_search_child_required (tree, GPA_TAG_SETTINGS_INFO);
	if (child == NULL)
		return FALSE;
	settings->values = gpa_values_new_from_node (child,
						     GPA_TAG_SETTINGS_INFO);
	
	if (!gpa_settings_verify (settings, TRUE))
		return NULL;
	
	return settings;
}


/**
 * gpa_settigns_load_default_paths_from_node:
 * @tree: the node which we should load the settings from
 * 
 * Given a DefaultSettingsNode, it loads all the settings into a GList
 * of gchar containing settings paths. Like "PaperSize-Letter" or "Resolution-300"
 * 
 * Return Value: a GList of gchar pointers
 **/
gboolean
gpa_settigns_load_default_paths_from_node (xmlNodePtr tree_, GPAModel *model)
{
	xmlNodePtr tree;
	xmlNodePtr child;
	xmlNodePtr child_child;
	GList *paths;
	GList *list;
	gchar *path;
	gchar *test_path;
	gint size;
	
	debug (FALSE, "");

	g_return_val_if_fail (tree_ != NULL, FALSE);

	tree = gpa_include_node (tree_);

	if (!gpa_xml_node_verify (tree, GPA_TAG_DEFAULT_SETTINGS))
		return FALSE;

	paths = model->default_settings;
	
	child_child = gpa_xml_search_child_required (tree, GPA_TAG_PATHS);
	if (child_child == NULL)
		return FALSE;

	child = child_child->childs;
	
	for (; child != NULL; child = child->next) {
		skip_text (child);
		path = xmlNodeGetContent (child);
		
		/* Search for the path to see if we are duplicating it,
		 * if we are, remove the old path and add the new one.
		 * we need this since you can specify default settings inside
		 * the nodes or inside the <default_settings> node.
		 * we take the nodes default settings, unless there is an
		 * entry in the <DefaultSettings->Paths> */
		for (list = paths; list != NULL; list = list->next) {
			test_path = list->data;
			size = tu_get_pos_of_last_delimiter (test_path, GPA_PATH_DELIMITER_CHAR);
			g_return_val_if_fail (size > 0, FALSE);
			if (strncmp (path, test_path, size) == 0) {
				g_free (test_path);
				list->data = path;
				path = NULL;
				break;
			}
		}

		if (path == NULL)
			continue;
		
		paths = g_list_prepend (paths, path);
	}

	model->default_settings = g_list_reverse (paths);

	return TRUE;
}

GPAList *
gpa_settings_list_load_from_node (xmlNodePtr tree, GPAPrinter *printer)
{
	GpaSettings *settings;
	xmlNodePtr child;
	GPANode *list = NULL;
	GPANode *last;

	debug (FALSE, "");
	
	g_return_val_if_fail (tree != NULL, NULL);
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);
	
	if (!gpa_xml_node_verify (tree, GPA_TAG_SETTINGS_LIST))
		return NULL;

	list = gpa_settings_list_new ();
	last = NULL;

	child = tree->childs;

	while (child != NULL) {
		skip_text (child);
		settings = gpa_settings_new_from_node (child, printer);
		if (!GPA_IS_SETTINGS (settings))
			return NULL;
		gpa_node_add_child (list, GPA_NODE (settings), last);
		child = child->next;
		last = GPA_NODE (settings);
	}
	
	return GPA_LIST (list);
}


static GList *
gpa_selected_options_copy (GList *selected_options)
{
	GList *new_list = NULL;
	GList *list;
	gchar *new_path;
	gchar *path;

	list = selected_options;
	for (; list != NULL; list = list->next) {
		path = (gchar *)list->data;
		new_path = g_strdup (path);
		new_list = g_list_prepend (new_list, new_path);
	}

	return g_list_reverse (new_list);
}

#if 0
/* This is handled by gpa_node_duplicate */
GPAList *
gpa_settings_list_copy (GPAList *settings_list)
{
	GpaSettings *settings;
	GpaSettings *new_settings;
	GPAList *new_settings_list = NULL;
	GPAItem *item;
	GPANode *last;

	new_settings_list = (GPAList *) gpa_settings_list_new (NULL);
	last = NULL;

	item = settings_list->items;
	for (; item != NULL; item = item->next) {
		settings = (GpaSettings *) item;
		
		if (!GPA_IS_SETTINGS (settings))
			return NULL;

		new_settings = gpa_settings_copy (settings);
		if (new_settings == NULL)
			return NULL;
		gpa_node_add_child (GPA_NODE (new_settings_list), GPA_NODE (new_settings), last);
		last = (GPANode *) new_settings;
	}

	return GPA_LIST (new_settings_list);
}
#endif
		
gchar*
gpa_settings_value_dup (GpaSettings *settings, const gchar *key)
{
	GpaValue *value;
	GList *list;
	
	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_SETTINGS (settings), NULL);
	g_return_val_if_fail (key != NULL,    NULL);

	list = settings->values;
	for (; list != NULL; list = list->next) {
		value = (GpaValue *)list->data;
		if (strcmp (value->key, key) == 0)
			return g_strdup (value->value);
	}

	g_warning ("Could not find the \"%s\" key in the \"%s\" settings values list\n",
		   key, gpa_settings_get_name (settings));
	
	return NULL;
}

gchar*
gpa_settings_value_dup_required (GpaSettings *settings, const gchar *key)
{
	gchar *value;
	
	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_SETTINGS (settings), NULL);
	g_return_val_if_fail (key != NULL,    NULL);

	value = gpa_settings_value_dup (settings, key);

	if (value == NULL)
		gpa_error ("The required value \"%s\" was not found in the "
			   "\"%s\" settings hash table", key, settings->name);
	

	debug (FALSE, "end");

	return g_strdup (value);
}

gboolean
gpa_settings_value_get_from_option_double (GpaSettings *settings,
					   GpaOption *option,
					   gdouble *value)
{
	gchar *str_value;
	gchar *key;

	debug (FALSE, "");

	*value = 0.0;

	g_return_val_if_fail (GPA_IS_SETTINGS (settings), FALSE);
	g_return_val_if_fail (option != NULL, FALSE);

	key = g_strdup_printf ("%s" GPA_PATH_DELIMITER "%s", GPA_ITEM (option->parent)->id, GPA_ITEM (option)->id);

	str_value = gpa_settings_value_dup (settings, key);

	if (str_value == NULL) 
		return FALSE;

	*value = atof (str_value);

	g_free (str_value);

	return TRUE;
}

gboolean
gpa_settings_value_get_from_option_int (GpaSettings *settings,
					GpaOption *option,
					gint *value)
{
	gchar *str_value;
	gchar *key;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_SETTINGS (settings), FALSE);
	g_return_val_if_fail (option != NULL, FALSE);

	key = g_strdup_printf ("%s" GPA_PATH_DELIMITER "%s", GPA_ITEM (option->parent)->id, GPA_ITEM (option)->id);

	str_value = gpa_settings_value_dup (settings, key);

	if (str_value == NULL) 
		return FALSE;

	*value = atoi (str_value);

	g_free (str_value);

	return TRUE;
}

gboolean
gpa_settings_value_replace (GpaSettings *settings, const gchar *key, const gchar *new_value)
{
	GpaValue *value;
	GList *list;
	
	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_SETTINGS (settings), FALSE);
	g_return_val_if_fail (key != NULL,    FALSE);


	list = settings->values;
	for (; list != NULL; list = list->next) {
		value = (GpaValue *)list->data;
		if (strcmp (value->key, key) == 0) {
			g_free (value->value);
			value->value = g_strdup (new_value);
			return TRUE;
		}
	}
	
	gpa_error ("Value not found. Settings:\"%s\" Key:\"%s\"",
		   gpa_settings_get_name (settings), key);

	return TRUE;
}

gboolean
gpa_settings_value_insert (GpaSettings *settings, const gchar *key, const gchar *value)
{
	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_SETTINGS (settings), FALSE);
	g_return_val_if_fail (key != NULL,    FALSE);

	settings->values = gpa_value_insert (settings->values, key, value);

	return TRUE;
}



gboolean
gpa_settings_select (GpaSettings *settings_in,
		     GPAList *settings_list)
{
	GpaSettings *settings = NULL;
	GPAItem *item;
	gboolean found = FALSE;

	g_return_val_if_fail (GPA_IS_SETTINGS (settings_in), FALSE);
	g_return_val_if_fail (settings_list != NULL, FALSE);

	item = settings_list->items;
	for (; item != NULL; item = item->next) {
		settings = GPA_SETTINGS (item);
		if (!GPA_IS_SETTINGS (settings)) {
			return FALSE;
		}
		if (settings == settings_in) {
			found = TRUE;
			settings->selected = TRUE;
		} else {
			settings->selected = FALSE;
		}
	}

	if (!found) {
		gpa_error ("Could not select settings, settings not found in list");
		return FALSE;
	}

	return TRUE;
}

gboolean
gpa_settings_name_taken (GPAList *settings_list, const gchar *name)
{
	GpaSettings *settings;
	GPAItem *item;

	g_return_val_if_fail (settings_list != NULL, TRUE);
	g_return_val_if_fail (name != NULL, TRUE);

	item = settings_list->items;
	for (; item != NULL; item = item->next) {
		settings = GPA_SETTINGS (item);
		if (settings == NULL)
			return FALSE;
		if (strcmp (settings->name, name) == 0)
			return TRUE;
	}

	return FALSE;
}

gchar *
gpa_settings_get_free_name (GpaSettings *settings, GPAList *settings_list)
{
	gint number = 2;
	gchar *name;

	g_return_val_if_fail (GPA_IS_SETTINGS (settings), NULL);
	g_return_val_if_fail (settings_list != NULL, NULL);
	
	name = g_strdup_printf ("Copy of %s", settings->name);

	while (gpa_settings_name_taken (settings_list, name)) {
		g_free (name);
		name = g_strdup_printf ("Copy of %s (%i)", settings->name, number++);
	}

	return name;
}



/* Access to the struct from the world */
const GPAModel*
gpa_settings_get_model (GpaSettings *settings)
{
	g_return_val_if_fail (GPA_IS_SETTINGS (settings), NULL);
	g_return_val_if_fail (GPA_IS_PRINTER (settings->printer), NULL);
	
	return settings->printer->model;
}

gchar *
gpa_settings_dup_name (GpaSettings *settings)
{
	g_return_val_if_fail (GPA_IS_SETTINGS (settings), NULL);
	g_return_val_if_fail (settings->name != NULL, NULL);
	
	return g_strdup (settings->name);
}

const gchar *
gpa_settings_get_name (GpaSettings *settings)
{
	g_return_val_if_fail (GPA_IS_SETTINGS (settings), NULL);
	g_return_val_if_fail (settings->name != NULL, NULL);
	
	return settings->name;
}


gchar *
gpa_settings_dup_command (GpaSettings *settings)
{
	g_return_val_if_fail (GPA_IS_SETTINGS (settings), NULL);
	g_return_val_if_fail (settings->command != NULL, NULL);
	
	return g_strdup (settings->command);
}

const gchar *
gpa_settings_get_command (GpaSettings *settings)
{
	g_return_val_if_fail (GPA_IS_SETTINGS (settings), NULL);
	g_return_val_if_fail (settings->command != NULL, NULL);
	
	return settings->command;
}

void
gpa_settings_name_replace (GpaSettings *settings, const gchar *name)
{
	g_return_if_fail (GPA_IS_SETTINGS (settings));
	g_return_if_fail (name != NULL);

	if (settings->name == NULL)
		g_warning ("Settings name should not be null. [gpa-settings.c]");
	else
		g_free (settings->name);

	settings->name = g_strdup (name);
	
}

gboolean
gpa_settings_is_selected (const GpaSettings *settings)
{
	g_return_val_if_fail (GPA_IS_SETTINGS (settings), FALSE);

	return settings->selected;
}


GList *
gpa_settings_get_selected_options (GpaSettings *settings)
{
	g_return_val_if_fail (GPA_IS_SETTINGS (settings), NULL);

	return settings->selected_options;
}

void
gpa_settings_set_selected_options (GpaSettings *settings,
				   GList *selected_options)
{
	g_return_if_fail (GPA_IS_SETTINGS (settings));

	settings->selected_options = selected_options;
}






/**
 * gpa_options_unselect:
 * @options: 
 * @settings: 
 * 
 * Removes from the settings List of selected paths
 * the selected option for @options. This is used when
 * a selected option is changed, we must first remove
 * the old selected option and then add the new option
 * path to the GList
 * 
 * Return Value: TRUE on success, FALSE on error
 **/
static gboolean
gpa_settings_unselect_options (GpaSettings *settings,
			       const GpaOptions *options)
{
	GList *selected_options;
	GList *list;
	gchar *path;
	gchar *findme;
	gchar *options_path;
	gint pos;

	debug (FALSE, "");

	selected_options = gpa_settings_get_selected_options (settings);

	g_return_val_if_fail (selected_options != NULL, FALSE);
	g_return_val_if_fail (options != NULL, FALSE);

	/* get the option path with the last token stripped */
	options_path = gpa_options_dup_path (options);
	
	g_return_val_if_fail (options_path != NULL, FALSE);

	list = selected_options;
	for (; list != NULL; list = list->next) {
		path = (gchar *)list->data;
		pos = gpa_text_utils_search_backwards (path, GPA_PATH_DELIMITER);
		if (pos == -1) {
			gpa_error ("Invalid path %s (1)\n", path);
			return FALSE;
		}
		findme = g_strndup (path, pos);

		if (strcmp (options_path, findme) == 0) {
			/* FIXME: use the list directly */
			selected_options = g_list_remove_link (selected_options, list);
			g_free (path);
			g_free (findme);
			g_free (options_path);
			gpa_settings_set_selected_options (settings, selected_options);
			return TRUE;
		}
		g_free (findme);
	}

	g_free (options_path);
	
	return FALSE;
}

/**
 * gpa_settings_selected_option:
 * @settings: 
 * @option: 
 * 
 * Select the @option in the @settings Object
 **/
gboolean
gpa_settings_select_option (GpaSettings *settings,
			    GpaOption *option)
{
	gchar *new_path;
	
 	g_return_val_if_fail (GPA_IS_SETTINGS (settings), FALSE);
	g_return_val_if_fail (GPA_IS_OPTION (option), FALSE);

	/* Remove the previously selected option */
	if (!gpa_settings_unselect_options (settings, option->parent)) {
		gpa_error ("Could not unselect the Option from the %s Options\n",
			   gpa_options_get_name (option->parent));
	}

	new_path = gpa_option_dup_path (option);
	settings->selected_options = g_list_prepend (settings->selected_options,
						      new_path);

	return TRUE;
}
								      


void
gpa_settings_replace_command (GpaSettings *settings,  const gchar *command)
{
	g_return_if_fail (GPA_IS_SETTINGS (settings));
	g_return_if_fail (command != NULL);

	g_free (settings->command);
	settings->command = g_strdup (command);
		
}



/**
 * gpa_settings_query_options:
 * @settings: 
 * @options_id: 
 * 
 * Query a Settings object for a selected option.
 * the settings are not const because if there isn't a selected
 * option, we need to select one. This can happen
 * if ... (see : gpa_options_get_selected_option)
 * 
 * Return Value: a pointer to the selected option id
 **/
const gchar *
gpa_settings_query_options (GpaSettings *settings,
			    const gchar *options_id)
{
	const GPAModel *model;
	GpaOptions *options;
	GpaOption *option;
	
	g_return_val_if_fail (GPA_IS_SETTINGS (settings), NULL);
	g_return_val_if_fail (options_id != NULL, NULL);

	model = settings->printer->model;
	
	options = gpa_model_get_options_from_id (model, options_id);

	if (options == NULL) {
		gchar *value;
		value = gpa_node_get_path_value (GPA_NODE (settings->printer->model), "Name");
		gpa_error ("Could not find the \"%s\" options in the \"%s\" model "
			   "options list",
			   options_id, value);
		g_free (value);
	}

	option = gpa_options_get_selected_option (settings, options, FALSE);

	g_return_val_if_fail (option != NULL, NULL);

	return gpa_option_get_id (option);
}


/**
 * gpa_settings_query_options_boolean:
 * @settings: 
 * @options_id: 
 * 
 * 
 * Return Value: a boolean setting.
 **/
gboolean
gpa_settings_query_options_boolean (GpaSettings *settings,
				    const gchar *options_id)
{
	const gchar *id;

	id = gpa_settings_query_options (settings, options_id);

	if (strcmp (id, GPA_TAG_FALSE) == 0)
		return FALSE;
	else if (strcmp (id, GPA_TAG_TRUE) == 0)
		return TRUE;

	gpa_error ("Invalid boolean option (Options [%s], OptionId [%s]\n",
		   options_id, id);

	return FALSE;

}


/* FIXME : this should go somewhere else. Is there a function that does
 * this already ?
 */
static gint
gpa_text_utils_search_backwards (const gchar *buffer, gchar findme)
{
	gint length;
	gint n;

	length = strlen (buffer);
	
	for (n = length; n > 0; n--)
		if (buffer [n-1] == findme)
			return n - 1;
	
	return -1;
}

#endif
