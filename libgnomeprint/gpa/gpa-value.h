#ifndef __GPA_VALUE_H__
#define __GPA_VALUE_H__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <glib.h>

G_BEGIN_DECLS

#define GPA_TYPE_VALUE (gpa_value_get_type ())
#define GPA_VALUE(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GPA_TYPE_VALUE, GPAValue))
#define GPA_VALUE_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GPA_TYPE_VALUE, GPAValueClass))
#define GPA_IS_VALUE(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GPA_TYPE_VALUE))
#define GPA_IS_VALUE_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GPA_TYPE_VALUE))
#define GPA_VALUE_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GPA_TYPE_VALUE, GPAValueClass))

typedef struct _GPAValue GPAValue;
typedef struct _GPAValueClass GPAValueClass;

#include "tree.h"
#include "gpa-node-private.h"

struct _GPAValue {
	GPANode node;
	guchar *value;
};

struct _GPAValueClass {
	GPANodeClass node_class;
};

#define GPA_VALUE_VALUE(v) ((v) ? GPA_VALUE (v)->value : NULL)

GType gpa_value_get_type (void);

GPANode *gpa_value_new (const guchar *id, const guchar *content);

GPANode *gpa_value_new_from_tree (const guchar *id, xmlNodePtr tree);

gboolean gpa_value_set_value_forced (GPAValue *value, const guchar *val);

G_END_DECLS

#endif
