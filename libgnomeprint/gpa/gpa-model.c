#define __GPA_MODEL_C__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <string.h>
#include <parser.h>
#include <xmlmemory.h>

#include "gpa-utils.h"
#include "gpa-value.h"
#include "gpa-reference.h"
#include "gpa-vendor.h"
#include "gpa-option.h"
#include "gpa-model.h"

/* GPAModel */

static void gpa_model_class_init (GPAModelClass *klass);
static void gpa_model_init (GPAModel *model);
static void gpa_model_finalize (GObject *object);

static gboolean gpa_model_verify (GPANode *node);
static guchar *gpa_model_get_value (GPANode *node);
static GPANode *gpa_model_get_child (GPANode *node, GPANode *ref);
static GPANode *gpa_model_lookup (GPANode *node, const guchar *path);
static void gpa_model_modified (GPANode *node);

static GHashTable *modeldict = NULL;
static GPANodeClass *parent_class = NULL;

GType
gpa_model_get_type (void)
{
	static GType type = 0;
	if (!type) {
		static const GTypeInfo info = {
			sizeof (GPAModelClass),
			NULL, NULL,
			(GClassInitFunc) gpa_model_class_init,
			NULL, NULL,
			sizeof (GPAModel),
			0,
			(GInstanceInitFunc) gpa_model_init
		};
		type = g_type_register_static (GPA_TYPE_NODE, "GPAModel", &info, 0);
	}
	return type;
}

static void
gpa_model_class_init (GPAModelClass *klass)
{
	GObjectClass *object_class;
	GPANodeClass *node_class;

	object_class = (GObjectClass *) klass;
	node_class = (GPANodeClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gpa_model_finalize;

	node_class->verify = gpa_model_verify;
	node_class->get_value = gpa_model_get_value;
	node_class->get_child = gpa_model_get_child;
	node_class->lookup = gpa_model_lookup;
	node_class->modified = gpa_model_modified;
}

static void
gpa_model_init (GPAModel *model)
{
	model->loaded = FALSE;

	model->name = NULL;
	model->vendor = NULL;
	model->options = NULL;
}

static void
gpa_model_finalize (GObject *object)
{
	GPAModel *model;

	model = GPA_MODEL (object);

	if (GPA_NODE_ID (model)) {
		g_assert (modeldict != NULL);
		g_assert (g_hash_table_lookup (modeldict, GPA_NODE_ID (model)) != NULL);
		g_hash_table_remove (modeldict, GPA_NODE_ID (model));
	}

	model->name = gpa_node_detach_unref (GPA_NODE (model), GPA_NODE (model->name));
	model->vendor = gpa_node_detach_unref (GPA_NODE (model), GPA_NODE (model->vendor));
	model->options = (GPAList *) gpa_node_detach_unref (GPA_NODE (model), GPA_NODE (model->options));

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static gboolean
gpa_model_verify (GPANode *node)
{
	GPAModel *model;

	model = GPA_MODEL (node);

	if (!model->name) return FALSE;
	if (!gpa_node_verify (model->name)) return FALSE;
	if (model->loaded) {
		if (!model->vendor) return FALSE;
		if (!gpa_node_verify (model->vendor)) return FALSE;
		if (!model->options) return FALSE;
		if (!gpa_node_verify (GPA_NODE (model->options))) return FALSE;
	}

	return TRUE;
}

static guchar *
gpa_model_get_value (GPANode *node)
{
	GPAModel *model;

	model = GPA_MODEL (node);

	if (node->id) return g_strdup (node->id);

	return NULL;
}

static GPANode *
gpa_model_get_child (GPANode *node, GPANode *ref)
{
	GPAModel *model;
	GPANode *child;

	model = GPA_MODEL (node);

	child = NULL;
	if (ref == NULL) {
		child = model->name;
	} else if (ref == model->name) {
		child = model->vendor;
	} else if (ref == model->vendor) {
		child = GPA_NODE (model->options);
	}

	if (child) gpa_node_ref (child);

	return NULL;
}

static GPANode *
gpa_model_lookup (GPANode *node, const guchar *path)
{
	GPAModel *model;
	GPANode *child;

	model = GPA_MODEL (node);

	child = NULL;

	if (gpa_node_lookup_ref (&child, GPA_NODE (model->name), path, "Name")) return child;
	if (gpa_node_lookup_ref (&child, GPA_NODE (model->vendor), path, "Vendor")) return child;
	if (gpa_node_lookup_ref (&child, GPA_NODE (model->options), path, "Options")) return child;

	return NULL;
}

static void
gpa_model_modified (GPANode *node)
{
	GPAModel *model;

	model = GPA_MODEL (node);

	if (model->name && (GPA_NODE_FLAGS (model->name) & GPA_MODIFIED_FLAG)) {
		gpa_node_emit_modified (model->name);
	}
	if (model->vendor && (GPA_NODE_FLAGS (model->vendor) & GPA_MODIFIED_FLAG)) {
		gpa_node_emit_modified (model->vendor);
	}
	if (model->options && (GPA_NODE_FLAGS (model->options) & GPA_MODIFIED_FLAG)) {
		gpa_node_emit_modified (GPA_NODE (model->options));
	}
}

/* Public Methods */

GPANode *
gpa_model_new_from_info_tree (xmlNodePtr tree)
{
	GPAModel *model;
	xmlChar *xmlid;
	xmlNodePtr xmlc;
	GPANode *name;
	guchar *filename;

	g_return_val_if_fail (tree != NULL, NULL);

	/* Check that tree is <Model> */
	if (strcmp (tree->name, "Model")) {
		g_warning ("file %s: line %d: Base node is <%s>, should be <Model>", __FILE__, __LINE__, tree->name);
		return NULL;
	}
	/* Check that model has Id */
	xmlid = xmlGetProp (tree, "Id");
	if (!xmlid) {
		g_warning ("file %s: line %d: Model node does not have Id", __FILE__, __LINE__);
		return NULL;
	}

	/* Check for model file */
	filename = g_strdup_printf (DATADIR "/gnome-print-2.0/models/%s.model", xmlid);
	if (!g_file_test (filename, G_FILE_TEST_IS_REGULAR)) {
		g_warning ("Model description file is missing %s", xmlid);
		xmlFree (xmlid);
		return NULL;
	}
	g_free (filename);

	if (!modeldict) modeldict = g_hash_table_new (g_str_hash, g_str_equal);
	model = g_hash_table_lookup (modeldict, xmlid);
	if (model != NULL) {
		gpa_node_ref (GPA_NODE (model));
		return GPA_NODE (model);
	}

	model = NULL;
	name = NULL;

	for (xmlc = tree->xmlChildrenNode; xmlc != NULL; xmlc = xmlc->next) {
		if (!strcmp (xmlc->name, "Name")) {
			name = gpa_value_new_from_tree ("Name", xmlc);
		}
	}

	if (name) {
		model = (GPAModel *) gpa_node_new (GPA_TYPE_MODEL, xmlid);
		model->name = name;
		name->parent = GPA_NODE (model);
		g_hash_table_insert (modeldict, GPA_NODE (model)->id, model);
	} else {
		g_warning ("Incomplete model description");
	}

	xmlFree (xmlid);

	return (GPANode *) model;
}

GPANode *
gpa_model_new_from_tree (xmlNodePtr tree)
{
	GPAModel *model;
	xmlChar *xmlid, *xmlver;
	xmlNodePtr xmlc;
	GPANode *name;
	GPANode *vendor;
	GPAList *options;

	g_return_val_if_fail (tree != NULL, NULL);

	/* Check that tree is <Model> */
	if (strcmp (tree->name, "Model")) {
		g_warning ("file %s: line %d: Base node is <%s>, should be <Model>", __FILE__, __LINE__, tree->name);
		return NULL;
	}
	/* Check that model has Id */
	xmlid = xmlGetProp (tree, "Id");
	if (!xmlid) {
		g_warning ("file %s: line %d: Model node does not have Id", __FILE__, __LINE__);
		return NULL;
	}
	/* Check Model definition version */
	xmlver = xmlGetProp (tree, "Version");
	if (!xmlver || strcmp (xmlver, "1.0")) {
		g_warning ("file %s: line %d: Wrong model version %s, should be 1.0", __FILE__, __LINE__, xmlver);
		xmlFree (xmlid);
		if (xmlver) xmlFree (xmlver);
		return NULL;
	}
	xmlFree (xmlver);

	/* Create modeldict, if not already present */
	if (!modeldict) modeldict = g_hash_table_new (g_str_hash, g_str_equal);
	/* Check, whether model is already loaded */
	model = g_hash_table_lookup (modeldict, xmlid);
	if (model != NULL) {
		gpa_node_ref (GPA_NODE (model));
		return GPA_NODE (model);
	}

	model = NULL;
	name = NULL;
	vendor = NULL;
	options = NULL;

	for (xmlc = tree->xmlChildrenNode; xmlc != NULL; xmlc = xmlc->next) {
		if (!strcmp (xmlc->name, "Name")) {
			/* Create <Name> node */
			name = gpa_value_new_from_tree ("Name", xmlc);
		} else if (!strcmp (xmlc->name, "Vendor")) {
			xmlChar *vendorid;
			vendorid = xmlNodeGetContent (xmlc);
			if (vendorid) {
				/* Create #vendor node */
				vendor = gpa_vendor_get_by_id (vendorid);
				xmlFree (vendorid);
			}
		} else if (!strcmp (xmlc->name, "Options")) {
			/* Create <Options> node */
			options = gpa_option_list_new_from_tree (xmlc);
		}
	}

	if (name && vendor && options) {
		/* Everything is OK */
		model = (GPAModel *) gpa_node_new (GPA_TYPE_MODEL, xmlid);
		model->name = name;
		name->parent = GPA_NODE (model);
		g_hash_table_insert (modeldict, GPA_NODE (model)->id, model);
		model->vendor = gpa_reference_new (vendor);
		model->vendor->parent = GPA_NODE (model);
		gpa_node_unref (vendor);
		model->options = options;
		GPA_NODE (model->options)->parent = GPA_NODE (model);
	} else {
		if (!name) {
			g_warning ("file %s: line %d: Model does not have valid name", __FILE__, __LINE__);
		}
		if (!vendor) {
			g_warning ("file %s: line %d: Model does not have valid vendor", __FILE__, __LINE__);
		}
		if (!options) {
			g_warning ("file %s: line %d: Model does not have valid options", __FILE__, __LINE__);
		}
		if (name) gpa_node_unref (name);
		if (vendor) gpa_node_unref (vendor);
		if (options) gpa_node_unref (GPA_NODE (options));
	}

	xmlFree (xmlid);

	return (GPANode *) model;
}

/* GPAModelList */

GPAList *
gpa_model_list_new_from_info_tree (xmlNodePtr tree)
{
	GPAList *models;
	xmlNodePtr xmlc;
	GSList *l;

	g_return_val_if_fail (!strcmp (tree->name, "Models"), NULL);

	l = NULL;
	for (xmlc = tree->xmlChildrenNode; xmlc != NULL; xmlc = xmlc->next) {
		if (!strcmp (xmlc->name, "Model")) {
			GPANode *model;
			model = gpa_model_new_from_info_tree (xmlc);
			if (model) l = g_slist_prepend (l, model);
		}
	}

	models = GPA_LIST (gpa_list_new (GPA_TYPE_MODEL, FALSE));
	gpa_node_construct (GPA_NODE (models), "Models");

	while (l) {
		GPANode *model;
		model = GPA_NODE (l->data);
		l = g_slist_remove (l, model);
		model->parent = GPA_NODE (models);
		model->next = models->children;
		models->children = model;
	}

	return models;
}

GPANode *
gpa_model_get_by_id (const guchar *id)
{
	GPAModel *model;
	gchar *path;
	xmlDocPtr doc;
	xmlNodePtr root;

	g_return_val_if_fail (id != NULL, NULL);
	g_return_val_if_fail (*id != '\0', NULL);

	if (!modeldict) modeldict = g_hash_table_new (g_str_hash, g_str_equal);
	model = g_hash_table_lookup (modeldict, id);
	if (model) {
		gpa_node_ref (GPA_NODE (model));
		return GPA_NODE (model);
	}

	path = g_strdup_printf (DATADIR "/gnome-print-2.0/models/%s.model", id);
	if (!g_file_test (path, G_FILE_TEST_IS_REGULAR)) {
		g_free (path);
		return NULL;
	}

	doc = xmlParseFile (path);
	g_free (path);
	if (!doc) {
		return NULL;
	}

	model = NULL;
	root = doc->xmlRootNode;
	if (!strcmp (root->name, "Model")) {
		model = GPA_MODEL (gpa_model_new_from_tree (root));
	}
	xmlFreeDoc (doc);

	return GPA_NODE (model);
}

gboolean
gpa_model_load (GPAModel *model)
{
	gchar *path;
	xmlDocPtr doc;
	xmlNodePtr root;
	xmlChar *xmlid;
	xmlNodePtr xmlc;
	GPANode *vendor;
	GPAList *options;

	g_return_val_if_fail (model != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_MODEL (model), FALSE);
	g_return_val_if_fail (!model->loaded, FALSE);

	path = g_strdup_printf (DATADIR "/gnome-print-2.0/models/%s.model", GPA_NODE (model)->id);
	if (!g_file_test (path, G_FILE_TEST_IS_REGULAR)) {
		g_warning ("Model description file missing %s", GPA_NODE (model)->id);
		g_free (path);
		return FALSE;
	}

	doc = xmlParseFile (path);
	g_free (path);
	if (!doc) {
		g_warning ("Invalid model description file %s", GPA_NODE (model)->id);
		return FALSE;
	}

	root = doc->xmlRootNode;
	if (strcmp (root->name, "Model")) {
		g_warning ("Invalid model description file %s", GPA_NODE (model)->id);
		return FALSE;
	}

	xmlid = xmlGetProp (root, "Id");
	if (!xmlid || strcmp (xmlid, GPA_NODE (model)->id)) {
		g_warning ("Missing \"Id\" node in model description %s", GPA_NODE (model)->id);
		return FALSE;
	}

	vendor = NULL;
	options = NULL;

	for (xmlc = root->xmlChildrenNode; xmlc != NULL; xmlc = xmlc->next) {
		/* fixme: vendor should be initialized from info tree */
		if (!strcmp (xmlc->name, "Vendor")) {
			xmlChar *vendorid;
			vendorid = xmlNodeGetContent (xmlc);
			if (vendorid) {
				vendor = gpa_vendor_get_by_id (vendorid);
				xmlFree (vendorid);
			}
		} else if (!strcmp (xmlc->name, "Options")) {
			options = gpa_option_list_new_from_tree (xmlc);
		}
	}

	if (vendor && options) {
		/* Fixme: currently we are assuming, that you cannot delete vendor before deleting all its models */
		/* Fixme: Probably should implement weak reference object */
		model->vendor = gpa_reference_new (vendor);
		model->vendor->parent = GPA_NODE (model);
		gpa_node_unref (vendor);
		model->options = options;
		GPA_NODE (model->options)->parent = GPA_NODE (model);
	} else {
		g_warning ("Incomplete model description");
		if (vendor) gpa_node_unref (vendor);
		if (options) gpa_node_unref (GPA_NODE (options));
		return FALSE;
	}

	xmlFree (xmlid);
	xmlFreeDoc (doc);

	model->loaded = TRUE;

	return TRUE;
}

#if 0
gboolean
gpa_model_verify_with_settings (const GPAModel *model, GpaSettings *settings)
{
	if (!gpa_node_verify (GPA_NODE (model))) return FALSE;

	if (!gpa_options_list_verify_with_settings (model, settings)) return FALSE;

	return TRUE;
}


/**
 * gpa_model_add_missing_setting_from_options:
 * @options: options to add to the list if the list does not contains it
 * @list_: 
 * 
 * If the list_ does not contains a path for the options object, it is added
 * to the list. see 
 * 
 * Return Value: TRUE on success, FALSE otherwise
 **/
static gboolean
gpa_model_add_missing_settings_from_options (GpaOptions *options, GList **list_)
{
	const gchar *test_path;
	GpaOption *option;
	GList *list;
	gchar *options_path;
	gchar *option_path;
	gint size;
	
	g_return_val_if_fail (GPA_IS_OPTIONS (options), FALSE);

	/* Scan the list of options to check the suboptions */
	list = options->children;
	for (; list != NULL; list = list->next) {
		option = list->data;
		if (option->children != NULL)
			gpa_model_add_missing_settings_from_options (option->children, list_);
	}
			
	
	/* Scan the list of options to see if this option is already included */
	options_path = gpa_options_dup_path (options);
	list = *list_;
	for (; list != NULL; list = list->next) {
		test_path = list->data;
		size = tu_get_pos_of_last_delimiter (test_path, GPA_PATH_DELIMITER_CHAR);
		g_return_val_if_fail (size > 0, FALSE);
		if (strncmp (options_path, test_path, size) == 0) {
			g_free (options_path);
			return TRUE;
		}
	}
	g_free (options_path);

	/* If it was not found, add it to the list */
	list = *list_;
	g_return_val_if_fail (options->children != NULL, FALSE);
	option = options->children->data;
	option_path = gpa_option_dup_path (option);
	list = g_list_prepend (list, option_path);
	*list_ = list;
	
	return TRUE;
}


/**
 * gpa_model_add_mising_default_settings_and_values:
 * @model: 
 * 
 * The xml file describing the model contains a list of Default settings.
 * This funciton adds to model the default settings the where not specified
 * by the xml file.
 * 
 * Return Value: 
 **/
gboolean
gpa_model_add_missing_default_settings_and_values (GPAModel *model)
{
	GPAItem *item;
	GpaValue *value;
	GList *settings_list;
	GList *list;
	gint number;
	gint n;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_MODEL (model), FALSE);
	g_return_val_if_fail (model->options_list != NULL, FALSE);

	settings_list = model->default_settings;

	for (item = model->options_list->items; item != NULL; item = item->next) {
		GpaOptions *options;
		options = GPA_OPTIONS (item);
		if (!gpa_model_add_missing_settings_from_options (options, &settings_list))
			return FALSE;
	}
	model->default_settings = settings_list;

	/* Now add the required values if they are not found */
	number = sizeof (gpa_required_settings_values) / sizeof (GpaRequiredSettingsValues);
	for (n = 0; n < number; n++) {
		const gchar *key;
		key = gpa_required_settings_values [n].key;
		list = model->default_values;
		for (; list != NULL; list = list->next) {
			value = (GpaValue *)list->data;
			if (strcmp (value->key, key) == 0)
				break;
		}
		if (list == NULL) {
			const gchar *default_value = gpa_required_settings_values[n].default_value;
			value = gpa_value_new (key, default_value);
			model->default_values = g_list_prepend (model->default_values, value);
		}
	}
	

	return TRUE;
}

static GPAModel *
gpa_model_load (GPAModel *model)
{
	XmlParseContext *context;
	gchar *full_path;
	xmlNodePtr tree;
	xmlNodePtr child;
	xmlNodePtr child_child;
	GPAList *list;

	g_return_val_if_fail (model != NULL, NULL);
	g_return_val_if_fail (GPA_IS_MODEL (model), NULL);
	g_return_val_if_fail (GPA_ITEM (model)->id != NULL, NULL);
	g_return_val_if_fail (model->name != NULL, NULL);
	g_return_val_if_fail (GPA_MODEL_VENDOR (model) != NULL, NULL);
	g_return_val_if_fail (!model->loaded, NULL);

	full_path = g_strdup_printf (GPA_DATA_DIR "%s/%s%s", "models", GPA_ITEM (model)->id, GPA_MODEL_EXTENSION_NAME);
	context = gpa_xml_parse_context_new_from_path (full_path, GPA_TAG_MODEL_NAME_SPACE, GPA_TAG_PRINTER_MODEL);
	g_free (full_path);
	g_return_val_if_fail (context != NULL, NULL);

	tree = context->doc->root;

	if (!gpa_xml_node_verify (tree, GPA_TAG_PRINTER_MODEL)) return NULL;

	child = gpa_xml_search_child_required (tree, GPA_TAG_MODEL_INFO);
	if (child == NULL) return NULL;

	/* Load the backedns */
	model->backend_list   = gpa_backend_list_new_from_node (tree);
	if (model->backend_list == NULL) return NULL;

	/* Load options list */
	list  = gpa_options_list_new_from_node (tree);
	g_return_val_if_fail (list != NULL, NULL);
	gpa_node_add_child (GPA_NODE (model), GPA_NODE (list), NULL);
	model->default_settings = gpa_options_list_parse_default (list, NULL);

	/* Load the values */
	child = gpa_xml_search_child_required (tree, GPA_TAG_MODEL_INFO);
	if (child == NULL) return NULL;

	model->model_info = gpa_xml_utils_new_hash_from_node (child, GPA_TAG_MODEL_INFO);

	/* Load the default settings */
	child = gpa_xml_search_child_required (tree, GPA_TAG_DEFAULT_SETTINGS);
	if (child == NULL) return NULL;

	if (!gpa_settigns_load_default_paths_from_node (child, model)) return NULL;
	
	/* Load the SettingsInfo */
	child_child = gpa_xml_search_child_required (child, GPA_TAG_SETTINGS_INFO);
	if (child_child == NULL) return NULL;

	model->default_values = gpa_values_new_from_node (child_child, GPA_TAG_SETTINGS_INFO);

	/* We have loaded the deafult settings from the xml file, now we need
	   to add the paths of the settings that where not specified in the xml file.
	   And add a default value for the numerical options */
	if (!gpa_model_add_missing_default_settings_and_values (model)) return NULL;
	
	if (model->model_info == NULL) return NULL;
	if (model->default_values == NULL) return NULL;

	/* Load the constraints */
	child = gpa_xml_search_child_required (tree, GPA_TAG_CONSTRAINTS);
	if (child == NULL) return NULL;

	if (!gpa_constraints_load_from_node (child, model)) return NULL;

	/* Load the code fragments */
	child = gpa_xml_search_child (tree, GPA_TAG_CODE_FRAGMENTS);
	if (child != NULL) {
		model->code_fragments = gpa_code_fragments_new_from_node (child);
		if (model->code_fragments == NULL) return NULL;
	}

	if (!gpa_constraints_double_links_model (model)) return NULL;

	gpa_xml_parse_context_free (context);

	if (!gpa_node_verify (GPA_NODE (model))) {
		gpa_error ("The model was not loaded from \"%s\" because it contained "
			   "at least one error", full_path);
		return NULL;
	}

	return model;
}

static GPAModel *
gpa_model_new_from_xml_tree (XmlParseContext *context, xmlNodePtr tree)
{
	GPAModel *model;
	/* fixme: */
	GPAVendor *vendor;
	xmlNodePtr child;
	gchar *vendor_id;
	gchar *name;
	gchar *id;
	
	g_return_val_if_fail (context != NULL, NULL);
	g_return_val_if_fail (tree != NULL, NULL);

	debug (FALSE, "");

	if (!gpa_xml_node_verify (tree, GPA_TAG_PRINTER_MODEL)) return NULL;

	child = gpa_xml_search_child_required (tree, GPA_TAG_MODEL_INFO);
	if (child == NULL) return NULL;
	name = gpa_xml_get_value_string_required (child, GPA_TAG_MODEL_NAME, NULL);
	if (name == NULL) return NULL;

	id = xmlGetProp (child, "Id");
	if (id == NULL) return NULL;

	vendor_id = gpa_xml_get_value_string_required (child, "VendorId", NULL);
	if (vendor_id == NULL) return NULL;

	vendor = gpa_vendor_list_get_vendor_by_id (GPA->vendors, vendor_id);
	g_free (vendor_id);
	if (vendor == NULL) return NULL;

	model = gpa_model_new (id, name);
	g_free (name);
	xmlFree (id);
	gpa_node_unref (GPA_NODE (vendor));
	g_return_val_if_fail (model != NULL, NULL);
	
	/* fixme: */
	gpa_node_append_child (GPA_NODE (vendor), GPA_NODE (model));
	model = gpa_model_load (model);
	return model;
}

static xmlNodePtr
gpa_model_write (XmlParseContext *context, GPAModel *model)
{
	xmlNodePtr node;
	xmlNodePtr child;
	xmlNodePtr child_child;
	xmlNsPtr gmr;
	
	g_return_val_if_fail (context != NULL,                NULL);
	g_return_val_if_fail (GPA_IS_MODEL (model), NULL);
	g_return_val_if_fail (model->model_info != NULL,      NULL);
	g_return_val_if_fail (model->default_values != NULL,  NULL);

	if (!gpa_node_verify (GPA_NODE (model))) {
		gpa_error ("Could not save model, because it could not be verified");
		return NULL;
	}

	node = xmlNewDocNode (context->doc, context->ns, GPA_TAG_PRINTER_MODEL, NULL);
	if (node == NULL)
		return NULL;

	gmr = xmlNewNs (node, GPA_TAG_MODEL_NAME_SPACE, "gmr");
	xmlSetNs (node, gmr);

	context->ns = gmr;

	child = xmlNewDocNode (context->doc, context->ns, GPA_TAG_DEFAULT_SETTINGS, NULL);
	if (child != NULL)
		xmlAddChild (node, child);

	child_child = gpa_options_write_paths_from_model (context, model);
	if (child != NULL)
		xmlAddChild (child, child_child);

	child_child = xmlNewDocNode (context->doc, context->ns, GPA_TAG_SETTINGS_INFO, NULL);
	if (child != NULL)
		xmlAddChild (child, child_child);
	
	child = gpa_hash_write (context, model->model_info, GPA_TAG_MODEL_INFO);
	if (child != NULL)
		xmlAddChild (node, child);

	child = gpa_backend_list_write (context, model->backend_list);
	if (child != NULL)
		xmlAddChild (node, child);

	child = xmlNewDocNode (context->doc, context->ns, GPA_TAG_CONSTRAINTS, NULL);
	if (child != NULL)
		xmlAddChild (node, child);
	
	child = gpa_options_list_write (context, model->options_list);
	if (child != NULL)
		xmlAddChild (node, child);

	child = gpa_code_fragments_write (context, model->code_fragments);
	if (child != NULL)
		xmlAddChild (node, child);

	return node;
}


gboolean
gpa_model_save (GPAModel *model)
{
	XmlParseContext *context;
	xmlDocPtr xml;
	gchar *full_path;
	gint ret;

	debug (FALSE, "");
	
	g_return_val_if_fail (model != NULL, FALSE);

	full_path = g_strdup_printf (GPA_DATA_DIR "%s/%s%s",
				     "models", GPA_ITEM (model)->id, GPA_MODEL_EXTENSION_NAME);

	g_print ("Saving in %s\n", full_path);
	
	xml = xmlNewDoc ("1.0");
	if (xml == NULL) {
		gpa_error ("Could not create xml doc");
		return FALSE;
	}

	context = gpa_xml_parse_context_new (xml, NULL);
	xml->root = gpa_model_write (context, model);
	gpa_xml_parse_context_destroy (context);
	if (xml->root == NULL)
		return FALSE;

	ret = xmlSaveFile (full_path, xml);
	xmlFreeDoc (xml);

	if (ret < 0) {
		gpa_error ("Could not save File");
		return FALSE;
	}

	return TRUE;
}

GpaBackend *
gpa_model_get_default_backend (GPAModel *model)
{
	GpaBackend *backend = NULL;
	GpaBackend *def = NULL;
	GList *list;

	g_return_val_if_fail (model != NULL, NULL);
	g_return_val_if_fail (GPA_IS_MODEL (model), NULL);

	/* fixme: */
	if (!model->backend_list) gpa_model_load (model);
	g_return_val_if_fail (model->backend_list != NULL, NULL);

	list = model->backend_list;

	for (; list != NULL; list = list->next) {
		backend = (GpaBackend *) list->data;
		if ((def == NULL) ||
		    (backend->def))
			def = backend;
	}

	return def;
}

/* Access to the struct */
GpaOptions *
gpa_model_get_options_from_id (const GPAModel *model, const gchar *id)
{
	GPANode *options;

	g_return_val_if_fail (model != NULL, NULL);
	g_return_val_if_fail (GPA_IS_MODEL (model), NULL);
	g_return_val_if_fail (id != NULL, NULL);
	g_return_val_if_fail (*id != '\0', NULL);
	
	options = NULL;

	if (model->options_list) {
		options = gpa_list_get_item_by_id (model->options_list, id);
	}

	return (GpaOptions *) options;
}
#endif

