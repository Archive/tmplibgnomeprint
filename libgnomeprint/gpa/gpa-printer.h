#ifndef __GPA_PRINTER_H__
#define __GPA_PRINTER_H__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <glib.h>

G_BEGIN_DECLS

#define GPA_TYPE_PRINTER (gpa_printer_get_type ())
#define GPA_PRINTER(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GPA_TYPE_PRINTER, GPAPrinter))
#define GPA_PRINTER_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GPA_TYPE_PRINTER, GPAPrinterClass))
#define GPA_IS_PRINTER(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GPA_TYPE_PRINTER))
#define GPA_IS_PRINTER_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GPA_TYPE_PRINTER))
#define GPA_PRINTER_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GPA_TYPE_PRINTER, GPAPrinterClass))

typedef struct _GPAPrinter GPAPrinter;
typedef struct _GPAPrinterClass GPAPrinterClass;

#include <tree.h>
#include "gpa-list.h"
#include "gpa-model.h"

/* GPAPrinter */

struct _GPAPrinter {
	GPANode node;

	GPANode *name;
	GPANode *model;
	GPAList *settings;
};

struct _GPAPrinterClass {
	GPANodeClass node_class;
};

GType gpa_printer_get_type (void);

GPANode *gpa_printer_new_from_tree (xmlNodePtr tree);

GPANode *gpa_printer_get_default (void);
GPANode *gpa_printer_get_by_id (const guchar *id);

GPANode *gpa_printer_get_default_settings (GPAPrinter *printer);

/* Manipulation */

GPANode *gpa_printer_new_from_model (GPAModel *model, const guchar *name);

gboolean gpa_printer_save (GPAPrinter *printer);

/* GPAPrinterList */

GPAList *gpa_printer_list_load (void);

#if 0
GPANode * gpa_printer_new_from_model (GPANode *model, const gchar *name);

typedef enum {
	GNOME_PRINT_PRINTER_ACTIVE,
	GNOME_PRINT_PRINTER_INACTIVE,
	GNOME_PRINT_PRINTER_OFFLINE,
	GNOME_PRINT_PRINTER_NET_FAILURE,
} GpaStatus;

/* GPAPrinterList */

GPANode *gpa_printer_list_new (void);

#if 0
GPAPrinter *gpa_printer_list_lookup (GPAList *list, const gchar *id);
#endif

#define GPA_PRINTER_ID(p) (GPA_ITEM (p)->id)

GPAPrinter * gpa_printer_new (const guchar *id);
GPAPrinter * gpa_printer_new_from_file (const gchar *filename);

/* Basic "Printer" object operations */
gboolean gpa_printer_verify (GPAPrinter *printer, gboolean fail);
GPAPrinter * gpa_printer_copy (GPAPrinter *copy_from);

/* Status */
GpaStatus gpa_printer_get_status (GPAPrinter *printer);
const char * gpa_printer_str_status (GpaStatus status);       

/* Settings List */
GPAList * gpa_printer_settings_list_get (const GPAPrinter *printer);
void gpa_printer_settings_list_swap (GPAPrinter *printer_1, GPAPrinter *printer_2);
gint gpa_printer_settings_list_get_size (const GPAPrinter *printer);

/* Settings */
GpaSettings * gpa_printer_settings_get_first (GPAPrinter *printer);
GpaSettings * gpa_printer_settings_get_selected (const GPAPrinter *printer);
void gpa_printer_settings_select (const GPAPrinter *printer, GpaSettings *settings);
void gpa_printer_settings_append (GPAPrinter *printer, GpaSettings *settings);
void gpa_printer_settings_remove (GPAPrinter *printer, GpaSettings *settings);

/* Default Printer */
GPAPrinter * gpa_printer_get_default (GPAList *printer_list);
void gpa_printer_set_default (GPAList *printer_list, GPAPrinter *printer);
gboolean gpa_printer_is_default (const GPAPrinter *printer);

/* Access to the struct */
const gchar * gpa_printer_get_name (const GPAPrinter *printer);
const gchar * gpa_printer_get_id (const GPAPrinter *printer);
gchar * gpa_printer_dup_name (GPAPrinter *printer);
gchar * gpa_printer_dup_id (const GPAPrinter *printer);

GPAModel * gpa_printer_get_model (GPAPrinter *printer);
GList * gpa_printer_get_backend_list (const GPAPrinter *printer);

/* Backend info, the drivers use it to query printers*/
const gchar * gpa_printer_backend_info_get (const GPAPrinter *printer, const gchar *backend, const gchar *id);
#endif

G_END_DECLS

#endif /* __GPA_PRINTER_H__ */
