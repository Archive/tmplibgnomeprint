#ifndef _GPA_CONFIG_H_
#define _GPA_CONFIG_H_

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <glib.h>

G_BEGIN_DECLS

#define GPA_TYPE_CONFIG (gpa_config_get_type ())
#define GPA_CONFIG(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GPA_TYPE_CONFIG, GPAConfig))
#define GPA_CONFIG_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GPA_TYPE_CONFIG, GPAConfigClass))
#define GPA_IS_CONFIG(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GPA_TYPE_CONFIG))
#define GPA_IS_CONFIG_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GPA_TYPE_CONFIG))
#define GPA_CONFIG_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GPA_TYPE_CONFIG, GPAConfigClass))

typedef struct _GPAConfig GPAConfig;
typedef struct _GPAConfigClass GPAConfigClass;

#include "gpa-list.h"

struct _GPAConfig {
	GPANode node;
	GPANode *globals;
	GPANode *printer;
	GPANode *settings;
};

struct _GPAConfigClass {
	GPANodeClass node_class;
};

GType gpa_config_get_type (void);

GPANode *gpa_config_new (void);

G_END_DECLS

#endif
