#ifndef __GPA_KEY_H__
#define __GPA_KEY_H__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <glib.h>

G_BEGIN_DECLS

#define GPA_TYPE_KEY (gpa_key_get_type ())
#define GPA_KEY(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GPA_TYPE_KEY, GPAKey))
#define GPA_KEY_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GPA_TYPE_KEY, GPAKeyClass))
#define GPA_IS_KEY(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GPA_TYPE_KEY))
#define GPA_IS_KEY_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GPA_TYPE_KEY))
#define GPA_KEY_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GPA_TYPE_KEY, GPAKeyClass))

typedef struct _GPAKey GPAKey;
typedef struct _GPAKeyClass GPAKeyClass;

#include <tree.h>
#include "gpa-list.h"

/* GPAKey */

struct _GPAKey {
	GPANode node;

	GPANode *children;
	GPANode *option;
	guchar *value;
};


struct _GPAKeyClass {
	GPANodeClass node_class;
};

GType gpa_key_get_type (void);

GPANode *gpa_key_new_from_option (GPANode *node);

xmlNodePtr gpa_key_write (xmlDocPtr doc, GPANode *key);

gboolean gpa_key_merge_from_tree (GPANode *key, xmlNodePtr tree);

gboolean gpa_key_merge_from_key (GPAKey *dst, GPAKey *src);

gboolean gpa_key_copy (GPANode *dst, GPANode *src);

#define GPA_KEY_ID(k) (GPA_KEY (k)->id)
#define GPA_KEY_HAS_OPTION(k) ((k) && GPA_KEY (k)->option)
#define GPA_KEY_OPTION(k) ((k) && GPA_KEY (k)->option ? GPA_OPTION (GPA_KEY (k)->option) : NULL)

#if 0
gboolean gpa_settigns_load_default_paths_from_node (xmlNodePtr tree_, GPAModel *model);

GPAList * gpa_key_list_load_from_node (xmlNodePtr tree, GPAPrinter *printer);

xmlNodePtr gpa_key_list_write (XmlParseContext *context, GPAList *key_list);

/* From public header */

typedef enum {
	GNOME_PRINT_ORIENT_PORTRAIT,
	GNOME_PRINT_ORIENT_LANDSCAPE,
	GNOME_PRINT_ORIENT_REVERSE_PORTRAIT,
	GNOME_PRINT_ORIENT_REVERSE_LANDSCAPE
} GnomePrintOrient;

/* Basic "GpaKey" Object operations either by an individual
 * object, or by a list of them */
GpaKey * gpa_key_new_from_model (GPAModel *model, GPAPrinter *printer);

/* fixme: Remove these from source eventually (Lauris) */
#define gpa_key_copy(s) ((GpaKey *) gpa_node_duplicate (GPA_NODE (s)))
#define gpa_key_list_copy(s) ((GPAList *) gpa_node_duplicate (GPA_NODE (s)))

gboolean gpa_key_verify (GpaKey *key, gboolean fail);
gboolean gpa_key_list_verify (GPAList *key_list, gboolean fail);

/* Selected Key. Get, select and query  */
GpaKey * gpa_key_get_selected (GPAList *key_list);
gboolean gpa_key_select (GpaKey *key_in, GPAList *key_list);
gboolean gpa_key_is_selected (const GpaKey *key);

/* Key name convenience functions. Used for Copy & Rename Key */
gboolean gpa_key_name_taken (GPAList *key_list, const gchar *name);
void gpa_key_name_replace (GpaKey *key, const gchar *name);
gchar *gpa_key_get_free_name (GpaKey *key, GPAList *key_list);

/* Values stuff. This are the actual key that we modify */
gchar *gpa_key_value_dup (GpaKey *key, const gchar *key);
gchar *gpa_key_value_dup_required (GpaKey *key, const gchar *key);
gboolean gpa_key_value_insert (GpaKey *key, const gchar *key, const gchar *value);
gboolean gpa_key_value_replace (GpaKey *key, const gchar *key, const gchar *new_value);
gboolean gpa_key_value_get_from_option_int (GpaKey *key, GpaOption *option, gint *value);
gboolean gpa_key_value_get_from_option_double (GpaKey *key, GpaOption *option, gdouble *value);

/* Access to the struct */
gchar * gpa_key_dup_name (GpaKey *key);
const gchar * gpa_key_get_name (GpaKey *key);

gchar * gpa_key_dup_command (GpaKey *key);
const gchar * gpa_key_get_command (GpaKey *key);

const GPAModel* gpa_key_get_model (GpaKey *key);

/* Selected options list */
GList *  gpa_key_get_selected_options (GpaKey *key);
void     gpa_key_set_selected_options (GpaKey *key, GList *list);
gboolean gpa_key_select_option (GpaKey *key, GpaOption *option);

/* This is used to set the command to print to (i.e. "lpr")
 * should go away in the future when we add better spooler
 * manupitaion support */
void gpa_key_replace_command (GpaKey *key, const gchar *command);

/* Query code for the printer drivers */
const gchar * gpa_key_query_options (GpaKey *key, const gchar *options_id);
gboolean gpa_key_query_options_boolean (GpaKey *key, const gchar *options_id);
#endif

G_END_DECLS

#endif /* __GPA_KEY_H__ */
