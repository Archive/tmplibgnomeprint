#define __GPA_PRINTER_C__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <string.h>
#include <dirent.h> /* For the DIR structure stuff */
#include <xmlmemory.h>
#include <parser.h>

#include "gpa-utils.h"
#include "gpa-value.h"
#include "gpa-reference.h"
#include "gpa-settings.h"
#include "gpa-model.h"
#include "gpa-printer.h"

/* GPAPrinter */

static void gpa_printer_class_init (GPAPrinterClass *klass);
static void gpa_printer_init (GPAPrinter *printer);

static void gpa_printer_finalize (GObject *object);

static gboolean gpa_printer_verify (GPANode *node);
static guchar *gpa_printer_get_value (GPANode *node);
static GPANode *gpa_printer_get_child (GPANode *node, GPANode *ref);
static GPANode *gpa_printer_lookup (GPANode *node, const guchar *path);
static void gpa_printer_modified (GPANode *node);

static GPANode *gpa_printer_new_from_file (const gchar *filename);

static GHashTable *namedict = NULL;

static GPANodeClass *parent_class = NULL;

GType
gpa_printer_get_type (void) {
	static GType type = 0;
	if (!type) {
		static const GTypeInfo info = {
			sizeof (GPAPrinterClass),
			NULL, NULL,
			(GClassInitFunc) gpa_printer_class_init,
			NULL, NULL,
			sizeof (GPAPrinter),
			0,
			(GInstanceInitFunc) gpa_printer_init
		};
		type = g_type_register_static (GPA_TYPE_NODE, "GPAPrinter", &info, 0);
	}
	return type;
}

static void
gpa_printer_class_init (GPAPrinterClass *klass)
{
	GObjectClass *object_class;
	GPANodeClass *node_class;

	object_class = (GObjectClass *) klass;
	node_class = (GPANodeClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gpa_printer_finalize;

	node_class->verify = gpa_printer_verify;
	node_class->get_value = gpa_printer_get_value;
	node_class->get_child = gpa_printer_get_child;
	node_class->lookup = gpa_printer_lookup;
	node_class->modified = gpa_printer_modified;
}

static void
gpa_printer_init (GPAPrinter *printer)
{
	printer->name = NULL;
	printer->settings = NULL;
	printer->model = NULL;
}

static void
gpa_printer_finalize (GObject *object)
{
	GPAPrinter *printer;

	printer = GPA_PRINTER (object);

	if (printer->name) {
		g_assert (namedict != NULL);
		g_assert (g_hash_table_lookup (namedict, GPA_VALUE (printer->name)->value));
		g_hash_table_remove (namedict, GPA_VALUE (printer->name)->value);
	}

	printer->name = gpa_node_detach_unref (GPA_NODE (printer), GPA_NODE (printer->name));
	printer->settings = (GPAList *) gpa_node_detach_unref (GPA_NODE (printer), GPA_NODE (printer->settings));
	printer->model = gpa_node_detach_unref (GPA_NODE (printer), GPA_NODE (printer->settings));

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static gboolean
gpa_printer_verify (GPANode *node)
{
	GPAPrinter *printer;

	printer = GPA_PRINTER (node);

	if (!node->id) return FALSE;

	if (!printer->name) return FALSE;
	if (!gpa_node_verify (printer->name)) return FALSE;
	if (!printer->settings) return FALSE;
	if (!gpa_node_verify (GPA_NODE (printer->settings))) return FALSE;
	if (!printer->model) return FALSE;
	if (!gpa_node_verify (printer->model)) return FALSE;

	return TRUE;
}

static guchar *
gpa_printer_get_value (GPANode *node)
{
	GPAPrinter *printer;

	printer = GPA_PRINTER (node);

	if (node->id) return g_strdup (node->id);

	return NULL;
}

static GPANode *
gpa_printer_get_child (GPANode *node, GPANode *ref)
{
	GPAPrinter *printer;
	GPANode *child;

	printer = GPA_PRINTER (node);

	g_return_val_if_fail (printer->settings != NULL, NULL);
	g_return_val_if_fail (printer->model != NULL, NULL);
	/* Model is reference */

	child = NULL;
	if (ref == NULL) {
		child = printer->name;
	} else if (ref == printer->name) {
		child = GPA_NODE (printer->settings);
	} else if (ref == GPA_NODE (printer->settings)) {
		child = printer->model;
	}

	if (child) gpa_node_ref (child);

	return child;
}

static GPANode *
gpa_printer_lookup (GPANode *node, const guchar *path)
{
	GPAPrinter *printer;
	GPANode *child;

	printer = GPA_PRINTER (node);

	child = NULL;

	if (gpa_node_lookup_ref (&child, GPA_NODE (printer->name), path, "Name")) return child;
	if (gpa_node_lookup_ref (&child, GPA_NODE (printer->settings), path, "Settings")) return child;
	if (gpa_node_lookup_ref (&child, GPA_NODE (printer->model), path, "Model")) return child;

	return NULL;
}

static void
gpa_printer_modified (GPANode *node)
{
	GPAPrinter *printer;

	printer = GPA_PRINTER (node);

	if (printer->name && (GPA_NODE_FLAGS (printer->name) & GPA_MODIFIED_FLAG)) {
		gpa_node_emit_modified (printer->name);
	}
	if (printer->model && (GPA_NODE_FLAGS (printer->model) & GPA_MODIFIED_FLAG)) {
		gpa_node_emit_modified (printer->model);
	}
	if (printer->settings && (GPA_NODE_FLAGS (printer->settings) & GPA_MODIFIED_FLAG)) {
		gpa_node_emit_modified (GPA_NODE (printer->settings));
	}
}

/* Public methods */

GPANode *
gpa_printer_new_from_tree (xmlNodePtr tree)
{
	static GHashTable *iddict = NULL;
	GPAPrinter *printer;
	xmlChar *xmlid, *xmlver;
	xmlNodePtr xmlc;
	GPANode *name;
	GPANode *model;
	GSList *l;

	g_return_val_if_fail (tree != NULL, NULL);

	/* Check that tree is <Printer> */
	if (strcmp (tree->name, "Printer")) {
		g_warning ("file %s: line %d: Base node is <%s>, should be <Printer>", __FILE__, __LINE__, tree->name);
		return NULL;
	}
	/* Check that printer has Id */
	xmlid = xmlGetProp (tree, "Id");
	if (!xmlid) {
		g_warning ("file %s: line %d: Printer node does not have Id", __FILE__, __LINE__);
		return NULL;
	}
	/* fixme: This crashes, if printer will be destroyed */
	if (!iddict) iddict = g_hash_table_new (g_str_hash, g_str_equal);
	if (g_hash_table_lookup (iddict, xmlid)) {
		xmlFree (xmlid);
		return NULL;
	}
	/* Check Printer definition version */
	xmlver = xmlGetProp (tree, "Version");
	if (!xmlver || strcmp (xmlver, "1.0")) {
		g_warning ("file %s: line %d: Wrong printer version %s, should be 1.0", __FILE__, __LINE__, xmlver);
		xmlFree (xmlid);
		if (xmlver) xmlFree (xmlver);
		return NULL;
	}
	xmlFree (xmlver);

	if (!namedict) namedict = g_hash_table_new (g_str_hash, g_str_equal);

	printer = NULL;
	name = NULL;
	model = NULL;
	l = NULL;

	for (xmlc = tree->xmlChildrenNode; xmlc != NULL; xmlc = xmlc->next) {
		g_print ("-- %s\n", xmlc->name);
		if (!strcmp (xmlc->name, "Name")) {
			xmlChar *content;
			content = xmlNodeGetContent (xmlc);
			if (content && *content) {
				if (!g_hash_table_lookup (namedict, content)) {
					name = gpa_value_new ("Name", content);
				}
				xmlFree (content);
			}
		} else if (!strcmp (xmlc->name, "Settings")) {
			if (model) {
				GPANode *settings;
				settings = gpa_settings_new_from_model_and_tree (model, xmlc);
				if (settings) l = g_slist_prepend (l, settings);
			} else {
				g_warning ("Settings without model in printer definition");
			}
		} else if (!strcmp (xmlc->name, "Model")) {
			xmlChar *content;
			content = xmlNodeGetContent (xmlc);
			if (content && *content) {
				model = gpa_model_get_by_id (content);
				xmlFree (content);
			}
		}
	}

	if (name && model && l) {
		printer = (GPAPrinter *) gpa_node_new (GPA_TYPE_PRINTER, xmlid);
		/* Name */
		printer->name = name;
		name->parent = GPA_NODE (printer);
		g_assert (!g_hash_table_lookup (namedict, GPA_VALUE (name)->value));
		g_hash_table_insert (namedict, GPA_VALUE (name)->value, printer);
		/* Settings */
		/* fixme: here are defaults again... */
		printer->settings = GPA_LIST (gpa_list_new (GPA_TYPE_SETTINGS, TRUE));
		GPA_NODE (printer->settings)->parent = GPA_NODE (printer);
		while (l) {
			GPANode *settings;
			settings = GPA_NODE (l->data);
			l = g_slist_remove (l, settings);
			settings->parent = GPA_NODE (printer->settings);
			settings->next = printer->settings->children;
			printer->settings->children = settings;
		}
		/* fixme: */
		if (printer->settings->children) gpa_list_set_default (printer->settings, printer->settings->children);
		/* Model */
		printer->model = gpa_reference_new (model);
		printer->model->parent = GPA_NODE (printer);
		gpa_node_unref (GPA_NODE (model));
	} else {
		if (name) gpa_node_unref (name);
		if (model) gpa_node_unref (model);
		while (l) {
			gpa_node_unref (GPA_NODE (l->data));
			l = g_slist_remove (l, l->data);
		}
	}

	xmlFree (xmlid);

	if (printer) g_hash_table_insert (iddict, GPA_NODE_ID (printer), printer);

	return (GPANode *) printer;
}

static GPANode *
gpa_printer_new_from_file (const gchar *filename)
{
	GPANode *printer;
	xmlDocPtr doc;
	xmlNodePtr root;

	doc = xmlParseFile (filename);
	if (!doc) return NULL;
	root = doc->xmlRootNode;
	printer = NULL;
	if (!strcmp (root->name, "Printer")) {
		printer = gpa_printer_new_from_tree (root);
	}
	xmlFreeDoc (doc);
	return printer;
}

/* GPAPrinterList */

static void gpa_printer_list_load_from_dir (GPAList *printers, const gchar *dirname);

static GPAList *printers = NULL;

GPAList *
gpa_printer_list_load (void)
{
	if (!printers) {
		gchar *dirname;

		printers = GPA_LIST (gpa_node_new (GPA_TYPE_LIST, "Printers"));
		gpa_list_construct (GPA_LIST (printers), GPA_TYPE_PRINTER, TRUE);

		dirname = g_strdup_printf ("%s/%s", g_get_home_dir (), ".gnome/printers");
		gpa_printer_list_load_from_dir (printers, dirname);
		g_free (dirname);
		/* fixme: */
		gpa_printer_list_load_from_dir (printers, DATADIR "/gnome-print-2.0/printers");
	}

	/* fixme: During parsing, please */
	if (printers->children) {
		gpa_node_set_path_value (GPA_NODE (printers), "Default", GPA_NODE_ID (printers->children));
	}

	gpa_node_ref (GPA_NODE (printers));

	return printers;
}

static void
gpa_printer_list_load_from_dir (GPAList *printers, const gchar *dirname)
{
	DIR *dir;
	struct dirent *dent;
	GSList *l;

	dir = opendir (dirname);
	if (!dir) return;

	l = NULL;
	while ((dent = readdir (dir))) {
		gint len;
		gchar *filename;
		GPANode *printer;
		len = strlen (dent->d_name);
		if (len < 9) continue;
		if (strcmp (dent->d_name + len - 8, ".printer")) continue;
		filename = g_strdup_printf ("%s/%s", dirname, dent->d_name);
		printer = gpa_printer_new_from_file (filename);
		g_free (filename);
		/* fixme: test name clashes */
		if (printer) l = g_slist_prepend (l, printer);
	}

	closedir (dir);

	while (l) {
		/* fixme: ordering */
		GPANode *printer;
		printer = GPA_NODE (l->data);
		l = g_slist_remove (l, printer);
		printer->next = printers->children;
		printers->children = printer;
		printer->parent = GPA_NODE (printers);
	}
}

/* Deprecated */

GPANode *
gpa_printer_get_default (void)
{
	GPANode *def;

	if (printers->def) {
		def = GPA_REFERENCE_REFERENCE (printers->def);
	} else {
		def = printers->children;
	}

	if (def) gpa_node_ref (def);

	return def;
}

GPANode *
gpa_printer_get_by_id (const guchar *id)
{
	g_return_val_if_fail (id != NULL, NULL);
	g_return_val_if_fail (*id != '\0', NULL);

	if (printers) {
		GPANode *child;
		for (child = printers->children; child != NULL; child = child->next) {
			g_assert (GPA_IS_PRINTER (child));
			if (child->id && !strcmp (id, child->id)) {
				gpa_node_ref (child);
				return child;
			}
		}
	}

	return NULL;
}

GPANode *
gpa_printer_get_default_settings (GPAPrinter *printer)
{
	g_return_val_if_fail (printer != NULL, NULL);
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);

	if (printer->settings && printer->settings->children) {
		g_assert (GPA_IS_SETTINGS (printer->settings->children));
		gpa_node_ref (printer->settings->children);
		return printer->settings->children;
	}

	return NULL;
}

/* fixme: Thorough check */

GPANode *
gpa_printer_new_from_model (GPAModel *model, const guchar *name)
{
	GPAPrinter *printer;
	GPANode *settings;
	guchar *id;

	g_return_val_if_fail (model != NULL, NULL);
	g_return_val_if_fail (GPA_IS_MODEL (model), NULL);
	g_return_val_if_fail (name != NULL, NULL);
	g_return_val_if_fail (*name != '\0', NULL);

	if (!printers) gpa_printer_list_load ();
	if (!namedict) namedict = g_hash_table_new (g_str_hash, g_str_equal);

	g_return_val_if_fail (!g_hash_table_lookup (namedict, name), NULL);

	id = gpa_id_new (GPA_NODE (model)->id);
	printer = (GPAPrinter *) gpa_node_new (GPA_TYPE_PRINTER, id);
	g_free (id);

	/* Name */
	printer->name = gpa_node_attach (GPA_NODE (printer), gpa_value_new ("Name", name));
	g_hash_table_insert (namedict, GPA_VALUE (printer->name)->value, printer);

	/* Settings */
	/* fixme: Implement helper */
	/* fixme: defaults again */
	printer->settings = GPA_LIST (gpa_node_attach (GPA_NODE (printer), gpa_list_new (GPA_TYPE_SETTINGS, TRUE)));
	settings = gpa_settings_new_from_model (GPA_NODE (model), "Default");
	gpa_list_add_child (printer->settings, settings, NULL);
	gpa_node_unref (settings);
	/* fixme: */
	gpa_list_set_default (printer->settings, settings);

	/* Model */
	printer->model = gpa_node_attach (GPA_NODE (printer), gpa_reference_new (GPA_NODE (model)));

	gpa_list_add_child (printers, GPA_NODE (printer), NULL);

	return (GPANode *) printer;
}

gboolean
gpa_printer_save (GPAPrinter *printer)
{
	xmlDocPtr doc;
	xmlNodePtr root, xmln;
	GPANode *child;
	guchar *filename;

	g_return_val_if_fail (printer != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_PRINTER (printer), FALSE);

	g_return_val_if_fail (gpa_node_verify (GPA_NODE (printer)), FALSE);

	doc = xmlNewDoc ("1.0");
	root = xmlNewDocNode (doc, NULL, "Printer", NULL);
	xmlSetProp (root, "Version", "1.0");
	xmlSetProp (root, "Id", GPA_NODE (printer)->id);
	xmlDocSetRootElement (doc, root);

	xmln = xmlNewChild (root, NULL, "Name", GPA_VALUE (printer->name)->value);

	xmln = xmlNewChild (root, NULL, "Model", GPA_REFERENCE (printer->model)->ref->id);

	for (child = printer->settings->children; child != NULL; child = child->next) {
		xmln = gpa_settings_write (doc, child);
		if (xmln) xmlAddChild (root, xmln);
	}

	filename = g_strdup_printf ("%s/.gnome/printers/%s.printer", g_get_home_dir (), GPA_NODE (printer)->id);
	xmlSaveFile (filename, doc);
	g_free (filename);

	xmlFreeDoc (doc);

	return TRUE;
}

#if 0
static xmlNodePtr
gpa_printer_write (XmlParseContext *context, GPAPrinter *printer)
{
	xmlNodePtr child;
	xmlNodePtr node;
	xmlNodePtr item;
	xmlNsPtr   gmr;

	g_return_val_if_fail (context != NULL, NULL);
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);

	if (!gpa_printer_verify (printer, FALSE)) {
		gpa_error ("Could not save printer, because it could not be verified");
		return NULL;
	}
		
	node = xmlNewDocNode (context->doc, context->ns, GPA_TAG_PRINTER, NULL);
	if (node == NULL)
		return NULL;

	gmr = xmlNewNs (node, GPA_TAG_PRINTER_NAME_SPACE, "gmr");
	xmlSetNs (node, gmr);
	context->ns = gmr;

	item = xmlNewChild (node, gmr, GPA_TAG_PRINTER_NAME, printer->name);
	if (item == NULL) {
		gpa_error ("Could not add the key \"%s\" with value \"%s\" to the tree",
			   GPA_TAG_PRINTER_NAME, printer->name);
		return NULL;
	}
	
	item = xmlNewChild (node, gmr, GPA_TAG_PRINTER_ID, GPA_PRINTER_ID (printer));
	if (item == NULL) {
		gpa_error ("Could not add the key \"%s\" with value \"%s\" to the tree",
			   GPA_TAG_PRINTER_ID, GPA_PRINTER_ID (printer));
		return NULL;
	}
	
	item = xmlNewChild (node, gmr, GPA_TAG_PRINTER_IS_DEFAULT,
			    printer->def ? GPA_TAG_TRUE : GPA_TAG_FALSE);
	if (item == NULL) {
		gpa_error ("Could not add the key \"%s\" with value \"%s\" to the tree",
			   GPA_TAG_PRINTER_IS_DEFAULT,
			   printer->def ? GPA_TAG_TRUE : GPA_TAG_FALSE);
		return NULL;
	}
	
	item = xmlNewChild (node, gmr, GPA_TAG_MODEL_ID, GPA_MODEL_ID (printer->model));
	if (item == NULL) {
		gpa_error ("Could not add the key \"%s\" with value \"%s\" to the tree",
			   GPA_TAG_MODEL_NAME, GPA_MODEL_ID (printer->model));
		return NULL;
	}

	child = gpa_settings_list_write (context, printer->settings_list);
	
	if (child != NULL)
		xmlAddChild (node, child);
	
	return node;
}

gboolean
gpa_printer_save (GPAPrinter *printer)
{
	XmlParseContext *context;
	xmlDocPtr xml;
	gchar *file_name;
	gchar *user_dir;
	gint ret;
#if 0	
	struct stat s;
#endif	

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_PRINTER (printer), FALSE);
	g_return_val_if_fail (GPA_PRINTER_ID (printer) != NULL, FALSE);
	g_return_val_if_fail (strlen (GPA_PRINTER_ID (printer)) == GPA_PRINTER_ID_LENGTH, FALSE);
	
	user_dir = gnome_util_home_file ("printers");
#if 1
	if (!g_file_test (user_dir, G_FILE_TEST_IS_DIR))
#else	
	if (!stat (user_dir, &s))
#endif	
		gpa_error ("\"%s\" does not exist\n"
			   "Please create the directory.\n"
			   "This is broken, yes. But the real solution is not\n"
			   "to mkdir the directory inside the code. \n"
			   "~/gnome/ is NOT the place to store this info\n"
			   "Sorry.",
			   user_dir);
	file_name = g_strdup_printf ("%s/%s.printer", user_dir, GPA_PRINTER_ID (printer));
	g_free (user_dir);

	xml = xmlNewDoc ("1.0");
	if (xml == NULL) {
		gpa_error ("Could not create xml doc");
		return FALSE;
	}

	context = gpa_xml_parse_context_new (xml, NULL);
	xml->root = gpa_printer_write (context, printer);
	gpa_xml_parse_context_destroy (context);

	ret = xmlSaveFile (file_name, xml);
	xmlFreeDoc (xml);

	if (ret < 0) {
		gpa_error ("Could not save File");
		return FALSE;
	}

	return TRUE;
}

/* GPAPrinterList */

static void gpa_printer_list_load (GPAList *gpl);
static void gpa_printers_load_from_dir (GPAList *gpl, const char *dirname);

GPANode *
gpa_printer_list_new (void)
{
	GPAList *gpl;

	gpl = (GPAList *) gpa_list_new ("printers", GPA_TYPE_PRINTER);

	gpa_printer_list_load (gpl);

	return (GPANode *) gpl;
}

static void
gpa_printer_list_load (GPAList *gpl)
{
	gchar *user_dir;

	user_dir = gnome_util_home_file ("printers");
	gpa_printers_load_from_dir (gpl, user_dir);
	g_free (user_dir);
}

static void
gpa_printers_load_from_dir (GPAList *gpl, const char *dirname)
{
	GPAPrinter *printer, *last;
	DIR *dir;
	gchar *file_name;
	struct dirent *dent;
	const int expected_length = 40;

	g_return_if_fail (dirname != NULL);
	g_assert (gpl->items == NULL);
	
	last = NULL;

	dir = opendir (dirname);
	if (!dir) return;

	while ((dent = readdir (dir)) != NULL){
		int len = strlen (dent->d_name);

		if (len != expected_length)
			continue;

		if (strcmp (dent->d_name + 32, GPA_PRINTER_EXTENSION_NAME))
			continue;

#if 1
		file_name = g_strdup_printf ("%s/%s", dirname, dent->d_name);
#else	
		if (dirname [strlen(dirname) - 1] != PATH_SEP)
			file_name = g_strconcat (dirname, PATH_SEP_STR, dent->d_name, NULL);
		else
			file_name = g_strconcat (dirname, dent->d_name, NULL);
#endif	
		printer = gpa_printer_new_from_file (file_name);
		if (printer != NULL) {
			gpa_node_add_child (GPA_NODE (gpl), GPA_NODE (printer), (GPANode *) last);
			last = printer;
		}

		g_free (file_name);
	}
	closedir (dir);
}

GPAPrinter *
gpa_printer_new (const guchar *id)
{
	GPAPrinter *printer;
  
	g_return_val_if_fail (id != NULL, NULL);
	g_return_val_if_fail (*id != '\0', NULL);

	printer = gpa_node_new (GPA_TYPE_PRINTER, id);
	
	return printer;
}

/* fixme: Clean up on errors (Lauris) */

GPAPrinter *
gpa_printer_new_from_file (const gchar *filename)
{
	GPAPrinter *printer;
	XmlParseContext *context;
	gchar *full_path;
	GPAModel *model;
	xmlNodePtr tree, child;
	gchar *name;
	gchar *id;
	gchar *model_id;
	gchar *def;

	g_return_val_if_fail (filename != NULL, NULL);
	
	if (!gpa_printer_find_file (filename, &full_path)) return NULL;

	context = gpa_xml_parse_context_new_from_path (full_path, GPA_TAG_PRINTER_NAME_SPACE, GPA_TAG_PRINTER);
	g_free (full_path);
	if (context == NULL) return NULL;

	tree = context->doc->root;

	if (!gpa_xml_node_verify (tree, GPA_TAG_PRINTER)) return NULL;
	
	name     = gpa_xml_get_value_string_required (tree, GPA_TAG_PRINTER_NAME, NULL);
	id       = gpa_xml_get_value_string_required (tree, GPA_TAG_PRINTER_ID, NULL);
	def      = gpa_xml_get_value_string_required (tree, GPA_TAG_PRINTER_IS_DEFAULT, NULL);
	model_id = gpa_xml_get_value_string_required (tree, GPA_TAG_MODEL_ID, NULL);

	if ((name == NULL) || (id == NULL) || (model_id == NULL) || (def == NULL)) return NULL;

	/* fixme: this sucks, but otherwise we have to add VendorId to printer description */
	model = gpa_model_get_by_id (model_id);
	g_free (model_id);

	if (model == NULL) return NULL;
	
	printer = gpa_printer_new (id);
	g_free (id);

	printer->name  = name;
	printer->model = model;
	printer->def = (strcmp (def, GPA_TAG_TRUE) == 0);
	g_free (def);

	/* Load settings list */
	child = gpa_xml_search_child_required (tree, GPA_TAG_SETTINGS_LIST);
	if (child == NULL) return NULL;

	printer->settings_list = gpa_settings_list_load_from_node (child, printer);
	if (printer->settings_list == NULL) return FALSE;

	gpa_xml_parse_context_free (context);

	if (!gpa_printer_verify (printer, TRUE)) {
		gpa_error ("The printer was not loaded from \"%s\" because it contained "
			   "at least one error", full_path);
		return NULL;
	}

	return printer;

}

/**
 * gnome_printer_get_status:
 * @printer: The printer
 *
 * Returns the current status of @printer
 */
GpaStatus
gpa_printer_get_status (GPAPrinter *printer)
{
	g_return_val_if_fail (printer != NULL, GNOME_PRINT_PRINTER_INACTIVE);
	g_return_val_if_fail (GPA_IS_PRINTER (printer), GNOME_PRINT_PRINTER_INACTIVE);

	return GNOME_PRINT_PRINTER_INACTIVE;
}

/**
 * gnome_printer_str_status
 * @status: A status type
 *
 * Returns a string representation of the printer status code @status
 */
const char *
gpa_printer_str_status (GpaStatus status)
{
	switch (status){
	case GNOME_PRINT_PRINTER_ACTIVE:
		return _("Printer is active");

	case GNOME_PRINT_PRINTER_INACTIVE:
		return _("Printer is ready to print");

	case GNOME_PRINT_PRINTER_OFFLINE:
		return _("Printer is off-line");

	case GNOME_PRINT_PRINTER_NET_FAILURE:
		return _("Can not communicate with printer");

	}
	return _("Unknown status");
}


static gboolean
gpa_printer_find_file (const gchar *file_name, gchar **full_path)
{
	gchar *path;
#if 0	
	struct stat s;
#endif	

#if 1	
	if (!g_file_test (file_name, G_FILE_TEST_IS_REGULAR)) {
#else	
	if (!stat (file_name, &s)) {
#endif	
		gpa_error ("%s file, could not be found\n", file_name);
		return FALSE;
	}

	path = g_strdup (file_name);

	*full_path = path;

	return TRUE;
}

gboolean
gpa_printer_verify (GPAPrinter *printer, gboolean fail)
{
	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_PRINTER (printer), FALSE);

	if (printer->name == NULL) {
		gpa_error ("The printer does not containt a name");
		return FALSE;
	}	
	if (GPA_PRINTER_ID (printer) == NULL) {
		gpa_error ("The printer \"%s\" does not containt an id", printer->name);
		return FALSE;
	}	
	if (strlen (GPA_PRINTER_ID (printer)) != GPA_PRINTER_ID_LENGTH) {
		gpa_error ("The printer id is does not contain a valid length (%s)(%i)",
			   GPA_PRINTER_ID (printer),
			   strlen (GPA_PRINTER_ID (printer)));
		return FALSE;
	}	
	if (printer->model == NULL) {
		gpa_error ("The printer does not contain a model");
		return FALSE;
	}

	if (!gpa_settings_list_verify (printer->settings_list, fail))
		return FALSE;
	
	return TRUE;
}


GPAPrinter *
gpa_printer_copy (GPAPrinter *printer)
{
	GPAPrinter *new_printer;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);

	new_printer = gpa_printer_new (GPA_PRINTER_ID (printer));
	new_printer->name  = g_strdup (printer->name);
	new_printer->model = printer->model;

	new_printer->settings_list = printer->settings_list;
	new_printer->settings_list = gpa_settings_list_copy (printer->settings_list);
	
	if (!gpa_printer_verify (new_printer, TRUE)) {
		gpa_error ("Could not copy the printer");
		return NULL;
	}
	
	return new_printer;
}


/*
 * This is public
 *
 */

GPANode *
gpa_printer_new_from_model (GPANode *model, const gchar *name)
{
	GpaSettings *settings;
	GPAPrinter *printer;
	
	g_return_val_if_fail (model != NULL, NULL);
	g_return_val_if_fail (GPA_IS_MODEL (model), NULL);
	g_return_val_if_fail (name != NULL, NULL);

	printer = gpa_printer_new (gpa_id_new ());
	printer->name = g_strdup (name);
	gpa_node_ref (model);
	printer->model = GPA_MODEL (model);

	settings = gpa_settings_new_from_model (GPA_MODEL (model), printer);
	g_return_val_if_fail (GPA_IS_SETTINGS (settings), NULL);
	
	printer->settings_list = (GPAList *) gpa_settings_list_new ();
	gpa_node_add_child (GPA_NODE (printer->settings_list), GPA_NODE (settings), NULL);

	if (!gpa_printer_verify (printer, TRUE)) {
		gpa_error ("Could not greate printer from model info, (!verified)");
		return NULL;
	}
			
	gpa_node_add_child (GPA_NODE (GPA->printers), GPA_NODE (printer), NULL);

	return (GPANode *) printer;
}
		



/**
 * gpa_printer_backend_info_get:
 * @printer: The printer to get the info from
 * @backend: the string that distinguish the backend
 * @id: The id for the info
 * 
 * Get information from a printer related to a backend.
 * 
 * Return Value: a pointer to the string for this info, NULL on error
 **/
const gchar *
gpa_printer_backend_info_get (const GPAPrinter *printer,
			      const gchar *backend_name,
			      const gchar *id)
{
	GpaBackend *backend;
	const gchar *info = NULL;
	
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);
	g_return_val_if_fail (backend_name != NULL, NULL);
	g_return_val_if_fail (id != NULL, NULL);

	backend = gpa_backend_get_from_id (printer->model, backend_name);
	if (backend == NULL) {
		gpa_error ("Could not get *%s* from printer->model->backend %s\n",
			   id, backend_name);
		return NULL;
	}

	if (backend->values != NULL)
		info = gpa_hash_item_get (backend->values, id);

	return info;
}


void
gpa_printer_set_default (GPAList *printer_list, GPAPrinter *printer)
{
	GPAItem *item;
	
	g_return_if_fail (GPA_IS_PRINTER (printer));

	/* Scan the printer list for the current default
	 * printer and unset it.
	 */
	item = printer_list->items;
	for (;item != NULL; item = item->next) {
		g_return_if_fail (GPA_IS_PRINTER (item));
		GPA_PRINTER (item)->def = FALSE;
	}

	printer->def = TRUE;
}

GPAPrinter *
gpa_printer_get_default (GPAList *printer_list)
{
	GPAPrinter *printer;
	GPAItem *item;

	g_return_val_if_fail (printer_list != NULL, NULL);

	/* Scan the printer list for the current default
	 * printer and unset it.
	 */
	item = printer_list->items;
	for (;item != NULL; item = item->next) {
		g_return_val_if_fail (GPA_IS_PRINTER (item), NULL);
		printer = GPA_PRINTER (item);
		if (printer->def)
			return printer;
	}

	if (printer_list && printer_list->items) {
		GPA_PRINTER (printer_list->items)->def = TRUE;
		return GPA_PRINTER (printer_list->items);
	}
	
	return NULL;
}

/* Access to the struct */
GPAList *
gpa_printer_settings_list_get (const GPAPrinter *printer)
{
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);

	return printer->settings_list;
}

GList *
gpa_printer_get_backend_list (const GPAPrinter *printer)
{
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);
	
	return printer->model->backend_list;
}

gchar *
gpa_printer_dup_name (GPAPrinter *printer)
{
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);

	return g_strdup (printer->name);
}

const gchar *
gpa_printer_get_name (const GPAPrinter *printer)
{
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);

	return printer->name;
}

const gchar *
gpa_printer_get_id (const GPAPrinter *printer)
{
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);

	return GPA_PRINTER_ID (printer);
}

gchar *
gpa_printer_dup_id (const GPAPrinter *printer)
{
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);

	return g_strdup (GPA_PRINTER_ID (printer));
}


GPAModel *
gpa_printer_get_model (GPAPrinter *printer)
{
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);

	return printer->model;
}


void
gpa_printer_settings_append (GPAPrinter *printer, GpaSettings *settings)
{
	g_return_if_fail (printer != NULL);
	g_return_if_fail (GPA_IS_PRINTER (printer));
	g_return_if_fail (settings != NULL);
	g_return_if_fail (GPA_IS_SETTINGS (settings));

	gpa_node_append_child (GPA_NODE (printer->settings_list), GPA_NODE (settings));
}

void
gpa_printer_settings_remove (GPAPrinter *printer, GpaSettings *settings)
{
	g_return_if_fail (printer != NULL);
	g_return_if_fail (GPA_IS_PRINTER (printer));
	g_return_if_fail (settings != NULL);
	g_return_if_fail (GPA_IS_SETTINGS (settings));

	gpa_node_remove_child (GPA_NODE (printer->settings_list), GPA_NODE (settings));
}


GpaSettings *
gpa_printer_settings_get_first (GPAPrinter *printer)
{
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);

	return (GpaSettings *) gpa_node_get_child (GPA_NODE (printer->settings_list), NULL);
}

GpaSettings *
gpa_printer_settings_get_selected (const GPAPrinter *printer)
{
	g_return_val_if_fail (GPA_IS_PRINTER (printer), NULL);

	return gpa_settings_get_selected (printer->settings_list);
}

#if 0
gint
gpa_printer_settings_list_get_size (const GPAPrinter *printer)
{
	g_return_val_if_fail (GPA_IS_PRINTER (printer), 0);

	return g_list_length (printer->settings_list);
}
#endif

void
gpa_printer_settings_select (const GPAPrinter *printer, GpaSettings *settings)
{
	g_return_if_fail (GPA_IS_PRINTER (printer));
	g_return_if_fail (GPA_IS_SETTINGS (settings));

	gpa_settings_select (settings, printer->settings_list);
}

void
gpa_printer_settings_list_swap (GPAPrinter *printer_1, GPAPrinter *printer_2)
{
	GPAList *list;
	
	g_return_if_fail (GPA_IS_PRINTER (printer_1));
	g_return_if_fail (GPA_IS_PRINTER (printer_2));
	
	list = printer_1->settings_list;
	printer_1->settings_list = printer_2->settings_list;
	printer_2->settings_list = list;
}

gboolean
gpa_printer_is_default (const GPAPrinter *printer)
{
	g_return_val_if_fail (GPA_IS_PRINTER (printer), FALSE);

	return printer->def;
}
		
#if 0
GPAPrinter *
gpa_printer_list_lookup (GPAList *list, const gchar *id)
{
	GPAItem *item;

	g_return_val_if_fail (list != NULL, NULL);
	g_return_val_if_fail (GPA_IS_LIST (list), NULL);
	g_return_val_if_fail (id != NULL, NULL);

	for (item = list->items; item != NULL; item = item->next) {
		GPAPrinter *printer;
		printer = GPA_PRINTER (item);
		if (!strcmp (printer->id, id)) {
			gpa_node_ref (GPA_NODE (printer));
			return printer;
		}
	}

	return NULL;
}
#endif


#if 0
static gboolean
gpa_printer_modelinfo_load_from_node (GPAPrinter *printer,
				      xmlNodePtr tree)
{
	xmlNodePtr child;
	xmlNodePtr vendor_node;
	gchar *model_id;
	gchar *model_name;
	gchar *vendor_name;

	debug (FALSE, "");

	child = gpa_xml_search_child_required (tree, GPA_TAG_MODEL_INFO);
	if (child == NULL)
		return FALSE;

	model_name = gpa_xml_get_value_string_required (child, GPA_TAG_NAME, NULL);
	if (model_name == NULL)
		return FALSE;

	model_id = gpa_xml_get_value_string_required (child, GPA_TAG_ID, NULL);
	if (model_id == NULL)
		return FALSE;

	vendor_node =  gpa_xml_search_child_required (child, GPA_TAG_VENDOR_INFO);
	if (vendor_node == NULL)
		return FALSE;
	
	vendor_name = gpa_xml_get_value_string_required (vendor_node, GPA_TAG_NAME, NULL);
	if (vendor_name == NULL)
		return FALSE;

	if (printer->model == NULL)
		printer->model = gpa_model_find_by_vendor_and_model_name (vendor_name,
									  model_name);
	if (printer->model == NULL)
		return FALSE;

	if ((strcmp (vendor_name, printer->model->vendor->name) != 0) ||
	    (strcmp (model_name,  printer->model->name)         != 0) ||
	    (strcmp (model_id,    GPA_MODEL_ID (printer->model))           != 0))
	{
		gpa_error ("The File info does not match the index info\n"
			   "pxr:Vendor name %s\ngpi:Vendor name %s\n"
			   "pxr:Model  name %s\ngpi:Model name  %s\n"
			   "pxr:Model  id   %s\ngpi:Model id    %s\n",
			   vendor_name, printer->model->vendor->name,
			   model_name, printer->model->name,
			   model_id,   GPA_MODEL_ID (printer->model));
		return FALSE;
	}

	g_free (vendor_name);
	g_free (model_name);
	g_free (model_id);

	gpa_printer_fileinfo_load_from_node (printer, child);
		
	debug (FALSE, "end");
	
	return TRUE;
}
#endif
#endif
