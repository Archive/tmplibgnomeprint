#ifndef __GNOME_PRINT_CONFIG_H__
#define __GNOME_PRINT_CONFIG_H__

/*
 * And frontend abstraction to whatever config system we eventually have
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors:
 *   Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright 2001 Ximian, Inc.
 *
 */

#include <glib.h>

G_BEGIN_DECLS

typedef struct _GnomePrintConfig GnomePrintConfig;

GnomePrintConfig *gnome_print_config_default (void);

GnomePrintConfig *gnome_print_config_ref (GnomePrintConfig *config);
GnomePrintConfig *gnome_print_config_unref (GnomePrintConfig *config);

guchar *gnome_print_config_get (GnomePrintConfig *config, const guchar *key);
gboolean gnome_print_config_set (GnomePrintConfig *config, const guchar *key, const guchar *value);

gboolean gnome_print_config_get_boolean (GnomePrintConfig *config, const guchar *key, gboolean *val);
gboolean gnome_print_config_get_int (GnomePrintConfig *config, const guchar *key, gint *val);
gboolean gnome_print_config_get_double (GnomePrintConfig *config, const guchar *key, gdouble *val);

gboolean gnome_print_config_set_boolean (GnomePrintConfig *config, const guchar *key, gboolean val);
gboolean gnome_print_config_set_int (GnomePrintConfig *config, const guchar *key, gint val);
gboolean gnome_print_config_set_double (GnomePrintConfig *config, const guchar *key, gdouble val);

G_END_DECLS

#endif






