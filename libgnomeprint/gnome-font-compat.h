#ifndef __GNOME_FONT_COMPAT_H__
#define __GNOME_FONT_COMPAT_H__

/*
 *  Copyright (C) 2000-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Jody Goldberg <jody@ximian.com>
 *    Miguel de Icaza <miguel@ximian.com>
 *    Lauris Kaplinski <lauris@ximian.com>
 *    Christopher James Lahey <clahey@ximian.com>
 *    Michael Meeks <michael@ximian.com>
 *    Morten Welinder <terra@diku.dk>
 *
 *  GnomeFont - basic user visible handle to scaled typeface
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glib.h>

G_BEGIN_DECLS

#include <libgnomeprint/gnome-font-face.h>

/* Various backward compatibility typedefs */
typedef struct _GnomeFontFacePrivate GnomeFontFacePrivate;
typedef struct _GnomeFontFace GnomeFontUnsized;
typedef struct _GnomeFontFace GnomeFontMap;

#define gnome_font_face_ref(f) g_object_ref (G_OBJECT (f))
#define gnome_font_face_unref(f) g_object_unref (G_OBJECT (f))

/*
 * Various backward-compatibility and conveninence methods
 *
 * NB! Those usually do not scale for international fonts, so use with
 * caution.
 */

gdouble gnome_font_face_get_ascender (const GnomeFontFace * face);
gdouble gnome_font_face_get_descender (const GnomeFontFace * face);
gdouble gnome_font_face_get_underline_position (const GnomeFontFace * face);
gdouble gnome_font_face_get_underline_thickness (const GnomeFontFace * face);

gdouble gnome_font_face_get_glyph_width (const GnomeFontFace * face, gint glyph);
gdouble gnome_font_face_get_glyph_kerning (const GnomeFontFace * face, gint glyph1, gint glyph2);
const gchar * gnome_font_face_get_glyph_ps_name (const GnomeFontFace * face, gint glyph);

GnomeFontWeight gnome_font_face_get_weight_code (const GnomeFontFace * face);
gboolean gnome_font_face_is_italic (const GnomeFontFace * face);
gboolean gnome_font_face_is_fixed_width (const GnomeFontFace * face);


gchar * gnome_font_face_get_pfa (const GnomeFontFace * face);

/*
 * Create font
 *
 * Those allow one to get sized font object from given typeface. Theoretically
 * GnomeFont should be optimized for certain resolution (resx, resy). If that
 * resolution is not known, get_font_default should give reasonably well-
 * looking font for most occasions
 */

GnomeFont * gnome_font_face_get_font (const GnomeFontFace * face, gdouble size, gdouble xres, gdouble yres);
GnomeFont * gnome_font_face_get_font_default (const GnomeFontFace * face, gdouble size);


/* Font */
GnomeFontWeight gnome_font_get_weight_code (const GnomeFont * font);
gboolean gnome_font_is_italic (const GnomeFont * font);

#ifdef PRESERVE_COMPATIBILITY
/*
 * Compatibility
 */
/* typedef struct
 * {
 *  GnomeFontUnsized *unsized_font;   font before scaling
 *  GnomeFont *gnome_font;            scaled font
 *  double scale;                     scaling factor requested by a view
 * 
 *  char *x_font_name;                x name that got us gdk_font
 *  GdkFont *gdk_font;                used for drawing
 * } GnomeDisplayFont;
 */

typedef struct _GnomeRFont GnomeDisplayFont;

GnomeDisplayFont * gnome_font_get_display_font (GnomeFont *font);
const char * gnome_font_weight_to_string (GnomeFontWeight gfw);
GnomeFontWeight string_to_gnome_font_weight (const char *weight);
GnomeDisplayFont * gnome_get_display_font (const char * family, GnomeFontWeight weight, gboolean italic, double points, double scale);
int gnome_display_font_height (GnomeDisplayFont * gdf);

/*
 * These are for attribute reading
 *
 * If writing fresh code, use rfont methods directly
 */
const GnomeFontFace * gnome_display_font_get_face (const GnomeDisplayFont * gdf);
const GnomeFont * gnome_display_font_get_font (const GnomeDisplayFont * gdf);
gdouble gnome_display_font_get_scale (const GnomeDisplayFont * gdf);
const gchar * gnome_display_font_get_x_font_name (const GnomeDisplayFont * gdf);
GdkFont * gnome_display_font_get_gdk_font (const GnomeDisplayFont * gdf);

void gnome_display_font_ref (GnomeDisplayFont * gdf);
void gnome_display_font_unref (GnomeDisplayFont * gdf);
#endif

G_END_DECLS

#endif
