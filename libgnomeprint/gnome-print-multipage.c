#define __GNOME_PRINT_MULTIPAGE_C__

/*
 *  Copyright (C) 1999-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Chris Lahey <clahey@helixcode.com>
 *
 *  Wrapper for printing several pages onto single output page
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <string.h>
#include <math.h>
#include <ctype.h>

#include <libart_lgpl/art_affine.h>
#include <libgnomeprint/gnome-print-private.h>
#include <libgnomeprint/gnome-print-multipage.h>
#include <libgnomeprint/gnome-font.h>

struct _GnomePrintMultipage {
	GnomePrintContext pc;

	GnomePrintContext *subpc;
	GList *affines; /* Of type double[6] */
	GList *subpage;
};

struct _GnomePrintMultipageClass {
	GnomePrintContextClass parent_class;
};

static void gnome_print_multipage_class_init (GnomePrintMultipageClass *klass);
static void gnome_print_multipage_init (GnomePrintMultipage *multipage);

static void gnome_print_multipage_finalize (GObject *object);

static int gnome_print_multipage_beginpage (GnomePrintContext *pc, const guchar *name);
static int gnome_print_multipage_showpage (GnomePrintContext *pc);
static int gnome_print_multipage_gsave (GnomePrintContext *pc);
static int gnome_print_multipage_grestore (GnomePrintContext *pc);
static int gnome_print_multipage_clip (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule);
static int gnome_print_multipage_fill (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule);
static int gnome_print_multipage_stroke (GnomePrintContext *pc, const ArtBpath *bpath);
static int gnome_print_multipage_image (GnomePrintContext *pc, const gdouble *affine, const guchar *px, gint w, gint h, gint rowstride, gint ch);
static int gnome_print_multipage_glyphlist (GnomePrintContext *pc, const gdouble *affine, GnomeGlyphList *gl);
static int gnome_print_multipage_close (GnomePrintContext *pc);

static GList *gnome_print_multipage_affine_list_duplicate(GList *affines);

static GnomePrintContextClass *parent_class = NULL;

GType
gnome_print_multipage_get_type (void)
{
	static GType type = 0;
	if (!type) {
		static const GTypeInfo info = {
			sizeof (GnomePrintMultipageClass),
			NULL, NULL,
			(GClassInitFunc) gnome_print_multipage_class_init,
			NULL, NULL,
			sizeof (GnomePrintMultipage),
			0,
			(GInstanceInitFunc) gnome_print_multipage_init
		};
		type = g_type_register_static (GNOME_TYPE_PRINT_CONTEXT, "GnomePrintMultipage", &info, 0);
	}
	return type;
}

static void
gnome_print_multipage_class_init (GnomePrintMultipageClass *klass)
{
	GObjectClass *object_class;
	GnomePrintContextClass *pc_class;

	object_class = (GObjectClass *) klass;
	pc_class = (GnomePrintContextClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_print_multipage_finalize;

	pc_class->beginpage = gnome_print_multipage_beginpage;
	pc_class->showpage = gnome_print_multipage_showpage;

	pc_class->gsave = gnome_print_multipage_gsave;
	pc_class->grestore = gnome_print_multipage_grestore;

	pc_class->clip = gnome_print_multipage_clip;
	pc_class->fill = gnome_print_multipage_fill;
	pc_class->stroke = gnome_print_multipage_stroke;

	pc_class->image = gnome_print_multipage_image;

	pc_class->glyphlist = gnome_print_multipage_glyphlist;

	pc_class->close = gnome_print_multipage_close;
}

static void
gnome_print_multipage_init (GnomePrintMultipage *multipage)
{
	multipage->affines = NULL;
	multipage->subpage = NULL;
	multipage->subpc = NULL;
}

static void
gnome_print_multipage_finalize (GObject *object)
{
	GnomePrintMultipage *multipage;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_IS_PRINT_MULTIPAGE (object));

	multipage = GNOME_PRINT_MULTIPAGE (object);
  
	g_object_unref (G_OBJECT (multipage->subpc));

	g_list_foreach(multipage->affines, (GFunc) g_free, NULL);
	g_list_free(multipage->affines);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static int
gnome_print_multipage_beginpage (GnomePrintContext *pc, const guchar *name)
{
	GnomePrintMultipage *multipage = GNOME_PRINT_MULTIPAGE (pc);
	return gnome_print_beginpage (multipage->subpc, name);
}

static int
gnome_print_multipage_showpage (GnomePrintContext *pc)
{
	GnomePrintMultipage *multipage = GNOME_PRINT_MULTIPAGE (pc);
	int error_code;

	error_code = gnome_print_grestore(multipage->subpc);
	if ( error_code ) return error_code;

	multipage->subpage = multipage->subpage->next;
	if ( multipage->subpage == NULL ) {
		multipage->subpage = multipage->affines;
		error_code = gnome_print_showpage (multipage->subpc);
		if ( error_code ) return error_code;
	}
	error_code = gnome_print_gsave(multipage->subpc);
	if ( error_code ) return error_code;
	error_code = gnome_print_concat(multipage->subpc, multipage->subpage->data);
	if ( error_code ) return error_code;
	return 0;
}

static int
gnome_print_multipage_gsave (GnomePrintContext *pc)
{
	GnomePrintMultipage *multipage = GNOME_PRINT_MULTIPAGE(pc);
	return gnome_print_gsave (multipage->subpc);
}

static int
gnome_print_multipage_grestore (GnomePrintContext *pc)
{
	GnomePrintMultipage *multipage = GNOME_PRINT_MULTIPAGE(pc);
	return gnome_print_grestore (multipage->subpc);
}

static int
gnome_print_multipage_clip (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule)
{
	GnomePrintMultipage *multipage = GNOME_PRINT_MULTIPAGE (pc);
	return gnome_print_clip_bpath_rule (multipage->subpc, bpath, rule);
}

static int
gnome_print_multipage_fill (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule)
{
	GnomePrintMultipage *multipage = GNOME_PRINT_MULTIPAGE(pc);
	gnome_print_setrgbcolor (multipage->subpc, gp_gc_get_red (pc->gc), gp_gc_get_green (pc->gc), gp_gc_get_blue (pc->gc));
	gnome_print_setopacity (multipage->subpc, gp_gc_get_opacity (pc->gc));
	gnome_print_fill_bpath_rule (multipage->subpc, bpath, rule);
	return gnome_print_fill_bpath_rule (multipage->subpc, bpath, rule);
}

static int
gnome_print_multipage_stroke (GnomePrintContext *pc, const ArtBpath *bpath)
{
	GnomePrintMultipage *multipage;
	const ArtVpathDash *dash;

	multipage = GNOME_PRINT_MULTIPAGE (pc);
	dash = gp_gc_get_dash (pc->gc);

	gnome_print_setrgbcolor (multipage->subpc, gp_gc_get_red (pc->gc), gp_gc_get_green (pc->gc), gp_gc_get_blue (pc->gc));
	gnome_print_setopacity (multipage->subpc, gp_gc_get_opacity (pc->gc));
	gnome_print_setlinewidth (multipage->subpc, gp_gc_get_linewidth (pc->gc));
	gnome_print_setmiterlimit (multipage->subpc, gp_gc_get_miterlimit (pc->gc));
	gnome_print_setlinejoin (multipage->subpc, gp_gc_get_linejoin (pc->gc));
	gnome_print_setlinecap (multipage->subpc, gp_gc_get_linecap (pc->gc));
	gnome_print_setdash (multipage->subpc, dash->n_dash, dash->dash, dash->offset);

	return gnome_print_stroke_bpath (multipage->subpc, bpath);
}

static int
gnome_print_multipage_image (GnomePrintContext *pc, const gdouble *affine, const guchar *px, gint w, gint h, gint rowstride, gint ch)
{
	GnomePrintMultipage *multipage = GNOME_PRINT_MULTIPAGE(pc);
	return gnome_print_image_transform (multipage->subpc, affine, px, w, h, rowstride, ch);
}

static int
gnome_print_multipage_glyphlist (GnomePrintContext *pc, const gdouble *affine, GnomeGlyphList *gl)
{
	GnomePrintMultipage *multipage = GNOME_PRINT_MULTIPAGE(pc);
	return gnome_print_glyphlist_transform (multipage->subpc, affine, gl);
}

static int
gnome_print_multipage_close (GnomePrintContext *pc)
{
	GnomePrintMultipage *multipage = GNOME_PRINT_MULTIPAGE(pc);
	if (multipage->affines != multipage->subpage)
		gnome_print_showpage (multipage->subpc);
	return gnome_print_context_close (multipage->subpc);
}

static GList *
gnome_print_multipage_affine_list_duplicate(GList *affines)
{
	gdouble *affine;
	GList *list;
  
	if (affines == NULL) return NULL;

	affine = g_new (gdouble, 6);
	memcpy (affine, affines->data, sizeof(gdouble) * 6);
	list = g_list_append (NULL, affine);
	list = g_list_append (list, gnome_print_multipage_affine_list_duplicate(g_list_next(affines)));
	return list;
}

/**
 * gnome_print_multipage_new:
 * @subpc: Where do we print
 * @affines: List of positions for pages.  There must be at least one item in this list.
 *
 * Creates a new Postscript printing context
 *
 * Returns: a new GnomePrintMultipage object in which you can issue GnomePrint commands.
 */
GnomePrintMultipage *
gnome_print_multipage_new (GnomePrintContext *subpc, GList *affines /* Of type double[6] */)
{
	GnomePrintMultipage *multipage;
	gint error_code;

	g_return_val_if_fail(subpc != NULL, NULL);
	g_return_val_if_fail(GNOME_IS_PRINT_CONTEXT(subpc), NULL);
	g_return_val_if_fail(affines != NULL, NULL);

	multipage = g_object_new (GNOME_TYPE_PRINT_MULTIPAGE, NULL);

	multipage->subpc = subpc;
	multipage->affines = gnome_print_multipage_affine_list_duplicate(affines);
	multipage->subpage = multipage->affines;
	g_object_ref (G_OBJECT(subpc));

	error_code = gnome_print_gsave(multipage->subpc);
	if ( error_code ) {
		g_object_unref (G_OBJECT(multipage));
		return NULL;
	}
	error_code = gnome_print_concat(multipage->subpc, multipage->subpage->data);
	if ( error_code ) {
		g_object_unref (G_OBJECT(multipage));
		return NULL;
	}
  
	return multipage;
}


/**
 * gnome_print_multipage_new_from_sizes:
 * @subpc: Where do we print
 * @paper_width: Width of paper to print on.
 * @paper_height: Height of paper to print on.
 * @page_width: Width of page to print.
 * @page_height: Height of page to print.
 *
 * Creates a new Postscript printing context
 *
 * Returns: a new GnomePrintMultipage object in which you can issue GnomePrint commands.
 */
GnomePrintMultipage *
gnome_print_multipage_new_from_sizes (GnomePrintContext *subpc, gdouble paper_width, gdouble paper_height, gdouble page_width, gdouble page_height)
{
	GnomePrintMultipage *multipage;
	gint same_count, opposite_count;
	gdouble start_affine[6];
	gdouble x_affine[6];
	gdouble y_affine[6];
	gdouble current_affine[6];
	int x_count;
	int y_count;
	int x;
	int y;
	gint error_code;

	g_return_val_if_fail(subpc != NULL, NULL);

	same_count = ((int)(paper_width / page_width)) * ((int)(paper_height / page_height));
	opposite_count = ((int)(paper_width / page_height)) * ((int)(paper_height / page_width));

	if (same_count >= opposite_count) {
		art_affine_translate(start_affine, 0, paper_height - page_height);
		art_affine_translate(x_affine, page_width, 0);
		art_affine_translate(y_affine, 0, -page_height);
		x_count = ((int)(paper_width / page_width));
		y_count = ((int)(paper_height / page_height));
	} else {
		gdouble translation[6];
		art_affine_rotate(start_affine, -90);
		art_affine_translate(translation, paper_width - page_height, paper_height);
		art_affine_multiply(start_affine, start_affine, translation);
		art_affine_translate(x_affine, 0, -page_width);
		art_affine_translate(y_affine, -page_height, 0);
		x_count = ((int)(paper_width / page_height));
		y_count = ((int)(paper_height / page_width));
	}

	multipage = g_object_new (GNOME_TYPE_PRINT_MULTIPAGE, NULL);

	multipage->subpc = subpc;
	for ( x = 0; x < x_count; x++ )
	{
		memcpy(current_affine, start_affine, 6 * sizeof(gdouble));
		for ( y = 0; y < y_count; y++ ) 
		{
			gdouble *affine;
			affine = g_new(gdouble, 6);
			memcpy(affine, current_affine, 6 * sizeof(gdouble));
			multipage->affines = g_list_append(multipage->affines, affine);
			art_affine_multiply(current_affine, current_affine, x_affine);
		}
		art_affine_multiply(start_affine, start_affine, y_affine);
	}
	multipage->subpage = multipage->affines;

	g_object_ref (G_OBJECT(subpc));

	error_code = gnome_print_gsave(multipage->subpc);
	if ( error_code ) {
		g_object_unref (G_OBJECT(multipage));
		return NULL;
	}
	error_code = gnome_print_concat(multipage->subpc, multipage->subpage->data);
	if ( error_code ) {
		g_object_unref (G_OBJECT(multipage));
		return NULL;
	}
  
	return multipage;
}

