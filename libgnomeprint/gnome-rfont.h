#ifndef __GNOME_RFONT_H__
#define __GNOME_RFONT_H__

/*
 *  Copyright (C) 2000-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Lauris Kaplinski <lauris@ximian.com>
 *
 *  GnomeFontFace - grid fitted font
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glib.h>

G_BEGIN_DECLS

#define GNOME_TYPE_RFONT (gnome_rfont_get_type ())
#define GNOME_RFONT(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GNOME_TYPE_RFONT, GnomeRFont))
#define GNOME_RFONT_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GNOME_TYPE_RFONT, GnomeRFontClass))
#define GNOME_IS_RFONT(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GNOME_TYPE_RFONT))
#define GNOME_IS_RFONT_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GNOME_TYPE_RFONT))
#define GNOME_RFONT_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GNOME_TYPE_RFONT, GnomeRFontClass))

typedef struct _GnomeRFont GnomeRFont;
typedef struct _GnomeRFontClass GnomeRFontClass;

#include <libart_lgpl/art_svp.h>
#include <libgnomeprint/gnome-font.h>
#include <libgnomeprint/gnome-pgl.h>

GType gnome_rfont_get_type (void);

/* Methods */

/*
 * Creates new RFont from given font and font->raster affine matrix
 * Matrix can be 2x2, although if read, all 2x3 values are retrieved
 * RFont is referenced, so you have to unref it somewhere
 */

GnomeRFont * gnome_font_get_rfont (GnomeFont * font, gdouble * transform);

#define gnome_rfont_ref(o) g_object_ref (G_OBJECT (o))
#define gnome_rfont_unref(o) g_object_unref (G_OBJECT (o))

/*
 * Attributes
 */

const GnomeFont * gnome_rfont_get_font (const GnomeRFont * rfont);
const GnomeFontFace * gnome_rfont_get_face (const GnomeRFont * rfont);
gdouble * gnome_rfont_get_matrix (const GnomeRFont * rfont, gdouble * matrix);

/*
 * Typeface attributes
 *
 * stdadvance is basically space width transformed to device coordinates
 */

ArtPoint * gnome_rfont_get_stdadvance (const GnomeRFont * rfont, ArtPoint * advance);

/*
 * Glyph Attributes
 */

ArtPoint * gnome_rfont_get_glyph_stdadvance (const GnomeRFont * rfont, gint glyph, ArtPoint * advance);
ArtDRect * gnome_rfont_get_glyph_stdbbox (const GnomeRFont * rfont, gint glyph, ArtDRect * bbox);

/*
 * I am not sure, whether these should be stdoutline and stdsvp
 */

const ArtBpath * gnome_rfont_get_glyph_bpath (const GnomeRFont * rfont, gint glyph);
const ArtSVP * gnome_rfont_get_glyph_svp (const GnomeRFont * rfont, gint glyph);

/*
 * Rendering flags
 *
 * Look into gnome-pgl.h for more meaningful definitions
 */

#define GNOME_RFONT_RENDER_DEFAULT 0

void gnome_rfont_render_glyph_rgba8 (const GnomeRFont * rfont, gint glyph,
				     guint32 rgba,
				     gdouble x, gdouble y,
				     guchar * buf,
				     gint width, gint height, gint rowstride,
				     guint flags);

void gnome_rfont_render_glyph_rgb8 (const GnomeRFont * rfont, gint glyph,
				    guint32 rgba,
				    gdouble x, gdouble y,
				    guchar * buf,
				    gint width, gint height, gint rowstride,
				    guint flags);

void gnome_rfont_render_pgl_rgba8 (const GnomePosGlyphList * pgl,
				   gdouble x, gdouble y,
				   guchar * buf,
				   gint width, gint height, gint rowstride,
				   guint flags);

void gnome_rfont_render_pgl_rgb8 (const GnomePosGlyphList * pgl,
				  gdouble x, gdouble y,
				  guchar * buf,
				  gint width, gint height, gint rowstride,
				  guint flags);

#if 0
/*
 * WARNING - THIS IS EXPERIMENTAL
 */

void gnome_rfont_render_glyph_gdk_drawable (const GnomeRFont * rfont, gint glyph,
				guint32 rgba,
				gdouble x, gdouble y,
				GdkDrawable * drawable,
				guint32 background,
				guint flags);

void gnome_rfont_render_pgl_gdk_drawable (const GnomePosGlyphList * pgl,
				gdouble x, gdouble y,
				GdkDrawable * drawable,
				guint32 background,
				guint flags);
#endif

G_END_DECLS

#endif /* __GNOME_RFONT_H__ */


