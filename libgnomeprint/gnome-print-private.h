#ifndef _GNOME_PRINT_PRIVATE_H_
#define _GNOME_PRINT_PRIVATE_H_

/*
 * Abstract base class for drivers
 *
 * Authors:
 *   Raph Levien (raph@acm.org)
 *   Miguel de Icaza (miguel@kernel.org)
 *   Lauris Kaplinski <lauris@ximian.com>
 *   Chema Celorio (chema@celorio.com)
 *
 * Copyright (C) 1999-2001 Ximian, Inc. and authors
 *
 */

#include <glib.h>

G_BEGIN_DECLS

#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_wind.h>
#include "gp-gc.h"
#include "gnome-print-transport.h"
#include "gnome-print.h"

#define dontALLOW_BROKEN_PGL

#define GNOME_PRINT_CONTEXT_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GNOME_TYPE_PRINT_CONTEXT, GnomePrintContextClass))
#define GNOME_IS_PRINT_CONTEXT_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GNOME_TYPE_PRINT_CONTEXT))
#define GNOME_PRINT_CONTEXT_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GNOME_TYPE_PRINT_CONTEXT, GnomePrintContextClass))

typedef struct _GnomePrintContextClass GnomePrintContextClass;

struct _GnomePrintContext {
	GObject object;

	GPANode *config;
	GnomePrintTransport *transport;

	GPGC * gc;
	gboolean haspage;
};

struct _GnomePrintContextClass {
	GObjectClass parent_class;

	gint (* construct) (GnomePrintContext *ctx);

	gint (* beginpage) (GnomePrintContext *pc, const guchar *name);
	gint (* showpage) (GnomePrintContext *pc);

	gint (* gsave) (GnomePrintContext *pc);
	gint (* grestore) (GnomePrintContext *pc);

	gint (* clip) (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule);
	gint (* fill) (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule);
	gint (* stroke) (GnomePrintContext *pc, const ArtBpath *bpath);

	gint (* image) (GnomePrintContext *pc, const gdouble *affine, const guchar *px, gint w, gint h, gint rowstride, gint ch);

	gint (* glyphlist) (GnomePrintContext * pc, const gdouble *affine, GnomeGlyphList *gl);

	gint (* close) (GnomePrintContext *pc);
};

gint gnome_print_context_construct (GnomePrintContext *ctx, GPANode *config);

gint gnome_print_context_create_transport (GnomePrintContext *ctx);

gint gnome_print_clip_bpath_rule (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule);
gint gnome_print_fill_bpath_rule (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule);
gint gnome_print_stroke_bpath (GnomePrintContext *pc, const ArtBpath *bpath);
gint gnome_print_image_transform (GnomePrintContext *pc, const gdouble *affine, const guchar *px, gint w, gint h, gint rowstride, gint ch);
gint gnome_print_glyphlist_transform (GnomePrintContext *pc, const gdouble *affine, GnomeGlyphList *gl);

G_END_DECLS

#endif


