#define __GNOME_FONT_FAMILY_C__

#include <string.h>
#include "gp-fontmap.h"
#include "gnome-font-family.h"

struct _GnomeFontFamily {
	GObject object;

	gchar * name;
};

struct _GnomeFontFamilyClass {
	GObjectClass parent_class;
};

static void gnome_font_family_class_init (GnomeFontFamilyClass * klass);
static void gnome_font_family_init (GnomeFontFamily * family);

static void gnome_font_family_finalize (GObject * object);

static GObjectClass * parent_class = NULL;

GType
gnome_font_family_get_type (void) {
	static GType family_type = 0;
	if (!family_type) {
		static const GTypeInfo family_info = {
			sizeof (GnomeFontFamilyClass),
			NULL, NULL,
			(GClassInitFunc) gnome_font_family_class_init,
			NULL, NULL,
			sizeof (GnomeFontFamily),
			0,
			(GInstanceInitFunc) gnome_font_family_init
		};
		family_type = g_type_register_static (G_TYPE_OBJECT, "GnomeFontFamily", &family_info, 0);
	}
	return family_type;
}

static void
gnome_font_family_class_init (GnomeFontFamilyClass * klass)
{
	GObjectClass * object_class;

	object_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_font_family_finalize;
}

static void
gnome_font_family_init (GnomeFontFamily * family)
{
	family->name = NULL;
}

static void
gnome_font_family_finalize (GObject * object)
{
	GnomeFontFamily * family;

	family = (GnomeFontFamily *) object;

	if (family->name) {
		g_free (family->name);
		family->name = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

GnomeFontFamily *
gnome_font_family_new (const gchar * name)
{
	GnomeFontFamily * family;
	GPFontMap * map;
	GPFamilyEntry * f;

	g_return_val_if_fail (name != NULL, NULL);

	family = NULL;

	map = gp_fontmap_get ();

	f = g_hash_table_lookup (map->familydict, name);

	if (f) {
		family = g_object_new (GNOME_TYPE_FONT_FAMILY, NULL);
		family->name = g_strdup (name);
	}

	gp_fontmap_release (map);

	return family;
}

GList *
gnome_font_family_style_list (GnomeFontFamily * family)
{
	GPFontMap * map;
	GPFamilyEntry * f;
	GList * list;

	g_return_val_if_fail (family != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FAMILY (family), NULL);

	list = NULL;

	map = gp_fontmap_get ();

	f = g_hash_table_lookup (map->familydict, family->name);

	if (f) {
		GSList * l;
		for (l = f->fonts; l != NULL; l = l->next) {
			GPFontEntry * e;
			e = (GPFontEntry *) l->data;
			list = g_list_prepend (list, g_strdup (e->speciesname));
		}
		list = g_list_reverse (list);
	}

	gp_fontmap_release (map);

	return list;
}

void
gnome_font_family_style_list_free (GList * list)
{
	while (list) {
		g_free (list->data);
		list = g_list_remove (list, list->data);
	}
}

GnomeFontFace *
gnome_font_family_get_face_by_stylename (GnomeFontFamily * family, const gchar * style)
{
	GnomeFontFace * face;
	GPFontMap * map;
	GPFamilyEntry * f;

	g_return_val_if_fail (family != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FAMILY (family), NULL);
	g_return_val_if_fail (style != NULL, NULL);

	face = NULL;

	map = gp_fontmap_get ();

	f = g_hash_table_lookup (map->familydict, family->name);

	if (f) {
		GSList * l;
		for (l = f->fonts; l != NULL; l = l->next) {
			GPFontEntry * e;
			e = (GPFontEntry *) l->data;
			if (!strcmp (style, e->speciesname)) {
				face = gnome_font_face_find (e->name);
			}
		}
	}

	gp_fontmap_release (map);

	return face;
}






