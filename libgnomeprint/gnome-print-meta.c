#define __GNOME_PRINT_META_C__

/*
 *  Copyright (C) 1999-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Miguel de Icaza (miguel@gnu.org)
 *    Michael Zucchi <notzed@helixcode.com>
 *    Morten Welinder (terra@diku.dk)
 *    Lauris Kaplinski <lauris@ximian.com>
 *
 *  Metafile implementation for gnome-print
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <string.h>
#include <math.h>

#include <libgnomeprint/gnome-glyphlist-private.h>
#include <libgnomeprint/gnome-print-private.h>
#include <libgnomeprint/gnome-print-meta.h>

struct _GnomePrintMeta {
	GnomePrintContext pc;

	char *buffer;

	int buffer_size;
	int current;
	int pages;

	int active_page;
};

struct _GnomePrintMetaClass {
	GnomePrintContextClass parent_class;
};

static GnomePrintContextClass *parent_class = NULL;

/* Note: this must continue to be the same length  as "GNOME_METAFILE-0.0" */
#define GNOME_METAFILE_SIGNATURE "GNOME_METAFILE-2.0"
#define GNOME_METAFILE_SIGNATURE_SIZE 18
#define BLOCKSIZE 4096

typedef struct {
	char signature [GNOME_METAFILE_SIGNATURE_SIZE];
	gint32  size;
} GnomeMetaFileHeader;

/* this is used to quickly scan pages through a metafile, it is
   stored at the start of the file, and after every showpage */
#define PAGE_SIGNATURE "PAGE"
typedef struct {
	char signature[4];	/* magic token for a page header */
	gint32 size;
} GnomeMetaPageHeader;

/* ENCODED sizes maybe != structure sizes (alignment) */
#define FILEHEADER_SIZE (GNOME_METAFILE_SIGNATURE_SIZE+4)
#define PAGEHEADER_SIZE (4+4)

static const char *decode_header (const char *data, GnomeMetaFileHeader *mh);

typedef enum {
	GNOME_META_BEGINPAGE,
	GNOME_META_SHOWPAGE,
	GNOME_META_GSAVE,
	GNOME_META_GRESTORE,
	GNOME_META_CLIP,
	GNOME_META_FILL,
	GNOME_META_STROKE,
	GNOME_META_IMAGE,
	GNOME_META_GLYPHLIST,
	GNOME_META_COLOR,
	GNOME_META_LINE,
	GNOME_META_DASH,
} GnomeMetaType;

typedef enum {
	GNOME_META_DOUBLE_INT,        /* An integer.  */
	GNOME_META_DOUBLE_INT1000,    /* An integer to be divided by 1000.  */
	GNOME_META_DOUBLE_I386        /* IEEE-xxxx little endian.  */
} GnomeMetaDoubleType;

#define GPM_ENSURE_SPACE(m,s) (((m)->current + (s) <= (m)->buffer_size) || gpm_ensure_space (m, s))

static void gnome_print_meta_class_init (GnomePrintMetaClass *klass);
static void gnome_print_meta_init (GnomePrintMeta *meta);

static void meta_finalize (GObject *object);

static int meta_beginpage (GnomePrintContext *pc, const guchar *name);
static int meta_showpage (GnomePrintContext *pc);
static int meta_gsave (GnomePrintContext *pc);
static int meta_grestore (GnomePrintContext *pc);
static int meta_clip (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule);
static int meta_fill (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule);
static int meta_stroke (GnomePrintContext *pc, const ArtBpath *bpath);
static int meta_image (GnomePrintContext *pc, const gdouble *affine, const guchar *px, gint w, gint h, gint rowstride, gint ch);
static int meta_glyphlist (GnomePrintContext *pc, const gdouble *affine, GnomeGlyphList *gl);
static int meta_close (GnomePrintContext *pc);

static void meta_color (GnomePrintContext *pc);
static void meta_line (GnomePrintContext *pc);
static void meta_dash (GnomePrintContext *pc);

static void gpm_close_page_header (GnomePrintMeta *pc);
static void gpm_encode_header (GnomePrintContext *pc, GnomeMetaFileHeader *mh);
static void gpm_encode_page_header (GnomePrintMeta *meta, GnomeMetaPageHeader *mh);
static void gpm_encode_string (GnomePrintContext *pc, const char *str);
static void gpm_encode_int (GnomePrintContext *pc, gint32 value);
static void gpm_encode_double (GnomePrintContext *pc, double d);
static void gpm_encode_int_header (GnomePrintContext *pc, gint32 value);
static void gpm_encode_block (GnomePrintContext *pc, int size, void const *data);
static gboolean gpm_ensure_space (GnomePrintMeta *meta, int size);
static void gpm_encode_bpath (GnomePrintContext *pc, const ArtBpath *bpath);
static const char *gpm_decode_bpath (const char *data, ArtBpath **bpath);

GType
gnome_print_meta_get_type (void)
{
	static GType meta_type = 0;
	if (!meta_type) {
		static const GTypeInfo meta_info = {
			sizeof (GnomePrintMetaClass),
			NULL, NULL,
			(GClassInitFunc) gnome_print_meta_class_init,
			NULL, NULL,
			sizeof (GnomePrintMeta),
			0,
			(GInstanceInitFunc) gnome_print_meta_init
		};
		meta_type = g_type_register_static (GNOME_TYPE_PRINT_CONTEXT, "GnomePrintMeta", &meta_info, 0);
	}

	return meta_type;
}

static void
gnome_print_meta_class_init (GnomePrintMetaClass *klass)
{
	GObjectClass *object_class;
	GnomePrintContextClass *pc_class;

	object_class = (GObjectClass *) klass;
	pc_class = (GnomePrintContextClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = meta_finalize;

	pc_class->beginpage = meta_beginpage;
	pc_class->showpage = meta_showpage;

	pc_class->gsave = meta_gsave;
	pc_class->grestore = meta_grestore;

	pc_class->clip = meta_clip;
	pc_class->fill = meta_fill;
	pc_class->stroke = meta_stroke;

	pc_class->image = meta_image;

	pc_class->glyphlist = meta_glyphlist;

	pc_class->close = meta_close;
}

static void
gnome_print_meta_init (GnomePrintMeta *meta)
{
	GnomeMetaFileHeader header = {GNOME_METAFILE_SIGNATURE, -1};

	meta->buffer_size = BLOCKSIZE;
	meta->buffer = g_malloc (meta->buffer_size);
	meta->active_page = -1;
	gpm_encode_header (GNOME_PRINT_CONTEXT (meta), &header);
}

static void
meta_finalize (GObject *object)
{
	GnomePrintMeta *meta;

	meta = GNOME_PRINT_META (object);

	g_free (meta->buffer);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static int
meta_beginpage (GnomePrintContext *pc, const guchar *name)
{
	static GnomeMetaPageHeader page_header = {PAGE_SIGNATURE, -1};
	GnomePrintMeta *meta;

	meta = GNOME_PRINT_META (pc);

	gpm_encode_page_header (meta, &page_header);
	meta->pages += 1;

	gpm_encode_int (pc, GNOME_META_BEGINPAGE);
	gpm_encode_string (pc, name);

	gp_gc_set_ctm_flag (pc->gc, GP_GC_FLAG_UNSET);
	gp_gc_set_color_flag (pc->gc, GP_GC_FLAG_UNSET);
	gp_gc_set_line_flag (pc->gc, GP_GC_FLAG_UNSET);
	gp_gc_set_dash_flag (pc->gc, GP_GC_FLAG_UNSET);

	return GNOME_PRINT_OK;
}

static int
meta_showpage (GnomePrintContext *pc)
{
	gpm_encode_int (pc, GNOME_META_SHOWPAGE);

	gpm_close_page_header (GNOME_PRINT_META (pc));

	return GNOME_PRINT_OK;
}

static int
meta_gsave (GnomePrintContext *pc)
{
	gpm_encode_int (pc, GNOME_META_GSAVE);

	return GNOME_PRINT_OK;
}

static int
meta_grestore (GnomePrintContext *pc)
{
	gpm_encode_int (pc, GNOME_META_GRESTORE);

	return GNOME_PRINT_OK;
}

static int
meta_clip (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule)
{
	gpm_encode_int (pc, GNOME_META_CLIP);
	gpm_encode_bpath (pc, bpath);
	gpm_encode_int (pc, rule);

	return GNOME_PRINT_OK;
}

static int
meta_fill (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule)
{
	meta_color (pc);

	gpm_encode_int (pc, GNOME_META_FILL);
	gpm_encode_bpath (pc, bpath);
	gpm_encode_int (pc, rule);

	return GNOME_PRINT_OK;
}

static int
meta_stroke (GnomePrintContext *pc, const ArtBpath *bpath)
{
	meta_color (pc);
	meta_line (pc);
	meta_dash (pc);

	gpm_encode_int (pc, GNOME_META_STROKE);
	gpm_encode_bpath (pc, bpath);

	return GNOME_PRINT_OK;
}

static int
meta_image (GnomePrintContext *pc, const gdouble *affine, const guchar *px, gint w, gint h, gint rowstride, gint ch)
{
	int i, y;

	gpm_encode_int (pc, GNOME_META_IMAGE);
	for (i = 0; i < 6; i++) gpm_encode_double (pc, affine[i]);
	gpm_encode_int (pc, h);
	gpm_encode_int (pc, w);
	gpm_encode_int (pc, ch);

	for (y = 0; y < h; y++){
		gpm_encode_block (pc, w * ch, px);
		px += rowstride;
	}

	return GNOME_PRINT_OK;
}

static int
meta_glyphlist (GnomePrintContext *pc, const gdouble *affine, GnomeGlyphList *gl)
{
	gint i;

	gpm_encode_int (pc, GNOME_META_GLYPHLIST);
	for (i = 0; i < 6; i++) gpm_encode_double (pc, affine[i]);

	gpm_encode_int (pc, gl->g_length);
	for (i = 0; i < gl->g_length; i++) {
		gpm_encode_int (pc, gl->glyphs[i]);
	}
	gpm_encode_int (pc, gl->r_length);
	for (i = 0; i < gl->r_length; i++) {
		gpm_encode_int (pc, gl->rules[i].code);
		switch (gl->rules[i].code) {
		case GGL_POSITION:
		case GGL_ADVANCE:
		case GGL_COLOR:
			gpm_encode_int (pc, gl->rules[i].value.ival);
			break;
		case GGL_MOVETOX:
		case GGL_MOVETOY:
		case GGL_RMOVETOX:
		case GGL_RMOVETOY:
		case GGL_LETTERSPACE:
		case GGL_KERNING:
			gpm_encode_double (pc, gl->rules[i].value.dval);
			break;
		case GGL_FONT:
			gpm_encode_double (pc, gnome_font_get_size (gl->rules[i].value.font));
			gpm_encode_string (pc, gnome_font_get_name (gl->rules[i].value.font));
			break;
		case GGL_PUSHCP:
		case GGL_POPCP:
		default:
			break;
		}
	}

	return GNOME_PRINT_OK;
}

static int
meta_close (GnomePrintContext *pc)
{
	gint32 l;
	GnomePrintMeta *meta = GNOME_PRINT_META (pc);

	l = g_htonl (meta->buffer_size);

	memcpy (meta->buffer + GNOME_METAFILE_SIGNATURE_SIZE, &l, sizeof (l));

	return GNOME_PRINT_OK;
}

/* Private GPGC methods */

static void
meta_color (GnomePrintContext *pc)
{
	if (gp_gc_get_color_flag (pc->gc) != GP_GC_FLAG_CLEAR) {
		gpm_encode_int (pc, GNOME_META_COLOR);
		gpm_encode_double (pc, gp_gc_get_red (pc->gc));
		gpm_encode_double (pc, gp_gc_get_green (pc->gc));
		gpm_encode_double (pc, gp_gc_get_blue (pc->gc));
		gpm_encode_double (pc, gp_gc_get_opacity (pc->gc));
		gp_gc_set_color_flag (pc->gc, GP_GC_FLAG_CLEAR);
	}
}

static void
meta_line (GnomePrintContext *pc)
{
	if (gp_gc_get_line_flag (pc->gc) != GP_GC_FLAG_CLEAR) {
		gpm_encode_int (pc, GNOME_META_LINE);
		gpm_encode_double (pc, gp_gc_get_linewidth (pc->gc));
		gpm_encode_double (pc, gp_gc_get_miterlimit (pc->gc));
		gpm_encode_int (pc, gp_gc_get_linejoin (pc->gc));
		gpm_encode_int (pc, gp_gc_get_linecap (pc->gc));
		gp_gc_set_line_flag (pc->gc, GP_GC_FLAG_CLEAR);
	}
}

static void
meta_dash (GnomePrintContext *pc)
{
	if (gp_gc_get_dash_flag (pc->gc) != GP_GC_FLAG_CLEAR) {
		const ArtVpathDash *dash;
		gint i;
		dash = gp_gc_get_dash (pc->gc);
		gpm_encode_int (pc, GNOME_META_DASH);
		gpm_encode_int (pc, dash->n_dash);
		for (i = 0; i < dash->n_dash; i++) {
			gpm_encode_double (pc, dash->dash[i]);
		}
		gpm_encode_double (pc, dash->offset);
		gp_gc_set_dash_flag (pc->gc, GP_GC_FLAG_CLEAR);
	}
}

/**
 * gnome_print_meta_new:
 *
 * Creates a new Metafile context GnomePrint object.
 *
 * Returns: An empty %GnomePrint context that represents a
 * metafile priting context.
 */

GnomePrintMeta *
gnome_print_meta_new (void)
{
	GnomePrintMeta *meta;

	meta = g_object_new (GNOME_TYPE_PRINT_META, NULL);

	return meta;
}

/**
 * gnome_print_meta_new_from:
 * @data: Pointer to a gnome_print_meta metadata.
 *
 * Creates a new Metafile context GnomePrint object.
 *
 * Initializes the contents from a buffer that contains a
 * GNOME_METAFILE stream.
 *
 * Returns: A new %GnomePrint context that represented the Metafile
 * printing context.
 */

GnomePrintMeta *
gnome_print_meta_new_from (const void *data)
{
	GnomePrintMeta *meta;
	GnomeMetaFileHeader header;
	int len;

	g_return_val_if_fail (data != NULL, NULL);

	decode_header (data, &header);
	if (strncmp (header.signature, GNOME_METAFILE_SIGNATURE, GNOME_METAFILE_SIGNATURE_SIZE) != 0)
		return NULL;

	len = header.size;
	meta = g_object_new (GNOME_TYPE_PRINT_META, NULL);

	if (meta->buffer_size < len){
		g_free (meta->buffer);
		meta->buffer = g_malloc (len);

		if (!meta->buffer){
			g_object_unref (G_OBJECT (meta));
			return NULL;
		}
	}
	memcpy (meta->buffer, data, len);
	meta->current = len;
	return meta;
}

/**
 * gnome_print_meta_access_buffer:
 * @meta:
 * @buffer: points to a void * that will be modified to
 *          point to the new buffer
 * @buflen: pointer to an integer that will be set to the lenght of buffer.
 *
 * Makes buffer point to the internal GnomePrintMeta information.
 *
 * Returns 0 on a failure, any other value on success
 */
int
gnome_print_meta_access_buffer (GnomePrintMeta *meta, void **buffer, int *buflen)
{
	char *p;

	g_return_val_if_fail (meta != NULL, 0);
	g_return_val_if_fail (GNOME_IS_PRINT_META (meta), 0);

	p = *buffer = meta->buffer;
	*((gint *)(p + GNOME_METAFILE_SIGNATURE_SIZE)) = g_htonl (meta->current);
	*buflen = meta->current;

	return 1;
}

/**
 * gnome_print_meta_get_copy:
 * @meta:
 * @buffer: points to a void * that will be modified to
 *          point to the new buffer
 * @buflen: pointer to an integer that will be set to the lenght of buffer.
 *
 * Duplicates the internal buffer and stores a pointer to the block with
 * a copy of this in *@buffer.  The size of the buffer is stored in @buflen.
 *
 * Returns 0 on a failure, any other value on success
 */
int
gnome_print_meta_get_copy (GnomePrintMeta *meta, void **buffer, int *buflen)
{
	char *p;

	g_return_val_if_fail (meta != NULL, 0);
	g_return_val_if_fail (GNOME_IS_PRINT_META (meta), 0);

	p = *buffer = g_malloc (meta->current);
	if (*buffer == NULL)
		return 0;

	memcpy (p, meta->buffer, meta->buffer_size);
	*((gint *)(p + FILEHEADER_SIZE)) = g_htonl (meta->current);
	*buflen = meta->current;

	return 1;
}

/* less compact, but gauranteed size (just makes things easier to encode
   fixed size data like headers) */
static const char *
decode_int_header (const char *data, gint32 *dest)
{
	gint32 nint;
	memcpy (&nint, data, sizeof (gint32));
	*dest = g_ntohl (nint);
	return data + sizeof (gint32);
}

static const char *
decode_int (const char *data, gint32 *dest)
{
	guint32 vabs = 0, mask = 0x3f;
	int bits = 6, shift = 0;
	int neg;
	char c;

	neg = (*data & 0x40);
	do {
		vabs |= ((c = *data++) & mask) << shift;
		shift += bits;
		bits = 7;
		mask = 0x7f;
	} while ((c & 0x80) == 0);

	*dest = neg ? -vabs : vabs;
	return data;
}

static const char *
decode_double (const char *data, double *dest)
{
	int i;
	int sd = sizeof (double);
	data = decode_int (data, &i);

	switch ((GnomeMetaDoubleType)i) {
	case GNOME_META_DOUBLE_INT:
		data = decode_int (data, &i);
		*dest = i;
		break;
	case GNOME_META_DOUBLE_INT1000:
		data = decode_int (data, &i);
		*dest = i / 1000.0;
		break;
	case GNOME_META_DOUBLE_I386:
#if G_BYTE_ORDER == G_BIG_ENDIAN
		for (i = 0; i < sd; i++)
			((guint8 *)dest)[sd - 1 - i] = data[i];
		data += sd;
#elif G_BYTE_ORDER == G_LITTLE_ENDIAN
		memcpy (dest, data, sd);
		data += sd;
#else
#error decode_double_needs_attention
#endif
		break;
	default:
		*dest = 0;   /* ??? */
	}

	return data;
}

static const char *
decode_string (const char *data, char **dest)
{
	int len;
	data = decode_int (data, &len);
	*dest = g_malloc (len + 1);
	memcpy (*dest, data, len);
	(*dest)[len] = 0;
	return data + len;
}

static const char *
decode_block (const char *data, void *out, int len)
{
	memcpy (out, data, len);
	return data+len;
}

static const char *
decode_header (const char *data, GnomeMetaFileHeader *mh)
{
	data = decode_block (data, mh->signature, sizeof (mh->signature));
	data = decode_int_header (data, &mh->size);
	return data;
}

static const char *
decode_page_header (const char *data, GnomeMetaPageHeader *mh)
{
	data = decode_block (data, mh->signature, sizeof (mh->signature));
	data = decode_int_header (data, &mh->size);
	return data;
}

static const char *
locate_page_header (const char *meta_stream, int page)
{
	const char *data = meta_stream + FILEHEADER_SIZE, *next;
	GnomeMetaPageHeader page_header;

	next = decode_page_header (data, &page_header);
	while (page>0 && page_header.size != -1) {
		data = next + page_header.size;
		next = decode_page_header (data, &page_header);
		page--;
	}
	if (page_header.size == -1)
		return NULL;
	return data;
}

/*
  from the current point in the data stream, print pages pages, -1
  means print all
*/
static gboolean
do_render (GnomePrintContext *dest, const char *data, int size, int pages)
{
	const char *end;
	GnomeMetaPageHeader page_header;

	end = data + size;

	/*
	 * Output if all pages were requested, or the first page was
	 *
	 * Note that this assumes each page is completely described
	 * by its own data block.
	 */
	while (data < end){
		gint32 opcode, i;
		gchar *cval;
		gint32 ival;
		gdouble dval;
		ArtBpath *bpath;

		data = decode_int (data, &opcode);

		switch ((GnomeMetaType) opcode) {
		case GNOME_META_BEGINPAGE:
			data = decode_string (data, &cval);
			gnome_print_beginpage (dest, cval);
			g_free (cval);
			break;
		case GNOME_META_SHOWPAGE:
			gnome_print_showpage (dest);
			data = decode_page_header (data, &page_header);
			if (pages!=-1) {
				pages -= 1;
				if (pages<=0) return TRUE;
			}
			break;
		case GNOME_META_GSAVE:
			gnome_print_gsave (dest);
			break;
		case GNOME_META_GRESTORE:
			gnome_print_grestore (dest);
			break;
		case GNOME_META_CLIP:
			data = gpm_decode_bpath (data, &bpath);
			data = decode_int (data, &ival);
			gnome_print_clip_bpath_rule (dest, bpath, ival);
			g_free (bpath);
			break;
		case GNOME_META_FILL:
			data = gpm_decode_bpath (data, &bpath);
			data = decode_int (data, &ival);
			gnome_print_fill_bpath_rule (dest, bpath, ival);
			g_free (bpath);
			break;
		case GNOME_META_STROKE:
			data = gpm_decode_bpath (data, &bpath);
			gnome_print_stroke_bpath (dest, bpath);
			g_free (bpath);
			break;
		case GNOME_META_IMAGE: {
			gdouble affine[6];
			gint32 width, height, channels;
			guchar *buf;

			data = decode_double (data, &affine[0]);
			data = decode_double (data, &affine[1]);
			data = decode_double (data, &affine[2]);
			data = decode_double (data, &affine[3]);
			data = decode_double (data, &affine[4]);
			data = decode_double (data, &affine[5]);
			data = decode_int (data, &height);
			data = decode_int (data, &width);
			data = decode_int (data, &channels);
			buf = g_new (guchar, height * width * channels);
			memcpy (buf, data, height * width * channels);
			data += height * width * channels;
			gnome_print_image_transform (dest, affine, buf, width, height, channels * width, channels);
			g_free (buf);
			break;
		}
		case GNOME_META_GLYPHLIST: {
			GnomeGlyphList *gl;
			gdouble affine[6];
			gint32 len, code, ival, i;
			gdouble dval;

			data = decode_double (data, &affine[0]);
			data = decode_double (data, &affine[1]);
			data = decode_double (data, &affine[2]);
			data = decode_double (data, &affine[3]);
			data = decode_double (data, &affine[4]);
			data = decode_double (data, &affine[5]);
			gl = gnome_glyphlist_new ();
			data = decode_int (data, &len);
			if (len > 0) {
				gl->glyphs = g_new (int, len);
				gl->g_length = len;
				gl->g_size = len;
				for (i = 0; i < len; i++) {
					data = decode_int (data, &ival);
					gl->glyphs[i] = ival;
				}
			}
			data = decode_int (data, &len);
			if (len > 0) {
				gl->rules = g_new (GGLRule, len);
				gl->r_length = len;
				gl->r_size = len;
				for (i = 0; i < len; i++) {
					data = decode_int (data, &code);
					gl->rules[i].code = code;
					switch (code) {
					case GGL_POSITION:
					case GGL_ADVANCE:
					case GGL_COLOR:
						data = decode_int (data, &ival);
						gl->rules[i].value.ival = ival;
						break;
					case GGL_MOVETOX:
					case GGL_MOVETOY:
					case GGL_RMOVETOX:
					case GGL_RMOVETOY:
					case GGL_LETTERSPACE:
					case GGL_KERNING:
						data = decode_double (data, &dval);
						gl->rules[i].value.dval = dval;
						break;
					case GGL_FONT: {
						GnomeFont *font;
						char *name;
						data = decode_double (data, &dval);
						data = decode_string (data, &name);
						font = gnome_font_find (name, dval);
						if (font == NULL) g_print ("Cannot find font: %s\n", name);
						g_free (name);
						gl->rules[i].value.font = font;
						break;
					}
					case GGL_PUSHCP:
					case GGL_POPCP:
					default:
						break;
					}
				}
			}
			gnome_print_glyphlist_transform (dest, affine, gl);
			gnome_glyphlist_unref (gl);
			break;
		}
		case GNOME_META_COLOR: {
			gdouble r, g, b, a;
			data = decode_double (data, &r);
			data = decode_double (data, &g);
			data = decode_double (data, &b);
			gnome_print_setrgbcolor (dest, r, g, b);
			data = decode_double (data, &a);
			gnome_print_setopacity (dest, a);
			break;
		}
		case GNOME_META_LINE:
			data = decode_double (data, &dval);
			gnome_print_setlinewidth (dest, dval);
			data = decode_double (data, &dval);
			gnome_print_setmiterlimit (dest, dval);
			data = decode_int (data, &ival);
			gnome_print_setlinejoin (dest, ival);
			data = decode_int (data, &ival);
			gnome_print_setlinecap (dest, ival);
			break;
		case GNOME_META_DASH: {
			int n;
			double *values, offset;

			data = decode_int (data, &n);
			values = g_new (double, n);
			for (i = 0; i < n; i++) {
				data = decode_double (data, &values [i]);
			}
			data = decode_double (data, &offset);
			gnome_print_setdash (dest, n, values, offset);
			g_free (values);
			break;
		}
		default:
			g_warning ("Serious print meta data corruption %d", opcode);
			break;
		}
	}
	return TRUE;
}

/**
 * gnome_print_meta_render:
 * @destination: Destination printer context.
 * @meta_stream: a metadata stream to replay
 *
 * Plays the @meta_steam metadata stream into the @destination printer.
 *
 * Returns TRUE on success.
 */
gboolean
gnome_print_meta_render (GnomePrintContext *destination, const void *meta_stream)
{
	const char *data;
	GnomeMetaFileHeader header;

	g_return_val_if_fail (destination != NULL, FALSE);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (destination), FALSE);
	g_return_val_if_fail (meta_stream != NULL, FALSE);

	data = meta_stream;
	data = decode_header (data, &header);
	if (strncmp (header.signature, GNOME_METAFILE_SIGNATURE, GNOME_METAFILE_SIGNATURE_SIZE) != 0)
		return FALSE;

	if (header.size == -1){
		g_warning ("This stream was not closed");
		return FALSE;
	}
	return do_render (destination, data + PAGEHEADER_SIZE,
			  header.size, -1);
}

/**
 * gnome_print_meta_render_from_object:
 * @destination: Destination printer context.
 * @source: an existing GnomePrintMeta printer context.
 *
 * Plays the contents of the GnomePrintMeta @source metadata stream
 * into the @destination printer.
 *
 * Returns: TRUE on success.
 */
gboolean
gnome_print_meta_render_from_object (GnomePrintContext *destination, const GnomePrintMeta *source)
{
#if 1
	const char * data;
	GnomeMetaFileHeader header;
#endif
	g_return_val_if_fail (destination != NULL, FALSE);
	g_return_val_if_fail (source != NULL, FALSE);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (destination), FALSE);
	g_return_val_if_fail (GNOME_IS_PRINT_META (source), FALSE);

#if 0
	return gnome_print_meta_render (destination, source->buffer);
#else
	/* Render even unclosed metafiles */

	data = source->buffer;
	data = decode_header (data, &header);

	if (strncmp (header.signature, GNOME_METAFILE_SIGNATURE, GNOME_METAFILE_SIGNATURE_SIZE) != 0)
		return FALSE;

	return do_render (destination, data + PAGEHEADER_SIZE,
			  source->current - FILEHEADER_SIZE - PAGEHEADER_SIZE, -1);
#endif
}

/**
 * gnome_print_meta_render_page:
 * @destination: Destination printer context.
 * @meta_stream: a metadata stream to replay
 * @page: Page to be rendered.
 *
 * Plays the @meta_steam metadata stream into the @destination printer.
 * Output commands will only take place for page @page.
 *
 * Returns TRUE on success.
 */
gboolean
gnome_print_meta_render_page (GnomePrintContext *destination, const void *meta_stream, int page)
{
	const char *data;
	GnomeMetaFileHeader header;
	const char *page_data;

	g_return_val_if_fail (destination != NULL, FALSE);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (destination), FALSE);
	g_return_val_if_fail (meta_stream != NULL, FALSE);

	data = meta_stream;
	data = decode_header (data, &header);
	if (strncmp (header.signature, GNOME_METAFILE_SIGNATURE, GNOME_METAFILE_SIGNATURE_SIZE) != 0)
		return FALSE;

	if (header.size == -1){
		g_warning ("This printing context has not been closed");
		return FALSE;
	}

	page_data = locate_page_header (meta_stream, page);
	if (page_data == NULL) {
		g_warning ("Trying to print a non-existant page");
		return FALSE;
	}
	return do_render (destination, page_data + PAGEHEADER_SIZE,
			  header.size, 1);
}

/**
 * gnome_print_meta_render_from_object_page:
 * @destination: Destination printer context.
 * @source: an existing GnomePrintMeta printer context.
 * @page: Page to be rendered.
 *
 * Plays the contents of the GnomePrintMeta @source metadata stream
 * into the @destination printer.  Output commands will only take place for page @page.
 *
 * Returns: TRUE on success.
 */
gboolean
gnome_print_meta_render_from_object_page (GnomePrintContext *destination,
					  const GnomePrintMeta *source,
					  int page)
{
	g_return_val_if_fail (destination != NULL, FALSE);
	g_return_val_if_fail (source != NULL, FALSE);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (destination), FALSE);
	g_return_val_if_fail (GNOME_IS_PRINT_META (source), FALSE);

	return gnome_print_meta_render_page (destination, source->buffer, page);
}

int
gnome_print_meta_pages (const GnomePrintMeta *meta)
{
	g_return_val_if_fail (meta != NULL, 0);
	g_return_val_if_fail (GNOME_IS_PRINT_META (meta), 0);

	return meta->pages;
}

/* sets the size of the last page */
static void
gpm_close_page_header (GnomePrintMeta *pc)
{
	if (pc->active_page != -1) {
		gint32 l;
		l = g_htonl (pc->current - pc->active_page - PAGEHEADER_SIZE);
		/* we cannot map the page header back into memory, so poke it back in */
		/* Note: unaligned access -- MW.  */
		memcpy ((pc->buffer + pc->active_page) + 4, &l, sizeof (l));
	}
}

static void
gpm_encode_page_header (GnomePrintMeta *meta, GnomeMetaPageHeader *mh)
{
	gpm_close_page_header (meta);
	meta->active_page = meta->current;
	gpm_encode_block ((GnomePrintContext *) meta, sizeof (mh->signature), mh->signature);
	gpm_encode_int_header ((GnomePrintContext *) meta, mh->size);
}

static void
gpm_encode_header (GnomePrintContext *pc, GnomeMetaFileHeader *mh)
{
	gpm_encode_block (pc, sizeof (mh->signature), mh->signature);
	gpm_encode_int_header (pc, mh->size);
}

static void
gpm_encode_string (GnomePrintContext *pc, const char *str)
{
	gint bytes;

	bytes = strlen (str);

	gpm_encode_int (pc, bytes);
	gpm_encode_block (pc, bytes, str);
}

static void
gpm_encode_int (GnomePrintContext *pc, gint32 value)
{
	GnomePrintMeta *meta;
	char *out, *out0;
	guint32 vabs, mask;
	int bits;

	meta = (GnomePrintMeta *) pc;

	g_return_if_fail (GPM_ENSURE_SPACE (meta, sizeof (value) * 8 / 7 + 1));

	/*
	 * We encode an integer as a sequence of bytes where all but the
	 * last have the high bit set to zero.  The final byte does have
	 * that bit set.
	 *
	 * Bit 6 of the first byte contains the sign bit.
	 *
	 * The remaining 6, 7, ..., 7 bits contain the absolute value,
	 * starting with the six least significant bits in the first
	 * byte.
	 */

	out0 = out = meta->buffer + meta->current;
	vabs = (value >= 0) ? value : -value;
	bits = 6;
	mask = 0x3f;

	do {
		*out++ = (vabs & mask);
		vabs >>= bits;
		bits = 7;
		mask = 0x7f;
	} while (vabs);

	out[-1] |= 0x80;
	if (value < 0) out0[0] |= 0x40;
	meta->current = out - meta->buffer;
}

static void
gpm_encode_double (GnomePrintContext *pc, double d)
{
	int sd = sizeof (d);

	if (d == (gint32)d) {
		gpm_encode_int (pc, GNOME_META_DOUBLE_INT);
		gpm_encode_int (pc, (int)d);
	} else {
		double d1000 = d * 1000;
		if (d1000 == (gint32)d1000) {
			gpm_encode_int (pc, GNOME_META_DOUBLE_INT1000);
			gpm_encode_int (pc, (int)d1000);
		} else {
			gpm_encode_int (pc, GNOME_META_DOUBLE_I386);
#if G_BYTE_ORDER == G_BIG_ENDIAN
			{
				int     i;
				guint8 *t  = (guint8 *)&d;
				guint8 block[sizeof (d)];

				for (i = 0; i < sd; i++)
					block[sd - 1 - i] = t[i];
				gpm_encode_block (pc, sd, &block);
			}
#elif G_BYTE_ORDER == G_LITTLE_ENDIAN
			gpm_encode_block (pc, sd, &d);
#else
#error encode_double_needs_attention
#endif
		}
	}
}

static void
gpm_encode_int_header (GnomePrintContext *pc, gint32 value)
{
	gint32 new;

	new = g_htonl (value);
	gpm_encode_block (pc, sizeof (gint32), &new);
}

static void
gpm_encode_block (GnomePrintContext *pc, int size, void const *data)
{
	GnomePrintMeta *meta;

	meta = (GnomePrintMeta *) pc;

	g_return_if_fail (GPM_ENSURE_SPACE (meta, size));

	memcpy (meta->buffer + meta->current, data, size);
	meta->current += size;
}

static gboolean
gpm_ensure_space (GnomePrintMeta *meta, int size)
{
	int req;
	guchar *new;

	req = MAX (BLOCKSIZE, meta->current + size - meta->buffer_size);

	new = g_realloc (meta->buffer, meta->buffer_size + req);
	g_return_val_if_fail (new != NULL, FALSE);

	meta->buffer = new;
	meta->buffer_size = meta->buffer_size + req;

	return TRUE;
}

static void
gpm_encode_bpath (GnomePrintContext *pc, const ArtBpath *bpath)
{
	gint len;

	len = 0;
	while (bpath[len].code != ART_END) len++;
	gpm_encode_int (pc, len + 1);

	while (bpath->code != ART_END) {
		gpm_encode_int (pc, bpath->code);
		switch (bpath->code) {
		case ART_CURVETO:
			gpm_encode_double (pc, bpath->x1);
			gpm_encode_double (pc, bpath->y1);
			gpm_encode_double (pc, bpath->x2);
			gpm_encode_double (pc, bpath->y2);
		case ART_MOVETO:
		case ART_MOVETO_OPEN:
		case ART_LINETO:
			gpm_encode_double (pc, bpath->x3);
			gpm_encode_double (pc, bpath->y3);
			break;
		default:
			g_warning ("Illegal pathcode in Bpath");
			break;
		}
		bpath += 1;
	}
	gpm_encode_int (pc, ART_END);
}

static const char *
gpm_decode_bpath (const char *data, ArtBpath **bpath)
{
	ArtBpath *p;
	gint32 len, code;

	data = decode_int (data, &len);
	*bpath = g_new (ArtBpath, len);

	p = *bpath;
	data = decode_int (data, &code);
	while (code != ART_END) {
		p->code = code;
		switch (code) {
		case ART_CURVETO:
			data = decode_double (data, &p->x1);
			data = decode_double (data, &p->y1);
			data = decode_double (data, &p->x2);
			data = decode_double (data, &p->y2);
		case ART_MOVETO:
		case ART_MOVETO_OPEN:
		case ART_LINETO:
			data = decode_double (data, &p->x3);
			data = decode_double (data, &p->y3);
			break;
		default:
			g_warning ("Illegal pathcode %d", code);
			break;
		}
		p += 1;
		data = decode_int (data, &code);
	}
	p->code = ART_END;

	return data;
}
