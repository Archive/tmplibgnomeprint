#define __GNOME_PRINT_C__

/*
 *  Copyright (C) 1999-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Raph Levien (raph@acm.org)
 *    Miguel de Icaza (miguel@kernel.org)
 *    Lauris Kaplinski <lauris@ximian.com>
 *    Chema Celorio (chema@celorio.com)
 *
 *  Abstract base class of gnome-print drivers
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <string.h>
#include <gmodule.h>

#include "gnome-print-i18n.h"
#include "gnome-print-private.h"

#include "gnome-print-ps2.h"
#include "gnome-print-frgba.h"

static void gnome_print_context_class_init (GnomePrintContextClass *klass);
static void gnome_print_context_init (GnomePrintContext *ctx);

static void gnome_print_context_finalize (GObject *object);

static GnomePrintContext *gnome_print_context_create (gpointer get_type, GPANode *config);

static GObjectClass *parent_class = NULL;

GType
gnome_print_context_get_type (void)
{
	static GType ctx_type = 0;
	if (!ctx_type) {
		static const GTypeInfo ctx_info = {
			sizeof (GnomePrintContextClass),
			NULL, NULL,
			(GClassInitFunc) gnome_print_context_class_init,
			NULL, NULL,
			sizeof (GnomePrintContext),
			0,
			(GInstanceInitFunc) gnome_print_context_init
		};
		ctx_type = g_type_register_static (G_TYPE_OBJECT, "GnomePrintContext", &ctx_info, 0);
	}
	return ctx_type;
}

static void
gnome_print_context_class_init (GnomePrintContextClass *klass)
{
	GObjectClass *object_class;

	object_class = (GObjectClass*) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_print_context_finalize;
}

static void
gnome_print_context_init (GnomePrintContext *ctx)
{
	ctx->config = NULL;
	ctx->transport = NULL;

	ctx->gc = gp_gc_new ();
	ctx->haspage = FALSE;
}

static void
gnome_print_context_finalize (GObject *object)
{
	GnomePrintContext *ctx;

	ctx = GNOME_PRINT_CONTEXT (object);

	if (ctx->transport) {
		g_warning ("file %s: line %d: Destorying Context with open transport", __FILE__, __LINE__);
		g_object_unref (G_OBJECT (ctx->transport));
		ctx->transport = NULL;
	}

	if (ctx->config) {
		gpa_node_unref (ctx->config);
		ctx->config = NULL;
	}

	gp_gc_unref (ctx->gc);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

gint
gnome_print_context_construct (GnomePrintContext *ctx, GPANode *config)
{
	g_return_val_if_fail (ctx != NULL, GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (ctx), GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (config != NULL, GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (GPA_IS_NODE (config), GNOME_PRINT_ERROR_UNKNOWN);

	g_return_val_if_fail (ctx->config == NULL, GNOME_PRINT_ERROR_UNKNOWN);

	ctx->config = config;
	gpa_node_ref (ctx->config);

	if (GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->construct)
		GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->construct (ctx);

	return GNOME_PRINT_OK;
}

gint
gnome_print_context_create_transport (GnomePrintContext *ctx)
{
	g_return_val_if_fail (ctx != NULL, GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (ctx), GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (ctx->config != NULL, GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (ctx->transport == NULL, GNOME_PRINT_ERROR_UNKNOWN);

	ctx->transport = gnome_print_transport_new (ctx->config);
	g_return_val_if_fail (ctx->transport != NULL, GNOME_PRINT_ERROR_UNKNOWN);

	return GNOME_PRINT_OK;
}

/* Direct class-method frontends */

/**
 * gnome_print_beginpage:
 * @ctx: A #GnomePrintContext
 * @name: Name of the page
 *
 * starts new output page with @name. Naming is used for interactive
 * contexts like #GnomePrintPreview and Document Structuring Convention
 * conformant PostScript output.
 * This function has to be called before any drawing methods and immediately
 * after each #gnome_print_showpage albeit the last one. It also resets
 * graphic state values (transformation, color, line properties, font),
 * so one has to define these again at the beginning of each page.
 *
 * Returns: #GNOME_PRINT_OK or positive value on success, negative error
 * code on failure.
 */
int
gnome_print_beginpage (GnomePrintContext *ctx, const guchar *name)
{
	g_return_val_if_fail (ctx != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (ctx), GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (ctx->gc != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (!ctx->haspage, GNOME_PRINT_ERROR_NOMATCH);

	gp_gc_reset (ctx->gc);
	ctx->haspage = TRUE;

	if (GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->beginpage)
		GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->beginpage (ctx, name);

	return GNOME_PRINT_OK;
}

/**
 * gnome_print_showpage:
 * @ctx: A #GnomePrintContext
 *
 * Finishes rendering of current page, and marks it as shown. All subsequent
 * drawing methods will fail, until new page is started with #gnome_print_newpage.
 * Printing contexts may process drawing methods differently - some do
 * rendering immediately (like #GnomePrintPreview), some accumulate all
 * operators to internal stack, and only after #gnome_print_showpage is
 * any output produced.
 *
 * Returns: #GNOME_PRINT_OK or positive value on success, negative error
 * code on failure.
 */
int
gnome_print_showpage (GnomePrintContext *ctx)
{
	gint ret;

	g_return_val_if_fail (ctx != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (ctx), GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (ctx->gc != NULL, GNOME_PRINT_ERROR_BADCONTEXT);

#ifdef ALLOW_BROKEN_PGL
	if (!ctx->haspage) {
		g_warning ("file %s: line %d: Missing beginpage in print job", __FILE__, __LINE__);
		gnome_print_beginpage (ctx, "Unnamed");
	}
#else
	g_return_val_if_fail (ctx->haspage, GNOME_PRINT_ERROR_NOPAGE);
#endif

	ret = GNOME_PRINT_OK;

	if (GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->showpage)
		GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->showpage (ctx);

	ctx->haspage = FALSE;

	return ret;
}

/**
 * gnome_print_gsave:
 * @ctx: A #GnomePrintContext
 *
 * Saves current graphic state (transformation, color, line properties, font)
 * into stack (push). Values itself remain unchanged.
 * You can later restore saved values, using #gnome_print_grestore, but not
 * over page boundaries. Graphic state stack has to be cleared for each
 * #gnome_print_showpage, i.e. the number of #gnome_print_gsave has to
 * match the number of #gnome_print_grestore for each page.
 *
 * Returns: #GNOME_PRINT_OK or positive value on success, negative error
 * code on failure.
 */
int
gnome_print_gsave (GnomePrintContext *ctx)
{
	gint ret;

	g_return_val_if_fail (ctx != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (ctx), GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (ctx->gc != NULL, GNOME_PRINT_ERROR_BADCONTEXT);

#ifdef ALLOW_BROKEN_PGL
	if (!ctx->haspage) {
		g_warning ("file %s: line %d: Missing beginpage in print job", __FILE__, __LINE__);
		gnome_print_beginpage (ctx, "Unnamed");
	}
#else
	g_return_val_if_fail (ctx->haspage, GNOME_PRINT_ERROR_NOPAGE);
#endif

	ret = GNOME_PRINT_OK;

	if (GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->gsave)
		GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->gsave (ctx);

	gp_gc_gsave (ctx->gc);

	return ret;
}

/**
 * gnome_print_grestore:
 * @ctx: A #GnomePrintContext
 *
 * Retrieves last saved graphic state from stack (pop). Stack has to be
 * at least the size of one.
 *
 * Returns: #GNOME_PRINT_OK or positive value on success, negative error
 * code on failure.
 */
int
gnome_print_grestore (GnomePrintContext *ctx)
{
	gint ret;

	g_return_val_if_fail (ctx != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (ctx), GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (ctx->gc != NULL, GNOME_PRINT_ERROR_BADCONTEXT);

#ifdef ALLOW_BROKEN_PGL
	if (!ctx->haspage) {
		g_warning ("file %s: line %d: Missing beginpage in print job", __FILE__, __LINE__);
		gnome_print_beginpage (ctx, "Unnamed");
	}
#else
	g_return_val_if_fail (ctx->haspage, GNOME_PRINT_ERROR_NOPAGE);
#endif

	ret = GNOME_PRINT_OK;

	if (GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->grestore)
		GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->grestore (ctx);

	gp_gc_grestore (ctx->gc);

	return ret;
}

int
gnome_print_clip_bpath_rule (GnomePrintContext *ctx, const ArtBpath *bpath, ArtWindRule rule)
{
	g_return_val_if_fail (ctx != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (ctx), GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (ctx->gc != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (ctx->haspage, GNOME_PRINT_ERROR_NOPAGE);
	g_return_val_if_fail ((rule == ART_WIND_RULE_NONZERO) || (rule == ART_WIND_RULE_ODDEVEN), GNOME_PRINT_ERROR_BADVALUE);

	if (GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->clip)
		GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->clip (ctx, bpath, rule);

	return GNOME_PRINT_OK;
}

int
gnome_print_fill_bpath_rule (GnomePrintContext *ctx, const ArtBpath *bpath, ArtWindRule rule)
{
	g_return_val_if_fail (ctx != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (ctx), GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (ctx->gc != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (ctx->haspage, GNOME_PRINT_ERROR_NOPAGE);
	g_return_val_if_fail ((rule == ART_WIND_RULE_NONZERO) || (rule == ART_WIND_RULE_ODDEVEN), GNOME_PRINT_ERROR_BADVALUE);

	if (GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->fill)
		GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->fill (ctx, bpath, rule);

	return GNOME_PRINT_OK;
}

int
gnome_print_stroke_bpath (GnomePrintContext *ctx, const ArtBpath *bpath)
{
	g_return_val_if_fail (ctx != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (ctx), GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (ctx->gc != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (ctx->haspage, GNOME_PRINT_ERROR_NOPAGE);
	g_return_val_if_fail (bpath != NULL, GNOME_PRINT_ERROR_BADVALUE);

	if (GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->stroke)
		GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->stroke (ctx, bpath);

	return GNOME_PRINT_OK;
}

int
gnome_print_image_transform (GnomePrintContext *ctx, const gdouble *affine, const guchar *px, gint w, gint h, gint rowstride, gint ch)
{
	g_return_val_if_fail (ctx != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (ctx), GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (ctx->gc != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (ctx->haspage, GNOME_PRINT_ERROR_NOPAGE);
	g_return_val_if_fail (affine != NULL, GNOME_PRINT_ERROR_BADVALUE);
	g_return_val_if_fail (px != NULL, GNOME_PRINT_ERROR_BADVALUE);
	g_return_val_if_fail (w > 0, GNOME_PRINT_ERROR_BADVALUE);
	g_return_val_if_fail (h > 0, GNOME_PRINT_ERROR_BADVALUE);
	g_return_val_if_fail (rowstride >= ch * w, GNOME_PRINT_ERROR_BADVALUE);
	g_return_val_if_fail ((ch == 1) || (ch == 3) || (ch == 4), GNOME_PRINT_ERROR_BADVALUE);

	if (GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->image)
		GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->image (ctx, affine, px, w, h, rowstride, ch);

	return GNOME_PRINT_OK;
}

int
gnome_print_glyphlist_transform (GnomePrintContext *ctx, const gdouble *affine, GnomeGlyphList *gl)
{
	g_return_val_if_fail (ctx != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (ctx), GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (ctx->gc != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (ctx->haspage, GNOME_PRINT_ERROR_NOPAGE);
	g_return_val_if_fail (affine != NULL, GNOME_PRINT_ERROR_BADVALUE);
	g_return_val_if_fail (gl != NULL, GNOME_PRINT_ERROR_BADVALUE);
	g_return_val_if_fail (GNOME_IS_GLYPHLIST (gl), GNOME_PRINT_ERROR_BADVALUE);

	if (GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->glyphlist)
		GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->glyphlist (ctx, affine, gl);

	return GNOME_PRINT_OK;
}

/**
 * gnome_print_context_close:
 * @ctx: A #GnomePrintContext
 *
 * Informs given #GnomePrintContext that application has finished print
 * job. From that point on, @ctx has to be considered illegal pointer,
 * and any further printing operation with it may kill application.
 * Some printing contexts may not start printing before context is
 * closed.
 *
 * Returns: #GNOME_PRINT_OK or positive value on success, negative error
 * code on failure.
 */
int
gnome_print_context_close (GnomePrintContext *ctx)
{
	gint ret;

	g_return_val_if_fail (ctx != NULL, GNOME_PRINT_ERROR_BADCONTEXT);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (ctx), GNOME_PRINT_ERROR_BADCONTEXT);

	ret = GNOME_PRINT_OK;

	if (GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->close)
		GNOME_PRINT_CONTEXT_GET_CLASS (ctx)->close (ctx);

	g_return_val_if_fail (ret == GNOME_PRINT_OK, GNOME_PRINT_ERROR_UNKNOWN);

	if (ctx->transport) {
		g_warning ("file %s: line %d: Closing Context did not clear transport", __FILE__, __LINE__);
		return GNOME_PRINT_ERROR_UNKNOWN;
	}

	return ret;
}

/**
 * gnome_print_context_new:
 * @printer: Selected #GnomePrinter
 *
 * This method gives you new #GnomePrintContext object, associated with given
 * #GnomePrinter. You should use the resulting object as 'black box', without
 * assuming anything about it's type, as depending on situation appropriate
 * wrapper context may be used instead of direct driver.
 *
 * Returns: The new #GnomePrintContext or %NULL, if there is an error
 */

GnomePrintContext *
gnome_print_context_new (GnomePrintConfig *config)
{
	GnomePrintContext *ctx;
	GPANode *cfgnode;
	guchar *drivername;

	g_return_val_if_fail (config != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (config), NULL);

	cfgnode = GPA_NODE (config);

	drivername = gpa_node_get_path_value (cfgnode, "Settings.Engine.Backend.Driver");
	g_return_val_if_fail (drivername != NULL, NULL);

	ctx = NULL;

	if (!strcmp (drivername, "gnome-print-ps")) {
		GnomePrintContext *ps;
		ps = gnome_print_ps2_new (cfgnode);
		ctx = gnome_print_frgba_new (ps);
		g_object_unref (G_OBJECT (ps));
	} else {
		guchar *modulename;
		modulename = gpa_node_get_path_value (cfgnode, "Settings.Engine.Backend.Driver.Module");
		if (modulename) {
			GModule *module;
			gchar *path;
			path = g_module_build_path (GNOME_PRINT_LIBDIR "/drivers", modulename);
			module = g_module_open (path, G_MODULE_BIND_LAZY);
			if (module) {
				gpointer get_type;
				if (g_module_symbol (module, "gnome_print__driver_get_type", &get_type)) {
					ctx = gnome_print_context_create (get_type, cfgnode);
				} else {
					g_warning ("Missing gnome_print__driver_get_type in %s\n", modulename);
					g_module_close (module);
				}
			} else {
				g_warning ("Cannot open module: %s\n", modulename);
			}
			g_free (path);
			g_free (modulename);
		} else {
			g_warning ("Unknown driver: %s", drivername);
		}
	}

	g_free (drivername);

	return ctx;
}

static GnomePrintContext *
gnome_print_context_create (gpointer get_type, GPANode *config)
{
	GnomePrintContext *ctx;
	GType (* driver_get_type) (void);
	GType type;

	driver_get_type = get_type;

	type = (* driver_get_type) ();
	g_return_val_if_fail (g_type_is_a (type, GNOME_TYPE_PRINT_CONTEXT), NULL);

	ctx = g_object_new (type, NULL);
	gnome_print_context_construct (ctx, config);

	return ctx;
}

