#ifndef __GNOME_FONT_H__
#define __GNOME_FONT_H__

/*
 *  Copyright (C) 2000-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Jody Goldberg <jody@ximian.com>
 *    Miguel de Icaza <miguel@ximian.com>
 *    Lauris Kaplinski <lauris@ximian.com>
 *    Christopher James Lahey <clahey@ximian.com>
 *    Michael Meeks <michael@ximian.com>
 *    Morten Welinder <terra@diku.dk>
 *
 *  GnomeFont - basic user visible handle to scaled typeface
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * I am not sure whether to preserve these, but they remain until binary break (Lauris)
 *
 * The following arguments are available:
 *
 * name		       type		read/write	description
 * --------------------------------------------------------------------------------
 * ascender            double           R               maximum ascender for the face
 * descender           double           R               maximum descender for the face
 * underline_position  double           R               underline position for the face
 * underline_thickness double           R               underline thickness for the face
 *
 */

#include <glib.h>

G_BEGIN_DECLS

#define GNOME_TYPE_FONT (gnome_font_get_type ())
#define GNOME_FONT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOME_TYPE_FONT, GnomeFont))
#define GNOME_IS_FONT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOME_TYPE_FONT))

typedef struct _GnomeFont GnomeFont;

#include <libgnomeprint/gnome-font-face.h>

GType gnome_font_get_type (void);

#define gnome_font_ref(f) g_object_ref (G_OBJECT (f))
#define gnome_font_unref(f) g_object_unref (G_OBJECT (f))

/*
 * Methods
 *
 * Look into gnome-font-face for explanations
 */

/* Naming */
const gchar *gnome_font_get_name (const GnomeFont *font);
const gchar *gnome_font_get_family_name (const GnomeFont *font);
const gchar *gnome_font_get_species_name (const GnomeFont *font);
const gchar *gnome_font_get_ps_name (const GnomeFont *font);

/* Unicode -> glyph translation */
gint gnome_font_lookup_default (const GnomeFont *font, gint unicode);

/*
 * Metrics
 *
 * Note that GnomeFont metrics are given in typographic points
 */
ArtPoint *gnome_font_get_glyph_stdadvance (const GnomeFont *font, gint glyph, ArtPoint * advance);
ArtDRect *gnome_font_get_glyph_stdbbox (const GnomeFont *font, gint glyph, ArtDRect * bbox);
const ArtBpath *gnome_font_get_glyph_stdoutline (const GnomeFont *font, gint glyph);

/*
 * Backward compatibility and convenience methods
 *
 * NB! Those usually do not scale for international fonts, so use with
 * caution.
 */
gdouble gnome_font_get_ascender (const GnomeFont * font);
gdouble gnome_font_get_descender (const GnomeFont * font);
gdouble gnome_font_get_underline_position (const GnomeFont * font);
gdouble gnome_font_get_underline_thickness (const GnomeFont * font);

gdouble gnome_font_get_glyph_width (const GnomeFont * font, gint glyph);
gdouble gnome_font_get_glyph_kerning (const GnomeFont * font, gint glyph1, gint glyph2);

const GnomeFontFace *gnome_font_get_face (const GnomeFont *font);
gdouble gnome_font_get_size (const GnomeFont *font);

/*
 * Font fetching
 */

/*
 * These numbers are very loosely adapted from the Univers numbering
 * convention. I've had to insert some to accomodate all the
 * distinctions in the Adobe font catalog.
 * Removed offseting and gaps (Lauris)
 */

typedef enum {
	GNOME_FONT_LIGHTEST = 1,
	GNOME_FONT_EXTRA_LIGHT = 1,
	GNOME_FONT_THIN = 2,
	GNOME_FONT_LIGHT = 3,
	GNOME_FONT_BOOK = 4,
	GNOME_FONT_REGULAR = 4,
	GNOME_FONT_MEDIUM = 5,
	GNOME_FONT_SEMI = 6,
	GNOME_FONT_DEMI = 6,
	GNOME_FONT_BOLD = 7,
	GNOME_FONT_HEAVY = 8,
	GNOME_FONT_EXTRABOLD = 9,
	GNOME_FONT_BLACK = 10,
	GNOME_FONT_EXTRABLACK = 11,
	GNOME_FONT_HEAVIEST = 11
} GnomeFontWeight;

/* Find the closest face matching the family name, weight, and italic */
/* This is not very intelligent, so use with caution (Lauris) */
GnomeFont *gnome_font_find_closest (const guchar *family, GnomeFontWeight weight, gboolean italic, gdouble size);
GnomeFontFace * gnome_font_face_find_closest (const char *family_name, GnomeFontWeight weight, gboolean italic);

GnomeFont *gnome_font_find (const gchar *name, gdouble size);
GnomeFont *gnome_font_find_from_full_name (const gchar *string);

/*
 * Font browsing
 */

/* Return a list of fonts, as a g_list of strings */

GList * gnome_font_list (void);
void gnome_font_list_free (GList *fontlist);

/* Return a list of font families, as a g_list of newly allocated strings */

GList * gnome_font_family_list (void);
void gnome_font_family_list_free (GList *fontlist);

/* Misc */
gchar * gnome_font_get_full_name (const GnomeFont *font);

/*
 * We keep these at moment, but in future better go with Pango/glyphlists
 */

/* these routines are fast; go ahead and use them */

#ifndef BREAK_COMPATIBILITY
/*
 * WARNING WARNING WARNING
 *
 * These functions expect string to be iso-8859-1
 *
 * WARNING WARNING WARNING
 */
double gnome_font_get_width_string (const GnomeFont *font, const char *s);
double gnome_font_get_width_string_n (const GnomeFont *font, const char *s, int n);
#endif

#ifndef BREAK_COMPATIBILITY
/* Normal utf8 functions */
/* These are still crap, as you cannot expect ANYTHING about layouting rules */
double gnome_font_get_width_utf8 (const GnomeFont *font, const char *s);
double gnome_font_get_width_utf8_sized (const GnomeFont *font, const char *s, int n);
#endif

G_END_DECLS

#endif /* __GNOME_FONT_H__ */



