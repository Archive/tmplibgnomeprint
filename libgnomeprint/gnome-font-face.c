#define __GNOME_FONT_FACE_C__

/*
 *  Copyright (C) 2000-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Jody Goldberg <jody@ximian.com>
 *    Miguel de Icaza <miguel@ximian.com>
 *    Lauris Kaplinski <lauris@ximian.com>
 *    Christopher James Lahey <clahey@ximian.com>
 *    Michael Meeks <michael@ximian.com>
 *    Morten Welinder <terra@diku.dk>
 *
 *  GnomeFontFace - unscaled typeface
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>
#include <libgnomeprint/gnome-print-i18n.h>
#include <libgnomeprint/gnome-font-private.h>
#include "gp-fontmap.h"
#include "gp-truetype-utils.h"

#include <freetype/ftoutln.h>

/*
 * Standard AFM attributes
 * Notice, that all distances are doubles (and FontBBox is ArtDRect)
 *
 * ItalicAngle, FontBBox, CapHeight, XHeight
 *
 * Type1 file names, if face is trivial type1 font, otherwise NULL
 * Notice, that caller has to free strings
 * Notice, that these WILL NOT be supported in gnome-font base face class,
 * but instead in some subclass
 *
 * afm, pfb, pfbname
 *
 */

static void gnome_font_face_class_init (GnomeFontFaceClass * klass);
static void gnome_font_face_init (GnomeFontFace * face);
static void gnome_font_face_finalize (GObject * object);

static void gff_load_metrics (GnomeFontFace *face, gint glyph);
static void gff_load_outline (GnomeFontFace *face, gint glyph);

GFPSObject *gff_pso_from_type1 (const GnomeFontFace * face, GPFontEntry *entry);
GFPSObject *gff_pso_from_truetype (const GnomeFontFace *face, GPFontEntry *entry);
GFPSObject *gff_empty_pso (const GnomeFontFace *face, GPFontEntry *entry);
static void gf_pso_sprintf (GFPSObject * pso, const gchar * format, ...);
static void gf_pso_ensure_space (GFPSObject * pso, gint size);

static GObjectClass * parent_class;

GType
gnome_font_face_get_type (void)
{
	static GType face_type = 0;
	if (!face_type) {
		static const GTypeInfo face_info = {
			sizeof (GnomeFontFaceClass),
			NULL, NULL,
			(GClassInitFunc) gnome_font_face_class_init,
			NULL, NULL,
			sizeof (GnomeFontFace),
			0,
			(GInstanceInitFunc) gnome_font_face_init
		};
		face_type = g_type_register_static (G_TYPE_OBJECT, "GnomeFontFace", &face_info, 0);
	}
	return face_type;
}

static void
gnome_font_face_class_init (GnomeFontFaceClass * klass)
{
	GObjectClass * object_class;

	object_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_font_face_finalize;
}

static void
gnome_font_face_init (GnomeFontFace * face)
{
	face->entry = NULL;
	face->num_glyphs = 0;
	face->glyphs = NULL;
	face->ft_face = NULL;
}

static void
gnome_font_face_finalize (GObject * object)
{
	GnomeFontFace * face;

	face = (GnomeFontFace *) object;

	if (face->entry) {
		g_assert (face->entry->face == face);
		face->entry->face = NULL;
		gp_font_entry_unref (face->entry);
		face->entry = NULL;
	}

	if (face->glyphs) {
		gint i;
		for (i = 0; i < face->num_glyphs; i++) {
			if (face->glyphs[i].bpath) g_free (face->glyphs[i].bpath);
		}
		g_free (face->glyphs);
		face->glyphs = NULL;
	}

	if (face->ft_face) {
		FT_Done_Face (face->ft_face);
		face->ft_face = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/*
 * Naming
 *
 * gnome_font_face_get_name () should return one "true" name for font, that
 * does not have to be its PostScript name.
 * In future those names can be possibly localized (except ps_name)
 */

const gchar *
gnome_font_face_get_name (const GnomeFontFace * face)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	return face->entry->name;
}

const gchar *
gnome_font_face_get_family_name (const GnomeFontFace * face)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	return face->entry->familyname;
}

const gchar *
gnome_font_face_get_species_name (const GnomeFontFace * face)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	return face->entry->speciesname;
}

const gchar *
gnome_font_face_get_ps_name (const GnomeFontFace * face)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	return face->entry->psname;
}

/*
 * Returns number of defined glyphs in typeface
 */

gint
gnome_font_face_get_num_glyphs (const GnomeFontFace * face)
{
	g_return_val_if_fail (face != NULL, 0);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), 0);

	if (!GFF_LOADED (face)) {
		g_warning ("file %s: line %d: Face %s: Cannot load face", __FILE__, __LINE__, face->entry->name);
		return 0;
	}

	return face->num_glyphs;
}

/*
 * Metrics
 *
 * Currently those return standard values for left to right, horizontal script
 * The prefix std means, that there (a) will hopefully be methods to extract
 * different metric values and (b) for given face one text direction can
 * be defined as "default"
 * All face metrics are given in 0.001 em units
 */

const ArtDRect *
gnome_font_face_get_stdbbox (const GnomeFontFace * face)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	if (!GFF_LOADED (face)) {
		g_warning ("file %s: line %d: Face %s: Cannot load face", __FILE__, __LINE__, face->entry->name);
		return NULL;
	}

	return &face->bbox;
}

ArtPoint *
gnome_font_face_get_glyph_stdadvance (const GnomeFontFace * face, gint glyph, ArtPoint * advance)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);
	g_return_val_if_fail (advance != NULL, NULL);

	if (!GFF_LOADED (face)) {
		g_warning ("file %s: line %d: Face %s: Cannot load face", __FILE__, __LINE__, face->entry->name);
		return NULL;
	}

	if ((glyph < 0) || (glyph >= face->num_glyphs)) glyph = 0;

	if (!face->glyphs[glyph].metrics) gff_load_metrics ((GnomeFontFace *) face, glyph);

	*advance = face->glyphs[glyph].advance;

	return advance;
}

ArtDRect *
gnome_font_face_get_glyph_stdbbox (const GnomeFontFace * face, gint glyph, ArtDRect * bbox)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);
	g_return_val_if_fail (bbox != NULL, NULL);

	if (!GFF_LOADED (face)) {
		g_warning ("file %s: line %d: Face %s: Cannot load face", __FILE__, __LINE__, face->entry->name);
		return NULL;
	}

	if ((glyph < 0) || (glyph >= face->num_glyphs)) glyph = 0;

	if (!face->glyphs[glyph].metrics) gff_load_metrics ((GnomeFontFace *) face, glyph);

	*bbox = face->glyphs[glyph].bbox;

	return bbox;
}

const ArtBpath *
gnome_font_face_get_glyph_stdoutline (const GnomeFontFace * face, gint glyph)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	if (!GFF_LOADED (face)) {
		g_warning ("file %s: line %d: Face %s: Cannot load face", __FILE__, __LINE__, face->entry->name);
		return NULL;
	}

	if ((glyph < 0) || (glyph >= face->num_glyphs)) glyph = 0;

	if (!face->glyphs[glyph].bpath) gff_load_outline ((GnomeFontFace *) face, glyph);

	return face->glyphs[glyph].bpath;
}

/*
 * Give (possibly localized) demonstration text for given face
 * Most usually this tells about quick fox and lazy dog...
 */

const gchar *
gnome_font_face_get_sample (const GnomeFontFace * face)
{
	return _("The quick brown fox jumps over the lazy dog.");
}

GnomeFontFace *
gnome_font_face_find (const gchar * name)
{
	GPFontMap * map;
	GPFontEntry * e;

	g_return_val_if_fail (name != NULL, NULL);

	map = gp_fontmap_get ();

	e = g_hash_table_lookup (map->fontdict, name);
	if (!e) {
		gp_fontmap_release (map);
		return NULL;
	}
	if (e->face) {
		gnome_font_face_ref (e->face);
		gp_fontmap_release (map);
		return e->face;
	}

	e->face = g_object_new (GNOME_TYPE_FONT_FACE, NULL);
	e->face->entry = e;
	gp_font_entry_ref (e);

	gp_fontmap_release (map);

	return (e->face);
}

/* 
 * Find the closest weight matching the family name, weight, and italic
 * specs. Return the unsized font.
 */

GnomeFontFace *
gnome_font_face_find_closest (const char *family_name, GnomeFontWeight weight, gboolean italic)
{
	GPFontMap * map;
	GPFontEntry * best, * entry;
	GnomeFontFace * face;
	int best_dist, dist;
	GSList * l;

	g_return_val_if_fail (family_name != NULL, NULL);

#if 0
	if (!fontmap) gff_refresh_fontmap ();
	g_return_val_if_fail (fontmap != NULL, NULL);
#endif

	/* This should be reimplemented to use the gnome_font_family_hash. */

	map = gp_fontmap_get ();

	best = NULL;
	best_dist = 1000000;
	face = NULL;

	for (l = map->fonts; l != NULL; l = l->next) {
		entry = (GPFontEntry *) l->data;
		if (!strcasecmp (family_name, entry->familyname)) {
			if (entry->type == GP_FONT_ENTRY_ALIAS) entry = ((GPFontEntryAlias *) entry)->ref;
			if (entry->type == GP_FONT_ENTRY_TYPE1) {
				GPFontEntryT1 * t1;
				t1 = (GPFontEntryT1 *) entry;
				dist = abs (weight - t1->Weight) +
					100 * (italic != (t1->ItalicAngle != 0));
				if (dist < best_dist) {
					best_dist = dist;
					best = entry;
				}
			} else if (entry->type == GP_FONT_ENTRY_TRUETYPE) {
				GPFontEntryTT * tt;
				tt = (GPFontEntryTT *) entry;
				dist = abs (weight - tt->Weight) +
					100 * (italic != (tt->ItalicAngle != 0));
				if (dist < best_dist) {
					best_dist = dist;
					best = entry;
				}
			}
		}
	}

	if (best) {
		face = gnome_font_face_find (best->name);
	} else {
		face = gnome_font_face_find ("Helvetica");
	}

	if (!face && map->fonts) {
		face = gnome_font_face_find (((GPFontEntry *) map->fonts)->name);
	}

	gp_fontmap_release (map);

	g_return_val_if_fail (face != NULL, NULL);

	return face;
}

GnomeFont *
gnome_font_face_get_font (const GnomeFontFace * face, gdouble size, gdouble xres, gdouble yres)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	return gnome_font_find (face->entry->name, size);
}

GnomeFont *
gnome_font_face_get_font_default (const GnomeFontFace * face, gdouble size)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	return gnome_font_face_get_font (face, size, 600.0, 600.0);
}



/* fixme: */
/* Returns the glyph width in 0.001 unit */

gdouble
gnome_font_face_get_glyph_width (const GnomeFontFace * face, gint glyph)
{
	ArtPoint p;

	g_return_val_if_fail (face != NULL, 0.0);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), 0.0);

	gnome_font_face_get_glyph_stdadvance (face, glyph, &p);

	return p.x;
}

/*
 * Get the glyph number corresponding to a given unicode, or -1 if it
 * is not mapped.
 *
 * fixme: We use ugly hack to avoid segfaults everywhere
 */

gint
gnome_font_face_lookup_default (const GnomeFontFace * face, gint unicode)
{
	g_return_val_if_fail (face != NULL, -1);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), -1);

	if (!GFF_LOADED (face)) {
		g_warning ("file %s: line %d: Face %s: Cannot load face", __FILE__, __LINE__, face->entry->name);
		return -1;
	}

	/* fixme: Nobody should ask mapping of 0 */
	if (unicode < 1) return 0;

	return FT_Get_Char_Index (face->ft_face, unicode);
}

gboolean
gff_load (GnomeFontFace *face)
{
	FT_Face ft_face;
	FT_Error ft_result;
	static FT_Library ft_library = NULL;
	GPFontEntry *entry;

	if (!ft_library) {
		ft_result = FT_Init_FreeType (&ft_library);
		g_return_val_if_fail (ft_result == FT_Err_Ok, FALSE);
	}

	if (face->entry->type == GP_FONT_ENTRY_ALIAS) {
		entry = ((GPFontEntryAlias *) face->entry)->ref;
	} else {
		entry = face->entry;
	}

	if (entry->type == GP_FONT_ENTRY_TYPE1) {
		GPFontEntryT1 *t1;
		t1 = (GPFontEntryT1 *) entry;
		ft_result = FT_New_Face (ft_library, t1->pfb.name, 0, &ft_face);
		g_return_val_if_fail (ft_result == FT_Err_Ok, FALSE);
		if (t1->afm.name) {
			ft_result = FT_Attach_File (ft_face, t1->afm.name);
			if (ft_result != FT_Err_Ok) {
				g_warning ("file %s: line %d: face %s: Cannot attach file %s", __FILE__, __LINE__, face->entry->name, t1->afm.name);
			}
		}
	} else if (entry->type == GP_FONT_ENTRY_TRUETYPE) {
		GPFontEntryTT *tt;
		tt = (GPFontEntryTT *) entry;
		ft_result = FT_New_Face (ft_library, tt->ttf.name, 0, &ft_face);
		g_return_val_if_fail (ft_result == FT_Err_Ok, FALSE);
	} else {
		g_assert_not_reached ();
	}

	/* fixme: scalability */
	/* fixme: glyph names */

	face->ft_face = ft_face;

	FT_Select_Charmap (ft_face, ft_encoding_unicode);

	face->num_glyphs = ft_face->num_glyphs;
	g_return_val_if_fail (face->num_glyphs > 0, FALSE);
	face->glyphs = g_new0 (GFFGlyphInfo, face->num_glyphs);

	face->ft2ps = 1000.0 / ft_face->units_per_EM;

	face->bbox.x0 = ft_face->bbox.xMin * face->ft2ps;
	face->bbox.y0 = ft_face->bbox.yMin * face->ft2ps;
	face->bbox.x1 = ft_face->bbox.xMax * face->ft2ps;
	face->bbox.y1 = ft_face->bbox.yMax * face->ft2ps;

	return TRUE;
}

static void
gff_load_metrics (GnomeFontFace *face, gint glyph)
{
	GFFGlyphInfo * gi;

	g_assert (face->ft_face);
	g_assert (!face->glyphs[glyph].metrics);

	gi = face->glyphs + glyph;

	FT_Load_Glyph (face->ft_face, glyph, FT_LOAD_NO_SCALE | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP);

        gi->bbox.x0 = -face->ft_face->glyph->metrics.horiBearingX * face->ft2ps;
	gi->bbox.y1 = face->ft_face->glyph->metrics.horiBearingY * face->ft2ps;
	gi->bbox.y0 = gi->bbox.y1 - face->ft_face->glyph->metrics.height * face->ft2ps;
	gi->bbox.x1 = gi->bbox.x0 + face->ft_face->glyph->metrics.width * face->ft2ps;
	gi->advance.x = face->ft_face->glyph->metrics.horiAdvance * face->ft2ps;
	gi->advance.y = 0.0;

	face->glyphs[glyph].metrics = TRUE;
}

static ArtBpath *gff_ol2bp (FT_Outline * ol, gdouble transform[]);

static void
gff_load_outline (GnomeFontFace *face, gint glyph)
{
	gdouble a[6];

	g_assert (face->ft_face);
	g_assert (!face->glyphs[glyph].bpath);

	FT_Load_Glyph (face->ft_face, glyph, FT_LOAD_NO_SCALE | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP);

	a[0] = a[3] = face->ft2ps;
	a[1] = a[2] = a[4] = a[5] = 0.0;

	face->glyphs[glyph].bpath = gff_ol2bp (&face->ft_face->glyph->outline, a);
}

/* Bpath methods */

typedef struct {
	ArtBpath * bp;
	gint start, end;
	gdouble * t;
} GFFT2OutlineData;

static int gfft2_move_to (FT_Vector * to, void * user)
{
	GFFT2OutlineData * od;
	ArtBpath * s;
	ArtPoint p;

	od = (GFFT2OutlineData *) user;

	s = &od->bp[od->end - 1];

	p.x = to->x * od->t[0] + to->y * od->t[2];
	p.y = to->x * od->t[1] + to->y * od->t[3];

	if ((p.x != s->x3) || (p.y != s->y3)) {
		od->bp[od->end].code = ART_MOVETO;
		od->bp[od->end].x3 = to->x * od->t[0] + to->y * od->t[2];
		od->bp[od->end].y3 = to->x * od->t[1] + to->y * od->t[3];
		od->end++;
	}

	return 0;
}

static int gfft2_line_to (FT_Vector * to, void * user)
{
	GFFT2OutlineData * od;
	ArtBpath * s;
	ArtPoint p;

	od = (GFFT2OutlineData *) user;

	s = &od->bp[od->end - 1];

	p.x = to->x * od->t[0] + to->y * od->t[2];
	p.y = to->x * od->t[1] + to->y * od->t[3];

	if ((p.x != s->x3) || (p.y != s->y3)) {
		od->bp[od->end].code = ART_LINETO;
		od->bp[od->end].x3 = to->x * od->t[0] + to->y * od->t[2];
		od->bp[od->end].y3 = to->x * od->t[1] + to->y * od->t[3];
		od->end++;
	}

	return 0;
}

static int gfft2_conic_to (FT_Vector * control, FT_Vector * to, void * user)
{
	GFFT2OutlineData * od;
	ArtBpath * s, * e;
	ArtPoint c;

	od = (GFFT2OutlineData *) user;
	g_return_val_if_fail (od->end > 0, -1);

	s = &od->bp[od->end - 1];
	e = &od->bp[od->end];

	e->code = ART_CURVETO;

	c.x = control->x * od->t[0] + control->y * od->t[2];
	c.y = control->x * od->t[1] + control->y * od->t[3];
	e->x3 = to->x * od->t[0] + to->y * od->t[2];
	e->y3 = to->x * od->t[1] + to->y * od->t[3];

	od->bp[od->end].x1 = c.x - (c.x - s->x3) / 3;
	od->bp[od->end].y1 = c.y - (c.y - s->y3) / 3;
	od->bp[od->end].x2 = c.x + (e->x3 - c.x) / 3;
	od->bp[od->end].y2 = c.y + (e->y3 - c.y) / 3;
	od->end++;

	return 0;
}

static int gfft2_cubic_to (FT_Vector * control1, FT_Vector * control2, FT_Vector * to, void * user)
{
	GFFT2OutlineData * od;

	od = (GFFT2OutlineData *) user;

	od->bp[od->end].code = ART_CURVETO;
	od->bp[od->end].x1 = control1->x * od->t[0] + control1->y * od->t[2];
	od->bp[od->end].y1 = control1->x * od->t[1] + control1->y * od->t[3];
	od->bp[od->end].x2 = control2->x * od->t[0] + control2->y * od->t[2];
	od->bp[od->end].y2 = control2->x * od->t[1] + control2->y * od->t[3];
	od->bp[od->end].x3 = to->x * od->t[0] + to->y * od->t[2];
	od->bp[od->end].y3 = to->x * od->t[1] + to->y * od->t[3];
	od->end++;

	return 0;
}

FT_Outline_Funcs gfft2_outline_funcs = {
	gfft2_move_to,
	gfft2_line_to,
	gfft2_conic_to,
	gfft2_cubic_to,
	0, 0
};

/*
 * We support only 4x4 matrix here (do you need more?)
 */

static ArtBpath *
gff_ol2bp (FT_Outline * ol, gdouble transform[])
{
	GFFT2OutlineData od;

	od.bp = g_new (ArtBpath, ol->n_points * 2 + ol->n_contours + 1);
	od.start = od.end = 0;
	od.t = transform;

	FT_Outline_Decompose (ol, &gfft2_outline_funcs, &od);

	od.bp[od.end].code = ART_END;

	/* fixme: g_renew */

	return od.bp;
}

#if 0
#define KERN_PAIR_HASH(g1, g2) ((g1) * 367 + (g2) * 31)

static gboolean
gff_load_afm (GnomeFontFace * face)
{
	GnomeFontFacePrivate * priv;
	GPFontEntryT1 * t1;
	Font_Info *fi;
	FILE *afm_f;
	int status;
	gint nglyphs;
	gint privcode;

	g_return_val_if_fail ((face->private->entry->type == GP_FONT_ENTRY_TYPE1) ||
			      (face->private->entry->type == GP_FONT_ENTRY_TYPE1_ALIAS),
			      FALSE);

	priv = face->private;
	t1 = (GPFontEntryT1 *) face->private->entry;

	afm_f = fopen (t1->afm.name, "r");
	if (afm_f != NULL) {
		status = parseFile (afm_f, &fi, P_G | P_M | P_P);
#ifdef VERBOSE
		debugmsg (_("status loading %s = %d\n"), t1->afm.name, status);
#endif
		if (status == 0) {
			GnomeFontLigList **ligtab;
			Ligature *ligs;
			int i;
			GnomeFontKernPair *ktab;
			int ktabsize;

			priv->ascender = fi->gfi->ascender;
			/* This is negated because adobe fonts use a negative number
			   for descenders below the baseline, but most people expect
			   a positive number for descenders below the line. */
			priv->descender = -fi->gfi->descender;
			priv->fixed_width = fi->gfi->isFixedPitch;

			/* Added for pdf */
			priv->capheight = (gdouble) fi->gfi->capHeight;
			priv->italics_angle = (gdouble) fi->gfi->italicAngle;
			priv->xheight = (gdouble) fi->gfi->xHeight;
			priv->bbox.x0 = (gdouble) fi->gfi->fontBBox.llx;
			priv->bbox.y0 = (gdouble) fi->gfi->fontBBox.lly;
			priv->bbox.x1 = (gdouble) fi->gfi->fontBBox.urx;
			priv->bbox.y1 = (gdouble) fi->gfi->fontBBox.ury;
			/* */
	  
			priv->underline_position = fi->gfi->underlinePosition;
			priv->underline_thickness = fi->gfi->underlineThickness;

			ligtab = g_malloc (256 * sizeof (GnomeFontLigList *));
			priv->ligs = ligtab;

			for (i = 0; i < 256; i++) {
				ligtab[i] = 0;
			}

			priv->glyphs = g_new0 (GFFGlyphInfo, fi->numOfChars + 1);
			priv->unimap = gp_uc_map_new ();
#if 0
			priv->glyphmap = g_hash_table_new (NULL, NULL);
#endif

			gff_fill_zero_glyph (face);

			nglyphs = 1;
			privcode = 0xe000;

			for (i = 0; i < fi->numOfChars; i++) {
				CharMetricInfo * info;
				gint unicode;
				gchar * constname;

				constname = NULL; /* Kill warning */
				info = fi->cmi + i;

				unicode = gp_unicode_from_ps (info->name);
				if (unicode < 1) unicode = gp_unicode_from_dingbats (info->name);

				if (unicode < 1) {
					if (!priv->privencoding) priv->privencoding = g_hash_table_new (g_str_hash, g_str_equal);
					if (!g_hash_table_lookup (priv->privencoding, info->name)) {
						unicode = privcode++;
						constname = g_strdup (info->name);
						g_hash_table_insert (priv->privencoding, constname, GINT_TO_POINTER (unicode));
					}
				} else {
					constname = (gchar *) gp_const_ps_from_ps (info->name);
				}

				if (unicode) {
					GFFGlyphInfo * gi;

					gi = priv->glyphs + nglyphs;

					gi->unicode = unicode;
					gi->psname = constname;

					gi->advance.x = info->wx;
					gi->advance.y = info->wy;

					gi->bbox.x0 = info->charBBox.llx;
					gi->bbox.y0 = info->charBBox.lly;
					gi->bbox.x1 = info->charBBox.urx;
					gi->bbox.y1 = info->charBBox.ury;

					if (gp_multi_from_ps (info->name)) {
						const GSList * l;
						l = gp_multi_from_ps (info->name);
						while (l) {
							gp_uc_map_insert (priv->unimap, GPOINTER_TO_INT (l->data), nglyphs);
#if 0
							gff_assign_glyph (face, GPOINTER_TO_INT (l->data), nglyphs);
#endif
							l = l->next;
						}
					} else {
						gp_uc_map_insert (priv->unimap, unicode, nglyphs);
#if 0
						gff_assign_glyph (face, unicode, nglyphs);
#endif
					}

					nglyphs++;
				}
			}

			priv->glyphs = g_renew (GFFGlyphInfo, priv->glyphs, nglyphs);
			priv->num_glyphs = nglyphs;

	  for (i = 0; i < fi->numOfChars; i++)
	    {
	      int code;

	      /* Get the width */
	      code = fi->cmi[i].code;
	      if (code >= 0 && code < 256)
		{
		  /* Get the ligature info */
		  for (ligs = fi->cmi[i].ligs; ligs != NULL; ligs = ligs->next)
		    {
		      int succ, lig;
		      GnomeFontLigList *ll;
		      gint u;

		      /* We assume here that dingbats do not have ligatures */
		      u = gp_unicode_from_ps (ligs->succ);
		      succ = gnome_font_face_lookup_default (face, u);
#if 0
		      succ = GPOINTER_TO_INT (g_hash_table_lookup (face->private->glyphmap, GINT_TO_POINTER (u)));
#endif
		      u = gp_unicode_from_ps (ligs->succ);
		      lig = gnome_font_face_lookup_default (face, u);
#if 0
		      lig = GPOINTER_TO_INT (g_hash_table_lookup (face->private->glyphmap, GINT_TO_POINTER (u)));
#endif
		      if ((succ > 0) && (lig > 0))
			{
			  ll = g_new (GnomeFontLigList, 1);
			  ll->succ = succ;
			  ll->lig = lig;
			  ll->next = ligtab[code];
			  ligtab[code] = ll;
			}
		    }
		}
	    }

	  /* process the kern pairs */
	  for (ktabsize = 1; ktabsize < fi->numOfPairs << 1; ktabsize <<= 1);
	  ktab = g_new (GnomeFontKernPair, ktabsize);
	  face->private->kerns = ktab;
	  face->private->num_kerns = ktabsize;
	  for (i = 0; i < ktabsize; i++)
	    {
	      ktab[i].glyph1 = -1;
	      ktab[i].glyph2 = -1;
	      ktab[i].x_amt = 0;
	    }

	  for (i = 0; i < fi->numOfPairs; i++)
	    {
	      int glyph1, glyph2;
	      int j;
	      gint u;

	      /* We assume here that dingbats do not have kerning */
	      u = gp_unicode_from_ps (fi->pkd[i].name1);
	      glyph1 = gnome_font_face_lookup_default (face, u);
#if 0
	      glyph1 = GPOINTER_TO_INT (g_hash_table_lookup (face->private->glyphmap, GINT_TO_POINTER (u)));
#endif
	      u = gp_unicode_from_ps (fi->pkd[i].name2);
	      glyph2 = gnome_font_face_lookup_default (face, u);
#if 0
	      glyph2 = GPOINTER_TO_INT (g_hash_table_lookup (face->private->glyphmap, GINT_TO_POINTER (u)));
#endif

	      for (j = KERN_PAIR_HASH (glyph1, glyph2) & (ktabsize - 1);
		   ktab[j].glyph1 != -1;
		   j = (j + 1) & (ktabsize - 1));
	      ktab[j].glyph1 = glyph1;
	      ktab[j].glyph2 = glyph2;
	      ktab[j].x_amt = fi->pkd[i].xamt;
#ifdef VERBOSE
	      debugmsg ("kern pair %s(%d) %s(%d) %d\n",
		       fi->pkd[i].name1, glyph1,
		       fi->pkd[i].name2, glyph2,
		       fi->pkd[i].xamt);
#endif
	    }

#ifdef VERBOSE
	  for (i = 0; i < ktabsize; i++)
	    debugmsg ("%d %d %d\n",
		     ktab[i].glyph1, ktab[i].glyph2, ktab[i].x_amt);
#endif
	}

      if (fi)
	parseFileFree (fi);

      fclose (afm_f);

      return TRUE;
    }

  return FALSE;
}

static void
gff_fill_zero_glyph (GnomeFontFace * face)
{
	GFFGlyphInfo * info;
	static ArtBpath undef[] = {{ART_MOVETO, 0.0, 0.0, 0.0, 0.0, 50.0, 50.0},
				   {ART_LINETO, 0.0, 0.0, 0.0, 0.0, 750.0, 50.0},
				   {ART_LINETO, 0.0, 0.0, 0.0, 0.0, 750.0, 950.0},
				   {ART_LINETO, 0.0, 0.0, 0.0, 0.0, 50.0, 950.0},
				   {ART_LINETO, 0.0, 0.0, 0.0, 0.0, 50.0, 50.0},
				   {ART_MOVETO, 0.0, 0.0, 0.0, 0.0, 100.0, 100.0},
				   {ART_LINETO, 0.0, 0.0, 0.0, 0.0, 700.0, 100.0},
				   {ART_LINETO, 0.0, 0.0, 0.0, 0.0, 700.0, 900.0},
				   {ART_LINETO, 0.0, 0.0, 0.0, 0.0, 100.0, 900.0},
				   {ART_LINETO, 0.0, 0.0, 0.0, 0.0, 100.0, 100.0},
				   {ART_END, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}};

	info = face->private->glyphs;

	info->unicode = 0;
	info->psname = ".notdef";

	info->advance.x = 800.0;
	info->advance.y = 0.0;

	info->bbox.x0 = 50.0;
	info->bbox.y0 = 50.0;
	info->bbox.x1 = 750.0;
	info->bbox.y1 = 900.0;

	info->bpath = undef;
}
#endif

/*
 * Create downloadable PS representation of given face
 */

GFPSObject *
gnome_font_face_create_ps_object (const GnomeFontFace * face)
{
	GPFontEntry *entry;

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	if (face->entry->type == GP_FONT_ENTRY_ALIAS) {
		entry = ((GPFontEntryAlias *) face->entry)->ref;
	} else {
		entry = face->entry;
	}

	if (!GFF_LOADED (face)) {
		g_warning ("file %s: line %d: Face %s: Cannot load face", __FILE__, __LINE__, face->entry->name);
		/* fixme: create empty font */
		return gff_empty_pso (face, entry);
	}

	switch (entry->type) {
	case GP_FONT_ENTRY_TYPE1:
		return gff_pso_from_type1 (face, entry);
		break;
	case GP_FONT_ENTRY_TRUETYPE:
		return gff_pso_from_truetype (face, entry);
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	return gff_empty_pso (face, entry);
}

#define INT32_LSB(q) ((q)[0] + ((q)[1] << 8) + ((q)[2] << 16) + ((q)[3] << 24))

GFPSObject *
gff_pso_from_type1 (const GnomeFontFace *face, GPFontEntry *entry)
{
	GPFontEntryT1 * t1;
	GFPSObject * pso;
	struct stat st;
	guchar * fbuf;
	gint fh;
	const gchar * embeddedname;

	t1 = (GPFontEntryT1 *) entry;

	/* mmap file */
	if (stat (t1->pfb.name, &st) != 0) return gff_empty_pso (face, entry);
	fh = open (t1->pfb.name, O_RDONLY);
	if (fh < 0) return gff_empty_pso (face, entry);
	fbuf = mmap (NULL, st.st_size, PROT_READ, MAP_SHARED, fh, 0);
	close (fh);
	if (fbuf == NULL) return gff_empty_pso (face, entry);
	if (fbuf == (guchar *) -1) return gff_empty_pso (face, entry);

	pso = g_new (GFPSObject, 1);
	pso->bufsize = st.st_size + 1024;
	pso->length = 0;
	pso->buf = g_new (guchar, pso->bufsize);
	pso->encodedname = NULL;
	pso->encodedbytes = 0;

	if (*fbuf == 0x80) {
		const char hextab[16] = "0123456789abcdef";
		gint idx;
		/* this is actually the same as a pfb to pfa converter
		 * Reference: Adobe technical note 5040, "Supporting Downloadable PostScript
		 * Language Fonts", page 9
		 */
		idx = 0;
		while (idx < st.st_size) {
			gint length, i;
			if (fbuf[idx] != 0x80) {
				g_warning ("Corrupted pfb file %s", t1->pfb.name);
				munmap (fbuf, st.st_size);
				g_free (pso->buf);
				g_free (pso);
				return gff_empty_pso (face, entry);
			}
			switch (fbuf[idx + 1]) {
			case 1:
				length = INT32_LSB (fbuf + idx + 2);
				gf_pso_ensure_space (pso, length);
				idx += 6;
				memcpy (pso->buf + pso->length, fbuf + idx, length);
				pso->length += length;
				idx += length;
				break;
			case 2:
				length = INT32_LSB (fbuf + idx + 2);
				gf_pso_ensure_space (pso, length * 3);
				idx += 6;
				for (i = 0; i < length; i++) {
					pso->buf[pso->length++] = hextab[fbuf[idx] >> 4];
					pso->buf[pso->length++] = hextab[fbuf[idx] & 15];
					idx += 1;
					if ((i & 31) == 31 || i == length - 1) pso->buf[pso->length++] = '\n';
				}
				break;
			case 3:
				/* Finished */
				gf_pso_ensure_space (pso, 1);
				pso->buf[pso->length++] = '\n';
				idx = st.st_size;
				break;
			default:
				g_warning ("Corrupted pfb file %s", t1->pfb.name);
				munmap (fbuf, st.st_size);
				g_free (pso->buf);
				g_free (pso);
				return gff_empty_pso (face, entry);
				break;
			}
		}
	} else {
		memcpy (pso->buf, fbuf, st.st_size);
		pso->buf[st.st_size] = '\0';
		pso->length = st.st_size;
	}

	munmap (fbuf, st.st_size);

	embeddedname = entry->psname;
	pso->encodedname = g_strdup_printf ("GnomeUni-%s", embeddedname);

	if (face->num_glyphs < 256) {
		gint glyph;
		/* 8-bit vector */
		pso->encodedbytes = 1;
		gf_pso_sprintf (pso, "/%s findfont dup length dict begin\n", embeddedname);
		gf_pso_sprintf (pso, "{1 index /FID ne {def} {pop pop} ifelse} forall\n");
		gf_pso_sprintf (pso, "/Encoding [\n");
		for (glyph = 0; glyph < 256; glyph++) {
			guint g;
			gchar c[256];
			FT_Error status;
			g = (glyph < face->num_glyphs) ? glyph : 0;
			status = FT_Get_Glyph_Name (face->ft_face, glyph, c, 256);
			if (status != FT_Err_Ok) {
				g_warning ("Cannot get name of glyph %d", glyph);
				g_free (pso->buf);
				g_free (pso);
				return gff_empty_pso (face, entry);
			}
			gf_pso_sprintf (pso, ((glyph & 0xf) == 0xf) ? "/%s\n" : "/%s ", c);
		}
		gf_pso_sprintf (pso, "] def currentdict end\n");
		gf_pso_sprintf (pso, "/%s exch definefont pop\n", pso->encodedname);
	} else {
		gint nfonts, nglyphs, i, j;
		/* 16-bit vector */
		pso->encodedbytes = 2;
		nglyphs = face->num_glyphs;
		nfonts = (nglyphs + 255) >> 8;

		gf_pso_sprintf (pso, "32 dict begin\n");
		/* Common entries */
		gf_pso_sprintf (pso, "/FontType 0 def\n");
		gf_pso_sprintf (pso, "/FontMatrix [1 0 0 1 0 0] def\n");
		gf_pso_sprintf (pso, "/FontName /%s-Glyph-Composite def\n", embeddedname);
		gf_pso_sprintf (pso, "/LanguageLevel 2 def\n");

		/* Type 0 entries */
		gf_pso_sprintf (pso, "/FMapType 2 def\n");

		/* Bitch 'o' bitches */
		gf_pso_sprintf (pso, "/FDepVector [\n");
		for (i = 0; i < nfonts; i++) {
			gf_pso_sprintf (pso, "/%s findfont dup length dict begin\n", embeddedname);
			gf_pso_sprintf (pso, "{1 index /FID ne {def} {pop pop} ifelse} forall\n");
			gf_pso_sprintf (pso, "/Encoding [\n");
			for (j = 0; j < 256; j++) {
				gint glyph;
				gchar c[256];
				FT_Error status;
				glyph = 256 * i + j;
				if (glyph >= nglyphs) glyph = 0;
				status = FT_Get_Glyph_Name (face->ft_face, glyph, c, 256);
				if (status != FT_Err_Ok) {
					g_warning ("Cannot get name of glyph %d", glyph);
					g_free (pso->buf);
					g_free (pso);
					return gff_empty_pso (face, entry);
				}
				gf_pso_sprintf (pso, ((j & 0xf) == 0xf) ? "/%s\n" : "/%s ", c);
			}
			gf_pso_sprintf (pso, "] def\n");
			gf_pso_sprintf (pso, "currentdict end /%s-Glyph-Page-%d exch definefont\n", embeddedname, i);
		}
		gf_pso_sprintf (pso, "] def\n");
		gf_pso_sprintf (pso, "/Encoding [\n");
		for (i = 0; i < 256; i++) {
			gint fn;
			fn = (i < nfonts) ? i : 0;
			gf_pso_sprintf (pso, ((i & 0xf) == 0xf) ? "%d\n" : "%d  ", fn);
		}
		gf_pso_sprintf (pso, "] def\n");
		gf_pso_sprintf (pso, "currentdict end\n");
		gf_pso_sprintf (pso, "/%s exch definefont pop\n", pso->encodedname);
	}

	return pso;
}

/*
 * References:
 * Adobe Inc., PostScript Language Reference, 3rd edition, Addison Wesley 1999
 * Adobe Inc., The Type42 Font Format Specification, 1998 <http://partners.adobe.com/asn/developer/PDFS/TN/5012.Type42_Spec.pdf>
 */

GFPSObject *
gff_pso_from_truetype (const GnomeFontFace *face, GPFontEntry *entry)
{
	GPFontEntryTT * tt;
	GFPSObject * pso;
	struct stat st;
	guchar * fbuf;
	gint fh;
	GSList *strings;
	const gchar * embeddedname;
	gdouble TTVersion, MfrRevision;
	const ArtDRect *bbox;
	gint i;

	tt = (GPFontEntryTT *) entry;

	/* mmap file */
	if (stat (tt->ttf.name, &st) != 0) return gff_empty_pso (face, entry);
	fh = open (tt->ttf.name, O_RDONLY);
	if (fh < 0) return gff_empty_pso (face, entry);
	fbuf = mmap (NULL, st.st_size, PROT_READ, MAP_SHARED, fh, 0);
	close (fh);
	if (fbuf == NULL) return gff_empty_pso (face, entry);
	if (fbuf == (guchar *) -1) return gff_empty_pso (face, entry);

	strings = gp_tt_split_file (fbuf, st.st_size);
	if (strings == NULL) {
		munmap (fbuf, st.st_size);
		return gff_empty_pso (face, entry);
	}

	/* Create PSO */
	pso = g_new (GFPSObject, 1);
	pso->bufsize = st.st_size + 1024;
	pso->length = 0;
	pso->buf = g_new (guchar, pso->bufsize);
	pso->encodedname = g_strdup_printf ("GnomeUni-%s", entry->psname);
	pso->encodedbytes = 0;

	embeddedname = entry->psname;

	/* Download master font */
	TTVersion = 1.0;
	MfrRevision = 1.0;
	gf_pso_sprintf (pso, "%%!PS-TrueTypeFont-%g-%g\n", TTVersion, MfrRevision);
	gf_pso_sprintf (pso, "11 dict begin\n");
	gf_pso_sprintf (pso, "/FontName /%s def\n", embeddedname);
	gf_pso_sprintf (pso, "/Encoding 256 array\n");
	gf_pso_sprintf (pso, "0 1 255 {1 index exch /.notdef put} for\n");
	gf_pso_sprintf (pso, "readonly def\n");
	gf_pso_sprintf (pso, "/PaintType 0 def\n");
	gf_pso_sprintf (pso, "/FontMatrix [1 0 0 1 0 0] def\n");
	/* fixme: */
	bbox = gnome_font_face_get_stdbbox (face);
	gf_pso_sprintf (pso, "/FontBBox [%g %g %g %g] def\n", bbox->x0, bbox->y0, bbox->x1, bbox->y1);
	gf_pso_sprintf (pso, "/FontType 42 def\n");
	/* fixme: XUID */
	/* fixme: Be more intelligent */
	gf_pso_sprintf (pso, "/sfnts [\n");
	while (strings) {
		guint start, next, size, i;
		start = GPOINTER_TO_UINT (strings->data);
		strings = g_slist_remove (strings, strings->data);
		next = strings ? GPOINTER_TO_UINT (strings->data) : st.st_size;
		size = next - start;
		gf_pso_sprintf (pso, "<\n");
		for (i = start; i < next; i+= 32) {
			gint e, j;
			e = MIN (i + 32, next);
			for (j = i; j < e; j++) {
				gf_pso_sprintf (pso, "%.2x", *(fbuf + j));
			}
			gf_pso_sprintf (pso, "\n");
		}
		gf_pso_sprintf (pso, strings ? ">\n" : "00>\n");
	}
	gf_pso_sprintf (pso, "] def\n");
	/* fixme: Use CID or something */
	gf_pso_sprintf (pso, "/CharStrings %d dict dup begin\n", face->num_glyphs);
	gf_pso_sprintf (pso, "/.notdef 0 def\n");
	for (i = 1; i < face->num_glyphs; i++) {
		gf_pso_sprintf (pso, "/g%d %d def\n", i, i);
	}
	gf_pso_sprintf (pso, "end readonly def\n");
	gf_pso_sprintf (pso, "FontName currentdict end definefont pop\n");

	/* Unmap font file */
	munmap (fbuf, st.st_size);

	/*
	 * We have font downloaded (hopefully)
	 *
	 * With CharStrings: .nodef g1 g2 g3 g4 g5...
	 *
	 * Now we have to build usable 2-byte encoded font from it
	 *
	 */

	/* fixme: That is crap. We should use CID */
	/* Bitch 'o' bitches (Lauris) ! */
	if (face->num_glyphs < 256) {
		gint glyph;
		/* 8-bit vector */
		pso->encodedbytes = 1;
		gf_pso_sprintf (pso, "/%s findfont dup length dict begin\n", embeddedname);
		gf_pso_sprintf (pso, "{1 index /FID ne {def} {pop pop} ifelse} forall\n");
		gf_pso_sprintf (pso, "/Encoding [\n");
		for (glyph = 0; glyph < 256; glyph++) {
			guint g;
			g = (glyph < face->num_glyphs) ? glyph : 0;
			if (g == 0) {
				gf_pso_sprintf (pso, ((glyph & 0xf) == 0xf) ? "/.notdef\n" : "/.notdef ", g);
			} else {
				gf_pso_sprintf (pso, ((glyph & 0xf) == 0xf) ? "/g%d\n" : "/g%d ", g);
			}
		}
		gf_pso_sprintf (pso, "] def currentdict end\n");
		gf_pso_sprintf (pso, "/%s exch definefont pop\n", pso->encodedname);
	} else {
		gint nfonts, nglyphs, i, j;
		/* 16-bit vector */
		pso->encodedbytes = 2;
		nglyphs = face->num_glyphs;
		nfonts = (nglyphs + 255) >> 8;

		gf_pso_sprintf (pso, "32 dict begin\n");
		/* Common entries */
		gf_pso_sprintf (pso, "/FontType 0 def\n");
		gf_pso_sprintf (pso, "/FontMatrix [1 0 0 1 0 0] def\n");
		gf_pso_sprintf (pso, "/FontName /%s-Glyph-Composite def\n", embeddedname);
		gf_pso_sprintf (pso, "/LanguageLevel 2 def\n");
		/* Type 0 entries */
		gf_pso_sprintf (pso, "/FMapType 2 def\n");
		/* Bitch 'o' bitches */
		gf_pso_sprintf (pso, "/FDepVector [\n");
		for (i = 0; i < nfonts; i++) {
			gf_pso_sprintf (pso, "/%s findfont dup length dict begin\n", embeddedname);
			gf_pso_sprintf (pso, "{1 index /FID ne {def} {pop pop} ifelse} forall\n");
			gf_pso_sprintf (pso, "/Encoding [\n");
			for (j = 0; j < 256; j++) {
				gint glyph;
				glyph = 256 * i + j;
				if (glyph >= nglyphs) glyph = 0;
				if (glyph == 0) {
					gf_pso_sprintf (pso, ((j & 0xf) == 0xf) ? "/.notdef\n" : "/.notdef ");
				} else {
					gf_pso_sprintf (pso, ((j & 0xf) == 0xf) ? "/g%d\n" : "/g%d ", glyph);
				}
			}
			gf_pso_sprintf (pso, "] def\n");
			gf_pso_sprintf (pso, "currentdict end /%s-Glyph-Page-%d exch definefont\n", embeddedname, i);
		}
		gf_pso_sprintf (pso, "] def\n");
		gf_pso_sprintf (pso, "/Encoding [\n");
		for (i = 0; i < 256; i++) {
			gint fn;
			fn = (i < nfonts) ? i : 0;
			gf_pso_sprintf (pso, ((i & 0xf) == 0xf) ? "%d\n" : "%d  ", fn);
		}
		gf_pso_sprintf (pso, "] def\n");
		gf_pso_sprintf (pso, "currentdict end\n");
		gf_pso_sprintf (pso, "/%s exch definefont pop\n", pso->encodedname);
	}

	return pso;
}

GFPSObject *
gff_empty_pso (const GnomeFontFace *face, GPFontEntry *entry)
{
	GFPSObject *pso;

	/* Create PSO */
	pso = g_new (GFPSObject, 1);
	pso->bufsize = 1024;
	pso->length = 0;
	pso->buf = g_new (guchar, pso->bufsize);
	pso->encodedname = g_strdup_printf ("GnomeEmpty-%s", entry->psname);
	pso->encodedbytes = 1;

	gf_pso_sprintf (pso, "%%Empty font generated by gnome-print\n");
	gf_pso_sprintf (pso, "8 dict begin");
	gf_pso_sprintf (pso, "/FontType 3 def\n");
	gf_pso_sprintf (pso, "/FontMatrix [.001 0 0 .001 0 0] def\n");
	gf_pso_sprintf (pso, "/FontBBox [0 0 750 950] def\n");
	gf_pso_sprintf (pso, "/Encoding 256 array def\n");
	gf_pso_sprintf (pso, "0 1 255 {Encoding exch /.notdef put} for\n");
	gf_pso_sprintf (pso, "/CharProcs 2 dict def\n");
	gf_pso_sprintf (pso, "CharProcs begin\n");
	gf_pso_sprintf (pso, "/.notdef {\n");
	gf_pso_sprintf (pso, "0 0 moveto 750 0 lineto 750 950 lineto 0 950 lineto closepath\n");
	gf_pso_sprintf (pso, "50 50 moveto 700 50 lineto 700 900 lineto 50 900 lineto closepath\n");
	gf_pso_sprintf (pso, "eofill\n");
	gf_pso_sprintf (pso, "} bind def\n");
	gf_pso_sprintf (pso, "end\n");
	gf_pso_sprintf (pso, "/BuildGlyph {\n");
	gf_pso_sprintf (pso, "1000 0 0 0 750 950 setcachedevice\n");
	gf_pso_sprintf (pso, "exch /CharProcs get exch\n");
	gf_pso_sprintf (pso, "2 copy known not {pop /.notdef} if\n");
	gf_pso_sprintf (pso, "get exec\n");
	gf_pso_sprintf (pso, "} bind def\n");
	gf_pso_sprintf (pso, "/BuildChar {1 index /Encoding get exch get\n");
	gf_pso_sprintf (pso, "1 index /BuildGlyph get exec\n");
	gf_pso_sprintf (pso, "} bind def\n");
	gf_pso_sprintf (pso, "currentdict\n");
	gf_pso_sprintf (pso, "end\n");
	gf_pso_sprintf (pso, "/%s exch definefont pop\n", pso->encodedname);

	return pso;
}

static void
gf_pso_sprintf (GFPSObject * pso, const gchar * format, ...)
{
	va_list arguments;
	gchar *oldlocale;
	gchar *text;
	gint len;
	
	oldlocale = setlocale (LC_NUMERIC, NULL);
	setlocale (LC_NUMERIC, "C");
		
	va_start (arguments, format);
	text = g_strdup_vprintf (format, arguments);
	va_end (arguments);

	len = strlen (text);
	gf_pso_ensure_space (pso, len);
	memcpy (pso->buf + pso->length, text, len);
	pso->length += len;
	g_free (text);

	setlocale (LC_NUMERIC, oldlocale);
}

static void
gf_pso_ensure_space (GFPSObject * pso, gint size)
{
	if (pso->length + size > pso->bufsize) {
		while (pso->length + size > pso->bufsize) pso->bufsize <<= 1;
		pso->buf = g_renew (guchar, pso->buf, pso->bufsize);
	}
}

