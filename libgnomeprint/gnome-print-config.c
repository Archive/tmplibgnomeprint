#define __GNOME_PRINT_CONFIG_C__

/*
 * And frontend abstraction to whatever config system we eventually have
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors:
 *   Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright 2001 Ximian, Inc.
 *
 */

#include "gpa/gpa-node-private.h"
#include "gnome-print-config.h"

struct _GnomePrintConfig {
	GPANode *node;
};

GnomePrintConfig *
gnome_print_config_default (void)
{
	return (GnomePrintConfig *) gpa_defaults ();
}

GnomePrintConfig *
gnome_print_config_ref (GnomePrintConfig *config)
{
	g_return_val_if_fail (config != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (config), NULL);

	gpa_node_ref ((GPANode *) config);

	return config;
}

GnomePrintConfig *
gnome_print_config_unref (GnomePrintConfig *config)
{
	g_return_val_if_fail (config != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (config), NULL);

	gpa_node_unref ((GPANode *) config);

	return NULL;
}

guchar *
gnome_print_config_get (GnomePrintConfig *config, const guchar *key)
{
	g_return_val_if_fail (config != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (config), NULL);
	g_return_val_if_fail (key != NULL, NULL);
	g_return_val_if_fail (*key != '\0', NULL);

	return gpa_node_get_path_value ((GPANode *) config, key);
}

gboolean
gnome_print_config_set (GnomePrintConfig *config, const guchar *key, const guchar *value)
{
	g_return_val_if_fail (config != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (config), FALSE);
	g_return_val_if_fail (key != NULL, FALSE);
	g_return_val_if_fail (*key != '\0', FALSE);

	return gpa_node_set_path_value ((GPANode *) config, key, value);
}

gboolean
gnome_print_config_get_boolean (GnomePrintConfig *config, const guchar *key, gboolean *val)
{
	g_return_val_if_fail (config != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (config), FALSE);
	g_return_val_if_fail (key != NULL, FALSE);
	g_return_val_if_fail (*key != '\0', FALSE);
	g_return_val_if_fail (val != NULL, FALSE);

	return gpa_node_get_bool_path_value ((GPANode *) config, key, val);
}

gboolean
gnome_print_config_get_int (GnomePrintConfig *config, const guchar *key, gint *val)
{
	g_return_val_if_fail (config != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (config), FALSE);
	g_return_val_if_fail (key != NULL, FALSE);
	g_return_val_if_fail (*key != '\0', FALSE);
	g_return_val_if_fail (val != NULL, FALSE);

	return gpa_node_get_int_path_value ((GPANode *) config, key, val);
}

gboolean
gnome_print_config_get_double (GnomePrintConfig *config, const guchar *key, gdouble *val)
{
	g_return_val_if_fail (config != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (config), FALSE);
	g_return_val_if_fail (key != NULL, FALSE);
	g_return_val_if_fail (*key != '\0', FALSE);
	g_return_val_if_fail (val != NULL, FALSE);

	return gpa_node_get_double_path_value ((GPANode *) config, key, val);
}

gboolean
gnome_print_config_set_boolean (GnomePrintConfig *config, const guchar *key, gboolean val)
{
	g_return_val_if_fail (config != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (config), FALSE);
	g_return_val_if_fail (key != NULL, FALSE);
	g_return_val_if_fail (*key != '\0', FALSE);

	return gpa_node_set_bool_path_value ((GPANode *) config, key, val);
}

gboolean
gnome_print_config_set_int (GnomePrintConfig *config, const guchar *key, gint val)
{
	g_return_val_if_fail (config != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (config), FALSE);
	g_return_val_if_fail (key != NULL, FALSE);
	g_return_val_if_fail (*key != '\0', FALSE);

	return gpa_node_set_int_path_value ((GPANode *) config, key, val);
}

gboolean
gnome_print_config_set_double (GnomePrintConfig *config, const guchar *key, gdouble val)
{
	g_return_val_if_fail (config != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (config), FALSE);
	g_return_val_if_fail (key != NULL, FALSE);
	g_return_val_if_fail (*key != '\0', FALSE);

	return gpa_node_set_double_path_value ((GPANode *) config, key, val);
}


