#define _GP_FONTMAP_C_

/*
 * Fontmap implementation
 *
 * Authors:
 *   Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * TODO: Recycle font entries, if they are identical for different maps
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <parser.h>
#include <xmlmemory.h>
#include "gp-fontmap.h"

static GPFontMap * gp_fontmap_load (void);
static void gp_fontmap_ref (GPFontMap * map);
static void gp_fontmap_unref (GPFontMap * map);
static void gp_family_entry_ref (GPFamilyEntry * entry);
static void gp_family_entry_unref (GPFamilyEntry * entry);
static void gp_fm_load_fonts (GPFontMap * map, xmlDoc * doc);
/* Fontmap 1.0 */
static void gp_fm_load_font_1_0 (GPFontMap * map, xmlNodePtr node);
static void gp_fm_load_font_1_0_alias (GPFontMap * map, xmlNodePtr node);
/* Fontmap 2.0 */
static void gp_fm_load_font_2_0 (GPFontMap * map, xmlNodePtr node, gboolean alias);
static void gp_fm_load_font_2_0_alias (GPFontMap * map, xmlNodePtr node);
/* Fontmap 3.0 */
static void gp_fm_load_font_3_0 (GPFontMap * map, xmlNodePtr node, gboolean alias);
static void gp_fm_load_font_3_0_tt (GPFontMap * map, xmlNodePtr node);
static void gp_fm_load_font_3_0_alias (GPFontMap * map, xmlNodePtr node);

static void gp_fm_load_aliases (GPFontMap * map, xmlDoc * doc);
static gchar * gp_xmlGetPropString (xmlNodePtr node, const gchar * name);
static gint gp_fe_sortname (gconstpointer a, gconstpointer b);
static gint gp_fe_sortspecies (gconstpointer a, gconstpointer b);
static gint gp_familyentry_sortname (gconstpointer a, gconstpointer b);
static gchar * gp_fm_get_species_name (const gchar * fullname, const gchar * familyname);
static gboolean gp_fm_is_changed (GPFontMap * map);

/* Fontlist -> FontMap */
static GHashTable * fontlist2map = NULL;
/* Familylist -> FontMap */
static GHashTable * familylist2map = NULL;

GPFontMap *
gp_fontmap_get (void)
{
	static GPFontMap * map = NULL;

	if (map) {
		if (gp_fm_is_changed (map)) {
#if 0
			g_print ("Fontmap is changed, reloading\n");
#endif
			gp_fontmap_release (map);
			map = NULL;
		}
	}

	if (!map) map = gp_fontmap_load ();

	map->refcount++;

	return map;
}

void
gp_fontmap_release (GPFontMap * map)
{
	gp_fontmap_unref (map);
}

static void
gp_fontmap_ref (GPFontMap * map)
{
	g_return_if_fail (map != NULL);

	map->refcount++;
}

static void
gp_fontmap_unref (GPFontMap * map)
{
	g_return_if_fail (map != NULL);

	if (--map->refcount < 1) {
#if 0
		g_print ("Releasing fontmap\n");
#endif
		if (map->user.name) g_free (map->user.name);
		if (map->system.name) g_free (map->system.name);
		if (map->familydict) g_hash_table_destroy (map->familydict);
		if (map->fontdict) g_hash_table_destroy (map->fontdict);
		if (map->familylist) {
			g_hash_table_remove (familylist2map, map->familylist);
			g_list_free (map->familylist);
		}
		if (map->fontlist) {
			g_hash_table_remove (fontlist2map, map->fontlist);
			g_list_free (map->fontlist);
		}
		while (map->families) {
			gp_family_entry_unref ((GPFamilyEntry *) map->families->data);
			map->families = g_slist_remove (map->families, map->families->data);
		}
		while (map->fonts) {
			gp_font_entry_unref ((GPFontEntry *) map->fonts->data);
			map->fonts = g_slist_remove (map->fonts, map->fonts->data);
		}
		g_free (map);
	}
}

static void
gp_family_entry_ref (GPFamilyEntry * entry)
{
	entry->refcount++;
}

static void
gp_family_entry_unref (GPFamilyEntry * entry)
{
	if (--entry->refcount < 1) {
		if (entry->name) g_free (entry->name);
		if (entry->fonts) g_slist_free (entry->fonts);
		g_free (entry);
	}
}

static GPFontMap *
gp_fontmap_load (void)
{
	GPFontMap * map;
	gchar * home, * name;
	xmlDoc * usermap, * systemmap;
	struct stat s;
	GSList * l;

	map = g_new (GPFontMap, 1);
	map->refcount = 1; /* Permanent fontmap */
	map->num_fonts = 0;
	map->user.name = NULL;
	map->system.name = NULL;
	map->fontdict = g_hash_table_new (g_str_hash, g_str_equal);
	map->familydict = g_hash_table_new (g_str_hash, g_str_equal);
	map->fonts = NULL;
	map->families = NULL;
	map->fontlist = NULL;
	map->familylist = NULL;

	/* Usermap */

	usermap = NULL;
	home = getenv ("HOME");
	g_return_val_if_fail (home != NULL, NULL);
	name = g_strdup_printf ("%s/.gnome/fonts/fontmap", home);

	if ((stat (name, &s) == 0) && S_ISREG (s.st_mode) && (usermap = xmlParseFile (name))) {
		map->user.name = name;
		map->user.size = s.st_size;
		map->user.mtime = s.st_mtime;
	} else {
		g_free (name);
	}

	/* System map */

	systemmap = NULL;
	name = g_strdup (DATADIR "/fonts/fontmap3");
	if ((stat (name, &s) == 0) && S_ISREG (s.st_mode) && (systemmap = xmlParseFile (name))) {
		map->system.name = name;
		map->system.size = s.st_size;
		map->system.mtime = s.st_mtime;
	} else {
		g_free (name);
	}
	if (!systemmap) {
		name = g_strdup (DATADIR "/fonts/fontmap2");
		if ((stat (name, &s) == 0) && S_ISREG (s.st_mode) && (systemmap = xmlParseFile (name))) {
			map->system.name = name;
			map->system.size = s.st_size;
			map->system.mtime = s.st_mtime;
		} else {
			g_free (name);
		}
	}
	if (!systemmap) {
		name = g_strdup (DATADIR "/fonts/fontmap");
		if ((stat (name, &s) == 0) && S_ISREG (s.st_mode) && (systemmap = xmlParseFile (name))) {
			map->system.name = name;
			map->system.size = s.st_size;
			map->system.mtime = s.st_mtime;
		} else {
			g_free (name);
		}
	}

	if (usermap) gp_fm_load_fonts (map, usermap);
	if (systemmap) gp_fm_load_fonts (map, systemmap);
	if (usermap) gp_fm_load_aliases (map, usermap);
	if (systemmap) gp_fm_load_aliases (map, systemmap);
	if (usermap) xmlFreeDoc (usermap);
	if (systemmap) xmlFreeDoc (systemmap);

	map->fonts = g_slist_sort (map->fonts, gp_fe_sortname);

	for (l = map->fonts; l != NULL; l = l->next) {
		GPFontEntry * e;
		GPFamilyEntry * f;
		e = (GPFontEntry *) l->data;
		if (!e->hidden) {
			f = g_hash_table_lookup (map->familydict, e->familyname);
			if (!f) {
				f = g_new0 (GPFamilyEntry, 1);
				gp_family_entry_ref (f);
				f->name = g_strdup (e->familyname);
				f->fonts = g_slist_prepend (f->fonts, e);
				g_hash_table_insert (map->familydict, f->name, f);
				map->families = g_slist_prepend (map->families, f);
			} else {
				f->fonts = g_slist_prepend (f->fonts, e);
			}
		}
	}

	for (l = map->families; l != NULL; l = l->next) {
		GPFamilyEntry * f;
		f = (GPFamilyEntry *) l->data;
		f->fonts = g_slist_sort (f->fonts, gp_fe_sortspecies);
	}

	map->families = g_slist_sort (map->families, gp_familyentry_sortname);

	return map;
}

/* Load fontmap from xml tree */

static void
gp_fm_load_fonts (GPFontMap * map, xmlDoc * doc)
{
	xmlNodePtr root;

	root = xmlDocGetRootElement (doc);

	if (!strcmp (root->name, "fontmap")) {
		xmlChar * version;
		version = xmlGetProp (root, "version");
		if (!version) {
			/* Version 1.0 */
			xmlNodePtr child;
			for (child = root->xmlChildrenNode; child != NULL; child = child->next) {
				xmlChar *format;
				format = xmlGetProp (child, "format");
				if (format && !strcmp (format, "type1")) {
					gp_fm_load_font_1_0 (map, child);
				}
				if (format) xmlFree (format);
			}
		} else if (!strcmp (version, "2.0")) {
			/* Version 2.0 */
			xmlNodePtr child;
			for (child = root->xmlChildrenNode; child != NULL; child = child->next) {
				xmlChar *format;
				format = xmlGetProp (child, "format");
				if (format && !strcmp (format, "type1")) {
					gp_fm_load_font_2_0 (map, child, FALSE);
				}
				if (format) xmlFree (format);
			}
			xmlFree (version);
		} else if (!strcmp (version, "3.0")) {
			/* Version 3.0 */
			xmlNodePtr child;
			for (child = root->xmlChildrenNode; child != NULL; child = child->next) {
				xmlChar *format;
				format = xmlGetProp (child, "format");
				if (format && !strcmp (format, "type1")) {
					gp_fm_load_font_3_0 (map, child, FALSE);
				} else if (format && !strcmp (format, "truetype")) {
					gp_fm_load_font_3_0_tt (map, child);
				}
				if (format) xmlFree (format);
			}
			xmlFree (version);
		} else {
			g_warning ("file %s: line %d: Unknown fontmap version %s", __FILE__, __LINE__, version);
			xmlFree (version);
		}
	}
}

/* Load alias or type1alias entries and build font entries from these */
static void
gp_fm_load_aliases (GPFontMap * map, xmlDoc * doc)
{
	xmlNodePtr root;

	root = xmlDocGetRootElement (doc);

	if (!strcmp (root->name, "fontmap")) {
		xmlChar * version;
		version = xmlGetProp (root, "version");
		if (!version) {
			/* Version 1.0 */
			xmlNodePtr child;
			for (child = root->xmlChildrenNode; child != NULL; child = child->next) {
				if (!strcmp (child->name, "font")) {
					xmlChar *alias;
					alias = xmlGetProp (child, "alias");
					if (alias) {
						gp_fm_load_font_1_0_alias (map, child);
					}
					if (alias) xmlFree (alias);
				}
			}
		} else if (!strcmp (version, "2.0")) {
			xmlNodePtr child;
			xmlFree (version);
			for (child = root->xmlChildrenNode; child != NULL; child = child->next) {
				if (!strcmp (child->name, "font")) {
					xmlChar * format;
					format = xmlGetProp (child, "format");
					if (format && !strcmp (format, "type1alias")) {
						gp_fm_load_font_2_0_alias (map, child);
					}
					if (format) xmlFree (format);
				}
			}
		} else if (!strcmp (version, "3.0")) {
			xmlNodePtr child;
			xmlFree (version);
			for (child = root->xmlChildrenNode; child != NULL; child = child->next) {
				if (!strcmp (child->name, "font")) {
					xmlChar * format;
					format = xmlGetProp (child, "format");
					if (format && !strcmp (format, "alias")) {
						gp_fm_load_font_3_0_alias (map, child);
					}
					if (format) xmlFree (format);
				}
			}
		} else {
			g_warning ("file %s: line %d: Unknown fonrmap version %s", __FILE__, __LINE__, version);
			xmlFree (version);
		}
	}
}

static void
gp_fm_load_font_1_0 (GPFontMap * map, xmlNodePtr node)
{
	GPFontEntryT1 *t1;
	GPFontEntry *e;
	xmlChar *alias;
	gchar *fullname, *p;

	alias = xmlGetProp (node, "alias");
	if (alias) {
		/* We build aliases later */
		xmlFree (alias);
		return;
	}

	fullname = gp_xmlGetPropString (node, "fullname");
	if (!fullname) return;
	if (g_hash_table_lookup (map->fontdict, fullname)) {
		g_free (fullname);
		return;
	}

	t1 = g_new0 (GPFontEntryT1, 1);
	e = (GPFontEntry *) t1;
	e->type = GP_FONT_ENTRY_TYPE1;

	e->refcount = 1;
	e->face = NULL;

	t1->afm.name = gp_xmlGetPropString (node, "metrics");
	t1->pfb.name = gp_xmlGetPropString (node, "glyphs");
	e->name = fullname;
	e->version = gp_xmlGetPropString (node, "version");
	e->familyname = gp_xmlGetPropString (node, "familyname");
	e->psname = gp_xmlGetPropString (node, "name");

	if (!(t1->pfb.name && e->familyname && e->psname)) {
		gp_font_entry_unref (e);
		return;
	}

	/* fixme: check integrity */

	/* Read fontmap 1.0 weight string */
	e->weight = gp_xmlGetPropString (node, "weight");
	if (e->weight) {
		t1->Weight = gp_fontmap_lookup_weight (e->weight);
	} else {
		e->weight = g_strdup ("Book");
		t1->Weight = GNOME_FONT_BOOK;
	}

	/* Discover species name */
	e->speciesname = gp_fm_get_species_name (e->name, e->familyname);

	/* Parse Italic from species name */
	p = strstr (e->speciesname, "Italic");
	if (!p) p = strstr (e->speciesname, "Oblique");

	if (p) {
		t1->ItalicAngle = -10.0;
	} else {
		t1->ItalicAngle = 0.0;
	}

	g_hash_table_insert (map->fontdict, e->name, e);
	map->num_fonts++;
	map->fonts = g_slist_prepend (map->fonts, e);
}

static void
gp_fm_load_font_1_0_alias (GPFontMap * map, xmlNodePtr node)
{
	GPFontEntryAlias *a;
	GPFontEntry *e, *o;
	gchar *fullname, *alias, *p;
	GSList *f;

	fullname = gp_xmlGetPropString (node, "fullname");
	if (!fullname) return;
	if (g_hash_table_lookup (map->fontdict, fullname)) {
		g_free (fullname);
		return;
	}

	alias = gp_xmlGetPropString (node, "alias");
	g_return_if_fail (alias != NULL);
	/* Alias is specified as psname, so do some searching */
	o = NULL;
	for (f = map->fonts; f != NULL; f = f->next) {
		GPFontEntry *e;
		e = (GPFontEntry *) f->data;
		if (!strcmp (alias, e->psname)) {
			o = e;
			break;
		}
	}
	if (!o) {
		/* Original is not registered, so we have to build hidden font */
		GPFontEntryT1 *t1;
		t1 = g_new0 (GPFontEntryT1, 1);
		o = (GPFontEntry *) t1;
		o->type = GP_FONT_ENTRY_TYPE1;
		o->refcount = 1;
		o->hidden = TRUE;
		o->face = NULL;
		/* We ignore afm here */
		t1->pfb.name = gp_xmlGetPropString (node, "glyphs");
		/* Build fake fullname */
		if (strchr (alias, '-')) {
			gchar *p;
			o->name = g_strdup (alias);
			for (p = o->name; *p; p++) if (*p == '-') *p = ' ';
			o->familyname = g_strdup (alias);
			p = strchr (o->familyname, '-');
			*p = '\0';
		} else {
			o->name = g_strdup_printf ("%s Regular", alias);
			o->familyname = g_strdup (alias);
		}
		o->version = g_strdup ("1.0");
		o->psname = alias;
		if (!(t1->pfb.name && o->familyname && o->psname)) {
			gp_font_entry_unref (o);
			g_free (alias);
			return;
		}
		/* fixme: check integrity */
		/* Read fontmap 1.0 weight string */
		o->weight = gp_xmlGetPropString (node, "weight");
		if (o->weight) {
			t1->Weight = gp_fontmap_lookup_weight (o->weight);
		} else {
			o->weight = g_strdup ("Book");
			t1->Weight = GNOME_FONT_BOOK;
		}
		/* Discover species name */
		o->speciesname = gp_fm_get_species_name (o->name, o->familyname);
		/* Parse Italic from species name */
		p = strstr (o->speciesname, "Ital");
		if (!p) p = strstr (o->speciesname, "Obli");
		if (p) {
			t1->ItalicAngle = -10.0;
		} else {
			t1->ItalicAngle = 0.0;
		}
		g_hash_table_insert (map->fontdict, o->name, o);
		map->num_fonts++;
		map->fonts = g_slist_prepend (map->fonts, o);
	}
	if (!o) return;
	g_free (alias);

	a = g_new0 (GPFontEntryAlias, 1);
	e = (GPFontEntry *) a;
	e->type = GP_FONT_ENTRY_ALIAS;
	e->refcount = 1;
	e->face = NULL;

	e->name = fullname;
	e->version = gp_xmlGetPropString (node, "version");
	e->familyname = gp_xmlGetPropString (node, "familyname");
	e->psname = gp_xmlGetPropString (node, "name");

	if (!(e->familyname && e->psname)) {
		gp_font_entry_unref (e);
		return;
	}

	/* fixme: check integrity */

	/* Read fontmap 1.0 weight string */
	e->weight = gp_xmlGetPropString (node, "weight");
	if (!e->weight) {
		e->weight = g_strdup ("Book");
	}

	/* Discover species name */
	e->speciesname = gp_fm_get_species_name (e->name, e->familyname);

	gp_font_entry_ref (o);
	a->ref = o;

	g_hash_table_insert (map->fontdict, e->name, e);
	map->num_fonts++;
	map->fonts = g_slist_prepend (map->fonts, e);
}

static void
gp_fm_load_font_2_0 (GPFontMap * map, xmlNodePtr node, gboolean type1alias)
{
	GPFontEntryT1 * t1;
	GPFontEntry * e;
	gchar * p;
	xmlNodePtr child;
	xmlChar * t;

	t1 = g_new0 (GPFontEntryT1, 1);
	e = (GPFontEntry *) t1;
	e->type = GP_FONT_ENTRY_TYPE1;
	e->refcount = 1;
	e->hidden = type1alias;
	e->face = NULL;

	child = node->xmlChildrenNode;
	while (child) {
		if (!strcmp (child->name, "file")) {
			xmlChar * type;
			type = xmlGetProp (child, "type");
			if (!type1alias && !strcmp (type, "afm")) {
				t1->afm.name = gp_xmlGetPropString (child, "path");
				t = xmlGetProp (child, "size");
				if (t) t1->afm.size = atoi (t);
				if (t) xmlFree (t);
				t = xmlGetProp (child, "mtime");
				if (t) t1->afm.mtime = atoi (t);
				if (t) xmlFree (t);
			} else if (!strcmp (type, "pfb")) {
				t1->pfb.name = gp_xmlGetPropString (child, "path");
				t = xmlGetProp (child, "size");
				if (t) t1->pfb.size = atoi (t);
				if (t) xmlFree (t);
				t = xmlGetProp (child, "mtime");
				if (t) t1->pfb.mtime = atoi (t);
				if (t) xmlFree (t);
			}
			xmlFree (type);
		}
		if (t1->afm.name && t1->pfb.name) break;
		child = child->next;
	}

	if (!t1->pfb.name) {
		gp_font_entry_unref (e);
		return;
	}

	e->name = gp_xmlGetPropString (node, "name");
	e->version = gp_xmlGetPropString (node, "version");
	e->familyname = gp_xmlGetPropString (node, "familyname");
	e->speciesname = gp_xmlGetPropString (node, "speciesname");
	e->psname = gp_xmlGetPropString (node, "psname");

	if (!(e->name && e->familyname && e->psname)) {
		gp_font_entry_unref (e);
		return;
	}

	/* fixme: check integrity */

	/* Read fontmap 1.0 weight string */
	e->weight = gp_xmlGetPropString (node, "weight");
	if (e->weight) {
		t1->Weight = gp_fontmap_lookup_weight (e->weight);
	} else {
		e->weight = g_strdup ("Book");
		t1->Weight = GNOME_FONT_BOOK;
	}

	/* Discover species name */
	if (!e->speciesname) e->speciesname = gp_fm_get_species_name (e->name, e->familyname);

	/* Parse Italic */
	t = xmlGetProp (node, "italicangle");
	if (!t) {
		p = strstr (e->speciesname, "Italic");
		if (!p) p = strstr (e->speciesname, "Oblique");
		if (p) {
			t1->ItalicAngle = -10.0;
		} else {
			t1->ItalicAngle = 0.0;
		}
	} else {
		t1->ItalicAngle = atof (t);
		xmlFree (t);
	}

	/* fixme: fixme: fixme: */
	if (g_hash_table_lookup (map->fontdict, e->name)) {
		gp_font_entry_unref (e);
		return;
	}

	g_hash_table_insert (map->fontdict, e->name, e);
	map->num_fonts++;
	map->fonts = g_slist_prepend (map->fonts, e);
}

/* Load type1alias font type */

static void
gp_fm_load_font_2_0_alias (GPFontMap *map, xmlNodePtr node)
{
	GPFontEntryAlias *alias;
	GPFontEntry *e, *o;
	xmlChar *t;
	gchar *xmlalias;
	GSList *f;

	o = NULL;

	/* Search for original using psname */
	xmlalias = xmlGetProp (node, "alias");
	g_return_if_fail (xmlalias != NULL);
	/* Alias is specified as psname, so do some searching */
	for (f = map->fonts; f != NULL; f = f->next) {
		GPFontEntry *e;
		e = (GPFontEntry *) f->data;
		if (!strcmp (xmlalias, e->psname)) {
			o = e;
			break;
		}
	}
	if (!o) {
		/* Original is not registered, so we have to build hidden font */
		GPFontEntryT1 *t1;
		xmlNodePtr child;
		gchar *p;
		t1 = g_new0 (GPFontEntryT1, 1);
		o = (GPFontEntry *) t1;
		o->type = GP_FONT_ENTRY_TYPE1;
		o->refcount = 1;
		o->hidden = TRUE;
		o->face = NULL;
		/* We ignore afm here */
		for (child = node->xmlChildrenNode; child != NULL; child = child->next) {
			if (!strcmp (child->name, "file")) {
				xmlChar * type;
				type = xmlGetProp (child, "type");
				if (!strcmp (type, "pfb")) {
					t1->pfb.name = gp_xmlGetPropString (child, "path");
					t = xmlGetProp (child, "size");
					if (t) t1->pfb.size = atoi (t);
					if (t) xmlFree (t);
					t = xmlGetProp (child, "mtime");
					if (t) t1->pfb.mtime = atoi (t);
					if (t) xmlFree (t);
				}
				xmlFree (type);
			}
			if (t1->pfb.name) break;
		}
		if (!t1->pfb.name) {
			gp_font_entry_unref (o);
			xmlFree (xmlalias);
			return;
		}
		/* Build fake fullname */
		if (strchr (xmlalias, '-')) {
			gchar *p;
			o->name = g_strdup (xmlalias);
			for (p = o->name; *p; p++) if (*p == '-') *p = ' ';
			o->familyname = g_strdup (xmlalias);
			p = strchr (o->familyname, '-');
			*p = '\0';
		} else {
			o->name = g_strdup_printf ("%s Regular", xmlalias);
			o->familyname = g_strdup (xmlalias);
		}
		o->version = g_strdup ("1.0");
		o->psname = g_strdup (xmlalias);
		if (!(t1->pfb.name && o->familyname && o->psname)) {
			gp_font_entry_unref (o);
			xmlFree (xmlalias);
			return;
		}
		/* fixme: check integrity */
		/* Read fontmap 1.0 weight string */
		o->weight = gp_xmlGetPropString (node, "weight");
		if (o->weight) {
			t1->Weight = gp_fontmap_lookup_weight (o->weight);
		} else {
			o->weight = g_strdup ("Book");
			t1->Weight = GNOME_FONT_BOOK;
		}
		/* Discover species name */
		o->speciesname = gp_fm_get_species_name (o->name, o->familyname);
		/* Parse Italic from species name */
		p = strstr (o->speciesname, "Ital");
		if (!p) p = strstr (o->speciesname, "Obli");
		if (p) {
			t1->ItalicAngle = -10.0;
		} else {
			t1->ItalicAngle = 0.0;
		}
		g_hash_table_insert (map->fontdict, o->name, o);
		map->num_fonts++;
		map->fonts = g_slist_prepend (map->fonts, o);
	}
	xmlFree (xmlalias);
	if (!o) return;

	alias = g_new0 (GPFontEntryAlias, 1);
	alias->entry.type = GP_FONT_ENTRY_ALIAS;

	e = (GPFontEntry *) alias;
	e->refcount = 1;
	e->face = NULL;

	e->name = gp_xmlGetPropString (node, "name");
	e->version = gp_xmlGetPropString (node, "version");
	e->familyname = gp_xmlGetPropString (node, "familyname");
	e->speciesname = gp_xmlGetPropString (node, "speciesname");
	e->psname = gp_xmlGetPropString (node, "psname");

	if (!(e->name && e->familyname && e->psname)) {
		gp_font_entry_unref (e);
		return;
	}

	/* fixme: check integrity */

	/* Read fontmap 1.0 weight string */
	gp_font_entry_ref (o);
	alias->ref = o;

	t = xmlGetProp (node, "hideoriginal");
	if (t != NULL) {
		alias->ref->hidden = TRUE;
		xmlFree (t);
	}

	if (!e->speciesname) e->speciesname = gp_fm_get_species_name (e->name, e->familyname);

	/* fixme: fixme: fixme: */

	if (g_hash_table_lookup (map->fontdict, e->name)) {
		gp_font_entry_unref (e);
		return;
	}

	g_hash_table_insert (map->fontdict, e->name, e);
	map->num_fonts++;
	map->fonts = g_slist_prepend (map->fonts, e);
}

static void
gp_fm_load_font_3_0 (GPFontMap * map, xmlNodePtr node, gboolean type1alias)
{
	GPFontEntryT1 * t1;
	GPFontEntry * e;
	gchar * p;
	xmlNodePtr child;
	xmlChar * t;

	t1 = g_new0 (GPFontEntryT1, 1);
	e = (GPFontEntry *) t1;
	e->type = GP_FONT_ENTRY_TYPE1;
	e->refcount = 1;
	e->hidden = type1alias;
	e->face = NULL;

	child = node->xmlChildrenNode;
	while (child) {
		if (!strcmp (child->name, "file")) {
			xmlChar * type;
			type = xmlGetProp (child, "type");
			if (!type1alias && !strcmp (type, "afm")) {
				t1->afm.name = gp_xmlGetPropString (child, "path");
				t = xmlGetProp (child, "size");
				if (t) t1->afm.size = atoi (t);
				if (t) xmlFree (t);
				t = xmlGetProp (child, "mtime");
				if (t) t1->afm.mtime = atoi (t);
				if (t) xmlFree (t);
			} else if (!strcmp (type, "pfb")) {
				t1->pfb.name = gp_xmlGetPropString (child, "path");
				t = xmlGetProp (child, "size");
				if (t) t1->pfb.size = atoi (t);
				if (t) xmlFree (t);
				t = xmlGetProp (child, "mtime");
				if (t) t1->pfb.mtime = atoi (t);
				if (t) xmlFree (t);
			}
			xmlFree (type);
		}
		if (t1->afm.name && t1->pfb.name) break;
		child = child->next;
	}

	if (!t1->pfb.name) {
		gp_font_entry_unref (e);
		return;
	}

	e->name = gp_xmlGetPropString (node, "name");
	e->version = gp_xmlGetPropString (node, "version");
	e->familyname = gp_xmlGetPropString (node, "familyname");
	e->speciesname = gp_xmlGetPropString (node, "speciesname");
	e->psname = gp_xmlGetPropString (node, "psname");

	if (!(e->name && e->familyname && e->psname)) {
		gp_font_entry_unref (e);
		return;
	}

	/* fixme: check integrity */

	/* Read fontmap 1.0 weight string */
	e->weight = gp_xmlGetPropString (node, "weight");
	if (e->weight) {
		t1->Weight = gp_fontmap_lookup_weight (e->weight);
	} else {
		e->weight = g_strdup ("Book");
		t1->Weight = GNOME_FONT_BOOK;
	}

	/* Discover species name */
	if (!e->speciesname) e->speciesname = gp_fm_get_species_name (e->name, e->familyname);

	/* Parse Italic */
	t = xmlGetProp (node, "italicangle");
	if (!t) {
		p = strstr (e->speciesname, "Italic");
		if (!p) p = strstr (e->speciesname, "Oblique");
		if (p) {
			t1->ItalicAngle = -10.0;
		} else {
			t1->ItalicAngle = 0.0;
		}
	} else {
		t1->ItalicAngle = atof (t);
		xmlFree (t);
	}

	/* fixme: fixme: fixme: */
	if (g_hash_table_lookup (map->fontdict, e->name)) {
		gp_font_entry_unref (e);
		return;
	}

	g_hash_table_insert (map->fontdict, e->name, e);
	map->num_fonts++;
	map->fonts = g_slist_prepend (map->fonts, e);
}

static void
gp_fm_load_font_3_0_tt (GPFontMap * map, xmlNodePtr node)
{
	GPFontEntryTT * tt;
	GPFontEntry * e;
	gchar * p;
	xmlNodePtr child;
	xmlChar * t;

	tt = g_new0 (GPFontEntryTT, 1);
	tt->entry.type = GP_FONT_ENTRY_TRUETYPE;

	child = node->xmlChildrenNode;
	while (child) {
		if (!strcmp (child->name, "file")) {
			xmlChar * type;
			type = xmlGetProp (child, "type");
			if (!strcmp (type, "ttf")) {
				tt->ttf.name = gp_xmlGetPropString (child, "path");
				t = xmlGetProp (child, "size");
				if (t) tt->ttf.size = atoi (t);
				if (t) xmlFree (t);
				t = xmlGetProp (child, "mtime");
				if (t) tt->ttf.mtime = atoi (t);
				if (t) xmlFree (t);
			}
			xmlFree (type);
		}
		if (tt->ttf.name) break;
		child = child->next;
	}

	e = (GPFontEntry *) tt;

	e->refcount = 1;
	e->face = NULL;

	if (!tt->ttf.name) {
		gp_font_entry_unref (e);
		return;
	}

	e->name = gp_xmlGetPropString (node, "name");
	e->version = gp_xmlGetPropString (node, "version");
	e->familyname = gp_xmlGetPropString (node, "familyname");
	e->speciesname = gp_xmlGetPropString (node, "speciesname");
	e->psname = gp_xmlGetPropString (node, "psname");

	if (!(e->name && e->familyname && e->psname)) {
		gp_font_entry_unref (e);
		return;
	}

	/* fixme: check integrity */

	/* Read fontmap 1.0 weight string */

	e->weight = gp_xmlGetPropString (node, "weight");
	if (e->weight) {
		tt->Weight = gp_fontmap_lookup_weight (e->weight);
	} else {
		e->weight = g_strdup ("Book");
		tt->Weight = GNOME_FONT_BOOK;
	}

	/* Discover species name */

	if (!e->speciesname) e->speciesname = gp_fm_get_species_name (e->name, e->familyname);

	/* Parse Italic */

	t = xmlGetProp (node, "italicangle");
	if (!t) {
		p = strstr (e->speciesname, "Italic");
		if (!p) p = strstr (e->speciesname, "Oblique");
		if (p) {
			tt->ItalicAngle = -10.0;
		} else {
			tt->ItalicAngle = 0.0;
		}
	} else {
		tt->ItalicAngle = atof (t);
		xmlFree (t);
	}

	/* fixme: fixme: fixme: */

	if (g_hash_table_lookup (map->fontdict, e->name)) {
		gp_font_entry_unref (e);
		return;
	}

	g_hash_table_insert (map->fontdict, e->name, e);
	map->num_fonts++;
	map->fonts = g_slist_prepend (map->fonts, e);
}

/* Load alias font type */

static void
gp_fm_load_font_3_0_alias (GPFontMap *map, xmlNodePtr node)
{
	GPFontEntryAlias * alias;
	GPFontEntry *e, *o;
	xmlChar *t;

	o = NULL;

	t = xmlGetProp (node, "original");
	if (t != NULL) {
		GPFontEntry * ref;
		ref = g_hash_table_lookup (map->fontdict, t);
		/* Redirection is not allowed */
		if (ref && ref->type != GP_FONT_ENTRY_ALIAS) {
			o = ref;
		}
		xmlFree (t);
	}
	if (!o) return;

	alias = g_new0 (GPFontEntryAlias, 1);
	alias->entry.type = GP_FONT_ENTRY_ALIAS;

	e = (GPFontEntry *) alias;
	e->refcount = 1;
	e->face = NULL;

	e->name = gp_xmlGetPropString (node, "name");
	e->version = gp_xmlGetPropString (node, "version");
	e->familyname = gp_xmlGetPropString (node, "familyname");
	e->speciesname = gp_xmlGetPropString (node, "speciesname");
	e->psname = g_strdup (o->psname);

	if (!(e->name && e->familyname && e->psname)) {
		gp_font_entry_unref (e);
		return;
	}

	/* fixme: check integrity */

	/* Read fontmap 1.0 weight string */
	gp_font_entry_ref (o);
	alias->ref = o;

	t = xmlGetProp (node, "hideoriginal");
	if (t != NULL) {
		alias->ref->hidden = TRUE;
		xmlFree (t);
	}

	if (!e->speciesname) e->speciesname = gp_fm_get_species_name (e->name, e->familyname);

	/* fixme: fixme: fixme: */

	if (g_hash_table_lookup (map->fontdict, e->name)) {
		gp_font_entry_unref (e);
		return;
	}

	g_hash_table_insert (map->fontdict, e->name, e);
	map->num_fonts++;
	map->fonts = g_slist_prepend (map->fonts, e);
}



/*
 * Font Entry stuff
 *
 * If face is created, it has to reference entry
 */

void
gp_font_entry_ref (GPFontEntry * entry)
{
	g_return_if_fail (entry != NULL);
	g_return_if_fail (entry->refcount > 0);

	entry->refcount++;
}

void
gp_font_entry_unref (GPFontEntry * entry)
{
	g_return_if_fail (entry != NULL);
	/* refcount can be 1 or 2 at moment */
	g_return_if_fail (entry->refcount > 0);

	if (--entry->refcount < 1) {
		GPFontEntryT1 * t1;
#if 0
		GPFontEntryT1Alias * t1a;
#endif
		GPFontEntryAlias * a;

		g_return_if_fail (entry->face == NULL);

		if (entry->name) g_free (entry->name);
		if (entry->version) g_free (entry->version);
		if (entry->familyname) g_free (entry->familyname);
		if (entry->speciesname) g_free (entry->speciesname);
		if (entry->psname) g_free (entry->psname);
		if (entry->weight) g_free (entry->weight);

		switch (entry->type) {
#if 0
		case GP_FONT_ENTRY_TYPE1_ALIAS:
			t1a = (GPFontEntryT1Alias *) entry;
			if (t1a->alias) g_free (t1a->alias);
#endif
		case GP_FONT_ENTRY_TYPE1:
			t1 = (GPFontEntryT1 *) entry;
			if (t1->afm.name) g_free (t1->afm.name);
			if (t1->pfb.name) g_free (t1->pfb.name);
			break;
		case GP_FONT_ENTRY_ALIAS:
			a = (GPFontEntryAlias *) entry;
			if (a->ref) gp_font_entry_unref (a->ref);
			break;
		default:
			g_assert_not_reached ();
			break;
		}
		g_free (entry);
	}
}

/*
 * Font list stuff
 *
 * We use Hack'O'Hacks here:
 * Getting list saves list->fontmap mapping and refs fontmap
 * Freeing list releases mapping and frees fontmap
 */

GList *
gnome_font_list ()
{
	GPFontMap * map;
	GSList * l;

	map = gp_fontmap_get ();

	if (!map->fontlist) {
		for (l = map->fonts; l != NULL; l = l->next) {
			GPFontEntry * e;
			e = (GPFontEntry *) l->data;
			if (!e->hidden) {
				map->fontlist = g_list_prepend (map->fontlist, e->name);
			}
		}
		map->fontlist = g_list_reverse (map->fontlist);
		if (!fontlist2map) fontlist2map = g_hash_table_new (NULL, NULL);
		g_hash_table_insert (fontlist2map, map->fontlist, map);
	}

	return map->fontlist;
}

void
gnome_font_list_free (GList * fontlist)
{
	GPFontMap * map;

	g_return_if_fail (fontlist != NULL);

	map = g_hash_table_lookup (fontlist2map, fontlist);
	g_return_if_fail (map != NULL);

	gp_fontmap_unref (map);
}

GList *
gnome_font_family_list ()
{
	GPFontMap * map;
	GSList * l;

	map = gp_fontmap_get ();

	if (!map->familylist) {
		for (l = map->families; l != NULL; l = l->next) {
			GPFamilyEntry * f;
			GSList * ll;
			f = (GPFamilyEntry *) l->data;
			for (ll = f->fonts; ll != NULL; ll = ll->next) {
				GPFontEntry * e;
				e = (GPFontEntry *) l->data;
				if (!e->hidden) break;
			}
			if (ll != NULL) {
				map->familylist = g_list_prepend (map->familylist, f->name);
			}
		}
		map->familylist = g_list_reverse (map->familylist);
		if (!familylist2map) familylist2map = g_hash_table_new (NULL, NULL);
		g_hash_table_insert (familylist2map, map->familylist, map);
	}

	gp_fontmap_ref (map);

	gp_fontmap_release (map);

	return map->familylist;
}

void
gnome_font_family_list_free (GList * fontlist)
{
	GPFontMap * map;

	g_return_if_fail (fontlist != NULL);

	map = g_hash_table_lookup (familylist2map, fontlist);
	g_return_if_fail (map != NULL);

	gp_fontmap_unref (map);
}

/*
 * Returns newly allocated string or NULL
 */

static gchar *
gp_xmlGetPropString (xmlNodePtr node, const gchar * name)
{
	xmlChar * prop;
	gchar * str;

	prop = xmlGetProp (node, name);
	if (prop) {
		str = g_strdup (prop);
		xmlFree (prop);
		return str;
	}

	return NULL;
}

static gint
gp_fe_sortname (gconstpointer a, gconstpointer b)
{
	return strcasecmp (((GPFontEntry *) a)->name, ((GPFontEntry *) b)->name);
}

static gint
gp_fe_sortspecies (gconstpointer a, gconstpointer b)
{
	return strcasecmp (((GPFontEntry *) a)->speciesname, ((GPFontEntry *) b)->speciesname);
}

static gint
gp_familyentry_sortname (gconstpointer a, gconstpointer b)
{
	return strcasecmp (((GPFamilyEntry *) a)->name, ((GPFamilyEntry *) b)->name);
}

static gchar *
gp_fm_get_species_name (const gchar * fullname, const gchar * familyname)
{
	gchar * p;

	p = strstr (fullname, familyname);

	if (!p) return g_strdup ("Normal");

	p = p + strlen (familyname);

	while (*p && (*p < 'A')) p++;

	if (!*p) return g_strdup ("Normal");

	return g_strdup (p);
}

GnomeFontWeight
gp_fontmap_lookup_weight (const gchar * weight)
{
	static GHashTable * weights = NULL;
	GnomeFontWeight wcode;

	if (!weights) {
		weights = g_hash_table_new (g_str_hash, g_str_equal);

		g_hash_table_insert (weights, "Extra Light", GINT_TO_POINTER (GNOME_FONT_EXTRA_LIGHT));
		g_hash_table_insert (weights, "Extralight", GINT_TO_POINTER (GNOME_FONT_EXTRA_LIGHT));

		g_hash_table_insert (weights, "Thin", GINT_TO_POINTER (GNOME_FONT_THIN));

		g_hash_table_insert (weights, "Light", GINT_TO_POINTER (GNOME_FONT_LIGHT));

		g_hash_table_insert (weights, "Book", GINT_TO_POINTER (GNOME_FONT_BOOK));
		g_hash_table_insert (weights, "Roman", GINT_TO_POINTER (GNOME_FONT_BOOK));
		g_hash_table_insert (weights, "Regular", GINT_TO_POINTER (GNOME_FONT_BOOK));

		g_hash_table_insert (weights, "Medium", GINT_TO_POINTER (GNOME_FONT_MEDIUM));

		g_hash_table_insert (weights, "Semi", GINT_TO_POINTER (GNOME_FONT_SEMI));
		g_hash_table_insert (weights, "Semibold", GINT_TO_POINTER (GNOME_FONT_SEMI));
		g_hash_table_insert (weights, "Demi", GINT_TO_POINTER (GNOME_FONT_SEMI));
		g_hash_table_insert (weights, "Demibold", GINT_TO_POINTER (GNOME_FONT_SEMI));

		g_hash_table_insert (weights, "Bold", GINT_TO_POINTER (GNOME_FONT_BOLD));

		g_hash_table_insert (weights, "Heavy", GINT_TO_POINTER (GNOME_FONT_HEAVY));
 
		g_hash_table_insert (weights, "Extra", GINT_TO_POINTER (GNOME_FONT_EXTRABOLD));
		g_hash_table_insert (weights, "Extra Bold", GINT_TO_POINTER (GNOME_FONT_EXTRABOLD));

		g_hash_table_insert (weights, "Black", GINT_TO_POINTER (GNOME_FONT_BLACK));

		g_hash_table_insert (weights, "Extra Black", GINT_TO_POINTER (GNOME_FONT_EXTRABLACK));
		g_hash_table_insert (weights, "Extrablack", GINT_TO_POINTER (GNOME_FONT_EXTRABLACK));
		g_hash_table_insert (weights, "Ultra Bold", GINT_TO_POINTER (GNOME_FONT_EXTRABLACK));
	};

	wcode = GPOINTER_TO_INT (g_hash_table_lookup (weights, weight));

	return wcode;
}

static gboolean
gp_fm_is_changed (GPFontMap * map)
{
	struct stat s;

	if (map->user.name) {
		if (stat (map->user.name, &s) < 0) return TRUE;
		if (!S_ISREG (s.st_mode)) return TRUE;
		if (s.st_size != map->user.size) return TRUE;
		if (s.st_mtime != map->user.mtime) return TRUE;
	} else {
		gchar * home;
		home = getenv ("HOME");
		if (home) {
			gchar * name;
			name = g_strdup_printf ("%s/.gnome/fonts/fontmap", home);
			if ((stat (name, &s) == 0) && S_ISREG (s.st_mode)) {
				g_free (name);
				return TRUE;
			}
			g_free (name);
		}
	}

	if (map->system.name) {
		if (stat (map->system.name, &s) < 0) return TRUE;
		if (!S_ISREG (s.st_mode)) return TRUE;
		if (s.st_size != map->system.size) return TRUE;
		if (s.st_mtime != map->system.mtime) return TRUE;
	} else {
		gchar * name;
		name = g_strdup (DATADIR "/fonts/fontmap3");
		if ((stat (name, &s) == 0) && S_ISREG (s.st_mode)) {
			g_free (name);
			return TRUE;
		}
		g_free (name);
		name = g_strdup (DATADIR "/fonts/fontmap2");
		if ((stat (name, &s) == 0) && S_ISREG (s.st_mode)) {
			g_free (name);
			return TRUE;
		}
		g_free (name);
		name = g_strdup (DATADIR "/fonts/fontmap");
		if ((stat (name, &s) == 0) && S_ISREG (s.st_mode)) {
			g_free (name);
			return TRUE;
		}
		g_free (name);
	}

	return FALSE;
}
