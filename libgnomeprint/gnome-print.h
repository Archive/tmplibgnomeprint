#ifndef __GNOME_PRINT_H__
#define __GNOME_PRINT_H__

/*
 * Abstract base class for drivers
 *
 * Authors:
 *   Raph Levien (raph@acm.org)
 *   Miguel de Icaza (miguel@kernel.org)
 *   Lauris Kaplinski <lauris@ximian.com>
 *   Chema Celorio (chema@celorio.com)
 *
 * Copyright (C) 1999-2001 Ximian, Inc. and authors
 *
 */

#include <glib.h>

G_BEGIN_DECLS

#include <glib-object.h>
#include <libgnomeprint/gnome-print-config.h>
#include <libgnomeprint/gnome-font.h>
#include <libgnomeprint/gnome-glyphlist.h>
#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_bpath.h>

/*
 * These are currently defined values for gnome_print_xxx results
 * feel free to add more, if those make sense
 */

typedef enum {
	GNOME_PRINT_OK = 0,
	GNOME_PRINT_ERROR_UNKNOWN = -1,
	GNOME_PRINT_ERROR_BADVALUE = -2,
	GNOME_PRINT_ERROR_NOCURRENTPOINT = -3,
	GNOME_PRINT_ERROR_NOCURRENTPATH = -4,
	GNOME_PRINT_ERROR_TEXTCORRUPT = -5,
	GNOME_PRINT_ERROR_BADCONTEXT = -6,
	GNOME_PRINT_ERROR_NOPAGE = -7,
	GNOME_PRINT_ERROR_NOMATCH = -8
} GnomePrintReturnCode;

#define GNOME_TYPE_PRINT_CONTEXT (gnome_print_context_get_type ())
#define GNOME_PRINT_CONTEXT(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GNOME_TYPE_PRINT_CONTEXT, GnomePrintContext))
#define GNOME_IS_PRINT_CONTEXT(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GNOME_TYPE_PRINT_CONTEXT))

typedef struct _GnomePrintContext GnomePrintContext;

/*
 * INTERNALS ARE PRIVATE UNTIL STABILIZED
 */

GType gnome_print_context_get_type (void);

/*
 * These generate default context for given printer and do not allow
 * setting any driver-specific parameters.
 * The real constructor method will be handled by printmanager
 */

GnomePrintContext *gnome_print_context_new (GnomePrintConfig *config);

/*
 * This has to be called at the end of print job
 * Some drivers do not send any data to spooler before closing
 */

gint gnome_print_context_close (GnomePrintContext *pc);

/* Path manipulation */

gint gnome_print_newpath (GnomePrintContext *pc);
gint gnome_print_moveto (GnomePrintContext *pc, gdouble x, gdouble y);
gint gnome_print_lineto (GnomePrintContext *pc, gdouble x, gdouble y);
gint gnome_print_curveto (GnomePrintContext *pc, gdouble x1, gdouble y1, gdouble x2, gdouble y2, gdouble x3, gdouble y3);
gint gnome_print_closepath (GnomePrintContext *pc);
gint gnome_print_strokepath (GnomePrintContext *pc);
gint gnome_print_bpath (GnomePrintContext *pc, const ArtBpath *bpath, gboolean append);
gint gnome_print_vpath (GnomePrintContext *pc, const ArtVpath *vpath, gboolean append);

/* Graphic state manipulation */

gint gnome_print_setrgbcolor (GnomePrintContext *pc, gdouble r, gdouble g, gdouble b);
gint gnome_print_setopacity (GnomePrintContext *pc, gdouble opacity);
gint gnome_print_setlinewidth (GnomePrintContext *pc, gdouble width);
gint gnome_print_setmiterlimit (GnomePrintContext *pc, gdouble limit);
gint gnome_print_setlinejoin (GnomePrintContext *pc, gint jointype);
gint gnome_print_setlinecap (GnomePrintContext *pc, gint captype);
gint gnome_print_setdash (GnomePrintContext *pc, gint n_values, const gdouble *values, gdouble offset);
gint gnome_print_setfont (GnomePrintContext *pc, const GnomeFont *font);
gint gnome_print_clip (GnomePrintContext *pc);
gint gnome_print_eoclip (GnomePrintContext *pc);

/* CTM manipulation */

gint gnome_print_concat (GnomePrintContext *pc, const gdouble matrix[]);
gint gnome_print_scale (GnomePrintContext *pc, gdouble sx, gdouble sy);
gint gnome_print_rotate (GnomePrintContext *pc, gdouble theta_in_degrees);
gint gnome_print_translate (GnomePrintContext *pc, gdouble x, gdouble y);

/* Stack */

gint gnome_print_gsave (GnomePrintContext *pc);
gint gnome_print_grestore (GnomePrintContext *pc);

/* Painting */

gint gnome_print_fill (GnomePrintContext *pc);
gint gnome_print_eofill (GnomePrintContext *pc);
gint gnome_print_stroke (GnomePrintContext *pc);

/* Text drawing */

gint gnome_print_show (GnomePrintContext *pc, const gchar *text);
gint gnome_print_show_sized (GnomePrintContext *pc, const gchar *text, gint bytes);
gint gnome_print_glyphlist (GnomePrintContext *pc, GnomeGlyphList * glyphlist);

/* Images */

gint gnome_print_grayimage (GnomePrintContext *pc, const gchar *data, gint width, gint height, gint rowstride);
gint gnome_print_rgbimage (GnomePrintContext *pc, const gchar *data, gint width, gint height, gint rowstride);
gint gnome_print_rgbaimage (GnomePrintContext *pc, const gchar *data, gint width, gint height, gint rowstride);

/* General */

gint gnome_print_beginpage (GnomePrintContext *pc, const guchar *name);
gint gnome_print_showpage (GnomePrintContext *pc);

G_END_DECLS

#endif /* __GNOME_PRINT_H__ */

