#define __GNOME_PRINT_PS2_C__

/*
 * gnome-print-ps2.c: A Postscript driver for GnomePrint based on
 * gnome-print-pdf which was based on the PS driver by Raph Levien.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors:
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright 2000-2001 Ximian, Inc. and authors
 *
 */

#include <math.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <locale.h>

#include <libart_lgpl/art_affine.h>
#include <libart_lgpl/art_misc.h>
#include <libgnome/gnome-paper.h>
#include <libgnomeprint/gnome-print-private.h>
#include <libgnomeprint/gnome-font-private.h>
#include <libgnomeprint/gnome-print-encode-private.h>

#include <libgnomeprint/gnome-pgl-private.h>

#include "gnome-print-ps2.h"

#define EOL "\n"

typedef struct _GPPS2Font GPPS2Font;
typedef struct _GPPS2Page GPPS2Page;

struct _GPPS2Font {
	GPPS2Font *next;
	GnomeFontFace *face;
	GFPSObject *pso;
};

struct _GPPS2Page {
	GPPS2Page *next;
	gchar *name;
	gint number;
	gboolean shown;
	GSList *fonts;
};

struct _GnomePrintPS2 {
	GnomePrintContext pc;

	/* List of used fonts */
	GPPS2Font *fonts;
	/* Active font */
	GPPS2Font *active_font;
	gdouble active_font_size;
	/* Active color */
        gdouble r, g, b;
	gint active_color_flag;
	/* List of pages */
	GPPS2Page *pages;
	/* Bookkeeping */
	gint gsave_level;
	gint ps_level;
	ArtDRect bbox;
	/* Buffer */
	FILE *buf;
	gchar *bufname;
};

struct _GnomePrintPS2Class {
	GnomePrintContextClass parent_class;
};

static void gnome_print_ps2_class_init (GnomePrintPS2Class *klass);
static void gnome_print_ps2_init (GnomePrintPS2 *PS2);
static void gnome_print_ps2_finalize (GObject *object);

static gint gnome_print_ps2_construct (GnomePrintContext *ctx);
static gint gnome_print_ps2_gsave (GnomePrintContext *pc);
static gint gnome_print_ps2_grestore (GnomePrintContext *pc);
static gint gnome_print_ps2_fill (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule);
static gint gnome_print_ps2_clip (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule);
static gint gnome_print_ps2_stroke (GnomePrintContext *pc, const ArtBpath *bpath);
static gint gnome_print_ps2_image (GnomePrintContext *pc, const gdouble *affine, const guchar *px, gint w, gint h, gint rowstride, gint ch);
static gint gnome_print_ps2_glyphlist (GnomePrintContext *pc, const gdouble *affine, GnomeGlyphList *gl);
static gint gnome_print_ps2_beginpage (GnomePrintContext *pc, const guchar *name);
static gint gnome_print_ps2_showpage (GnomePrintContext *pc);
static gint gnome_print_ps2_close (GnomePrintContext *pc);

static gint gp_ps2_set_color (GnomePrintPS2 *ps2);
static gint gp_ps2_set_line (GnomePrintPS2 *ps2);
static gint gp_ps2_set_dash (GnomePrintPS2 *ps2);

static gint gp_ps2_set_color_private (GnomePrintPS2 *ps2, gdouble r, gdouble g, gdouble b);
static gint gp_ps2_set_font_private (GnomePrintPS2 *ps2, const GnomeFont *font);

static gint gp_ps2_print_bpath (GnomePrintPS2 *ps2, const ArtBpath *bpath);

static gchar *gnome_print_ps2_get_date (void);
static gint gp_ps2_fprintf (GnomePrintPS2 *ps2, const char *format, ...);

static GnomePrintContextClass *parent_class;

GType
gnome_print_ps2_get_type (void)
{
	static GType ps2_type = 0;
	if (!ps2_type) {
		static const GTypeInfo ps2_info = {
			sizeof (GnomePrintPS2Class),
			NULL, NULL,
			(GClassInitFunc) gnome_print_ps2_class_init,
			NULL, NULL,
			sizeof (GnomePrintPS2),
			0,
			(GInstanceInitFunc) gnome_print_ps2_init
		};
		ps2_type = g_type_register_static (GNOME_TYPE_PRINT_CONTEXT, "GnomePrintPS2", &ps2_info, 0);
	}
	return ps2_type;
}

static void
gnome_print_ps2_class_init (GnomePrintPS2Class *klass)
{
	GObjectClass *object_class;
	GnomePrintContextClass *pc_class;

	object_class = (GObjectClass *) klass;
	pc_class = (GnomePrintContextClass *)klass;

	parent_class = g_type_class_peek_parent (klass);
	
	object_class->finalize = gnome_print_ps2_finalize;

	pc_class->construct = gnome_print_ps2_construct;
	pc_class->gsave = gnome_print_ps2_gsave;
	pc_class->grestore = gnome_print_ps2_grestore;
	pc_class->fill = gnome_print_ps2_fill;
	pc_class->clip = gnome_print_ps2_clip;
	pc_class->stroke = gnome_print_ps2_stroke;
	pc_class->glyphlist = gnome_print_ps2_glyphlist;
	pc_class->image = gnome_print_ps2_image;
	pc_class->beginpage = gnome_print_ps2_beginpage;
	pc_class->showpage = gnome_print_ps2_showpage;
	pc_class->close = gnome_print_ps2_close;
}

static void
gnome_print_ps2_init (GnomePrintPS2 *ps2)
{
	ps2->ps_level = 2;
	
	ps2->gsave_level = 0;

	ps2->fonts = NULL;

	ps2->active_font = NULL;
	ps2->active_font_size = 0.0;
	ps2->active_color_flag = GP_GC_FLAG_UNSET;

	ps2->pages = NULL;

	ps2->buf = NULL;
}

static void
gnome_print_ps2_finalize (GObject *object)
{
	GnomePrintPS2 *ps2;

	ps2 = GNOME_PRINT_PS2 (object);

	if (ps2->buf) {
		g_warning ("file %s: line %d: Destroying PS2 context with open buffer", __FILE__, __LINE__);
		if (fclose (ps2->buf)) {
			g_warning ("Error closing buffer");
		}
		ps2->buf = NULL;
		if (unlink (ps2->bufname)) {
			g_warning ("Error unlinking buffer");
		}
		ps2->bufname = NULL;
	}

	while (ps2->pages) {
		GPPS2Page *p;
		p = ps2->pages;
		if (!p->shown) g_warning ("Page %d %s was not shown", p->number, p->name);
		if (p->name) g_free (p->name);
		while (p->fonts) p->fonts = g_slist_remove (p->fonts, p->fonts->data);
		ps2->pages = p->next;
		g_free (p);
	}

	while (ps2->fonts) {
		GPPS2Font *f;
		f = ps2->fonts;
		if (f->face) g_object_unref (G_OBJECT (f->face));
		if (f->pso) {
			g_free (f->pso->buf);
			g_free (f->pso->encodedname);
			g_free (f->pso);
		}
		ps2->fonts = f->next;
		g_free (f);
	}

	ps2->active_font = NULL;
	ps2->active_font_size = 0.0;
	ps2->active_color_flag = GP_GC_FLAG_UNSET;

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static gint
gnome_print_ps2_construct (GnomePrintContext *ctx)
{
	GnomePrintPS2 *ps2;
	gchar *tmp;
	gint ret, fd;

	ps2 = GNOME_PRINT_PS2 (ctx);

	ret = gnome_print_context_create_transport (ctx);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gnome_print_transport_open (ctx->transport);
	g_return_val_if_fail (ret >= 0, ret);

	tmp = g_strdup ("/tmp/gnome-print-XXXXXX");
	fd = mkstemp (tmp);
	if (fd < 0) {
		g_warning ("file %s: line %d: Cannot create temporary file", __FILE__, __LINE__);
		g_free (tmp);
		return GNOME_PRINT_ERROR_UNKNOWN;
	}
	ps2->buf = fdopen (fd, "r+");
	ps2->bufname = tmp;

	ps2->bbox.x0 = 0.0;
	ps2->bbox.y0 = 0.0;
	ps2->bbox.x1 = 21.0 * (72.0 / 2.54);
	ps2->bbox.y1 = 29.7 * (72.0 / 2.54);
	gpa_node_get_double_path_value (ctx->config, "Output.Media.PhysicalSize.Width", &ps2->bbox.x1);
	gpa_node_get_double_path_value (ctx->config, "Output.Media.PhysicalSize.Height", &ps2->bbox.y1);

	return GNOME_PRINT_OK;
}

static gint
gnome_print_ps2_gsave (GnomePrintContext *ctx)
{
	GnomePrintPS2 *ps2;

	ps2 = GNOME_PRINT_PS2 (ctx);

	ps2->gsave_level += 1;

	return gp_ps2_fprintf (ps2, "q" EOL);
}

static gint
gnome_print_ps2_grestore (GnomePrintContext *ctx)
{
	GnomePrintPS2 *ps2;

	ps2 = GNOME_PRINT_PS2 (ctx);

	ps2->gsave_level -= 1;
	ps2->active_font = NULL;
	ps2->active_color_flag = GP_GC_FLAG_UNSET;

	return gp_ps2_fprintf (ps2, "Q" EOL);
}

static gint
gnome_print_ps2_clip (GnomePrintContext *ctx, const ArtBpath *bpath, ArtWindRule rule)
{
	GnomePrintPS2 *ps2;
	gint ret;

	ps2 = GNOME_PRINT_PS2 (ctx);

	ret = gp_ps2_print_bpath (ps2, bpath);
	g_return_val_if_fail (ret >= 0, ret);

	if (rule == ART_WIND_RULE_NONZERO) {
		ret = gp_ps2_fprintf (ps2, "W" EOL);
	} else {
		ret = gp_ps2_fprintf (ps2, "W*" EOL);
	}

	return ret;
}

static gint
gnome_print_ps2_fill (GnomePrintContext *ctx, const ArtBpath *bpath, ArtWindRule rule)
{
	GnomePrintPS2 *ps2;
	gint ret;

	ps2 = GNOME_PRINT_PS2 (ctx);

	ret = gp_ps2_set_color (ps2);
	g_return_val_if_fail (ret >= 0, ret);

	ret = gp_ps2_print_bpath (ps2, bpath);
	g_return_val_if_fail (ret >= 0, ret);

	if (rule == ART_WIND_RULE_NONZERO) {
		ret = gp_ps2_fprintf (ps2, "f" EOL);
	} else {
		ret = gp_ps2_fprintf (ps2, "f*" EOL);
	}

	return ret;
}

static gint
gnome_print_ps2_stroke (GnomePrintContext *ctx, const ArtBpath *bpath)
{
	GnomePrintPS2 *ps2;
	gint ret;

	ps2 = GNOME_PRINT_PS2 (ctx);

	ret = gp_ps2_set_color (ps2);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gp_ps2_set_line (ps2);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gp_ps2_set_dash (ps2);
	g_return_val_if_fail (ret >= 0, ret);

	ret = gp_ps2_print_bpath (ps2, bpath);
	g_return_val_if_fail (ret >= 0, ret);

	ret = gp_ps2_fprintf (ps2, "S" EOL);

	return ret;
}

static int
gnome_print_ps2_image (GnomePrintContext *pc, const gdouble *ctm, const guchar *px, gint w, gint h, gint rowstride, gint ch)
{
	GnomePrintPS2 *ps2;
	gint ret;
	gchar *hex;
	gint hex_size;
	gint r;

	ps2 = GNOME_PRINT_PS2 (pc);

	ret = gp_ps2_fprintf (ps2, "q" EOL);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gp_ps2_fprintf (ps2, "[%g %g %g %g %g %g]cm" EOL, ctm[0], ctm[1], ctm[2], ctm[3], ctm[4], ctm[5]);
	g_return_val_if_fail (ret >= 0, ret);

	/* Image commands */
	ret = gp_ps2_fprintf (ps2, "/buf %d string def" EOL "%d %d 8" EOL, w * ch, w, h);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gp_ps2_fprintf (ps2, "[%d 0 0 %d 0 %d]" EOL, w, -h, h);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gp_ps2_fprintf (ps2, "{ currentfile buf readhexstring pop }\n");
	g_return_val_if_fail (ret >= 0, ret);

	if (ch == 1) {
		ret = gp_ps2_fprintf (ps2, "image" EOL);
	} else {
		ret = gp_ps2_fprintf (ps2, "false %d colorimage" EOL, ch);
	}
	g_return_val_if_fail (ret >= 0, ret);

	hex = g_new (gchar, gnome_print_encode_hex_wcs (w * ch));

	for (r = 0; r < h; r++) {
		hex_size = gnome_print_encode_hex (px + r * rowstride, hex, w * ch);
		ret = fwrite (hex, sizeof (gchar), hex_size, ps2->buf);
		g_return_val_if_fail (ret >= 0, ret);
		ret = gp_ps2_fprintf (ps2, EOL);
		g_return_val_if_fail (ret >= 0, ret);
	}

	g_free (hex);
	
	ret = gp_ps2_fprintf (ps2, "Q" EOL);
	g_return_val_if_fail (ret >= 0, ret);

	return GNOME_PRINT_OK;
}

#define EPSILON 1e-9

static gint
gnome_print_ps2_glyphlist (GnomePrintContext *pc, const gdouble *a, GnomeGlyphList *gl)
{
	static gdouble id[] = {1.0, 0.0, 0.0, 1.0, 0.0, 0.0};
	GnomePrintPS2 *ps2;
	GnomePosGlyphList *pgl;
	gboolean identity;
	gdouble dx, dy;
	gint ret, s;

	ps2 = (GnomePrintPS2 *) pc;

	identity = ((fabs (a[0] - 1.0) < EPSILON) && (fabs (a[1]) < EPSILON) && (fabs (a[2]) < EPSILON) && (fabs (a[3] - 1.0) < EPSILON));

	if (!identity) {
		ret = gp_ps2_fprintf (ps2, "q" EOL);
		g_return_val_if_fail (ret >= 0, ret);
		ret = gp_ps2_fprintf (ps2, "[%g %g %g %g %g %g]cm" EOL, a[0], a[1], a[2], a[3], a[4], a[5]);
		g_return_val_if_fail (ret >= 0, ret);
		dx = dy = 0.0;
	} else {
		dx = a[4];
		dy = a[5];
	}

	pgl = gnome_pgl_from_gl (gl, id, GNOME_PGL_RENDER_DEFAULT);

	for (s = 0; s < pgl->num_strings; s++) {
		GnomePosString * ps;
		gint i;

		ps = pgl->strings + s;

		ret = gp_ps2_set_font_private (ps2, gnome_rfont_get_font (ps->rfont));
		g_return_val_if_fail (ret >= 0, ret);
		ret = gp_ps2_set_color_private (ps2,
						((ps->color >> 24) & 0xff) / 255.0,
						((ps->color >> 16) & 0xff) / 255.0,
						((ps->color >>  8) & 0xff) / 255.0);
		g_return_val_if_fail (ret >= 0, ret);
		ret = gp_ps2_fprintf (ps2, "%g %g m" EOL, pgl->glyphs[ps->start].x + dx, pgl->glyphs[ps->start].y + dy);
		g_return_val_if_fail (ret >= 0, ret);
		/* Build string */
		ret = gp_ps2_fprintf (ps2, "(");
		for (i = ps->start; i < ps->start + ps->length; i++) {
			gint glyph;
			glyph = pgl->glyphs[i].glyph & 0xff;
			if (ps2->active_font->pso->encodedbytes == 1) {
				/* 8 bit encoding */
				ret = gp_ps2_fprintf (ps2, "\\%o", glyph);
				g_return_val_if_fail (ret >= 0, ret);
			} else {
				gint page;
				/* 16 bit encoding */
				page = (pgl->glyphs[i].glyph >> 8) & 0xff;
				ret = gp_ps2_fprintf (ps2, "\\%o\\%o", page, glyph);
				g_return_val_if_fail (ret >= 0, ret);
			}
		}
		ret = gp_ps2_fprintf (ps2, ")" EOL);
		g_return_val_if_fail (ret >= 0, ret);
		/* Build array */
		ret = gp_ps2_fprintf (ps2, "[");
		g_return_val_if_fail (ret >= 0, ret);
		for (i = ps->start + 1; i < ps->start + ps->length; i++) {
			ret = gp_ps2_fprintf (ps2, "%g %g ",
					      pgl->glyphs[i].x - pgl->glyphs[i-1].x,
					      pgl->glyphs[i].y - pgl->glyphs[i-1].y);
			g_return_val_if_fail (ret >= 0, ret);
		}
		ret = gp_ps2_fprintf (ps2, "0 0] ");
		g_return_val_if_fail (ret >= 0, ret);
		/* xyshow */
		ret = gp_ps2_fprintf (ps2, "xyshow" EOL);
		g_return_val_if_fail (ret >= 0, ret);
	}

	if (!identity) {
		ret = gp_ps2_fprintf (ps2, "Q" EOL);
		g_return_val_if_fail (ret >= 0, ret);
		ps2->active_font = NULL;
		ps2->active_color_flag = GP_GC_FLAG_UNSET;
	}

	gnome_pgl_destroy (pgl);

	return GNOME_PRINT_OK;
}

static gint
gnome_print_ps2_beginpage (GnomePrintContext *pc, const guchar *name)
{
	GnomePrintPS2 *ps2;
	GPPS2Page *p;
	gint number;
	gint ret;

	ps2 = GNOME_PRINT_PS2 (pc);

	number = ps2->pages ? ps2->pages->number : 0;

	p = g_new (GPPS2Page, 1);
	p->next = ps2->pages;
	p->name = g_strdup (name);
	p->number = number + 1;
	p->shown = FALSE;
	p->fonts = NULL;

	ps2->pages = p;

	ps2->active_font = NULL;
	ps2->active_color_flag = GP_GC_FLAG_UNSET;

	ret = gp_ps2_fprintf (ps2, "%%%%Page: %s %d" EOL, name, p->number);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gp_ps2_fprintf (ps2, "%%%%PageResources: (atend)" EOL);
	g_return_val_if_fail (ret >= 0, ret);

	ret = gnome_print_newpath (pc);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gnome_print_moveto (pc, 0.0, 0.0);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gnome_print_lineto (pc, ps2->bbox.x1, 0.0);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gnome_print_lineto (pc, ps2->bbox.x1, ps2->bbox.y1);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gnome_print_lineto (pc, 0.0, ps2->bbox.y1);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gnome_print_lineto (pc, 0.0, 0.0);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gnome_print_closepath (pc);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gnome_print_clip (pc);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gnome_print_newpath (pc);
	g_return_val_if_fail (ret >= 0, ret);

	return GNOME_PRINT_OK;
}


static gint
gnome_print_ps2_showpage (GnomePrintContext *pc)
{
	GnomePrintPS2 *ps2;
	gint ret;

	ps2 = GNOME_PRINT_PS2 (pc);

	ps2->pages->shown = TRUE;
	ps2->active_font = NULL;
	ps2->active_color_flag = GP_GC_FLAG_UNSET;

	ret = gp_ps2_fprintf (ps2, "SP" EOL);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gp_ps2_fprintf (ps2, "%%%%PageTrailer" EOL);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gp_ps2_fprintf (ps2, "%%%%PageResources: procset gnome-print-procs-%s" EOL, VERSION);
	g_return_val_if_fail (ret >= 0, ret);
	while (ps2->pages->fonts) {
		ret = gp_ps2_fprintf (ps2, "%%%%+ font %s" EOL, (gchar *) ps2->pages->fonts->data);
		g_return_val_if_fail (ret >= 0, ret);
		ps2->pages->fonts = g_slist_remove (ps2->pages->fonts, ps2->pages->fonts->data);
	}

	return GNOME_PRINT_OK;
}

static gint
gnome_print_ps2_close (GnomePrintContext *pc)
{
	GnomePrintPS2 *ps2;
	gchar *date;
	gint len, ret;

	ps2 = GNOME_PRINT_PS2 (pc);

	g_return_val_if_fail (pc->transport != NULL, GNOME_PRINT_ERROR_UNKNOWN);

	if (!ps2->pages || !ps2->pages->shown) {
		g_warning ("file %s: line %d: Closing PS2 context without final showpage", __FILE__, __LINE__);
		ret = gnome_print_showpage (pc);
		g_return_val_if_fail (ret >= 0, ret);
	}

	/* Do header */
	/* Comments */
	date = gnome_print_ps2_get_date ();
	gnome_print_transport_printf (pc->transport, "%%!PS-Adobe-3.0" EOL);
	/* fixme: %%BoundingBox: */
	gnome_print_transport_printf (pc->transport, "%%%%Creator: Gnome Print Version %s" EOL, VERSION);
	gnome_print_transport_printf (pc->transport, "%%%%CreationDate: %s" EOL, date);
	/* fixme: %%DocumentData: */
	/* fixme: Should we use %%Extensions: and drop LanguageLevel? */
	gnome_print_transport_printf (pc->transport, "%%%%LanguageLevel: 2" EOL);
	gnome_print_transport_printf (pc->transport, "%%%%Pages: %d" EOL, ps2->pages->number);
	gnome_print_transport_printf (pc->transport, "%%%%BoundingBox: %d %d %d %d" EOL,
				      (gint) floor (ps2->bbox.x0),
				      (gint) floor (ps2->bbox.y0),
				      (gint) ceil (ps2->bbox.x1),
				      (gint) ceil (ps2->bbox.y1));
	/* fixme: Orientation: */
	gnome_print_transport_printf (pc->transport, "%%%%PageOrder: Ascend" EOL);
	gnome_print_transport_printf (pc->transport, "%%%%Title: %s" EOL, "Document Title goes here");
	gnome_print_transport_printf (pc->transport, "%%%%DocumentSuppliedResources: procset pnome-print-procs-%s" EOL, VERSION);
	if (ps2->fonts) {
		GPPS2Font *f;
		/* %%DocumentSuppliedResources: */
		for (f = ps2->fonts; f != NULL; f = f->next) {
			gnome_print_transport_printf (pc->transport, "%%%%+ font %s" EOL, f->pso->encodedname);
		}
	}
	gnome_print_transport_printf (pc->transport, "%%%%EndComments" EOL);
	g_free (date);
	/* Prolog */
	gnome_print_transport_printf (pc->transport, "%%%%BeginProlog" EOL);
	/* Abbreviations */
	gnome_print_transport_printf (pc->transport, "%%%%BeginResource: procset gnome-print-procs-%s" EOL, VERSION);
	gnome_print_transport_printf (pc->transport, "/B {load def} bind def" EOL);
	gnome_print_transport_printf (pc->transport, "/n /newpath B /m /moveto B /l /lineto B /c /curveto B /h /closepath B" EOL);
	gnome_print_transport_printf (pc->transport, "/q /gsave B /Q /grestore B" EOL);
	gnome_print_transport_printf (pc->transport, "/J /setlinecap B /j /setlinejoin B /w /setlinewidth B /M /setmiterlimit B" EOL);
	gnome_print_transport_printf (pc->transport, "/d /setdash B" EOL);
	gnome_print_transport_printf (pc->transport, "/rg /setrgbcolor B" EOL);
	gnome_print_transport_printf (pc->transport, "/W /clip B /W* /eoclip B" EOL);
	gnome_print_transport_printf (pc->transport, "/f /fill B /f* /eofill B" EOL);
	gnome_print_transport_printf (pc->transport, "/S /stroke B" EOL);
	gnome_print_transport_printf (pc->transport, "/cm /concat B" EOL);
	gnome_print_transport_printf (pc->transport, "/SP /showpage B" EOL);
	gnome_print_transport_printf (pc->transport, "/FF /findfont B /F {scalefont setfont} bind def" EOL);
	gnome_print_transport_printf (pc->transport, "%%%%EndResource" EOL);
	gnome_print_transport_printf (pc->transport, "%%%%EndProlog" EOL);
	/* Setup */
	gnome_print_transport_printf (pc->transport, "%%%%BeginSetup" EOL);
	/* Download fonts */
	while (ps2->fonts) {
		GPPS2Font *f;
		f = ps2->fonts;
		gnome_print_transport_printf (pc->transport, "%%%%BeginResource: font %s" EOL, f->pso->encodedname);
		gnome_print_transport_write (pc->transport, f->pso->buf, f->pso->length);
		gnome_print_transport_printf (pc->transport, "%%%%EndResource" EOL);
		ps2->fonts = f->next;
		g_free (f->pso->buf);
		g_free (f->pso->encodedname);
		g_free (f->pso);
		g_free (f);
	}
	gnome_print_transport_printf (pc->transport, "%%%%EndSetup" EOL);
	/* Write buffer */
	rewind (ps2->buf);
	len = 256;
	while (len > 0) {
		guchar b[256];
		len = fread (b, sizeof (guchar), 256, ps2->buf);
		if (len > 0) {
			gnome_print_transport_write (pc->transport, b, len);
		}
	}
	fclose (ps2->buf);
	ps2->buf = NULL;
	if (unlink (ps2->bufname)) {
		g_warning ("Error unlinking buffer");
	}
	ps2->bufname = NULL;

	gnome_print_transport_printf (pc->transport, "%%%%Trailer" EOL);
	gnome_print_transport_printf (pc->transport, "%%%%EOF" EOL);
 
 	gnome_print_transport_close (pc->transport);
	pc->transport = NULL;
 	
	return GNOME_PRINT_OK;
}

static gint
gp_ps2_set_color (GnomePrintPS2 *ps2)
{
	GnomePrintContext *ctx;

	ctx = GNOME_PRINT_CONTEXT (ps2);

	return gp_ps2_set_color_private (ps2, gp_gc_get_red (ctx->gc), gp_gc_get_green (ctx->gc), gp_gc_get_blue (ctx->gc));
}

static gint
gp_ps2_set_line (GnomePrintPS2 *ps2)
{
	GnomePrintContext *ctx;
	gint ret;

	ctx = GNOME_PRINT_CONTEXT (ps2);

	if (gp_gc_get_line_flag (ctx->gc) == GP_GC_FLAG_CLEAR) return 0;

	ret = gp_ps2_fprintf (ps2, "%g w %i J %i j %g M" EOL,
			      gp_gc_get_linewidth (ctx->gc),
			      gp_gc_get_linecap (ctx->gc),
			      gp_gc_get_linejoin (ctx->gc),
			      gp_gc_get_miterlimit (ctx->gc));
	gp_gc_set_line_flag (ctx->gc, GP_GC_FLAG_CLEAR);

	return ret;
}

static gint
gp_ps2_set_dash (GnomePrintPS2 *ps2)
{
	GnomePrintContext *ctx;
	const ArtVpathDash *dash;
	gint ret, i;

	ctx = GNOME_PRINT_CONTEXT (ps2);

	if (gp_gc_get_dash_flag (ctx->gc) == GP_GC_FLAG_CLEAR) return 0;

	dash = gp_gc_get_dash (ctx->gc);
	ret = gp_ps2_fprintf (ps2, "[");
	for (i = 0; i < dash->n_dash; i++) ret += gp_ps2_fprintf (ps2, " %g", dash->dash[i]);
	ret += gp_ps2_fprintf (ps2, "]%g d" EOL, dash->n_dash > 0 ? dash->offset : 0.0);
	gp_gc_set_dash_flag (ctx->gc, GP_GC_FLAG_CLEAR);

	return ret;
}

static gint
gp_ps2_set_color_private (GnomePrintPS2 *ps2, gdouble r, gdouble g, gdouble b)
{
	GnomePrintContext *ctx;
	gint ret;

	ctx = GNOME_PRINT_CONTEXT (ps2);

	if ((ps2->active_color_flag == GP_GC_FLAG_CLEAR) && (r == ps2->r) && (g == ps2->g) && (b == ps2->b)) return 0;

	ret = gp_ps2_fprintf (ps2, "%.3g %.3g %.3g rg" EOL, r, g, b);

	ps2->r = r;
	ps2->g = g;
	ps2->b = b;
	ps2->active_color_flag = GP_GC_FLAG_CLEAR;

	return ret;
}

static gint
gp_ps2_set_font_private (GnomePrintPS2 *ps2, const GnomeFont *font)
{
	GnomePrintContext *ctx;
	const GnomeFontFace *face;
	GPPS2Font *f;
	gint ret;

	ctx = GNOME_PRINT_CONTEXT (ps2);

	face = gnome_font_get_face (font);

	for (f = ps2->fonts; f != NULL; f = f->next) {
		if (f->face == face) {
			/* We found it */
			if (ps2->active_font
			    && (ps2->active_font->face == face)
			    && (ps2->active_font_size == gnome_font_get_size (font))) {
				return 0;
			}
			break;
		}
	}
	if (f == NULL) {
		GFPSObject * pso;
		pso = gnome_font_face_create_ps_object (face);
		g_return_val_if_fail (pso != NULL, GNOME_PRINT_ERROR_UNKNOWN);
		f = g_new (GPPS2Font, 1);
		f->next = ps2->fonts;
		ps2->fonts = f;
		f->face = (GnomeFontFace *) face;
		gnome_font_face_ref (face);
		f->pso = pso;
	}

	ret = gp_ps2_fprintf (ps2, "/%s FF %g F" EOL, f->pso->encodedname, gnome_font_get_size (font));
	ps2->active_font = f;
	ps2->active_font_size = gnome_font_get_size (font);

	return ret;
}

GnomePrintContext *
gnome_print_ps2_new (GPANode *config)
{
	GnomePrintContext *ctx;
	gint ret;

	g_return_val_if_fail (config != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (config), NULL);

	ctx = g_object_new (GNOME_TYPE_PRINT_PS2, NULL);

	ret = gnome_print_context_construct (ctx, config);

	if (ret != GNOME_PRINT_OK) {
		g_object_unref (G_OBJECT (ctx));
		ctx = NULL;
	}

	return ctx;
}

static gint
gp_ps2_print_bpath (GnomePrintPS2 *ps2, const ArtBpath *bpath)
{
	gboolean started, closed;
	
	gp_ps2_fprintf (ps2, "n" EOL);

	started = FALSE;
	closed = FALSE;
	while (bpath->code != ART_END) {
		switch (bpath->code) {
		case ART_MOVETO_OPEN:
			if (started && closed) gp_ps2_fprintf (ps2, "h" EOL);
			closed = FALSE;
			started = FALSE;
			gp_ps2_fprintf (ps2, "%g %g m" EOL, bpath->x3, bpath->y3);
			break;
		case ART_MOVETO:
			if (started && closed) gp_ps2_fprintf (ps2, "h" EOL);
			closed = TRUE;
			started = TRUE;
			gp_ps2_fprintf (ps2, "%g %g m" EOL, bpath->x3, bpath->y3);
			break;
		case ART_LINETO:
			gp_ps2_fprintf (ps2, "%g %g l" EOL, bpath->x3, bpath->y3);
			break;
		case ART_CURVETO:
			gp_ps2_fprintf (ps2, "%g %g %g %g %g %g c" EOL,
					bpath->x1, bpath->y1,
					bpath->x2, bpath->y2,
					bpath->x3, bpath->y3);
			break;
		default:
			g_warning ("Path structure is corrupted");
			return -1;
		}
		bpath += 1;
	}

	if (started && closed) gp_ps2_fprintf (ps2, "h" EOL);
	
	return 0;
}

/* Other stuff */

static gchar*
gnome_print_ps2_get_date (void)
{
	time_t clock;
	struct tm *now;
	gchar *date;

#ifdef ADD_TIMEZONE_STAMP
  extern char * tzname[];
	/* TODO : Add :
		 "[+-]"
		 "HH'" Offset from gmt in hours
		 "OO'" Offset from gmt in minutes
	   we need to use tz_time. but I don't
	   know how protable this is. Chema */
	gprint ("Timezone %s\n", tzname[0]);
	gprint ("Timezone *%s*%s*%li*\n", tzname[1], timezone);
#endif	

	clock = time (NULL);
	now = localtime (&clock);

	date = g_strdup_printf ("D:%04d%02d%02d%02d%02d%02d",
				now->tm_year + 1900,
				now->tm_mon + 1,
				now->tm_mday,
				now->tm_hour,
				now->tm_min,
				now->tm_sec);

	return date;
}

static gint
gp_ps2_fprintf (GnomePrintPS2 *ps2, const char *format, ...)
{
 	va_list arguments;
 	gchar *text;
	gchar *oldlocale;
	gint len;

	oldlocale = setlocale (LC_NUMERIC, NULL);
	setlocale (LC_NUMERIC, "C");
		
 	va_start (arguments, format);
 	text = g_strdup_vprintf (format, arguments);
 	va_end (arguments);

	len = fwrite (text, sizeof (gchar), strlen (text), ps2->buf);
	g_free (text);

	setlocale (LC_NUMERIC, oldlocale);

	return GNOME_PRINT_OK;
}

