#ifndef __GNOME_FONT_FACE_H__
#define __GNOME_FONT_FACE_H__

/*
 *  Copyright (C) 2000-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Jody Goldberg <jody@ximian.com>
 *    Miguel de Icaza <miguel@ximian.com>
 *    Lauris Kaplinski <lauris@ximian.com>
 *    Christopher James Lahey <clahey@ximian.com>
 *    Michael Meeks <michael@ximian.com>
 *    Morten Welinder <terra@diku.dk>
 *
 *  GnomeFontFace - unscaled typeface
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glib.h>

G_BEGIN_DECLS

#define GNOME_TYPE_FONT_FACE (gnome_font_face_get_type ())
#define GNOME_FONT_FACE(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), GNOME_TYPE_FONT_FACE, GnomeFontFace))
#define GNOME_IS_FONT_FACE(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), GNOME_TYPE_FONT_FACE))

typedef struct _GnomeFontFace GnomeFontFace;

#include <glib-object.h>
#include <libart_lgpl/art_rect.h>
#include <libart_lgpl/art_bpath.h>
#include <libgnomeprint/gnome-font.h>

GType gnome_font_face_get_type (void);

/*
 * Finding
 */
GnomeFontFace * gnome_font_face_find (const gchar *name);

/*
 * Naming
 *
 * gnome_font_face_get_name () should return one "true" name for font, that
 * does not have to be its PostScript name.
 * In future those names can be possibly localized (except ps_name)
 */

const gchar * gnome_font_face_get_name (const GnomeFontFace * face);
const gchar * gnome_font_face_get_family_name (const GnomeFontFace * face);
const gchar * gnome_font_face_get_species_name (const GnomeFontFace * face);
const gchar * gnome_font_face_get_ps_name (const GnomeFontFace * face);

/*
 * General information
 *
 */

gint gnome_font_face_get_num_glyphs (const GnomeFontFace * face);

/*
 * Get glyph number from unicode char
 *
 * In future there can probably be several different unicode to glyph
 * character mappings per font (for different languages), current function
 * uses the default one :)
 */

gint gnome_font_face_lookup_default (const GnomeFontFace * face, gint unicode);

/*
 * Metrics
 *
 * Currently those return standard values for left to right, horizontal script
 * The prefix std means, that there (a) will hopefully be methods to extract
 * different metric values and (b) for given face one text direction can
 * be defined as "default"
 * All face metrics are given in 0.001 em units
 */

const ArtDRect * gnome_font_face_get_stdbbox (const GnomeFontFace * face);

ArtPoint * gnome_font_face_get_glyph_stdadvance (const GnomeFontFace * face, gint glyph, ArtPoint * advance);
ArtDRect * gnome_font_face_get_glyph_stdbbox (const GnomeFontFace * face, gint glyph, ArtDRect * bbox);
const ArtBpath * gnome_font_face_get_glyph_stdoutline (const GnomeFontFace * face, gint glyph);

/*
 * Give (possibly localized) demonstration text for given face
 * Most usually this tells about quick fox and lazy dog...
 */

const gchar * gnome_font_face_get_sample (const GnomeFontFace * face);

G_END_DECLS

#endif /* __GNOME_FONT_FACE_H__ */



